package at.synaptos.synapphys.test.data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingItem;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.model.BodymapItem;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.FindingType;
import at.synaptos.synapphys.model.Gender;
import at.synaptos.synapphys.model.PaymentMethod;
import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.model.ServiceAttachment;
import at.synaptos.synapphys.model.ServiceDocumentation;
import at.synaptos.synapphys.model.ServiceTemplate;
import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.model.ServiceTypeCategory;
import at.synaptos.synapphys.model.SessionDocumentation;

public class TestDataFactory {

	/**
	 * Delivers a simple seriesDocumentation
	 * 
	 * @return
	 */
	public static SeriesDocumentation createSeriesDocumentation(Client client) {
		SeriesDocumentation seriesDocumentation = SeriesDocumentation.getInstance(client);
		seriesDocumentation.setReferralDate(new Date(System.currentTimeMillis() - 3 * 24 * 60 * 60 * 1000));
		seriesDocumentation.setApprovalDate(new Date(System.currentTimeMillis() - 2 * 24 * 60 * 60 * 1000));
		return seriesDocumentation;
	}

	/**
	 * Delivers a simple client
	 * 
	 * @return
	 */
	public static Client createClient() {
		Client client = Client.getInstance();
		client.setLastname("Ql" + System.currentTimeMillis());
		client.setFirstname("Qf" + System.currentTimeMillis());
		client.setGender(Gender.FEMALE);
		client.setBirthDate(new Date(System.currentTimeMillis() - 30 * 365 * 24 * 60 * 60 * 1000));
		client.setHealthInsuranceNo("4001240266");
		return client;
	}

	/**
	 * Delivers a simple physician
	 * 
	 * @return
	 */
	public static Physician createPhysician() {
		Physician physician = Physician.getInstance();
		physician.setLastname("Al" + System.currentTimeMillis());
		physician.setFirstname("Af" + System.currentTimeMillis());
		return physician;
	}

	/**
	 * Delivers a simple serviceType
	 * 
	 * @return
	 */
	public static ServiceType createServiceType() {
		ServiceType serviceType = ServiceType.getInstance();
		serviceType.setCategory(ServiceTypeCategory.C10_THERAPY);
		serviceType.setAmount(BigDecimal.valueOf(20.33d));
		serviceType.setName("P" + System.currentTimeMillis());
		serviceType.setCode("P" + serviceType.getName().substring(serviceType.getName().length() - 4));
		return serviceType;
	}

	/**
	 * Delivers a simple serviceTemplate
	 * 
	 * @return
	 */
	public static ServiceTemplate createServiceTemplate() {
		ServiceTemplate serviceTemplate = ServiceTemplate.getInstance();
		serviceTemplate.setQuantity(1);
		return serviceTemplate;
	}

	/**
	 * Delivers a simple serviceAttachment
	 * 
	 * @return
	 */
	public static ServiceAttachment createServiceAttachment() {
		ServiceAttachment serviceAttachment = ServiceAttachment.getInstance();
		serviceAttachment.setSubject(System.currentTimeMillis() + ", " + System.currentTimeMillis());
		serviceAttachment.setContentNote(System.currentTimeMillis() + ", " + System.currentTimeMillis());
		serviceAttachment.setContentType("image/jpeg");
		serviceAttachment.setSourceFilename("image.jpg");
		serviceAttachment.setTargetFilename("image_" + System.currentTimeMillis() + ".jpg");
		return serviceAttachment;
	}

	/**
	 * Delivers a simple sessionDocumentation
	 * 
	 * @return
	 */
	public static SessionDocumentation createSessionDocumentation(SeriesDocumentation seriesDocumentation) {
		return SessionDocumentation.getInstance(
				ScheduleItem.getInstance(LocalDateTime.now().minusMinutes(30), LocalDateTime.now()),
				seriesDocumentation);
	}

	/**
	 * Delivers a simple serviceDocumentation from serviceTemplate
	 * 
	 * @param serviceTemplate
	 * @return
	 */
	public static ServiceDocumentation createServiceDocumentation(ServiceTemplate serviceTemplate) {
		ServiceDocumentation serviceDocumentation = ServiceDocumentation.getInstance(serviceTemplate);
		return serviceDocumentation;
	}

	/**
	 * Delivers a simple BodymapItem
	 * 
	 * @return
	 */
	public static BodymapItem createBodymapItem() {
		BodymapItem bodymapItem = new BodymapItem();
		bodymapItem.setFindingType(FindingType.SKIN_LEAD_PAIN);
		bodymapItem.setFindingPositionX(100d);
		bodymapItem.setFindingPositionY(200d);
		return bodymapItem;
	}

	/**
	 * Delivers a simple ScheduleItem
	 * 
	 * @return
	 */
	public static ScheduleItem createScheduleItem() {
		ScheduleItem scheduleItem = ScheduleItem.getInstance(LocalDateTime.now().minusMinutes(30), LocalDateTime.now());
		scheduleItem.setPhone("+43 664 12 34 56 78");
		scheduleItem.setNote(System.currentTimeMillis() + ", " + System.currentTimeMillis());
		return scheduleItem;
	}

	/**
	 * Delivers a simple Billing
	 * 
	 * @return
	 */
	public static Billing createBilling() {
		Billing billing = Billing.getInstance().withBillingType(BillingType.RECEIPT);
		billing.setBillingDate(new Date(System.currentTimeMillis()));
		billing.setPaymentMethod(PaymentMethod.CASH);
		return billing;
	}

	/**
	 * Delivers a simple BillingItem
	 * 
	 * @return
	 */
	public static BillingItem createBillingItem(int quantity, double amount) {
		BillingItem billingItem = BillingItem.getInstance();
		billingItem.setSubject("Zahlung" + System.currentTimeMillis());
		billingItem.setQuantity(quantity);
		billingItem.setAmount(BigDecimal.valueOf(amount));
		return billingItem;
	}

}
