package at.synaptos.synapphys.test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import at.synaptos.synapphys.fiskaltrust.ReceiptResponse;
import at.synaptos.synapphys.fiskaltrust.ReceiptSigner;
import at.synaptos.synapphys.fiskaltrust.SignatureItem;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingSignature;
import at.synaptos.synapphys.model.BillingType;

public class FiskaltrustTest {

	private static final String FT_CASHBOX_ID = "48a6d9a0-8aea-4ef6-8607-ff53a69deb3f";
	private static final String FT_ACCESS_TOKEN = "BK2f1zV25UQll3LjdA4z4LAv1GpYQe30wiVrBqZt5sP06ryoUSV2qVzc9CNQYzCJ5wJMEPNfGxisF17G2ZXKENE=";
	private static final String CB_TERMINAL_ID = "1";

	@Test
	public void requestToJson() throws Exception {
		Billing receipt = Billing.getInstance().withBillingType(BillingType.RECEIPT);
		receipt.setCreatedDate(new Date());
		receipt.setCreatedUser("a");
		receipt.setBillingLdt(LocalDateTime.now());
		receipt.setReferenceSequence(6);
		receipt.setReferenceYear(2017);
		receipt.addBillingItemValues(1, "Physiotherapie 45 min", 1, BigDecimal.valueOf(45d));
		receipt.addBillingItemValues(2, "Handcreme", 2, BigDecimal.valueOf(10d), BigDecimal.valueOf(20d));
		receipt.setFtCashboxId(FT_CASHBOX_ID);
		receipt.setFtAccessToken(FT_ACCESS_TOKEN);
		receipt.setCbTerminalId(CB_TERMINAL_ID);

		BillingSignature receiptSignature = ReceiptSigner.sign(receipt);

		Assert.assertNotNull(receiptSignature);

		System.out.println(receiptSignature.getFtCashboxName());
		System.out.println(receiptSignature.getFtReceiptSignature());
		System.out.println(receiptSignature.getFtReceiptResponseLog());
		System.out.println("stateFlag=" + String.format("%X", receiptSignature.getFtStateFlag()));
		System.out.println("stateNo=" + receiptSignature.getFtStateNo());
		System.out.println("stateException=" + receiptSignature.getFtStateException());
	}

	@Test
	public void responseToJson() throws Exception {

		String statedata = ",\"ftStateData\":\"{\\\"Number\\\":37,\\\"Exception\\\":\\\"\\\",\\\"Signing\\\":true,\\\"Counting\\\":true,\\\"ZeroReceipt\\\":false}\"";

		String respdata = "{\"ftCashBoxID\":\"48a6d9a0-8aea-4ef6-8607-ff53a69deb3f\",\"ftQueueID\":\"cf4fe389-ef89-4c7d-9066-a4d0a84a24fe\",\"ftQueueItemID\":\"27e08f64-eecb-4c18-b0f7-d153f1703429\",\"ftQueueRow\":9,\"cbTerminalID\":\"1\",\"cbReceiptReference\":\"18\",\"ftCashBoxIdentification\":\"fiskaltrust2\",\"ftReceiptIdentification\":\"ft8#5\",\"ftReceiptMoment\":\"2017-01-26T11:34:57.3493134Z\",\"ftSignatures\":[{\"ftSignatureFormat\":1,\"ftSignatureType\":4707387510509011000,\"caption\":\"Decision 37\",\"data\":\"Sign: True Counter:True\"},{\"ftSignatureFormat\":1,\"ftSignatureType\":4707387510509011000,\"caption\":\"Counter Add\",\"data\":\"8.4\"},{\"ftSignatureFormat\":1,\"ftSignatureType\":4707387510509011000,\"caption\":\"S A N D B O X\",\"data\":\"cf4fe389-ef89-4c7d-9066-a4d0a84a24fe\"},{\"ftSignatureFormat\":3,\"ftSignatureType\":4707387510509011000,\"caption\":\"www.fiskaltrust.at\",\"data\":\"_R1-AT1_fiskaltrust2_ft8#5_2017-01-26T12:34:57_8,40_0,00_0,00_0,00_0,00_6+H5m/Y=_3d36822a_kQw4tJJAn1o=_ZrCBw7LBZwv/JFJTb4G/MJYi6/lta6WeH/OYcC+ynJUHEidoU5RVWBEfSrRXlNF1OJPiv+vQJ4yCGEG3x3qYtQ==\"}],\"ftState\":4707387510509011000"
				+ statedata + "}";

		Gson gson = new GsonBuilder()
				.registerTypeAdapter(LocalDateTime.class,
						(JsonDeserializer<LocalDateTime>) (json, type, jsonDeserializationContext) -> ZonedDateTime
								.parse(json.getAsJsonPrimitive().getAsString()).toLocalDateTime())
				.registerTypeAdapter(SignatureItem.Format.class,
						(JsonDeserializer<SignatureItem.Format>) (json, type,
								jsonDeserializationContext) -> SignatureItem.Format.fromKey(json.getAsLong()))
				.registerTypeAdapter(SignatureItem.Type.class, (JsonDeserializer<SignatureItem.Type>) (json, type,
						jsonDeserializationContext) -> SignatureItem.Type.fromKey(json.getAsLong()))
				.create();

		ReceiptResponse receiptResponse = gson.fromJson(respdata, ReceiptResponse.class);

		System.out.println(receiptResponse.getFtReceiptMoment());
	}

}
