package at.synaptos.synapphys.test.utils;

abstract public class AbstractPersistenceTestSet {
	
	abstract public void doUnitTests() throws Exception;
    
}
