package at.synaptos.synapphys.test.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RunAs;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.junit.Assert;

import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingItem;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.service.BillingService;
import at.synaptos.synapphys.test.data.TestDataFactory;
import at.synaptos.synapphys.utils.StringUtils;

@RunAs("user")
@PermitAll
@Stateless
public class BillingPersistenceTestSet extends AbstractPersistenceTestSet {

	@Inject
	private BillingService billingService;

	@Override
	public void doUnitTests() throws Exception {
		testAddBillingSerial();
	}

	public void testAddBillingSerial() throws Exception {

		List<BillingItem> billingItems = new ArrayList<BillingItem>();
		billingItems.add(TestDataFactory.createBillingItem(1, 10.99d));
		billingItems.add(TestDataFactory.createBillingItem(2, 10.77d));

		Billing managedBilling = insertABilling(billingItems);
		Assert.assertNotNull(managedBilling);
		Assert.assertNotNull(managedBilling.getReferenceYear());
		Assert.assertNotNull(managedBilling.getReferenceSequence());
		Assert.assertFalse(managedBilling.isNew());
		Assert.assertTrue(managedBilling.getReferenceYear().equals(managedBilling.getReferenceYear()));
		Assert.assertTrue(managedBilling.getReferenceSequence() > 0);

		List<BillingItem> billingItems2 = new ArrayList<BillingItem>();
		billingItems2.add(TestDataFactory.createBillingItem(1, 12.99d));
		billingItems2.add(TestDataFactory.createBillingItem(2, 12.77d));
		billingItems2.add(TestDataFactory.createBillingItem(1, 12.77d));

		Billing managedBilling2 = insertABilling(billingItems2);
		Assert.assertNotNull(managedBilling2);
		Assert.assertNotNull(managedBilling2.getReferenceYear());
		Assert.assertNotNull(managedBilling2.getReferenceSequence());
		Assert.assertFalse(managedBilling2.isNew());
		Assert.assertTrue(managedBilling2.getReferenceYear().equals(managedBilling2.getReferenceYear()));
		Assert.assertTrue(managedBilling2.getReferenceSequence() > 0);
		Assert.assertTrue(managedBilling2.getReferenceSequence() - managedBilling.getReferenceSequence() == 1);

	}

	public void testAddBillingParallel() throws Exception {

		Callable<Integer> task = new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {

				List<BillingItem> billingItems = new ArrayList<BillingItem>();

				billingItems.add(TestDataFactory.createBillingItem(1, 10.77d));
				billingItems.add(TestDataFactory.createBillingItem(1, 10.99d));

				Billing billing = insertABilling(billingItems);
				Assert.assertNotNull(billing);
				Assert.assertFalse(billing.isNew());

				Billing managedBilling = billingService.getEntity(billing.getId());
				Assert.assertNotNull(managedBilling);
				Assert.assertNotNull(managedBilling.getReferenceNo());

				return managedBilling.getReferenceSequence();

			}

		};

		Callable<Integer> task2 = new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {

				List<BillingItem> billingItems = new ArrayList<BillingItem>();

				billingItems.add(TestDataFactory.createBillingItem(2, 12.77d));
				billingItems.add(TestDataFactory.createBillingItem(2, 12.99d));

				Billing billing = insertABilling(billingItems);
				Assert.assertNotNull(billing);
				Assert.assertFalse(billing.isNew());

				Billing managedBilling = billingService.getEntity(billing.getId());
				Assert.assertNotNull(managedBilling);
				Assert.assertNotNull(managedBilling.getReferenceNo());

				return managedBilling.getReferenceSequence();

			}

		};

		Callable<Integer> task3 = new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {

				List<BillingItem> billingItems = new ArrayList<BillingItem>();

				billingItems.add(TestDataFactory.createBillingItem(3, 13.77d));
				billingItems.add(TestDataFactory.createBillingItem(3, 13.99d));

				Billing billing = insertABilling(billingItems);
				Assert.assertNotNull(billing);
				Assert.assertFalse(billing.isNew());

				Billing managedBilling = billingService.getEntity(billing.getId());
				Assert.assertNotNull(managedBilling);
				Assert.assertNotNull(managedBilling.getReferenceNo());

				return managedBilling.getReferenceSequence();

			}

		};

		FutureTask<Integer> futureTask = new FutureTask<Integer>(task);
		FutureTask<Integer> futureTask2 = new FutureTask<Integer>(task2);
		FutureTask<Integer> futureTask3 = new FutureTask<Integer>(task3);

		Thread t = new Thread(futureTask);
		Thread t2 = new Thread(futureTask2);
		Thread t3 = new Thread(futureTask3);

		t.start();
		t2.start();
		t3.start();

		Integer result = futureTask.get();
		Integer result2 = futureTask2.get();
		Integer result3 = futureTask3.get();

		Assert.assertNotNull(result);
		Assert.assertNotNull(result2);
		Assert.assertNotNull(result3);

		Assert.assertFalse(result.equals(result2));
		Assert.assertFalse(result.equals(result3));
		Assert.assertFalse(result2.equals(result3));

		Assert.assertTrue(result + result2 + result3 == 6);
	}

	public void testFindBillings() throws Exception {

		List<BillingItem> billingItems = new ArrayList<BillingItem>();
		billingItems.add(TestDataFactory.createBillingItem(1, 10.99d));
		billingItems.add(TestDataFactory.createBillingItem(2, 10.77d));

		Billing managedBilling = insertABilling(billingItems, "10.01.2016 10:00:00.0");
		Assert.assertNotNull(managedBilling);

		for (int i = 0; i < 2; i++) {
			billingItems = new ArrayList<BillingItem>();
			billingItems.add(TestDataFactory.createBillingItem(1, 10.99d));
			billingItems.add(TestDataFactory.createBillingItem(2, 10.77d));

			managedBilling = insertABilling(billingItems, "10.01.2016 10:30:00.0");
			Assert.assertNotNull(managedBilling);
		}

		for (int i = 0; i < 3; i++) {
			billingItems = new ArrayList<BillingItem>();
			billingItems.add(TestDataFactory.createBillingItem(1, 11.99d));
			billingItems.add(TestDataFactory.createBillingItem(2, 11.77d));

			managedBilling = insertABilling(billingItems, "11.01.2016 11:00:00.0");
			Assert.assertNotNull(managedBilling);
		}

		for (int i = 0; i < 3; i++) {
			billingItems = new ArrayList<BillingItem>();
			billingItems.add(TestDataFactory.createBillingItem(1, 11.99d));
			billingItems.add(TestDataFactory.createBillingItem(2, 11.77d));

			managedBilling = insertABilling(billingItems, "11.01.2016 22:01:00.0");
			Assert.assertNotNull(managedBilling);
		}

		for (int i = 0; i < 2; i++) {
			billingItems = new ArrayList<BillingItem>();
			billingItems.add(TestDataFactory.createBillingItem(1, 12.99d));
			billingItems.add(TestDataFactory.createBillingItem(2, 12.77d));

			managedBilling = insertABilling(billingItems, "12.01.2016 12:00:00.0");
			Assert.assertNotNull(managedBilling);
		}

		DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.sss");
		Date startDate = formatter.parse("10.01.2016 10:01:00.0");
		Date endDate = formatter.parse("11.01.2016 22:00:00.0");
		List<Billing> selectedBillings = billingService.getEntities(BillingType.RECEIPT, startDate, endDate);
		Assert.assertNotNull(selectedBillings);
		Assert.assertTrue(selectedBillings.size() == 5);

	}

	private Billing insertABilling(List<BillingItem> billingItems) throws Exception {
		return insertABilling(billingItems, null);
	}

	private Billing insertABilling(List<BillingItem> billingItems, String billingDate) throws Exception {
		Billing newBilling = TestDataFactory.createBilling();
		if (!StringUtils.isEmpty(billingDate)) {
			DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.sss");
			newBilling.setBillingDate(formatter.parse(billingDate));
		}
		newBilling.setBillingItems(billingItems);
		billingService.addEntity(newBilling);
		return newBilling;
	}

}
