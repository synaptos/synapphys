/**
 * @license naviNode.manager.js v1.0.0
 * Manage currently navimatrix nodes as singleton.
 * Copyright (c) 2015 Walter Quendler
 */

/**
 * Create the NaviNodeContext object for a given options
 * @constructor
 * @param {Object} options - An object literal containing options
 * @returns {Object}
 */
function NaviNodeContext(options) {
	var datakey = '__naviNodeContext__';

	// Allow instantiation without new keyword
	if (!(this instanceof NaviNodeContext)) {
		return new NaviNodeContext(options);
	}

	var instance = $('body').data(datakey);

	if (instance) {
		if (options === undefined || options === null) {
			return instance;
		} else {
			$('body').removeData(datakey);
		}
	}
	
	var $navi = $('#innerNavimatrixDiv');
	
	// Must have a navi matrix div
	if (!$navi.length) {
		$.error('NaviNodeContext creation failed: missing navi matrix div!');
		return;
	}
	
	this.$navi = $navi;

	var pnzm = this.$navi.panzoom('instance');

	// Must have a panzoom object
	if (pnzm === undefined || pnzm === null) {
		$.error('NaviNodeContext creation failed: missing navi matrix panzoom!');
		return;
	}

	this.pnzm = pnzm;
	
	this.options = options ? options : {};
	
	this.nodeGotZoomVisible = null;
	this.nodeForZoomVisible = null;
	
	if (!this.pnzm.options) {
		this.pnzm.options = {};
	}
	if (!this.pnzm.options.startScale) {
		this.pnzm.options.startScale = 1;
	}	
	if (!this.pnzm.options.startX) {
		this.pnzm.options.startX = 0;
	}
	if (!this.pnzm.options.startY) {
		this.pnzm.options.startY = 0;
	}

	var svg = this.options.svg;
	
	// Must have a svg object
	if (svg === undefined || svg === null) {
		svg = this.$navi.svg('get');		
	}
	if (svg === undefined || svg === null) {
		$.error("NaviNodeContext creation failed: missing jquery svg instance!");
		return;
	}
	
	this.svg = svg;
	
	$('body').data(datakey, this);
	
	this.hideNaviNodesVisible();
	
	this.bindNaviNodesEventHandler();
}

NaviNodeContext.prototype = {
	setNodeForZoomVisible: function(node) {
		var self = this;
		
		if (self.nodeGotZoomVisible) {
			var $zoom = $(self.nodeGotZoomVisible).find('[u-zoom-visible]');
			
			$zoom.each(function(i) {
				var $v = $(this);
				$v.css({'display':'none'});
			});
	
			self.nodeGotZoomVisible = null;
		}
		
		self.nodeForZoomVisible = node;
	},
	
	toggleNaviNodesVisibleByScale: function() {
		var self = this;
		
		var scale = +self.pnzm.getMatrix()[0];
		
		if (self.nodeGotZoomVisible) {
			var $zoom = $(self.nodeGotZoomVisible).find('[u-zoom-visible]');
			
			$zoom.each(function(i) {
				var $v = $(this);
				var vScale = (parseInt($v.attr('u-zoom-visible')) || 100)/100;
				if (scale >= vScale) {
					$v.css({'display':''});
				} else {
					$v.css({'display':'none'});
				}
			});	
		}

		if (self.nodeForZoomVisible) {
			var $zoom = $(self.nodeForZoomVisible).find('[u-zoom-visible]');
			
			$zoom.each(function(i) {
				var $v = $(this);
				var vScale = (parseInt($v.attr('u-zoom-visible')) || 100)/100;
				if (scale >= vScale) {
					$v.css({'display':''});
				} else {
					$v.css({'display':'none'});
				}
			});

			self.nodeGotZoomVisible = self.nodeForZoomVisible;
			self.nodeForZoomVisible = null;
		}
	},

	hideNaviNodesVisible: function(root) {
		if (!root) {
			root = this.$navi.find('#g_MENU');
		}

		var $zoom = $(root).find('[u-zoom-visible]');
		
		$zoom.each(function(i) {
			$(this).css({'display':'none'});
		});
	},
	
	bindNaviNodesEventHandler: function() {
		var self = this;
		
		var $click = $('#svg_MENU').find('[u-event-click-handler]');
		
		$click.each(function(i) {
			$v = $(this);
			
			var fnClick = $v.attr('u-event-click-handler');
	
			if (fnClick && fnClick.trim().length) {
				var fnDblclick = $v.attr('u-event-dblclick-handler');
				
				if (!fnDblclick) {
					var handler = window['navi_'+fnClick];
					
					if (!handler) {
						handler = self.displayNaviMacroNotSupported;
					}

					$v.click(handler);
				}
			}
		});
		
		$click = $('#svg_MENU').find('[u-event-dblclick-handler]');
		
		$click.each(function(i) {
			$v = $(this);
			
			var fnDblclick = $v.attr('u-event-dblclick-handler');
	
			if (fnDblclick && fnDblclick.trim().length) {
				if (fnDblclick.trim() == "explodeNode") {
					$v.panzoom({
						disablePan:true,
						disableZoom:true,
						cursor:'pointer',
						$context:self.$navi
					});
				} else {
					$v.dblclick(self.displayNaviMacroNotSupported);				
				}
			}
		});
	},
	
	displayNaviMacroNotSupported: function(e) {
		var fnName = $(e.target).attr('u-event-click-handler');
		throw "Makro is not supported yet: "+fnName;
	},
	
	/**
	 * Calculates drop event position, where node starts
	 * @param e
	 * @param offset
	 * @param matrix
	 * @returns
	 */
	getStartPosition: function(e,offset,matrix) {
		if (!e) {
			return null;
		}
		if (!offset) {
			return null;
		}
		if (!matrix) {
			return null;
		}
		var pageX = e.pageX;
		var pageY = e.pageY;
		if (e.changedTouches && e.changedTouches.length) {
			pageX = e.changedTouches[0].pageX;
			pageY = e.changedTouches[0].pageY;
		}

		//initial body scale
		var	startScale = this.pnzm.options.startScale;
		var startX = this.pnzm.options.startX;
		var startY = this.pnzm.options.startY;
		
		//current body scale
		var naviScale = startScale*+matrix[0];
	
		var x = (pageX-offset.left)/naviScale-(startX/startScale);
		var y = (pageY-offset.top)/naviScale-(startY/startScale);
		
		return {x:x,y:y};
	}
	
}
