///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TextEditor needs more functions

if (PrimeFaces.widget.TextEditor !== undefined) {				
	PrimeFaces.widget.TextEditor.prototype.undo = function() {
		this.editor.history.undo();
	}

	PrimeFaces.widget.TextEditor.prototype.redo = function() {
		this.editor.history.redo();
	}

	PrimeFaces.widget.TextEditor.prototype.focus = function() {
		this.editor.focus();
		this.editor.setSelection(this.editor.getLength(), 1);
	}
}
