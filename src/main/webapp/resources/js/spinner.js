
/**
 * Inkrement-Methode fuer die Spinner Component 
 * Erhoeht die value um 1, im angezeigten Text und im hiddenInput, solange die value kleiner als 10 ist
 */
function spinner_increment() {
	var $number = $(this).parent().prev().children("[id$='number']");
	if (Number($number.text()) < 10) {
		$number.text(Number($number.text()) + 1);
		var $hiddenInput = $(this).parent().prev().children("[id$='numberHidden']");
		$hiddenInput.val(Number($hiddenInput.val()) + 1);
	}
}


/**
 * Dekrement-Methode fuer die Spinner Component 
 * Reduziert die value um 1, im angezeigten Text und im hiddenInput, solange die value groesser als 0 ist
 */
function spinner_decrement() {
	var $number = $(this).parent().next().children("[id$='number']");
	if (Number($number.text()) > 0) {
		$number.text(Number($number.text()) - 1);
		var $hiddenInput = $(this).parent().next().children("[id$='numberHidden']");
		$hiddenInput.val(Number($hiddenInput.val()) - 1);
	}
}
