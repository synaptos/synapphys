///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Date/Time i18n configuration

var synapphys = {
	locales: {
		de: {
			monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
			monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
			dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
			dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
			dayNamesMin: ['S', 'M', 'D', 'M ', 'D', 'F ', 'S'],
		}
	}
}

PrimeFaces.locales['de'] = {
    closeText: 'Schließen',
    prevText: 'Zurück',
    nextText: 'Weiter',
    monthNames: synapphys.locales.de.monthNames,
    monthNamesShort: synapphys.locales.de.monthNamesShort,
    dayNames: synapphys.locales.de.dayNames,
    dayNamesShort: synapphys.locales.de.dayNamesShort,
    dayNamesMin: synapphys.locales.de.dayNamesMin,
    weekHeader: 'Woche',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Nur Zeit',
    timeText: 'Zeit',
    hourText: 'Stunde',
    minuteText: 'Minute',
    secondText: 'Sekunde',
    currentText: 'Heute',
    ampm: false,
    month: 'Monat',
    week: 'Woche',
    day: 'Tag',
    allDayText: 'Ganztägig',
	titleFormat: {
    	month: 'MMMM YYYY',
       	week: 'DD. MMMM YYYY',
       	day: 'DD. MMMM YYYY'
	},
	columnFormat: {
		month: 'ddd',
		week: 'ddd DD. MMM',
		day:'ddd'
	},
    MONTHS: synapphys.locales.de.monthNames,
    MONTHS_SHORT: synapphys.locales.de.monthNamesShort,
    DAYS: synapphys.locales.de.dayNames,
    DAYS_SHORT: synapphys.locales.de.dayNamesShort,
    ZOOM_IN: 'Vergrößern',
    ZOOM_OUT: 'Verkleinern',
    MOVE_LEFT: 'Nach links',
    MOVE_RIGHT: 'Nach rechts',
    CREATE_NEW_EVENT: 'Termin hinzufügen',
    NEW: 'Neu'
}
			
if (PrimeFaces.widget.Schedule !== undefined) {				
	PrimeFaces.widget.Schedule.prototype.configureLocale = function() {
    	var a = PrimeFaces.locales[this.cfg.locale];			        
        if (a) {
            this.cfg.firstDay = a.firstDay;
            this.cfg.monthNames = a.monthNames;
            this.cfg.monthNamesShort = a.monthNamesShort;
            this.cfg.dayNames = a.dayNames;
            this.cfg.dayNamesShort = a.dayNamesShort;
            this.cfg.buttonText = {
                today: a.currentText,
                month: a.month,
                week: a.week,
                day: a.day
            };
            this.cfg.allDayText = a.allDayText;
            if (a.eventLimitText) {
                this.cfg.eventLimitText = a.eventLimitText;
            }
            if (a.titleFormat) {
		    	this.cfg.titleFormat = a.titleFormat;
		    }
            if (a.columnFormat) {
		    	this.cfg.columnFormat = a.columnFormat;
		    }
        }						        							
	}
}
