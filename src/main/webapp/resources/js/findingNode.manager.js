/**
 * @license findingNode.manager.js v1.0.0
 * Manage currently selected body finding nodes as singleton.
 * Copyright (c) 2015 Walter Quendler
 */

/**
 * Create the FindingNodeContext object for a given options
 * @constructor
 * @param {Object} options - An object literal containing options
 * @returns {Object}
 */
function FindingNodeContext(options) {	
	var datakey = '__findingNodeContext__';

	// Allow instantiation without new keyword
	if (!(this instanceof FindingNodeContext)) {
		return new FindingNodeContext(options);
	}

	var instance = $('body').data(datakey);

	if (instance) {
		if (options === undefined || options === null) {
			return instance;
		} else {
			$('body').removeData(datakey);
		}
	}

	var $body = $('#innerBodymapDiv');
	
	// Must have a div
	if (!$body.length) {
		$.error('FindingNodeContext creation failed: missing inner body map div!');
		return;
	}
	
	this.$body = $body;

	var pnzm = this.$body.panzoom('instance');
	
	// Must have a panzoom object
	if (pnzm === undefined || pnzm === null) {
		$.error('FindingNodeContext creation failed: missing inner body map panzoom!');
		return;
	}

	this.pnzm = pnzm;
	
	this.options = options ? options : {};
	
	if (this.options.tipVisibleScale === undefined || this.options.tipVisibleScale === null) {
		this.options.tipVisibleScale = 1.0;
	}
	if (this.options.nodeCenterOffset === undefined || this.options.nodeCenterOffset === null) {
		this.options.nodeCenterOffset = 0;
	}	
	if (this.options.onAddNodeSelected === undefined || this.options.onAddNodeSelected === null) {
		this.options.onAddNodeSelected = function(node){};
	}		
	if (this.options.onClearNodeSelected === undefined || this.options.onClearNodeSelected === null) {
		this.options.onClearNodeSelected = function(nodesSelected){};
	}
	if (this.options.onDeleteNodesSelected === undefined || this.options.onDeleteNodesSelected === null) {
		this.options.onDeleteNodesSelected = function(nodesDeleted){};
	}

	if (this.options.onStartNodePnzm === undefined || this.options.onStartNodePnzm === null) {
		this.options.onStartNodePnzm = function(se,pnzm,e,touches){};
	}
	if (this.options.onChangeNodePnzm === undefined || this.options.onChangeNodePnzm === null) {
		this.options.onChangeNodePnzm = function(e,pnzm,transform){};
	}
	if (this.options.onPanNodePnzm === undefined || this.options.onPanNodePnzm === null) {
		this.options.onPanNodePnzm = function(e,pnzm,x,y){};
	}
	if (this.options.onEndNodePnzm === undefined || this.options.onEndNodePnzm === null) {
		this.options.onEndNodePnzm = function(e,pnzm,matrix,changed){};
	}

	if (this.options.onStartNodeTipPnzm === undefined || this.options.onStartNodeTipPnzm === null) {
		this.options.onStartNodeTipPnzm = function(se,pnzm,e,touches){};
	}
	if (this.options.onChangeNodeTipPnzm === undefined || this.options.onChangeNodeTipPnzm === null) {
		this.options.onChangeNodeTipPnzm = function(e,pnzm,transform){};
	}
	if (this.options.onEndNodeTipPnzm === undefined || this.options.onEndNodeTipPnzm === null) {
		this.options.onEndNodeTipPnzm = function(e,pnzm,matrix,changed){};
	}

	if (this.options.onStartNodeHookPnzm === undefined || this.options.onStartNodeHookPnzm === null) {
		this.options.onStartNodeHookPnzm = function(se,pnzm,e,touches){};
	}
	if (this.options.onChangeNodeHookPnzm === undefined || this.options.onChangeNodeHookPnzm === null) {
		this.options.onChangeNodeHookPnzm = function(e,pnzm,transform){};
	}
	if (this.options.onEndNodeHookPnzm === undefined || this.options.onEndNodeHookPnzm === null) {
		this.options.onEndNodeHookPnzm = function(e,pnzm,matrix,changed){};
	}
	
	if (this.options.tipVisibleScale < 0) {
		$.error("FindingNodeContext creation failed: tipVisibleScale < 0 isn't supported!");
		return;
	}
	
	this.nodesSelected = [];
	
	if (!this.pnzm.options) {
		this.pnzm.options = {};
	}
	if (!this.pnzm.options.startScale) {
		this.pnzm.options.startScale = 1;
	}
	if (!this.pnzm.options.startX) {
		this.pnzm.options.startX = 0;
	}
	if (!this.pnzm.options.startY) {
		this.pnzm.options.startY = 0;
	}

	var svg = this.options.svg;
	
	// Must have a svg object
	if (svg === undefined || svg === null) {
		svg = this.$body.svg('get');
	}	
	if (svg === undefined || svg === null) {
		$.error("FindingNodeContext creation failed: missing jquery svg instance!");
		return;
	}
	
	this.svg = svg;
	
	$('body').data(datakey, this);	
}

//TODO WQ: node tip text wrap, cut long words
//TODO WQ: more tolerance on drop symbol on same finding type (better for touch pad)
FindingNodeContext.prototype = {
	/**
	 * Extracts node type
	 * @param nodeId
	 * @returns
	 */
	getNodeType: function(nodeId) {
		if (nodeId === undefined || nodeId === null) {
			return null;
		}
		return nodeId.substring(nodeId.indexOf("_")+1,nodeId.lastIndexOf("_"));
	},
		
	/**
	 * Builds node path id
	 * @param nodeId
	 * @returns
	 */
	getNodePathId: function(nodeId) {
		if (nodeId === undefined || nodeId === null) {
			return null;
		}
		return nodeId.substring(0,nodeId.lastIndexOf("_")).replace("g_","path_");
	},	
	
	/**
	 * Extracts body map item id
	 * @param nodeId
	 * @returns
	 */
	getNodeBodymapItemId: function(nodeId) {
		if (nodeId === undefined || nodeId === null) {
			return null;
		}
		return +nodeId.substring(nodeId.lastIndexOf("_")+1);
	},
	
	/**
	 * Calculates drop event position, where node starts
	 * @param e
	 * @param offset
	 * @param matrix
	 * @returns
	 */
	getStartPosition: function(e,offset,matrix) {
		if (!e) {
			return null;
		}
		if (!offset) {
			return null;
		}
		if (!matrix) {
			return null;
		}
		var pageX = e.pageX;
		var pageY = e.pageY;
		var changedTouches = e.changedTouches || (e.originalEvent && e.originalEvent.changedTouches);
		if (changedTouches && changedTouches.length) {
			pageX = changedTouches[0].pageX;
			pageY = changedTouches[0].pageY;
		}
		if (pageX === undefined || pageX === null) {
			return null;
		}
		if (pageY === undefined || pageY === null) {
			return null;
		}

		//initial body scale
		var	startScale = this.pnzm.options.startScale;
		var startX = this.pnzm.options.startX;
		var startY = this.pnzm.options.startY;
		
		//current body scale
		var	bodyScale = +matrix[0]*startScale;
	
		var x = (pageX-offset.left)/bodyScale-(startX/startScale);
		var y = (pageY-offset.top)/bodyScale-(startY/startScale);
		
		return {x:x,y:y};
	},

	/**
	 * Calculates difference of given positions
	 * @param position1
	 * @param position2
	 * @returns
	 */
	getNodePositionOffset: function(position1,position2) {
		if (!position1) {
			return null;
		}
		if (!position1) {
			return null;
		}
		var offset = {left:position1.x-position2.x,top:position1.y-position2.y};
		return offset;		
	},

	/**
	 * Checks if node has already been selected
	 * @param node
	 * @returns {Boolean}
	 */
	isNodeSelected: function(node) {
		if (!node) {
			return false;
		}
		if (!this.nodesSelected.length) {
			return false;
		}
		
		var selected = false;	
		$.each(this.nodesSelected,function(i,v){
			if (v.id === node.id) {
				selected = true;
				return false; //break
			}
		});
		
		return selected;
	},
	
	/**
	 * Takes 2 nodes and checks if they are adjacent
	 * @param node1
	 * @param node2
	 * @returns {Boolean}
	 */
	getNodesAdjacent: function(node1,node2) {
		if (!node1) {
			return false;
		}
		if (!node2) {
			return false;
		}
		if (!this.nodesSelected.length) {
			return true;
		}			
		
		var pnzm1 = $(node1).panzoom('instance');
		var pnzm2 = $(node2).panzoom('instance');
		
		var matrix1 = pnzm1.getMatrix();
		var matrix2 = pnzm2.getMatrix();
		
		var x1 = +matrix1[4];
		var y1 = +matrix1[5];

		var x2 = +matrix2[4];
		var y2 = +matrix2[5];
		
		var a = x2-x1;
		var b = y1-y2;
		var c = Math.sqrt(Math.pow(a,2)+Math.pow(b,2));
		if (c <= this.options.nodeCenterOffset*2*1.25) {
			return true;
		}

		return false;
	},

	/**
	 * Checks if node intercepts another one
	 * @param node
	 * @returns {Boolean}
	 */
	isNodeIntercepting: function(node) {
		if (!node) {
			return false;
		}

		var pnzm = $(node).panzoom('instance');
		
		var matrix = pnzm.getMatrix();
		
		var x = +matrix[4];
		var y = +matrix[5];
					
		return this.isPositionIntercepting(node.id,x,y) !== null;
	},
	
	/**
	 * Checks if node covers another one
	 * @param node
	 * @returns {Boolean}
	 */
	isNodeCovering: function(node) {
		if (!node) {
			return false;
		}

		var pnzm = $(node).panzoom('instance');
		
		var matrix = pnzm.getMatrix();
		
		var x = +matrix[4];
		var y = +matrix[5];
					
		return this.isPositionCovering(node.id,x,y);
	},

	/**
	 * Check if there are nodes at that position
	 * @param nodeId
	 * @param x
	 * @param y
	 * @returns {Boolean}
	 */
	isPositionCovering: function(nodeId,x,y) {
		return this.isPositionIntercepting(nodeId,x,y,0.5) !== null;
	},
	
	/**
	 * Check if there are nodes around that position
	 * @param nodeId
	 * @param x
	 * @param y
	 * @param within
	 * @returns
	 */
	isPositionIntercepting: function(nodeId,x,y,within) {
		if (nodeId === undefined || nodeId === null) {
			return false;
		}
		if (x === undefined || x === null) {
			return false;
		}
		if (y === undefined || y === null) {
			return false;
		}
		if (y === undefined || y === null) {
			within = 1.75;
		}
		
		var x1 = x;
		var y1 = y;

		var nodeType = this.getNodeType(nodeId);

		if (nodeType === undefined || nodeType === null) {
			return false;
		}

		var $layer = this._getLayerByData({findingType:nodeType});		

		if (!$layer.length) {
			return false;
		}
		
		var self = this;
		
		var intercepting = null;
		var $flock = $layer.find('[id^="g_'+nodeType+'"]');
		$.each($flock,function(i,v){
			if (v.id !== nodeId) {
				var matrix2 = $(v).panzoom('instance').getMatrix();
														
				if (matrix2) {	
					var x2 = +matrix2[4];
					var y2 = +matrix2[5];
				
					var a = x2-x1;
					var b = y1-y2;
					var c = Math.sqrt(Math.pow(a,2)+Math.pow(b,2));
					if (c <= self.options.nodeCenterOffset*within) {
						intercepting = {x:x2,y:y2};
						return false; //break
					}
				}
			}
		});

		return intercepting;
	},
			
	/**
	 * Checks if node is adjacent to any selected one
	 * @param node
	 * @returns {Boolean}
	 */
	isNodeAdjacentSelected: function(node) {
		if (!node) {
			return false;
		}
		
		var self = this;
		
		var adjacent = false;
		$.each(self.nodesSelected,function(i,v){
			if (self.getNodesAdjacent(node,v)) {
				adjacent = true;
				return false; //break
			}
		});
		
		return adjacent;		
	},
	
	/**
	 * Gets adjacent nodes from the list of nodes for specified node
	 * @param {Element} node
	 * @param {Array} list
	 * @returns {Array|null} - If getting, returns an array of all nodes
	 *   which are adjacent to a given node
	 */
	getNodeAdjacentNodes: function(node,list) {
		if (!node) {
			return null;
		}
		if (!(list && list.length)) {
			return null;
		}
		var self = this;
		var connected = [];
		$.each(list,function(i,v) {
			if (self.getNodesAdjacent(node,v)) {
				connected.push(v);
			}
		});
		return connected;
	},	

	/**
	 * Finds inner nodes out of the list of nodes building a path from start to end node
	 * @param {Element} start
	 * @param {Element} end
	 * @param {Array} list
	 * @returns {Array|null}
	 */
	findNodeInnerPathOfAdjacentNodes: function(start,end,list) {
		if (!start) {
			return null;
		}
		if (!end) {
			return null;
		}		
		if (!list) {
			return null;
		}

		var self = this;
		
		var path = {};

		if (self.getNodesAdjacent(start,end)) {
			path[start.id] = start;
		}
		
		if (!list.length) {
			return path;
		}
				
		var connected = self.getNodeAdjacentNodes(start,list);
		
		if (connected) {
			$.each(connected,function(i,v) {
				var remaining = []; 
				$.each(list,function(j,n) {
					if (n.id !== v.id) {
						remaining.push(n);
					}
				});
				var vPath = self.findNodeInnerPathOfAdjacentNodes(v,end,remaining);
				if (vPath) {
					var i = 0;
					$.each(vPath,function(k,n) {
				        if (vPath.hasOwnProperty(k)) {
				        	path[k] = n;
				        	i++;
				        }
					});					
					if (i > 0) {
			        	path[start.id] = start;						
					}
				}
			});			
		}
					
		return path;
	},

	/**
	 * Finds inner nodes building a path from node to node selected
	 * @param {Element} node
	 * @returns {Array|null}
	 */
	findNodeInnerPathToNodeSelected: function(node) {
		if (!node) {
			return null;
		}
		
		var self = this;
		
		if (!self.nodesSelected.length) {
			return null;
		}
		
		var nodeId = node.id;

		if (nodeId === undefined || nodeId === null) {
			return null;
		}

		var nodeType = self.getNodeType(nodeId);

		if (nodeType === undefined || nodeType === null) {
			return null;
		}

		var end = self.nodesSelected[self.nodesSelected.length-1];

		if (nodeType !== self.getNodeType(end.id)) {
			return null;
		}

		var $layer = $(node).parent();		

		if (!$layer.length) {
			return false;
		}
		
		var list = [];
		var $all = $layer.find('[id^="g_'+nodeType+'"]');
		$.each($all,function(i,v){
			if (v.id !== node.id && v.id !== end.id) {
				list.push(v);
			}
		});

		var pathAsObj = self.findNodeInnerPathOfAdjacentNodes(node,end,list);
		
		var pathAsList = [];
		$.each(pathAsObj,function(k,v) {
	        if (pathAsObj.hasOwnProperty(k)) {
	        	pathAsList.push(v);
	        }
		});
		
		return pathAsList;
	},

	/**
	 * Puts a node to the list of selected
	 * @param {Element} node
	 * @param {Boolean} silent - If true, the add node event will not be triggered (default: false)
	 */
	addNodeSelected: function(node,silent) {
		if (!node) {
			return;
		}
		if (silent === undefined || silent === null) {
			silent = false;
		}
		
		if (this.isNodeSelected(node)) {
			return;
		}
		
		var $path = $(node).find('#'+this.getNodePathId(node.id));
		$path.css({'stroke':'#0000ff','stroke-width':2.5,'fill':'#0000ff','fill-opacity':0.3});
		
		this.nodesSelected.push(node);

		if (!silent) {
			this.options.onAddNodeSelected(node);
		}
	},

	/**
	 * Puts several nodes to the list of selected
	 * @param {Element} nodes
	 * @param {Boolean} silent - If true, the add node event will not be triggered (default: false)
	 */
	addNodesSelected: function(nodes,silent) {
		if (!(nodes && nodes.length)) {
			return;
		}
		if (silent === undefined || silent === null) {
			silent = false;
		}
		
		var self = this;
		
		$.each(nodes,function(i,v){
			self.addNodeSelected(v,true);
		});

		self.options.onAddNodeSelected(self.nodesSelected[self.nodesSelected.length-1]);
	},
	
	/**
	 * Gets nodes selected
	 * @returns {Array}
	 */
	getNodesSelected: function() {
		return this.nodesSelected;		
	},

	/**
	 * Removes a node from nodes selected
	 * @param {Element} node
	 */
	clearNodeSelected: function(node) {
		if (!node) {
			return;
		}
		if (!this.nodesSelected.length) {
			return;
		}
	
		var nodeId = node.id;

		if (nodeId === undefined || nodeId === null) {
			return;
		}
		
		var position = -1;		
		$.each(this.nodesSelected,function(i,v){
			if (v.id === nodeId) {
				position = i;
				return false; //break
			}			
		});
		
		if (position > -1) {
			var nodeType = this.getNodeType(nodeId);
			var $path = $(node).find('#'+this.getNodePathId(nodeId));
			if (nodeType === 'SKIN_LEAD_AREA') {
		    	$path.css({'stroke':'#000000','stroke-width':0.7,'fill':'#ffffff','fill-opacity':0.5});				
			} else {
		    	$path.css({'stroke':'#000000','stroke-width':0.7,'fill':'#000000','fill-opacity':0});				
			}
			
	    	this.nodesSelected.splice(position,1);
			
			this.options.onClearNodeSelected(this.nodesSelected);
	    }	    
	},
	
	/**
	 * Removes the nodes from nodes selected
	 */
	clearNodesSelected: function() {
		var self = this;

		if (!self.nodesSelected.length) {
			return;
		}
			
		$.each(self.nodesSelected,function(i,v){			
			var nodeType = self.getNodeType(v.id);
			var $path = $(v).find('#'+self.getNodePathId(v.id));
			if (nodeType === 'SKIN_LEAD_AREA') {
				$path.css({'stroke':'#000000','stroke-width':0.7,'fill':'#ffffff','fill-opacity':0.5});
			} else {
				$path.css({'stroke':'#000000','stroke-width':0.7,'fill':'#000000','fill-opacity':0});				
			}
		});
		
		self.nodesSelected = [];
		
		self.options.onClearNodeSelected(self.nodesSelected);
	},
	
	/**
	 * If a node is not selected, adds it and adjacent nodes as selected
	 * @param {Element} node
	 * @returns {Boolean}
	 */
	setNodeSelected: function(node){
		if (!this.isNodeSelected(node)) {
			var innerNodes = this.findNodeInnerPathToNodeSelected(node);
			if (innerNodes && innerNodes.length) {
				this.addNodesSelected(innerNodes);	
			} else {
				if (!this.isNodeAdjacentSelected(node)) {
					this.clearNodesSelected();			
				}
				this.addNodeSelected(node);
			}
			return true;
		}
		return false;
	},

	/**
	 * Sets a node as selected and clear others if not adjacent
	 * @param {Element} node
	 */
	toggleNodeSelected: function(node){
		if (!this.setNodeSelected(node)) {
			this.clearNodeSelected(node);
		}
	},
	
	/**
	 * Deletes all selected nodes
	 * @param {Boolean} silent - If true, the delete node event will not be triggered (default: false)
	 */
	deleteNodesSelected: function(silent) {
		if (silent === undefined || silent === null) {
			silent = false;
		}			

		var self = this;

		if (!self.nodesSelected.length) {
			return;
		}
		
		$.each(self.nodesSelected,function(i,v){
			$v = $(v);			
			var pnzm = $v.panzoom('instance');
			if (pnzm) {
				pnzm.destroy();
			}
			self.svg.remove($v);
		});
		
		self.options.onDeleteNodesSelected(self.nodesSelected);
		
		self.clearNodesSelected();
	},
	
	/**
	 * Shows/hides finding nodes of a layer according scale
	 * @param {Boolean} force - If true, switch visibility every time scale is above/below threshold (default: false), 
	 *    else visibility is set once threshold if scale is going through threshold
	 * @param {Number} increment 
	 */
	toogleNodesTipVisibleByScale: function(force,increment) {
		var self = this;
		if (force === undefined || force === null) {
			force = false;
		}
		var scale = +this.pnzm.getMatrix()[0];
		var options = this.pnzm.options;
		var zoomIncrement = options && options.increment ? options.increment : 0.1;
		if (increment !== undefined && increment != null) {
			zoomIncrement = increment
		}
		if (scale >= self.options.tipVisibleScale) {
			if (force || scale < self.options.tipVisibleScale+zoomIncrement) {
				var $layer = self.$body.find('[id^="g_FINDINGS_"]');			
				$.each($layer,function(i,v){
					var display = $(v).css('display');
					if (display === undefined || display === null || display !== 'none') {
						var $list = $(v).find('g');
						$.each($list,function(j,n){
							self.showNodeTip(n);
						});
					}
				});
			}
		} else if (force || scale >= self.options.tipVisibleScale-zoomIncrement) {
			var $layer = self.$body.find('[id^="g_FINDINGS_"]');
			$.each($layer,function(i,v){
				var display = $(v).css('display');
				if (display === undefined || display === null || display !== 'none') {
					var $list = $(v).find('g');
					$.each($list,function(j,n){
						self.hideNodeTip(n);
					});
				}
			});
		}		
	},
	
	/**
	 * Displays the tip of the node
	 * @param {Element} node
	 */
	showNodeTip: function(node) {
		//Check argument
		if (!node) {
			return;
		}

		var $tip = $(node).find('[id^="g_NODE_TIP_"]');
		
		if (!($tip && $tip.length)) {
			return;
		}

		$tip.css({'display':''});

		this.updateNodePathTip(node);
	},

	/**
	 * Hides the tip of the node
	 * @param {Element} node
	 */
	hideNodeTip: function(node) {
		//Check argument
		if (!node) {
			return;
		}
	
		var $tip = $(node).find('[id^="g_NODE_TIP_"]');
		
		if (!($tip && $tip.length)) {
			return;
		}

		$tip.css({'display':'none'});
		
		this.deleteNodePathTip(node);		
	},
	
	/**
	 * Finds the layer which contains the nodes
	 * @param {Object} data
	 * @returns {jQuery}
	 */
	_getLayerByData: function(data) {
		//Check argument
		if (!data) {
			return $();
		}
		//Check for valid fields
		if (data.findingType === undefined || data.findingType === null) {
			return $();
		}

		return this.$body.find('#g_FINDINGS_'+data.findingType.substring(0,data.findingType.indexOf("_")));
	},

	/**
	 * Finds the body node
	 * @param {Object} data
	 * @returns {jQuery}
	 */
	_getNodeByData: function(data) {
		var $layer = this._getLayerByData(data);

		if (!$layer.length) {
			return $();
		}

		if (data.bodymapItemId === undefined || data.bodymapItemId === null) {
			return $();
		}
		
		return $layer.find('#g_'+data.findingType+'_'+data.bodymapItemId);
	},
	
	/**
	 * Creates a node
	 * @param {Object} data
	 */
	createNode: function(data) {
		var $layer = this._getLayerByData(data);

		if (!$layer.length) {
			return;
		}
				
		//Check for valid fields
		if (data.bodymapItemId === undefined || data.bodymapItemId === null) {
			return;
		}
		if (data.findingPositionX === undefined || data.findingPositionX === null) {
			return;
		}
		if (data.findingPositionY === undefined || data.findingPositionY === null) {
			return;
		}
		
	   	var $tool = $('#g_'+data.findingType);

		if (!$tool.length) {
			return;
		}

		var clone = this.svg.clone($layer,$tool)[0];
		this.svg.change(clone,{id:'g_'+data.findingType+'_'+data.bodymapItemId,style:'display:'});

		$(clone).panzoom({
			disableZoom:true,
			cursor:'pointer',
			startTransform:'matrix(1,0,0,1,'+(data.findingPositionX-this.options.nodeCenterOffset)+','+(data.findingPositionY-this.options.nodeCenterOffset)+')',
			$context:this.$body,
			onStart:this.options.onStartNodePnzm,
			onChange:this.options.onChangeNodePnzm,
			onPan:this.options.onPanNodePnzm,
			onEnd:this.options.onEndNodePnzm
		});
		
		if (data.findingType === 'SKIN_LEAD_AREA') {
		    for (var n = 0; n < 4; n++) {
				$(clone).find('#g_SKIN_LEAD_AREA_H'+n).panzoom({
					disableZoom:true,
					cursor:'pointer',
					$context:this.$body,
					onStart:this.options.onStartNodeHookPnzm,
					onChange:this.options.onChangeNodeHookPnzm,
					onEnd:this.options.onEndNodeHookPnzm
				});
		    }
		} else if (data.findingType === 'SKIN_LEAD_VECTOR') {
			$(clone).find('#g_SKIN_LEAD_VECTOR_H').panzoom({
				disableZoom:true,
				cursor:'pointer',
				$context:this.$body,
				onStart:this.options.onStartNodeHookPnzm,
				onChange:this.options.onChangeNodeHookPnzm,
				onEnd:this.options.onEndNodeHookPnzm
			});
		}
	},

	/**
	 * Selects a node
	 * @param {Object} data
	 */
	selectNode: function(data) {
		var $node = this._getNodeByData(data);

		if (!$node.length) {
			return;
		}
		
		this.addNodeSelected($node[0]);
	},
	
	/**
	 * Updates a node
	 * @param {Object} data
	 */
	updateNode: function(data) {
		var $layer = this._getLayerByData(data);

		if (!$layer.length) {
			return;
		}
		
		//Check for valid fields
		if (data.bodymapItemId === undefined || data.bodymapItemId === null) {
			return;
		}
		if (data.tempFindingNodeId === undefined || data.tempFindingNodeId === null) {
			return;
		}

		var $node = $layer.find('#g_'+data.findingType+'_'+data.tempFindingNodeId);

		if (!$node.length) {
			return;
		}
		
		this.svg.change($node[0],{id:'g_'+data.findingType+'_'+data.bodymapItemId});
		
		this.updateNodeTip(data);
		
		this.updateNodeSlot(data);

		this.updateSequenceNo(data);
	},
	
	/**
	 * Updates the sequence no of a node
	 * @param {Object} data
	 */
	updateSequenceNo: function(data) {
		var $node = this._getNodeByData(data);

		if (!$node.length) {
			return;
		}
		
		if (!(data.sequenceNo === undefined || data.sequenceNo === null)) {
			var $tspan = $node.find('#text_NODE_SEQUENCE_NO > tspan');
			if ($tspan.length) {
				$tspan.text(data.sequenceNo);	
			}
		}		
	},

	/**
	 * Updates the slot of a node
	 * @param {Object} data
	 */
	updateNodeSlot: function(data) {
		var $node = this._getNodeByData(data);

		if (!$node.length) {
			return;
		}
		
		if (!(data.findingTypeModifier === undefined || data.findingTypeModifier === null)) {
			var $slot = $node.find('#g_NODE_S');
			if ($slot.length) {
				$slot.children().remove();
				
			   	var $tool = $('#g_'+data.findingTypeModifier);

				if (!$tool.length) {
					return;
				}
				
				this.svg.clone($slot,$tool.find('#g_NODE_C'));
			}
		}		
	},
	
	/**
	 * Updates the tip of a node
	 * @param {Object} data
	 */
	updateNodeTip: function(data) {
		var $node = this._getNodeByData(data);

		if (!$node.length) {
			return;
		}

		this.deleteNodeTip($node[0]);		   	

		if ((data.valueF === undefined || data.valueF === null) 
				&& (data.valueT === undefined || data.valueT === null)
				&& (data.note === undefined || data.note === null)) {				
			return;
		}
		
	   	var $tool = $('#findingToolTemplate_NODE_TIP').find('#g_NODE_TIP');

		if (!$tool.length) {
			return;
		}

		var clone = this.svg.clone($node,$tool)[0];
		this.svg.change(clone,{id:'g_NODE_TIP_'+(new Date().getTime()),style:'display:none'});

		var $tip = $(clone).panzoom({
			disableZoom:true,
			cursor:'pointer',
			startTransform:'matrix(1,0,0,1,'+data.tipPositionX+','+data.tipPositionY+')',
			$context:this.$body,
			onStart:this.options.onStartNodeTipPnzm,
			onChange:this.options.onChangeNodeTipPnzm,
			onEnd:this.options.onEndNodeTipPnzm
		});
		
		if (!(data.valueF === undefined || data.valueF === null)) {
			var $tspan = $tip.find('#text_NODE_TIP_S0 > tspan');
			if ($tspan.length) {
				if (!(data.valueT === undefined || data.valueT === null)) {
					var $rect = $tip.find('#rect_NODE_TIP');
					if ($rect.length) {
						$rect.attr('width',$rect.attr('u-tipMaxWidth'));							
					}
					var rangeValueX = $tspan.parent().attr('u-rangeValueX');
					$tspan.parent().attr('x',rangeValueX);
					$tspan.attr('x',rangeValueX);
					$tspan.text(data.valueF+"°");
				} else {
					$tspan.text(data.valueF);					
				}
				$tspan.parent().css({'display':''});
			}		
		}
		if (!(data.valueT === undefined || data.valueT === null)) {
			var $tspan = $tip.find('#text_NODE_TIP_S1 > tspan');
			if ($tspan.length) {
				var $path = $tip.find('#path_NODE_TIP_V0');
				if ($path.length) {
					$path.css({'display':''});
				}				
				var rangeValueX = $tspan.parent().attr('u-rangeValueX');
				$tspan.parent().attr('x',rangeValueX);
				$tspan.attr('x',rangeValueX);
				$tspan.text(data.valueT+"°");				
				$tspan.parent().css({'display':''});
			}
		}
		if (!(data.note === undefined || data.note === null)) {
			var $text = $tip.find('#text_NODE_TIP_S2');
			if ($text.length) {
				var $rect = $tip.find('#rect_NODE_TIP');
				var $path = $tip.find('#path_NODE_TIP_H0');
				if ($rect.length && $path.length) {
					var rectWidth = parseInt($rect.attr('u-tipMaxWidth')) || 0;
					$rect.attr('width',rectWidth-17);
					var rangeBoxHeight = 0;
					if (!(data.valueF === undefined || data.valueF === null) 
							|| !(data.valueT === undefined || data.valueT === null)) {
						rangeBoxHeight = 30;
						$rect.attr('height',rangeBoxHeight*2);
						$path.css({'display':''});
					}
					$text.text(data.note);
					$text.css({'display':''});
					var textBoxDims = this.textWrapToRect($text[0],$rect[0],4,2);
					var textBoxHeight = textBoxDims.lineCount*textBoxDims.lineHeight;
					$rect.attr('width',rectWidth);
					$rect.attr('height',rangeBoxHeight+textBoxHeight+6);
					$text = $tip.find('#text_NODE_TIP_S2');
					if ($text.length) {
						$text.attr('y',rangeBoxHeight);
						if (data.valueT === undefined || data.valueT === null) {
							if (textBoxDims.lineCount < 2) {
								var textWidth = textBoxDims.lineWidth+8;
								if (textWidth < 27) {
									textWidth = 27;
								}
								$rect.attr('width',textWidth);
								$path.attr('d','m 0,30 '+textWidth+',0');
							}
						}
					}
				}
			}
		}

		var bodyMatrix = this.pnzm.getMatrix();
		if (+bodyMatrix[0] >= this.options.tipVisibleScale) {
			this.showNodeTip($node[0]);
		}			
	},

	/**
	 * Deletes the tip of a node
	 * @param {Element} node
	 */
	deleteNodeTip: function(node) {
		//Check argument
		if (!node) {
			return;
		}
	
		var $tip = $(node).find('[id^="g_NODE_TIP_"]');

		if (!$tip.length) {
			return;
		}

	    this.svg.remove($tip);
		
		this.deleteNodePathTip(node);
	},
	
	/**
	 * Creates a path between the tip and the node
	 * @param {Element} node
	 * @returns {Element}
	 */
	createNodePathTip: function(node) {
		//Check argument
		if (!node) {
			return;
		}
		
	   	var $tool = $('#findingToolTemplate_NODE_TIP').find('#path_NODE_TIP_C');

	   	var clone = this.svg.clone($(node),$tool)[0];
		this.svg.change(clone,{id:'path_NODE_TIP_C_'+(new Date().getTime())});
		
		return clone;
	},
	
	/**
	 * Updates the path between the tip and the node
	 * @param {Element} node
	 */
	updateNodePathTip: function(node) {
		if (!node) {
			return;
		}
		
		var $tip = $(node).find('[id^="g_NODE_TIP_"]');

		if (!$tip.length) {
			return;
		}
		
		var tipPnzm = $tip.panzoom('instance');
		
		if (tipPnzm) {
			var $rect = $tip.find('#rect_NODE_TIP');

			var rw = $rect.attr('width');
			var rh = $rect.attr('height');
			var rAlpha = Math.atan(rh/rw);
			
			var cx = this.options.nodeCenterOffset;
			var cy = this.options.nodeCenterOffset;
			var cr = this.options.nodeCenterOffset;
			
			var tipMatrix = tipPnzm.getMatrix();
			
			var rx = +tipMatrix[4]+rw/2;
			var ry = +tipMatrix[5]+rh/2;
			
			var a0 = cy-ry;
			var aq = 1;
			if (a0 < 0) {
				aq = -1;
				a0 *= aq;
			}
			var b0 = rx-cx;
			var bq = 1;
			if (b0 < 0) {
				bq = -1;
				b0 *= bq;
			}
			var alpha0 = Math.atan(a0/b0);
			
			var a1 = cr*Math.sin(alpha0);
			var b1 = cr*Math.cos(alpha0);
				
			var x1 = cx+(b1*bq);
			var y1 = cy-(a1*aq);
			
			var a2;
			var b2;
			if (alpha0 > rAlpha) {
				a2 = rh/2;
				b2 = a2/Math.tan(alpha0);
			} else {
				b2 = rw/2;
				a2 = b2*Math.tan(alpha0);
			}
			
			var x2 = rx-(b2*bq);
			var y2 = ry+(a2*aq);
			
			if ((Math.abs(rx-x1) <= rw/2 && Math.abs(ry-y1) <= rh/2) 
					|| (Math.abs(cx-rx) <= cr && Math.abs(cy-ry) <= cr)) {
				this.deleteNodePathTip(node);
				return;
			}

			var $path = $(node).find('[id^="path_NODE_TIP_C_"]');
			
			if (!$path.length) {
				$path = $(this.createNodePathTip(node));
			}
			
		   	this.svg.change($path[0],{d:'m '+x1+','+y1+' '+(x2-x1)+','+(y2-y1)});
		}		
	},

	/**
	 * Updates the path between hook nodes
	 * @param {Element} node
	 */
	updateNodePathHook: function(node) {
		if (!node) {
			return;
		}

		var $shape = $(node).find('#path_SKIN_LEAD_AREA');
		if ($shape.length) {
			var d = '';
		    for (var n = 0; n < 4; n++) {
				var hookMatrix = $(node).find('#g_SKIN_LEAD_AREA_H'+n).panzoom('instance').getMatrix();
				var x = +hookMatrix[4];
				var y = +hookMatrix[5];
				d += (n === 0 ? 'M ' : ' L ')+x+','+y;
		    }
			d+=' z';
			this.svg.change($shape[0],{d:d});		
		} else {
			$shape = $(node).find('#path_SKIN_LEAD_VECTOR_J');
			if ($shape.length) {
				var d = 'M 34,34';
				var hookMatrix = $(node).find('#g_SKIN_LEAD_VECTOR_H').panzoom('instance').getMatrix();
				var x = +hookMatrix[4];
				var y = +hookMatrix[5];
				d += ' L '+x+','+y;
				this.svg.change($shape[0],{d:d});		
			}
		}
		
	},
	
	/**
	 * Deletes the path between the tip and the node
	 * @param {Element} node
	 */
	deleteNodePathTip: function(node) {
		//Check argument
		if (!node) {
			return;
		}
	
		var $path = $(node).find('[id^="path_NODE_TIP_C_"]');
		
		if (!$path.length) {
			return;
		}

		this.svg.remove($path);		
	},

	/**
	 * Wraps a text to its outer rect
	 * @param textNode
	 * @param rectNode
	 * @param padding
	 * @param linePadding
	 * @returns {Number}
	 */
	textWrapToRect: function(textNode, rectNode, padding, linePadding) {
		var fz = parseInt(window.getComputedStyle(textNode)['font-size']),
			rx = parseInt(rectNode.getAttribute('x')),
			ry = parseInt(rectNode.getAttribute('y')),
			rw = parseInt(rectNode.getAttribute('width'));

	    var lineHeight = fz+linePadding;

		// Clone the original text node to store and display the final wrapping text

	    // False means any TSPANs in the textNode will be discarded
	    var wrapping = textNode.cloneNode(false);
	    wrapping.setAttributeNS(null,'x',rx+padding);
	    wrapping.setAttributeNS(null,'y',ry+padding);
	    
		// Make a copy of this node and hide it to progressively draw, measure and calculate line breaks

	    var testing = wrapping.cloneNode(false);
	    testing.setAttributeNS(null,'visibility','hidden');

	    var testingTspan = document.createElementNS(null,'tspan');
	    
	    var testingTextNode = document.createTextNode(textNode.textContent);
	    testingTspan.appendChild(testingTextNode);

	    testing.appendChild(testingTspan);
		var tester = document.getElementsByTagName('svg')[0].appendChild(testing);
	    
	    var words = textNode.textContent.split(" ");
	    var line = testingText = "";
	    var testingWidth = 0;
	    var lineCount = 1;

	    for (var n = 0; n < words.length; n++) {
	    	testingText = line+words[n]+" ";
	    	testing.textContent = testingText;  	
	    	testingWidth = testing.getBBox().width;
	        
	        if ((testingWidth+2*padding) > rw) {
	            testingTspan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
	            testingTspan.setAttributeNS(null,'x',rx+padding);
	            testingTspan.setAttributeNS(null,'dy',lineHeight);
	            
	            testingTextNode = document.createTextNode(line);
	            testingTspan.appendChild(testingTextNode);
	            wrapping.appendChild(testingTspan);

	            line = words[n]+" ";

	            lineCount++;
	        }
	        else {
	            line = testingText;
	        }
	    }

        testingTspan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
	    testingTspan.setAttributeNS(null,'x',rx+padding);
	    testingTspan.setAttributeNS(null,'dy',lineHeight);

	    testingTextNode = document.createTextNode(line);
	    testingTspan.appendChild(testingTextNode);

	    wrapping.appendChild(testingTspan);

		testing.parentNode.removeChild(testing);
		textNode.parentNode.replaceChild(wrapping,textNode);

		return {lineCount:lineCount,lineWidth:testingWidth,lineHeight:lineHeight};
	},
	
	/**
	 * Calculates on symbol fusion the position offset for
	 * connecting points at border
	 * @param position1
	 * @param position2
	 */
	_calcBorderConnectPointOffset: function(position1,position2) {
		var cr = this.options.nodeCenterOffset;
		
		var a0 = position2.x-position1.x;
		var b0 = position2.y-position1.y;
		
		var ox = cr*Math.cos(90-Math.atan(a0/b0));
		var oy = cr*Math.sin(90-Math.atan(a0/b0));
			
		return {ox:ox,oy:oy};
	}
	
}
