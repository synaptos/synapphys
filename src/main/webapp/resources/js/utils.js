/**
 * Der Klick soll nicht an den Kontext weitergeleitet werden.
 */
function onClickStopPropagation(e){
	e.stopPropagation();
}
