insert into `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `spam`, `deleted`) values (1, 'a', '$a', 'a', 'a@synaptos.at', '', current_timestamp, '', 0, 'a', 0, 0);

insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'first_name', 'Max');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'last_name', 'Developer');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_first_name', 'Max');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_last_name', 'Developer');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_company', 'Physiotherapie Developer');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_address_1', '');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_address_2', '');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_city', '');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_postcode', '');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_email', '');
insert into `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) values (1, 'billing_phone', '');
