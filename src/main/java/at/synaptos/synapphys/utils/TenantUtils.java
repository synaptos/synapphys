package at.synaptos.synapphys.utils;

public class TenantUtils {

	public static String normalizeIdentifier(final String username) {
		return username.replace(" ", "_");
	}

	public static String getUploadsDir(final String username) {
		return UploadUtils.getUploadsDir(normalizeIdentifier(username));
	}

}
