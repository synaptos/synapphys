package at.synaptos.synapphys.utils;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.transaction.UserTransaction;

import org.jboss.logging.Logger;

public class TransactionInterceptor {
	
	@Inject
	private Logger logger;
	
	@Resource
	private UserTransaction utx;
	
	@AroundInvoke
	public Object doTransaction(InvocationContext ctx) throws Exception {
		try {
			utx.begin();
	        Object ret = ctx.proceed();
	        utx.commit();
	        return ret;
		} catch (Exception e0) {
			try {
				utx.rollback();
			} catch (Exception e1) {
				logger.warn("Couldn't roll back transaction from TransactionInterceptor");
			}
			throw e0;
		} 
	}
}
