package at.synaptos.synapphys.utils;

public class GlobalNames {

	public static final String APPLICATION_NAMESPACE = "synapphys";

	public static final String APPLICATION_URL_PARAM_AUTHTOKEN = "authtoken";
	public static final String APPLICATION_URL_PARAM_USERLOGIN = "userlogin";

	public static final String EXTERNAL_RESOURCE_URL = "/custom";

	public static final String MESSAGE_RESOUCE_BUNDLE_BASENAME = "i18n";

	public static final String MESSAGE_KEY_HANDLE_FILE_UPLOAD_SUCCESS = "handle.file.upload.SUCCESS";
	public static final String MESSAGE_KEY_HANDLE_FILE_UPLOAD_ERROR = "handle.file.upload.ERROR";

	public static final String MESSAGE_KEY_SCHEDULE_ITEM_START_DATE_INVALID = "scheduleItem.startDate.INVALID";

	public static final String MESSAGE_KEY_USERMETA_BILLING_COMPANY = "usermeta.billing.company";

	public static final String MESSAGE_KEY_INSTANCE_PROPERTY_BILLING_HEADER = "instanceProperty.billing.header";
	public static final String MESSAGE_KEY_INSTANCE_PROPERTY_BILLING_FOOTER = "instanceProperty.billing.footer";

	public static final String MESSAGE_KEY_BANK_TRANSFER_NO_INVOICE_SELECTED_ERROR = "bankTransfer.noInvoiceSelected.ERROR";
	public static final String MESSAGE_KEY_BANK_TRANSFER_INVOICE_HAS_BEEN_PAID_ERROR = "bankTransfer.invoiceHasBeenPaid.ERROR";
	public static final String MESSAGE_KEY_BANK_TRANSFER_BOOKING_DATE_TO_OLD_ERROR = "bankTransfer.bookingDateToOld.ERROR";
	public static final String MESSAGE_KEY_BANK_TRANSFER_AMOUNT_MISMATCH_WARN = "bankTransfer.amountMismatch.WARN";

	public static final String MESSAGE_KEY_ACTION_SAVE_SUCCESS = "action.save.SUCCESS";

	public static final String MESSAGE_LOG_INSTANCE_CONFIG_PROPERTY_SELECT_ERROR = "Instance property %s not found";

	public static final String SYSTEM_PROPERTY_KEY_UPLOADS_BASEDIR = "synapphys.uploads.basedir";
	public static final String SYSTEM_PROPERTY_KEY_LOGOUT_FROM_REMOTE_SESSION = "synapphys.logout.fromRemoteSession";

}
