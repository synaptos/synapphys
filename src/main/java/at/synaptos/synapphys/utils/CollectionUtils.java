package at.synaptos.synapphys.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class CollectionUtils {

	public static int size(Collection<?> collection) {
		if (collection == null) {
			return 0;
		}
		return collection.size();
	}

	public static boolean isEmpty(Collection<?> collection) {
		return size(collection) == 0;
	}

	public static int size(Map<?, ?> map) {
		if (map == null) {
			return 0;
		}
		return map.size();
	}

	public static boolean isEmpty(Map<?, ?> map) {
		return size(map) == 0;
	}

	/**
	 * lists conversion, e.g. List<String> stringList =
	 * Arrays.asList("1","2","3"); List<Integer> integerList =
	 * toList(stringList, s -> Integer.parseInt(s));
	 * 
	 * @param from
	 * @param func
	 * @return
	 */
	public static <T, U> List<U> toList(List<T> from, Function<T, U> func) {
		return from.stream().map(func).collect(Collectors.toList());
	}

	/**
	 * Returns a map where each entry is an item of {@code list} mapped by the
	 * key produced by applying {@code mapper} to the item, e.g. Map<Long,
	 * Client> clientById = toMap(clients, Client::getId);
	 *
	 * @param list
	 *            the list to map
	 * @param mapper
	 *            the function to produce the key from a list item
	 * @return the resulting map
	 * @throws IllegalStateException
	 *             on duplicate key
	 */
	public static <K, T> Map<K, T> toMap(List<T> list, Function<? super T, ? extends K> mapper) {
		return list.stream().collect(Collectors.toMap(mapper, Function.identity()));
	}

	/**
	 * arrays conversion, e.g. String[] stringArr = {"1","2","3"}; Double[]
	 * doubleArr = toArray(stringArr, Double::parseDouble, Double[]::new);
	 * 
	 * @param from
	 * @param func
	 * @param generator
	 * @return
	 */
	public static <T, U> U[] toArray(T[] from, Function<T, U> func, IntFunction<U[]> generator) {
		return Arrays.stream(from).map(func).toArray(generator);
	}

	public static <T> T getEqual(List<T> list, T val) {
		if (isEmpty(list)) {
			return null;
		}
		if (val == null) {
			return null;
		}
		int index = list.indexOf(val);
		if (index > -1) {
			return list.get(index);
		}
		return null;
	}

}
