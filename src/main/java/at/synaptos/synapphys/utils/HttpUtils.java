package at.synaptos.synapphys.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.jboss.logging.Logger;

public class HttpUtils {

	private static final Logger logger = Logger.getLogger(HttpUtils.class);

	public static String postJson(String endpoint, String reqdata, String cashboxid, String accesstoken) {
		logger.trace("endpoint=" + endpoint + ", reqdata=" + reqdata);
		HttpsURLConnection urlConnection = null;
		try {
			URL url = new URL(endpoint);
			urlConnection = (HttpsURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setUseCaches(false);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestProperty("cashboxid", cashboxid);
			urlConnection.setRequestProperty("accesstoken", accesstoken);
			urlConnection.connect();
			OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
			out.write(reqdata);
			out.flush();
			int responseCode = urlConnection.getResponseCode();
			logger.trace("responseCode=" + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) {
				StringBuilder sb = new StringBuilder();
				BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
				br.close();
				logger.trace("sb.length()=" + (sb != null ? sb.length() : 0));
				return sb.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null)
				urlConnection.disconnect();
		}
		return null;
	}

}
