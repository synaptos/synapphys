package at.synaptos.synapphys.utils;

public class PageUrl {

	public static final String CLIENT_SELECT = "/pages/patient/select";

	public static final String CLIENT_LIST = "/pages/masterdata/clientList";

	public static final String CLIENT_EDIT = "/pages/masterdata/clientEdit";

	public static final String PHYSICIAN_LIST = "/pages/masterdata/physicianList";

	public static final String PHYSICIAN_EDIT = "/pages/masterdata/physicianEdit";

	public static final String HEALTH_INSURANCE_LIST = "/pages/masterdata/healthInsuranceList";

	public static final String HEALTH_INSURANCE_EDIT = "/pages/masterdata/healthInsuranceEdit";

	public static final String MAIN_INSURED_CLIENT_EDIT = "/pages/masterdata/mainInsuredClientEdit";

	public static final String SERVICE_TYPE_LIST = "/pages/masterdata/serviceTypeList";

	public static final String SERVICE_TYPE_EDIT = "/pages/masterdata/serviceTypeEdit";

	public static final String SETTINGS_COMMON_EDIT = "/pages/masterdata/settingsCommonEdit";

	public static final String SETTINGS_MAIN = "/pages/masterdata/settingsMain";

	public static final String SETTINGS_SCHEDULE_EDIT = "/pages/masterdata/settingsScheduleEdit";

	public static final String SETTINGS_RECEIPT_EDIT = "/pages/masterdata/settingsReceiptEdit";

	public static final String SERIES_DASHBOARD = "/pages/series/dashboard";

	public static final String SERIES_FINDINGS_SHEET = "/pages/series/findingsSheet";

	public static final String SERIES_FINDINGS_LSP = "/pages/series/findingsLsp";

	public static final String SERIES_DOCUMENTATION_LIST = "/pages/documentation/seriesDocumentationList";

	public static final String SERIES_DOCUMENTATION_EDIT = "/pages/documentation/seriesDocumentationEdit";

	public static final String SERVICE_TEMPLATES_EDIT = "/pages/documentation/serviceTemplatesEdit";

	public static final String SERVICE_ATTACHMENTS_EDIT = "/pages/documentation/serviceAttachmentsEdit";

	public static final String SESSION_FINDINGS_MAP = "/pages/session/findingsMap";

	public static final String SESSION_DOCUMENTATION_EDIT = "/pages/documentation/sessionDocumentationEdit";

	public static final String BODYMAP_EDIT = "/pages/documentation/bodymapEdit";

	public static final String DASHBOARD_VIEW = "/pages/documentation/dashboardView";

	public static final String SCHEDULE_SHEET = "/pages/schedule/scheduleSheet";

	public static final String INVOICE_LIST = "/pages/billing/invoiceList";

	public static final String INVOICE_EDIT = "/pages/billing/invoiceView";

	public static final String RECEIPT_LIST = "/pages/billing/receiptList";

	public static final String RECEIPT_EDIT = "/pages/billing/receiptView";

	public static final String PAYMENT_VIEW = "/pages/billing/paymentView";

}
