package at.synaptos.synapphys.utils;

public class PageUrlHelper {

	public static String redirect(String pageUrl) {
		if (StringUtils.isEmpty(pageUrl)) {
			return null;
		}
		return pageUrl + "?faces-redirect=true";
	}

}
