package at.synaptos.synapphys.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.jboss.logging.Logger;

public class XmlRpcUtils extends XmlRpcClient {

	private static final Logger logger = Logger.getLogger(XmlRpcUtils.class);

	private static final String xmlrpcUrl = System
			.getProperty(GlobalNames.APPLICATION_NAMESPACE + "." + "xmlrpc.protocol", "http") + "://"
			+ System.getProperty(GlobalNames.APPLICATION_NAMESPACE + "." + "xmlrpc.host", "localhost") + "/"
			+ System.getProperty(GlobalNames.APPLICATION_NAMESPACE + "." + "xmlrpc.path", "xmlrpc.php");

	public static XmlRpcClient createClient() {
		logger.debug("xmlrpcUrl=" + xmlrpcUrl);

		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		try {
			config.setServerURL(new URL(xmlrpcUrl));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		XmlRpcClient client = new XmlRpcClient();
		client.setConfig(config);

		return client;
	}

	public static String[] getSessionData(String username, String password) {
		logger.debug("username=" + username);

		String method = "getRoleByCredentials";
		Object[] params = { username, password, GlobalNames.APPLICATION_NAMESPACE + "_" + System.currentTimeMillis() };

		String[] data = { null, null, null, null, null };

		try {
			Object[] result = (Object[]) createClient().execute(GlobalNames.APPLICATION_NAMESPACE + "." + method,
					params);
			logger.trace("result=" + Arrays.toString(result));

			if (result != null) {
				for (int i = 0; i < data.length && i < result.length; i++) {
					if (result[i] != null && !StringUtils.isEmpty(result[i].toString())) {
						data[i] = result[i].toString();
					}
				}
			}
		} catch (XmlRpcException e) {
			e.printStackTrace();
		}

		logger.trace("data=" + Arrays.toString(data));
		return data;
	}

	public static String[] getSessionData(String authtoken) {
		logger.debug("authtoken=" + authtoken);

		String method = "getRoleByAuthtoken";
		Object[] params = { authtoken, GlobalNames.APPLICATION_NAMESPACE + "_" + System.currentTimeMillis() };

		String[] data = { null, null, null, null, null };

		try {
			Object[] result = (Object[]) createClient().execute(GlobalNames.APPLICATION_NAMESPACE + "." + method,
					params);
			logger.trace("result=" + Arrays.toString(result));

			if (result != null) {
				if (result != null) {
					for (int i = 0; i < data.length && i < result.length; i++) {
						if (result[i] != null && !StringUtils.isEmpty(result[i].toString())) {
							data[i] = result[i].toString();
						}
					}
				}
			}
		} catch (XmlRpcException e) {
			e.printStackTrace();
		}

		logger.trace("data=" + Arrays.toString(data));
		return data;
	}

}