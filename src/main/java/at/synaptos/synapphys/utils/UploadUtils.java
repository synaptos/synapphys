package at.synaptos.synapphys.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.stream.FileImageOutputStream;

import org.jboss.logging.Logger;

public class UploadUtils {

	private static final Logger logger = Logger.getLogger(UploadUtils.class);

	public static String getUploadsDir(final String tenantIdentifier) {
		// build uploads directory
		String uploadsDir = System.getProperty(GlobalNames.SYSTEM_PROPERTY_KEY_UPLOADS_BASEDIR);
		if (StringUtils.isEmpty(uploadsDir)) {
			uploadsDir = System.getProperty("java.io.tmpdir") + "/" + GlobalNames.APPLICATION_NAMESPACE;
		}
		uploadsDir += "/" + tenantIdentifier;
		logger.debug("uploadsDir=" + uploadsDir);
		return uploadsDir;
	}

	public static void dumpFile(final InputStream in, final String uploadsDir, final String filename)
			throws IOException {
		File uploadDir = new File(uploadsDir);
		if (!uploadDir.exists()) {
			if (!uploadDir.mkdir()) {
				throw new IOException("Couldn't create uploads directory [" + uploadsDir + "]");
			}
		}

		OutputStream out = new FileOutputStream(new File(uploadsDir, filename));

		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = in.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}

		in.close();
		out.flush();
		out.close();
	}

	public static void dumpFile(final byte[] data, final String uploadsDir, final String filename) throws IOException {
		File uploadDir = new File(uploadsDir);
		if (!uploadDir.exists()) {
			if (!uploadDir.mkdir()) {
				throw new IOException("Couldn't create uploads directory [" + uploadsDir + "]");
			}
		}

		FileImageOutputStream imageOutput = new FileImageOutputStream(new File(uploadsDir, filename));
		imageOutput.write(data, 0, data.length);
		imageOutput.close();
	}

}
