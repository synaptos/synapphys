package at.synaptos.synapphys.utils;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;

public class Resources {
	
	@Produces
	public Logger produceLog(InjectionPoint injectionPoint) {
		return Logger.getLogger(injectionPoint.getMember().getDeclaringClass());
	}
	
	@RequestScoped
	@Produces
	public FacesContext produceFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	@RequestScoped
	@Produces
	public RequestContext produceRequestContext() {
		return RequestContext.getCurrentInstance();
	}
	
	@RequestScoped
	@Produces
	public HttpServletRequest produceRequest() {
		return (HttpServletRequest)FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}
	
	@RequestScoped
	@Produces
	public HttpServletResponse produceResponse() {
		return (HttpServletResponse)FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
	}
	
}
