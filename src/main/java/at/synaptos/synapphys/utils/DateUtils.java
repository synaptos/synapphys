package at.synaptos.synapphys.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

	public static final Date MIN;
	public static final Date MAX;
	static {
		MIN = asDate(LocalDateTime.of(1970, 1, 1, 0, 0));
		MAX = asDate(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 999));
	}

	public static String getDateAsString(Date date) {
		return getDateAsString(date, DateFormat.SHORT);
	}

	public static String getDateAsString(Date date, int style) {
		return DateFormat.getDateInstance(style, Locale.GERMAN).format(date);
	}

	public static String getDateAsDDMM(Date date) {
		return getDateAsString(date, "dd.MM.");
	}

	public static String getDateAsHHMM(Date date) {
		return getDateAsString(date, "HH:mm");
	}

	public static String getDateAsString(Date date, String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.format(date);
	}

	public static String getDateTimeAsShort(Date date) {
		return getDateTimeAsString(date, DateFormat.SHORT, DateFormat.SHORT);
	}

	public static String getDateTimeAsMedium(Date date) {
		return getDateTimeAsString(date, DateFormat.MEDIUM, DateFormat.MEDIUM);
	}

	public static String getDateTimeAsString(Date date, int dateStyle, int timeStyle) {
		return DateFormat.getDateTimeInstance(dateStyle, timeStyle, Locale.GERMAN).format(date);
	}

	public static Date resetTimeForDate(Date currDate) {
		if (currDate == null) {
			return null;
		}
		Calendar currCal = Calendar.getInstance();
		currCal.setTime(currDate);
		currCal.set(Calendar.HOUR_OF_DAY, 0);
		currCal.set(Calendar.MINUTE, 0);
		currCal.set(Calendar.SECOND, 0);
		currCal.set(Calendar.MILLISECOND, 0);
		return currCal.getTime();
	}

	public static LocalDateTime asLdt(Date currDate) {
		if (currDate == null) {
			return null;
		}
		Instant instant = Instant.ofEpochMilli(currDate.getTime());
		LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		return ldt.truncatedTo(ChronoUnit.MINUTES);
	}

	public static LocalDate asLd(Date currDate) {
		return asLdt(currDate).toLocalDate();
	}

	public static Date asDate(LocalDateTime currLdt) {
		if (currLdt == null) {
			return null;
		}
		ZonedDateTime currZdt = currLdt.atZone(ZoneId.systemDefault());
		Date currDate = Date.from(currZdt.toInstant());
		return currDate;
	}

	public static Date asDate(LocalDate currLd) {
		if (currLd == null) {
			return null;
		}
		LocalDateTime currLdt = LocalDateTime.of(currLd, LocalTime.of(0, 0));
		ZonedDateTime currZdt = currLdt.atZone(ZoneId.systemDefault());
		Date currDate = Date.from(currZdt.toInstant());
		return currDate;
	}

	public static Date asDate(LocalTime currLt) {
		if (currLt == null) {
			return null;
		}
		LocalDateTime currLdt = LocalDateTime.of(LocalDate.now(), currLt);
		ZonedDateTime currZdt = currLdt.atZone(ZoneId.systemDefault());
		Date currDate = Date.from(currZdt.toInstant());
		return currDate;
	}

	public static LocalDateTime nextOrSameQuarter(LocalDateTime currLdt) {
		if (currLdt == null) {
			return null;
		}
		if (currLdt.getMinute() == 0) {
			return currLdt.truncatedTo(ChronoUnit.MINUTES);
		}
		int quarter = currLdt.getMinute() / 15 + 1;
		return currLdt.truncatedTo(ChronoUnit.HOURS).plusMinutes(quarter * 15);
	}

	public static int weekOfYear(LocalDateTime currLdt) {
		return currLdt.get(WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear());
	}

	public static LocalDateTime adjustWeekStartLdt(LocalDateTime ldt) {
		if (ldt.getDayOfWeek().compareTo(DayOfWeek.FRIDAY) > 0) {
			return LocalDateTime.of(ldt.with(TemporalAdjusters.next(DayOfWeek.MONDAY)).toLocalDate(),
					LocalTime.of(0, 0));
		}
		return nextOrSameQuarter(ldt);
	}

	public static LocalDateTime adjustWeekEndLdt(LocalDateTime ldt) {
		return LocalDateTime.of(ldt.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY)).toLocalDate(),
				LocalTime.of(0, 0));
	}

}
