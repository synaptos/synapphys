package at.synaptos.synapphys.utils;

public class StringUtils {
	
    public static boolean isEmpty(String source) {
        return source == null || source.length() == 0;
    }
	
}
