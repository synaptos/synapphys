package at.synaptos.synapphys.utils;

public class TagAttribute {

	public static final String MESSAGE_FOR_GLOBAL_PANEL = "globalPanel";

	public static final String MESSAGE_FOR_EVENT_DIALOG = "eventDialog";

	public static final String MESSAGE_FOR_EDIT_PANEL = "editView";

	public static final String MESSAGE_DISPLAY = "text";

	public static final String DATATABLE_PAGINATOR_POSITION = "top";

	public static final String DATATABLE_ROWS_TEMPLATE = "5,10,15,25,50,100";

	public static final String DATATABLE_ROWS = "5";

	public static final String ICON_ADD = "fa fa-plus";

	public static final String ICON_EDIT = "fa fa-edit";

	public static final String ICON_DELETE = "fa fa-trash red";

	public static final String ICON_SAVE = "fa fa-check";

	public static final String ICON_CANCEL = "fa fa-times";

	public static final String ICON_CHECK = "fa fa-check";

	public static final String ICON_SEARCH = "fa fa-search";

	public static final String ICON_BACK = "fa fa-reply";

	public static final String ICON_NEXT = "fa fa-share";

	public static final String ICON_ALERT = "fa fa-exclamation-triangle";

	public static final String ICON_HOME = "fa fa-home";

	public static final String ICON_MENU = "fa fa-bars";

	public static final String ICON_LIST = "fa fa-list-alt";

	public static final String ICON_PROGRESS = "fa fa-spinner fa-spin";

	public static final String ICON_LOGIN = "fa fa-sign-in";

	public static final String ICON_LOGOUT = "fa fa-sign-out";

	public static final String ICON_GENDER = "fa fa-venus-mars";

	public static final String ICON_PHONE = "fa fa-phone";

	public static final String ICON_FAX = "fa fa-fax";

	public static final String ICON_EMAIL = "fa fa-envelope";

	public static final String ICON_REPEAT = "fa fa-repeat";

	public static final String ICON_REFRESH = "fa fa-refresh";

	public static final String ICON_SERVICE = "fa fa-heart";

	public static final String ICON_UPLOAD = "fa fa-upload";

	public static final String ICON_DOWNLOAD = "fa fa-download";

	public static final String ICON_FILTER = "fa fa-filter";

	public static final String ICON_SERIES = "fa fa-clipboard";

	public static final String ICON_BILLING = "fa fa-eur";

	public static final String ICON_INVOICING = "fa fa-eur";

	public static final String ICON_RECEIPTING = "fa fa-qrcode";

	public static final String ICON_MASTERDATA = "fa fa-puzzle-piece";

	public static final String ICON_SETTINGS = "fa fa-gear";

	public static final String ICON_PROFILE = "fa fa-tachometer";

	public static final String ICON_PHYSICIAN = "fa fa-user-md";

	public static final String ICON_HEALTH_INSURANCE = "fa fa-umbrella";

	public static final String ICON_BIRTH_DATE = "fa fa-birthday-cake";

	public static final String ICON_PRINT = "fa fa-print";

	public static final String ICON_BOOKMARK_ON = "fa fa-star";

	public static final String ICON_BOOKMARK_OFF = "fa fa-star-o";

	public static final String ICON_BOLT = "fa fa-bolt red";

	public static final String ICON_USER = "fa fa-user";

	public static final String ICON_FLAG = "fa fa-flag";

	public static final String CLIENT_SEARCH_KEY_BOOKMARKED = "_0";

	public static final String CLIENT_SEARCH_KEY_CURRENT = "_1";

}
