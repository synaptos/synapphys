package at.synaptos.synapphys.tenancy;

import javax.faces.context.FacesContext;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;

import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.TenantUtils;

public class CustomTenantIdentifierResolverResolver implements CurrentTenantIdentifierResolver {

	public static String getCurrentTenantIdentifier() {
		return TenantUtils.normalizeIdentifier(
				FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName());
	}

	@Override
	public String resolveCurrentTenantIdentifier() {
		if (FacesContext.getCurrentInstance() != null) {
			return "`" + GlobalNames.APPLICATION_NAMESPACE + "_" + getCurrentTenantIdentifier() + "`";
		} else {
			return "`mysql`";
		}
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return false;
	}

}
