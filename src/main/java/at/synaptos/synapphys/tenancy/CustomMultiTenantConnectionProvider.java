package at.synaptos.synapphys.tenancy;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.hibernate.engine.jndi.spi.JndiService;
import org.hibernate.service.spi.ServiceRegistryAwareService;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.service.spi.Stoppable;
import org.jboss.logging.Logger;

import at.synaptos.synapphys.utils.StringUtils;

public class CustomMultiTenantConnectionProvider
		implements ServiceRegistryAwareService, MultiTenantConnectionProvider, Stoppable {

	private static final long serialVersionUID = 4368575201221677384L;

	private static final Logger logger = Logger.getLogger(CustomMultiTenantConnectionProvider.class);

	private static final String SYSTEM_PROPERTY_KEY_CONNECTION_DATASOURCE = "synapphys.connection.datasource";

	private DataSource managedDatasource;

	// https://github.com/hibernate/hibernate-orm/blob/master/hibernate-core/src/main/java/org/hibernate/engine/jdbc/connections/spi/DataSourceBasedMultiTenantConnectionProviderImpl.java

	@Override
	public void injectServices(ServiceRegistryImplementor serviceRegistry) {
		final String jndiName = System.getProperty(SYSTEM_PROPERTY_KEY_CONNECTION_DATASOURCE);
		logger.debug("jndiName=" + jndiName);
		if (StringUtils.isEmpty(jndiName)) {
			throw new HibernateException("Improper set up of CustomMultiTenantConnectionProvider");
		}

		final JndiService jndiService = serviceRegistry.getService(JndiService.class);
		if (jndiService == null) {
			throw new HibernateException("Couldn't locate JndiService from CustomMultiTenantConnectionProvider");
		}

		final Object namedObject = jndiService.locate(jndiName);
		if (namedObject == null) {
			throw new HibernateException("Couldn't resolved JNDI name [" + jndiName + "]");
		}

		if (DataSource.class.isInstance(namedObject)) {
			managedDatasource = (DataSource) namedObject;
		} else {
			throw new HibernateException("Unknown object type [" + namedObject.getClass().getName()
					+ "] found in JNDI location [" + jndiName + "]");
		}
	}

	@Override
	public boolean supportsAggressiveRelease() {
		// Must be true in JTA environments to support
		// ConnectionReleaseMode.AFTER_STATEMENT
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean isUnwrappableAs(Class clazz) {
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> clazz) {
		return null;
	}

	@Override
	public Connection getAnyConnection() throws SQLException {
		return managedDatasource.getConnection();
	}

	@Override
	public Connection getConnection(String tenantIdentifier) throws SQLException {
		final Connection connection = getAnyConnection();
		try {
			// Mind, Connection.setSchema() doesn't work here!
			Statement statement = connection.createStatement();
			statement.execute("USE " + tenantIdentifier);
			statement.close();
		} catch (SQLException e) {
			throw new HibernateException("Could not alter JDBC connection to specified identifier " + tenantIdentifier,
					e);
		}

		return connection;
	}

	@Override
	public void releaseAnyConnection(Connection connection) throws SQLException {
		try {
			// Mind, Connection.setSchema() doesn't work here!
			Statement statement = connection.createStatement();
			statement.execute("USE `mysql`");
			statement.close();
		} catch (SQLException e) {
			throw new HibernateException("Could not alter JDBC connection to specified identifier `mysql`", e);
		}
		connection.close();
	}

	@Override
	public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
		releaseAnyConnection(connection);
	}

	@Override
	public void stop() {
	}

}