package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.model.Client;

public interface BillingService extends CommonEntityService<Billing> {

	public List<Billing> getEntities(final BillingType billingType);

	public List<Billing> getEntities(final BillingType billingType, final Client client);

	public List<Billing> getEntities(final BillingType billingType, final Date startDate, final Date endDate);

	public List<Billing> getUnpaidEntities(final BillingType billingType);

	public List<Billing> updateEntities(final List<Billing> billings);

	public boolean existsEntity(final BillingType billingType, final Date paymentDate, final String paymentNo);

	public int getReferenceSequence(final BillingType billingType, int referenceYear);

}
