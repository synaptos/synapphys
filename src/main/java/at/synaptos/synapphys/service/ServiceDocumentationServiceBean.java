package at.synaptos.synapphys.service;

import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

import at.synaptos.synapphys.model.ServiceDocumentation;

@RolesAllowed("user")
@Stateless
public class ServiceDocumentationServiceBean extends AbstractService implements ServiceDocumentationService {

	@Override
	public ServiceDocumentation getEntity(final Long serviceDocumentationId) {
		ServiceDocumentation managedServiceDocumentation = entityManager.find(ServiceDocumentation.class,
				serviceDocumentationId);
		return managedServiceDocumentation;
	}

	@Override
	public void addEntity(final ServiceDocumentation serviceDocumentation) {
		serviceDocumentation.setCreatedDate(new Date());
		serviceDocumentation.setCreatedUser(getCurrentUsername());
		serviceDocumentation.setUpdatedDate(serviceDocumentation.getCreatedDate());
		serviceDocumentation.setUpdatedUser(serviceDocumentation.getCreatedUser());
		entityManager.persist(serviceDocumentation);
	}

	@Override
	public ServiceDocumentation updateEntity(final ServiceDocumentation serviceDocumentation) {
		serviceDocumentation.setUpdatedDate(new Date());
		serviceDocumentation.setUpdatedUser(getCurrentUsername());
		ServiceDocumentation managedServiceDocumentation = entityManager.merge(serviceDocumentation);
		return managedServiceDocumentation;
	}

	@Override
	public void deleteEntity(final ServiceDocumentation serviceDocumentation) {
		serviceDocumentation.setDeletedDate(new Date());
		serviceDocumentation.setDeletedUser(getCurrentUsername());
		entityManager.merge(serviceDocumentation);
	}

}
