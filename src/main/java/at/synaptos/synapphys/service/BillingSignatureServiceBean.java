package at.synaptos.synapphys.service;

import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

import at.synaptos.synapphys.model.BillingSignature;

@RolesAllowed({ "user", "receipt" })
@Stateless
public class BillingSignatureServiceBean extends AbstractService implements BillingSignatureService {

	@Override
	public void addEntity(BillingSignature billingSignature) {
		billingSignature.setCreatedDate(new Date());
		billingSignature.setCreatedUser(getCurrentUsername());
		entityManager.persist(billingSignature);
	}

}
