package at.synaptos.synapphys.service;

import java.util.List;

import at.synaptos.synapphys.model.UsermetaProperty;

public interface WPUsermetaPropertyService {

	public List<UsermetaProperty> getUsermetaProperties();
	
	public List<UsermetaProperty> updateUsermetaProperties(final List<UsermetaProperty> usermetaProperties);

}
