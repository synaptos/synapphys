package at.synaptos.synapphys.service;

import java.util.List;

import at.synaptos.synapphys.model.Physician;

public interface PhysicianService extends CommonEntityService<Physician> {

	public List<Physician> getEntities();
	
}
