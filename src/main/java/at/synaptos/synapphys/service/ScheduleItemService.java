package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import at.synaptos.synapphys.model.ScheduleItem;

public interface ScheduleItemService extends CommonEntityService<ScheduleItem> {

	public List<ScheduleItem> getEntities();

	public List<ScheduleItem> getEntities(Date startDate, Date endDate);

}
