package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.BodymapItem;

public interface BodymapItemService extends CommonEntityService<BodymapItem> {
		
}
