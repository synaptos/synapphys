package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.AbstractEntity;

public interface CommonEntityService<T extends AbstractEntity> {
	
	public void addEntity(final T entity);
	
	public T getEntity(final Long entityId);

	public T updateEntity(final T entity);

	public void deleteEntity(final T entity);
	
}
