package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import at.synaptos.synapphys.model.SessionDocumentation;

public interface SessionDocumentationService extends CommonEntityService<SessionDocumentation> {
		
	public List<SessionDocumentation> getEntitiesBetween(Date startDate, Date endDate);
	
}
