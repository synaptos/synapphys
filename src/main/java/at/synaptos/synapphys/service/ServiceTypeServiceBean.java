package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed({ "user", "receipt" })
@Stateless
public class ServiceTypeServiceBean extends AbstractService implements ServiceTypeService {

	@Inject
	private Logger logger;

	@Override
	public List<ServiceType> getEntities() {
		TypedQuery<ServiceType> query = entityManager.createNamedQuery(ServiceType.FIND_ALL, ServiceType.class);
		List<ServiceType> serviceTypes = query.getResultList();
		logger.debug("serviceTypes.size()=" + CollectionUtils.size(serviceTypes));
		return serviceTypes;
	}

	@Override
	public ServiceType getEntity(final Long serviceTypeId) {
		ServiceType managedServiceType = entityManager.find(ServiceType.class, serviceTypeId);
		return managedServiceType;
	}

	@Override
	public void addEntity(final ServiceType serviceType) {
		serviceType.setCreatedDate(new Date());
		serviceType.setCreatedUser(getCurrentUsername());
		serviceType.setUpdatedDate(serviceType.getCreatedDate());
		serviceType.setUpdatedUser(serviceType.getCreatedUser());
		entityManager.persist(serviceType);
	}

	@Override
	public ServiceType updateEntity(final ServiceType serviceType) {
		serviceType.setUpdatedDate(new Date());
		serviceType.setUpdatedUser(getCurrentUsername());
		ServiceType managedServiceType = entityManager.merge(serviceType);
		return managedServiceType;
	}

	@Override
	public void deleteEntity(final ServiceType serviceType) {
		serviceType.setDeletedDate(new Date());
		serviceType.setDeletedUser(getCurrentUsername());
		entityManager.merge(serviceType);
	}

}
