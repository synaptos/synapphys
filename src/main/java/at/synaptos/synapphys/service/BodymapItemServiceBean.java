package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.Query;

import at.synaptos.synapphys.model.BodymapItem;
import at.synaptos.synapphys.model.FindingType;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed("user")
@Stateless
public class BodymapItemServiceBean extends AbstractService implements BodymapItemService {

	@Override
	public BodymapItem getEntity(final Long bodymapItemId) {
		BodymapItem managedBodymapItem = entityManager.find(BodymapItem.class, bodymapItemId);
		return managedBodymapItem;
	}

	@Override
	public void addEntity(final BodymapItem bodymapItem) {
		bodymapItem.setCreatedDate(new Date());
		bodymapItem.setCreatedUser(getCurrentUsername());
		bodymapItem.setUpdatedDate(bodymapItem.getCreatedDate());
		bodymapItem.setUpdatedUser(bodymapItem.getCreatedUser());
		// TODO WQ
		if (bodymapItem.getFindingType() == FindingType.SKIN_LEAD_TEXT
				|| bodymapItem.getFindingTypeModifier() == FindingType.SKIN_LEAD_TEXT) {
			Integer sequenceNo = 0;
			Query query = entityManager.createNamedQuery(BodymapItem.NEXT_SEQUENCE_NO_NATIVE);
			query.setParameter(1, bodymapItem.getSeriesDocumentation().getId());
			@SuppressWarnings("unchecked")
			List<Integer> result = query.getResultList();
			if (!CollectionUtils.isEmpty(result) && result.get(0) != null) {
				sequenceNo = result.get(0);
			}
			bodymapItem.setSequenceNo(++sequenceNo);
		}
		entityManager.persist(bodymapItem);
	}

	@Override
	public BodymapItem updateEntity(final BodymapItem bodymapItem) {
		bodymapItem.setUpdatedDate(new Date());
		bodymapItem.setUpdatedUser(getCurrentUsername());
		// TODO WQ
		if (bodymapItem.getSequenceNo() < 1) {
			if (bodymapItem.getFindingType() == FindingType.SKIN_LEAD_TEXT
					|| bodymapItem.getFindingTypeModifier() == FindingType.SKIN_LEAD_TEXT) {
				Integer sequenceNo = 0;
				Query query = entityManager.createNamedQuery(BodymapItem.NEXT_SEQUENCE_NO_NATIVE);
				query.setParameter(1, bodymapItem.getSeriesDocumentation().getId());
				@SuppressWarnings("unchecked")
				List<Integer> result = query.getResultList();
				if (!CollectionUtils.isEmpty(result) && result.get(0) != null) {
					sequenceNo = result.get(0);
				}
				bodymapItem.setSequenceNo(++sequenceNo);
			}
		}
		BodymapItem managedBodymapItem = entityManager.merge(bodymapItem);
		return managedBodymapItem;
	}

	@Override
	public void deleteEntity(final BodymapItem bodymapItem) {
		bodymapItem.setDeletedDate(new Date());
		bodymapItem.setDeletedUser(getCurrentUsername());
		entityManager.merge(bodymapItem);
	}

}
