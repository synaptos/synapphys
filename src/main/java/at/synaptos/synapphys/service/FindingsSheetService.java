package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.FindingsSheet;

public interface FindingsSheetService {

	public FindingsSheet getEntity(final Long entityId);

	public void addEntity(final FindingsSheet entity);

	public FindingsSheet updateEntity(final FindingsSheet entity);

}
