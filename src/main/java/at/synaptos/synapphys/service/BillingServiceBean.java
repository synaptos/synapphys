package at.synaptos.synapphys.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.PatientStatus;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed({ "user", "receipt" })
@Stateless
public class BillingServiceBean extends AbstractService implements BillingService {

	@Inject
	private Logger logger;

	@Inject
	private ClientService clientService;

	@Override
	public Billing getEntity(final Long billingId) {
		Billing managedBilling = entityManager.find(Billing.class, billingId);
		return managedBilling;
	}

	@Override
	public void addEntity(final Billing billing) {
		billing.setCreatedDate(new Date());
		billing.setCreatedUser(getCurrentUsername());
		billing.setUpdatedDate(billing.getCreatedDate());
		billing.setUpdatedUser(billing.getCreatedUser());
		if (billing.getBillingItems() != null) {
			billing.getBillingItems().forEach(pi -> {
				pi.setBilling(billing);
			});
		}
		entityManager.persist(billing);
		if (billing.isInvoice()) {
			billing.getSeriesDocumentation().getClient().setPatientStatus(null);
			billing.getSeriesDocumentation().getClient().setPatientStatusDate(null);
			clientService.updateEntity(billing.getSeriesDocumentation().getClient());
		}
	}

	@Override
	public Billing updateEntity(final Billing billing) {
		billing.setUpdatedDate(new Date());
		billing.setUpdatedUser(getCurrentUsername());
		Billing managedBilling = entityManager.merge(billing);
		return managedBilling;
	}

	@Override
	public void deleteEntity(final Billing billing) {
		billing.setDeletedDate(new Date());
		billing.setDeletedUser(getCurrentUsername());
		entityManager.merge(billing);
		if (billing.isInvoice()) {
			billing.getSeriesDocumentation().getClient().setPatientStatus(PatientStatus.CURRENT);
			billing.getSeriesDocumentation().getClient()
					.setPatientStatusDate(billing.getSeriesDocumentation().getAddedDate());
			clientService.updateEntity(billing.getSeriesDocumentation().getClient());
		}
	}

	@Override
	public List<Billing> getEntities(final BillingType billingType) {
		TypedQuery<Billing> query = entityManager.createNamedQuery(Billing.FIND_ALL, Billing.class);
		query.setParameter("billingType", billingType);
		List<Billing> billings = query.getResultList();
		logger.debug("billings.size()=" + CollectionUtils.size(billings));
		return billings;
	}

	@Override
	public List<Billing> getEntities(final BillingType billingType, final Client client) {
		logger.debug("billingType=" + billingType + ", client=" + client);
		Query query = entityManager.createNamedQuery(Billing.FIND_BY_CLIENT_NATIVE);
		query.setParameter(1, billingType.name());
		query.setParameter(2, client.getId());
		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();
		List<Billing> billings = result.stream()
				.map(objects -> ((Billing) objects[0]).withTotal(BigDecimal.valueOf((double) objects[1])))
				.collect(Collectors.toList());
		logger.debug("billingList.size()=" + CollectionUtils.size(billings));
		return billings;
	}

	@Override
	public List<Billing> getEntities(final BillingType billingType, final Date startDate, final Date endDate) {
		logger.debug("billingType=" + billingType + ", startDate=" + startDate + ", endDate=" + endDate);
		Query query = entityManager.createNamedQuery(Billing.FIND_BETWEEN_NATIVE);
		query.setParameter(1, billingType.name());
		query.setParameter(2, startDate, TemporalType.TIMESTAMP);
		query.setParameter(3, endDate, TemporalType.TIMESTAMP);
		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();
		List<Billing> billings = result.stream()
				.map(objects -> ((Billing) objects[0]).withTotal(BigDecimal.valueOf((double) objects[1])))
				.collect(Collectors.toList());
		logger.debug("billingList.size()=" + CollectionUtils.size(billings));
		return billings;
	}

	@Override
	public List<Billing> getUnpaidEntities(final BillingType billingType) {
		logger.debug("billingType=" + billingType);
		Query query = entityManager.createNamedQuery(Billing.FIND_UNPAID_NATIVE);
		query.setParameter(1, billingType.name());
		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();
		List<Billing> billings = result.stream()
				.map(objects -> ((Billing) objects[0]).withTotal(BigDecimal.valueOf((double) objects[1])))
				.collect(Collectors.toList());
		logger.debug("billingList.size()=" + CollectionUtils.size(billings));
		return billings;
	}

	@Override
	public List<Billing> updateEntities(final List<Billing> billings) {
		List<Billing> managedBillings = billings.stream().filter(billing -> billing.isChanged()).map(billing -> {
			billing.setUpdatedDate(new Date());
			billing.setUpdatedUser(getCurrentUsername());
			return entityManager.merge(billing);
		}).collect(Collectors.toList());
		return managedBillings;
	}

	@Override
	public boolean existsEntity(final BillingType billingType, final Date paymentDate, final String paymentNo) {
		logger.debug("billingType=" + billingType + ", paymentDate=" + paymentDate + ", paymentNo=" + paymentNo);
		Query query = entityManager.createNamedQuery(Billing.FIND_BY_TRANSFER_NATIVE);
		query.setParameter(1, billingType.name());
		query.setParameter(2, paymentDate, TemporalType.TIMESTAMP);
		query.setParameter(3, paymentNo);
		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();
		return !result.isEmpty();
	}

	@Override
	public int getReferenceSequence(BillingType billingType, int referenceYear) {
		logger.debug("billingType=" + billingType + ", referenceYear=" + referenceYear);
		Query query = entityManager.createNamedQuery(Billing.FIND_BY_REFERENCE_NATIVE);
		query.setParameter(1, billingType.name());
		query.setParameter(2, referenceYear);
		@SuppressWarnings("unchecked")
		List<Object> result = query.getResultList();
		return !result.isEmpty() ? ((BigInteger) result.get(0)).intValue() : 0;
	}

}
