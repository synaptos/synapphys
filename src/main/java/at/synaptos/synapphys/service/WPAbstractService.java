package at.synaptos.synapphys.service;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class WPAbstractService {
	
	@PersistenceContext(unitName = "WPApplicationPU")
	protected EntityManager entityManager;
	
	@Resource
	protected SessionContext sessionContext;
		
	protected String getCurrentUser() {
		return sessionContext.getCallerPrincipal().getName();
	}	
	
}
