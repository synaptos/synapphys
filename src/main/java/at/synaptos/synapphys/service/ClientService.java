package at.synaptos.synapphys.service;

import java.util.List;

import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.model.PatientStatus;

public interface ClientService extends CommonEntityService<Client> {

	public List<Client> getEntities();

	public List<Client> getMainInsuredClients();

	public boolean existsEntity(final String insuranceNo);

	public boolean existsEntity(final String insuranceNo, final HealthInsurance healthInsurance);

	public List<Object[]> getSearchItems();

	public List<Client> getEntities(final String searchKey);

	public List<Client> getEntities(final PatientStatus patientStatus);

}
