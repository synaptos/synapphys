package at.synaptos.synapphys.service;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractService {
	
	@PersistenceContext(unitName = "SynapphysApplicationPU")
	protected EntityManager entityManager;
	
	@Resource
	protected SessionContext sessionContext;
		
	protected String getCurrentUsername() {
		return sessionContext.getCallerPrincipal().getName();
	}	
	
}
