package at.synaptos.synapphys.service;

import java.util.List;

import at.synaptos.synapphys.model.SeriesDocumentation;

public interface SeriesDocumentationService extends CommonEntityService<SeriesDocumentation> {

	public List<SeriesDocumentation> getEntities();

}
