package at.synaptos.synapphys.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.UsermetaProperty;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed({ "user", "receipt" })
@Stateless
public class WPUsermetaPropertyServiceBean extends WPAbstractService implements WPUsermetaPropertyService {

	@Inject
	private Logger logger;

	@Override
	public List<UsermetaProperty> getUsermetaProperties() {
		String username = getCurrentUser();
		logger.debug("username=" + username);
		TypedQuery<UsermetaProperty> query = entityManager.createNamedQuery(UsermetaProperty.FIND_BY_USERNAME,
				UsermetaProperty.class);
		query.setParameter(1, username);
		List<UsermetaProperty> usermetaProperties = query.getResultList();
		logger.debug("usermetaProperties.size()=" + CollectionUtils.size(usermetaProperties));
		return usermetaProperties;
	}

	@Override
	public List<UsermetaProperty> updateUsermetaProperties(List<UsermetaProperty> usermetaProperties) {
		if (CollectionUtils.isEmpty(usermetaProperties) || usermetaProperties.get(0).getUserId() == null) {
			return null;
		}
		List<UsermetaProperty> managedUsermetaProperties = new ArrayList<UsermetaProperty>();
		usermetaProperties.forEach(usermetaProperty -> {
			if (usermetaProperty.isChanged()) {
				if (usermetaProperty.getUserId() == null) {
					usermetaProperty.setUserId(usermetaProperties.get(0).getUserId());
				}
				UsermetaProperty managedUsermetaProperty = entityManager.merge(usermetaProperty);
				managedUsermetaProperties.add(managedUsermetaProperty);
				usermetaProperty.setChanged(false);
			}
		});
		return managedUsermetaProperties;
	}

}
