package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.ServiceDocumentation;

public interface ServiceDocumentationService extends CommonEntityService<ServiceDocumentation> {

}
