package at.synaptos.synapphys.service;

import java.util.List;

import at.synaptos.synapphys.model.ServiceType;

public interface ServiceTypeService extends CommonEntityService<ServiceType> {
		
	public List<ServiceType> getEntities();
	
}
