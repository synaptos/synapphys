package at.synaptos.synapphys.service;

import java.util.List;

import at.synaptos.synapphys.model.InstanceProperty;

public interface InstancePropertyService {

	public List<InstanceProperty> getInstanceProperties();
	
	public List<InstanceProperty> updateInstanceProperties(final List<InstanceProperty> instanceProperties);

}
