package at.synaptos.synapphys.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.InstanceProperty;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed({ "user", "receipt" })
@Stateless
public class InstancePropertyServiceBean extends AbstractService implements InstancePropertyService {

	@Inject
	private Logger logger;

	@Override
	public List<InstanceProperty> getInstanceProperties() {
		TypedQuery<InstanceProperty> query = entityManager.createNamedQuery(InstanceProperty.FIND_ALL,
				InstanceProperty.class);
		List<InstanceProperty> instanceProperties = query.getResultList();
		logger.debug("instanceProperties.size()=" + CollectionUtils.size(instanceProperties));
		return instanceProperties;
	}

	@Override
	public List<InstanceProperty> updateInstanceProperties(List<InstanceProperty> instanceProperties) {
		List<InstanceProperty> managedInstanceProperties = new ArrayList<InstanceProperty>();
		instanceProperties.forEach(instanceProperty -> {
			if (instanceProperty.isChanged()) {
				instanceProperty.setUpdatedDate(System.currentTimeMillis());
				instanceProperty.setUpdatedUser(getCurrentUsername());
				InstanceProperty managedInstanceProperty = entityManager.merge(instanceProperty);
				managedInstanceProperties.add(managedInstanceProperty);
				instanceProperty.setChanged(false);
			}
		});
		return managedInstanceProperties;
	}

}
