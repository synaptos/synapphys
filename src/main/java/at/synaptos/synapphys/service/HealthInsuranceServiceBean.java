package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed("user")
@Stateless
public class HealthInsuranceServiceBean extends AbstractService implements HealthInsuranceService {

	@Inject
	private Logger logger;

	@Override
	public List<HealthInsurance> getEntities() {
		TypedQuery<HealthInsurance> query = entityManager.createNamedQuery(HealthInsurance.FIND_ALL,
				HealthInsurance.class);
		List<HealthInsurance> healthInsurances = query.getResultList();
		logger.debug("healthInsurances.size()=" + CollectionUtils.size(healthInsurances));
		return healthInsurances;
	}

	@Override
	public HealthInsurance getEntity(final Long healthInsuranceId) {
		HealthInsurance managedHealthInsurance = entityManager.find(HealthInsurance.class, healthInsuranceId);
		return managedHealthInsurance;
	}

	@Override
	public void addEntity(final HealthInsurance healthInsurance) {
		healthInsurance.setCreatedDate(new Date());
		healthInsurance.setCreatedUser(getCurrentUsername());
		healthInsurance.setUpdatedDate(healthInsurance.getCreatedDate());
		healthInsurance.setUpdatedUser(healthInsurance.getCreatedUser());
		entityManager.persist(healthInsurance);
	}

	@Override
	public HealthInsurance updateEntity(final HealthInsurance healthInsurance) {
		healthInsurance.setUpdatedDate(new Date());
		healthInsurance.setUpdatedUser(getCurrentUsername());
		HealthInsurance managedInsurance = entityManager.merge(healthInsurance);
		return managedInsurance;
	}

	@Override
	public void deleteEntity(final HealthInsurance healthInsurance) {
		healthInsurance.setDeletedDate(new Date());
		healthInsurance.setDeletedUser(getCurrentUsername());
		entityManager.merge(healthInsurance);
	}

}
