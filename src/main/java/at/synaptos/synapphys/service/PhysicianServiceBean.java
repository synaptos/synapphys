package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed("user")
@Stateless
public class PhysicianServiceBean extends AbstractService implements PhysicianService {

	@Inject
	private Logger logger;

	@Override
	public List<Physician> getEntities() {
		TypedQuery<Physician> query = entityManager.createNamedQuery(Physician.FIND_ALL, Physician.class);
		List<Physician> physicians = query.getResultList();
		logger.debug("physicians.size()=" + CollectionUtils.size(physicians));
		return physicians;
	}

	@Override
	public Physician getEntity(final Long physicianId) {
		Physician managedPhysician = entityManager.find(Physician.class, physicianId);
		return managedPhysician;
	}

	@Override
	public void addEntity(final Physician physician) {
		physician.setCreatedDate(new Date());
		physician.setCreatedUser(getCurrentUsername());
		physician.setUpdatedDate(physician.getCreatedDate());
		physician.setUpdatedUser(physician.getCreatedUser());
		entityManager.persist(physician);
	}

	@Override
	public Physician updateEntity(final Physician physician) {
		physician.setUpdatedDate(new Date());
		physician.setUpdatedUser(getCurrentUsername());
		Physician managedPhysician = entityManager.merge(physician);
		return managedPhysician;
	}

	@Override
	public void deleteEntity(final Physician physician) {
		physician.setDeletedDate(new Date());
		physician.setDeletedUser(getCurrentUsername());
		entityManager.merge(physician);
	}

}
