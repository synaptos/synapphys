package at.synaptos.synapphys.service;

import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

import at.synaptos.synapphys.model.ServiceTemplate;

@RolesAllowed("user")
@Stateless
public class ServiceTemplateServiceBean extends AbstractService implements ServiceTemplateService {

	@Override
	public ServiceTemplate getEntity(final Long serviceTemplateId) {
		ServiceTemplate managedServiceTemplate = entityManager.find(ServiceTemplate.class, serviceTemplateId);
		return managedServiceTemplate;
	}

	@Override
	public void addEntity(final ServiceTemplate serviceTemplate) {
		serviceTemplate.setCreatedDate(new Date());
		serviceTemplate.setCreatedUser(getCurrentUsername());
		serviceTemplate.setUpdatedDate(serviceTemplate.getCreatedDate());
		serviceTemplate.setUpdatedUser(serviceTemplate.getCreatedUser());
		entityManager.persist(serviceTemplate);
	}

	@Override
	public ServiceTemplate updateEntity(final ServiceTemplate serviceTemplate) {
		serviceTemplate.setUpdatedDate(new Date());
		serviceTemplate.setUpdatedUser(getCurrentUsername());
		ServiceTemplate managedServiceTemplate = entityManager.merge(serviceTemplate);
		return managedServiceTemplate;
	}

	@Override
	public void deleteEntity(final ServiceTemplate serviceTemplate) {
		serviceTemplate.setDeletedDate(new Date());
		serviceTemplate.setDeletedUser(getCurrentUsername());
		entityManager.merge(serviceTemplate);
	}

}
