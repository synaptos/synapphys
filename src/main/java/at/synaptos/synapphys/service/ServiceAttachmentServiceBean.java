package at.synaptos.synapphys.service;

import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

import at.synaptos.synapphys.model.ServiceAttachment;

@RolesAllowed("user")
@Stateless
public class ServiceAttachmentServiceBean extends AbstractService implements ServiceAttachmentService {

	@Override
	public ServiceAttachment getEntity(final Long serviceAttachmentId) {
		ServiceAttachment managedServiceAttachment = entityManager.find(ServiceAttachment.class, serviceAttachmentId);
		return managedServiceAttachment;
	}

	@Override
	public void addEntity(final ServiceAttachment serviceAttachment) {
		serviceAttachment.setCreatedDate(new Date());
		serviceAttachment.setCreatedUser(getCurrentUsername());
		serviceAttachment.setUpdatedDate(serviceAttachment.getCreatedDate());
		serviceAttachment.setUpdatedUser(serviceAttachment.getCreatedUser());
		entityManager.persist(serviceAttachment);
	}

	@Override
	public ServiceAttachment updateEntity(final ServiceAttachment serviceAttachment) {
		serviceAttachment.setUpdatedDate(new Date());
		serviceAttachment.setUpdatedUser(getCurrentUsername());
		ServiceAttachment managedServiceAttachment = entityManager.merge(serviceAttachment);
		return managedServiceAttachment;
	}

	@Override
	public void deleteEntity(final ServiceAttachment serviceAttachment) {
		serviceAttachment.setDeletedDate(new Date());
		serviceAttachment.setDeletedUser(getCurrentUsername());
		entityManager.merge(serviceAttachment);
	}

}
