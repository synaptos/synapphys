package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.FindingsLsp;

public interface FindingsLspService {

	public FindingsLsp getEntity(final Long entityId);

	public void addEntity(final FindingsLsp entity);

	public FindingsLsp updateEntity(final FindingsLsp entity);

}
