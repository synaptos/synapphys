package at.synaptos.synapphys.service;

import java.util.List;

import at.synaptos.synapphys.model.HealthInsurance;

public interface HealthInsuranceService extends CommonEntityService<HealthInsurance> {

	public List<HealthInsurance> getEntities();
	
}
