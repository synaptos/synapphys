package at.synaptos.synapphys.service;

import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

import at.synaptos.synapphys.model.FindingsLsp;

@RolesAllowed("user")
@Stateless
public class FindingsLspServiceBean extends AbstractService implements FindingsLspService {

	@Override
	public FindingsLsp getEntity(final Long findingsLspId) {
		FindingsLsp managedFindingsLsp = entityManager.find(FindingsLsp.class, findingsLspId);
		return managedFindingsLsp;
	}

	@Override
	public void addEntity(final FindingsLsp findingsLsp) {
		findingsLsp.setCreatedDate(new Date());
		findingsLsp.setCreatedUser(getCurrentUsername());
		findingsLsp.setUpdatedDate(findingsLsp.getCreatedDate());
		findingsLsp.setUpdatedUser(findingsLsp.getCreatedUser());
		entityManager.persist(findingsLsp);
	}

	@Override
	public FindingsLsp updateEntity(final FindingsLsp findingsLsp) {
		findingsLsp.setUpdatedDate(new Date());
		findingsLsp.setUpdatedUser(getCurrentUsername());
		FindingsLsp managedFindingsLsp = entityManager.merge(findingsLsp);
		return managedFindingsLsp;
	}

}
