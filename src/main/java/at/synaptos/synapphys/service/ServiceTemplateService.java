package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.ServiceTemplate;

public interface ServiceTemplateService extends CommonEntityService<ServiceTemplate> {
		
}
