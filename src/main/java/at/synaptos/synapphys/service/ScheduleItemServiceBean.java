package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed("user")
@Stateless
public class ScheduleItemServiceBean extends AbstractService implements ScheduleItemService {

	@Inject
	private Logger logger;

	@Override
	public List<ScheduleItem> getEntities() {
		TypedQuery<ScheduleItem> query = entityManager.createNamedQuery(ScheduleItem.FIND_ALL, ScheduleItem.class);
		List<ScheduleItem> scheduleItems = query.getResultList();
		logger.debug("scheduleItems.size()=" + CollectionUtils.size(scheduleItems));
		return scheduleItems;
	}

	@Override
	public ScheduleItem getEntity(final Long scheduleItemId) {
		ScheduleItem managedScheduleItem = entityManager.find(ScheduleItem.class, scheduleItemId);
		return managedScheduleItem;
	}

	@Override
	public void addEntity(final ScheduleItem scheduleItem) {
		scheduleItem.setCreatedDate(new Date());
		scheduleItem.setCreatedUser(getCurrentUsername());
		scheduleItem.setUpdatedDate(scheduleItem.getCreatedDate());
		scheduleItem.setUpdatedUser(scheduleItem.getCreatedUser());
		entityManager.persist(scheduleItem);
	}

	@Override
	public ScheduleItem updateEntity(final ScheduleItem scheduleItem) {
		scheduleItem.setUpdatedDate(new Date());
		scheduleItem.setUpdatedUser(getCurrentUsername());
		ScheduleItem managedScheduleItem = entityManager.merge(scheduleItem);
		return managedScheduleItem;
	}

	@Override
	public void deleteEntity(final ScheduleItem scheduleItem) {
		scheduleItem.setDeletedDate(new Date());
		scheduleItem.setDeletedUser(getCurrentUsername());
		entityManager.merge(scheduleItem);
	}

	@Override
	public List<ScheduleItem> getEntities(final Date startDate, final Date endDate) {
		logger.debug("startDate=" + startDate + ", endDate=" + endDate);
		TypedQuery<ScheduleItem> query = entityManager.createNamedQuery(ScheduleItem.FIND_BETWEEN, ScheduleItem.class);
		query.setParameter("startDate", startDate, TemporalType.TIMESTAMP);
		query.setParameter("endDate", endDate, TemporalType.TIMESTAMP);
		List<ScheduleItem> scheduleItemList = query.getResultList();
		logger.debug("scheduleItemList.size()=" + ((scheduleItemList != null) ? scheduleItemList.size() : null));
		return scheduleItemList;
	}

}
