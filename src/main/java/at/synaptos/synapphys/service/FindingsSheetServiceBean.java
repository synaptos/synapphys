package at.synaptos.synapphys.service;

import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

import at.synaptos.synapphys.model.FindingsSheet;

@RolesAllowed("user")
@Stateless
public class FindingsSheetServiceBean extends AbstractService implements FindingsSheetService {

	@Override
	public FindingsSheet getEntity(final Long findingsSheetId) {
		FindingsSheet managedFindingsSheet = entityManager.find(FindingsSheet.class, findingsSheetId);
		return managedFindingsSheet;
	}

	@Override
	public void addEntity(final FindingsSheet findingsSheet) {
		findingsSheet.setCreatedDate(new Date());
		findingsSheet.setCreatedUser(getCurrentUsername());
		findingsSheet.setUpdatedDate(findingsSheet.getCreatedDate());
		findingsSheet.setUpdatedUser(findingsSheet.getCreatedUser());
		entityManager.persist(findingsSheet);
	}

	@Override
	public FindingsSheet updateEntity(final FindingsSheet findingsSheet) {
		findingsSheet.setUpdatedDate(new Date());
		findingsSheet.setUpdatedUser(getCurrentUsername());
		FindingsSheet managedFindingsSheet = entityManager.merge(findingsSheet);
		return managedFindingsSheet;
	}

}
