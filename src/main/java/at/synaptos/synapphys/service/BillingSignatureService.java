package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.BillingSignature;

public interface BillingSignatureService {

	public void addEntity(final BillingSignature entity);

}
