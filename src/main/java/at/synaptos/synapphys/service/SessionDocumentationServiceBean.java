package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.SessionDocumentation;

@RolesAllowed("user")
@Stateless
public class SessionDocumentationServiceBean extends AbstractService implements SessionDocumentationService {

	@Inject
	private Logger logger;

	@Override
	public SessionDocumentation getEntity(final Long sessionDocumentationId) {
		SessionDocumentation managedSessionDocumentation = entityManager.find(SessionDocumentation.class,
				sessionDocumentationId);
		return managedSessionDocumentation;
	}

	@Override
	public void addEntity(final SessionDocumentation sessionDocumentation) {
		sessionDocumentation.setCreatedDate(new Date());
		sessionDocumentation.setCreatedUser(getCurrentUsername());
		sessionDocumentation.setUpdatedDate(sessionDocumentation.getCreatedDate());
		sessionDocumentation.setUpdatedUser(sessionDocumentation.getCreatedUser());
		sessionDocumentation.getServiceDocumentations().forEach(serviceDocumentation -> {
			serviceDocumentation.setCreatedDate(sessionDocumentation.getUpdatedDate());
			serviceDocumentation.setCreatedUser(sessionDocumentation.getUpdatedUser());
			serviceDocumentation.setUpdatedDate(serviceDocumentation.getCreatedDate());
			serviceDocumentation.setUpdatedUser(serviceDocumentation.getCreatedUser());
			serviceDocumentation.setSessionDocumentation(sessionDocumentation);
		});
		sessionDocumentation.getKpiItems().forEach(kpi -> {
			kpi.setCreatedDate(sessionDocumentation.getUpdatedDate());
			kpi.setCreatedUser(sessionDocumentation.getUpdatedUser());
			kpi.setUpdatedDate(kpi.getCreatedDate());
			kpi.setUpdatedUser(kpi.getCreatedUser());
			kpi.setSessionDocumentation(sessionDocumentation);
		});
		entityManager.persist(sessionDocumentation);
	}

	@Override
	public SessionDocumentation updateEntity(final SessionDocumentation sessionDocumentation) {
		sessionDocumentation.setUpdatedDate(new Date());
		sessionDocumentation.setUpdatedUser(getCurrentUsername());
		sessionDocumentation.getServiceDocumentations().forEach(serviceDocumentation -> {
			serviceDocumentation.setUpdatedDate(sessionDocumentation.getUpdatedDate());
			serviceDocumentation.setUpdatedUser(sessionDocumentation.getUpdatedUser());
			if (serviceDocumentation.getSessionDocumentation() == null) {
				serviceDocumentation.setCreatedDate(sessionDocumentation.getUpdatedDate());
				serviceDocumentation.setCreatedUser(sessionDocumentation.getUpdatedUser());
				serviceDocumentation.setSessionDocumentation(sessionDocumentation);
			}
		});
		sessionDocumentation.getKpiItems().forEach(kpi -> {
			kpi.setUpdatedDate(sessionDocumentation.getUpdatedDate());
			kpi.setUpdatedUser(sessionDocumentation.getUpdatedUser());
			if (kpi.getSessionDocumentation() == null) {
				kpi.setCreatedDate(sessionDocumentation.getUpdatedDate());
				kpi.setCreatedUser(sessionDocumentation.getUpdatedUser());
				kpi.setSessionDocumentation(sessionDocumentation);
			}
		});
		SessionDocumentation managedSessionDocumentation = entityManager.merge(sessionDocumentation);
		return managedSessionDocumentation;
	}

	@Override
	public void deleteEntity(final SessionDocumentation sessionDocumentation) {
		sessionDocumentation.setDeletedDate(new Date());
		sessionDocumentation.setDeletedUser(getCurrentUsername());
		entityManager.merge(sessionDocumentation);
	}

	@Override
	public List<SessionDocumentation> getEntitiesBetween(Date startDate, Date endDate) {
		logger.debug("startDate=" + ((startDate != null) ? startDate : null) + ", endDate="
				+ ((endDate != null) ? endDate : null));
		TypedQuery<SessionDocumentation> query = entityManager.createNamedQuery(SessionDocumentation.findBetween,
				SessionDocumentation.class);
		query.setParameter("startDate", startDate, TemporalType.TIMESTAMP);
		query.setParameter("endDate", endDate, TemporalType.TIMESTAMP);
		List<SessionDocumentation> sessionDocumentationList = query.getResultList();
		logger.debug("sessionDocumentationList.size()="
				+ ((sessionDocumentationList != null) ? sessionDocumentationList.size() : null));
		return sessionDocumentationList;
	}

}
