package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.model.PatientStatus;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed("user")
@Stateless
public class ClientServiceBean extends AbstractService implements ClientService {

	@Inject
	private Logger logger;

	@Override
	public List<Client> getEntities() {
		TypedQuery<Client> query = entityManager.createNamedQuery(Client.FIND_ALL, Client.class);
		List<Client> clients = query.getResultList();
		logger.debug("clients.size()=" + CollectionUtils.size(clients));
		return clients;
	}

	@Override
	public Client getEntity(final Long clientId) {
		Client managedClient = entityManager.find(Client.class, clientId);
		return managedClient;
	}

	@Override
	public void addEntity(final Client client) {
		client.setCreatedDate(new Date());
		client.setCreatedUser(getCurrentUsername());
		client.setUpdatedDate(client.getCreatedDate());
		client.setUpdatedUser(client.getCreatedUser());
		if (client.getMainInsuredClient() != null
				&& !client.getMainInsuredClient().getCoInsuredClients().contains(client)) {
			client.getMainInsuredClient().getCoInsuredClients().add(client);
		}
		entityManager.persist(client);
	}

	@Override
	public Client updateEntity(final Client client) {
		client.setUpdatedDate(new Date());
		client.setUpdatedUser(getCurrentUsername());
		Client managedClient = entityManager.merge(client);
		return managedClient;
	}

	@Override
	public void deleteEntity(final Client client) {
		client.setDeletedDate(new Date());
		client.setDeletedUser(getCurrentUsername());
		entityManager.merge(client);
		if (client.getMainInsuredClient() != null
				&& client.getMainInsuredClient().getCoInsuredClients().contains(client)) {
			client.getMainInsuredClient().getCoInsuredClients().remove(client);
		}
	}

	@Override
	public List<Client> getMainInsuredClients() {
		TypedQuery<Client> query = entityManager.createNamedQuery(Client.FIND_MAIN_INSURED, Client.class);
		List<Client> clients = query.getResultList();
		logger.debug("clients.size()=" + CollectionUtils.size(clients));
		return clients;
	}

	@Override
	public boolean existsEntity(final String insuranceNo) {
		TypedQuery<Client> query = entityManager.createNamedQuery(Client.FIND_BY_HEALTH_INSURANCE_NO, Client.class)
				.setParameter("healthInsuranceNo", insuranceNo);
		List<Client> clients = query.getResultList();
		logger.debug("clients.size()=" + CollectionUtils.size(clients));
		return !CollectionUtils.isEmpty(clients);
	}

	@Override
	public boolean existsEntity(final String insuranceNo, final HealthInsurance insurance) {
		TypedQuery<Client> query = entityManager.createNamedQuery(Client.FIND_BY_HEALTH_INSURANCE, Client.class)
				.setParameter("healthInsuranceNo", insuranceNo).setParameter("healthInsurance", insurance);
		List<Client> clients = query.getResultList();
		logger.debug("clients.size()=" + CollectionUtils.size(clients));
		return !CollectionUtils.isEmpty(clients);
	}

	@Override
	public List<Object[]> getSearchItems() {
		Query query = entityManager.createNamedQuery(Client.FIND_SEARCH_ITEMS_NATIVE);
		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();
		logger.debug("result.size()=" + CollectionUtils.size(result));
		return result;
	}

	@Override
	public List<Client> getEntities(final String searchKey) {
		TypedQuery<Client> query = entityManager.createNamedQuery(Client.FIND_BY_SEARCH_KEY, Client.class)
				.setParameter("searchKey", searchKey);
		List<Client> clients = query.getResultList();
		logger.debug("clients.size()=" + CollectionUtils.size(clients));
		return clients;
	}

	@Override
	public List<Client> getEntities(final PatientStatus patientStatus) {
		TypedQuery<Client> query = entityManager.createNamedQuery(Client.FIND_BY_PATIENT_STATUS, Client.class)
				.setParameter("patientStatus", patientStatus);
		List<Client> clients = query.getResultList();
		logger.debug("clients.size()=" + CollectionUtils.size(clients));
		return clients;
	}

}
