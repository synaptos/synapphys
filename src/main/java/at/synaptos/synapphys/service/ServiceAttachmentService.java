package at.synaptos.synapphys.service;

import at.synaptos.synapphys.model.ServiceAttachment;

public interface ServiceAttachmentService extends CommonEntityService<ServiceAttachment> {
	
}
