package at.synaptos.synapphys.service;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.PatientStatus;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.utils.CollectionUtils;

@RolesAllowed({ "user", "receipt" })
@Stateless
public class SeriesDocumentationServiceBean extends AbstractService implements SeriesDocumentationService {

	@Inject
	private Logger logger;

	@Inject
	private ClientService clientService;

	@Override
	public List<SeriesDocumentation> getEntities() {
		TypedQuery<SeriesDocumentation> query = entityManager.createNamedQuery(SeriesDocumentation.FIND_ALL,
				SeriesDocumentation.class);
		List<SeriesDocumentation> seriesDocumentations = query.getResultList();
		logger.debug("seriesDocumentations.size()=" + CollectionUtils.size(seriesDocumentations));
		return seriesDocumentations;
	}

	@Override
	public SeriesDocumentation getEntity(final Long seriesDocumentationId) {
		SeriesDocumentation managedSeriesDocumentations = entityManager.find(SeriesDocumentation.class,
				seriesDocumentationId);
		return managedSeriesDocumentations;
	}

	@Override
	public void addEntity(final SeriesDocumentation seriesDocumentation) {
		seriesDocumentation.setCreatedDate(new Date());
		seriesDocumentation.setCreatedUser(getCurrentUsername());
		seriesDocumentation.setUpdatedDate(seriesDocumentation.getCreatedDate());
		seriesDocumentation.setUpdatedUser(seriesDocumentation.getCreatedUser());
		entityManager.persist(seriesDocumentation);
		seriesDocumentation.getClient().setPatientStatus(PatientStatus.CURRENT);
		seriesDocumentation.getClient().setPatientStatusDate(seriesDocumentation.getAddedDate());
		clientService.updateEntity(seriesDocumentation.getClient());
	}

	@Override
	public SeriesDocumentation updateEntity(final SeriesDocumentation seriesDocumentation) {
		seriesDocumentation.setUpdatedDate(new Date());
		seriesDocumentation.setUpdatedUser(getCurrentUsername());
		if (seriesDocumentation.getTherapeuticalAnamnesis() != null
				&& seriesDocumentation.getTherapeuticalAnamnesis().getSeriesDocumentation() == null) {
			seriesDocumentation.getTherapeuticalAnamnesis().setSeriesDocumentation(seriesDocumentation);
		}
		if (seriesDocumentation.getTherapeuticalDiagnosis() != null
				&& seriesDocumentation.getTherapeuticalDiagnosis().getSeriesDocumentation() == null) {
			seriesDocumentation.getTherapeuticalDiagnosis().setSeriesDocumentation(seriesDocumentation);
		}
		SeriesDocumentation managedSeriesDocumentations = entityManager.merge(seriesDocumentation);
		if (!seriesDocumentation.isDone()) {
			if (seriesDocumentation.getClient().getPatientStatus() == null) {
				seriesDocumentation.getClient().setPatientStatus(PatientStatus.CURRENT);
				seriesDocumentation.getClient().setPatientStatusDate(seriesDocumentation.getAddedDate());
				clientService.updateEntity(seriesDocumentation.getClient());
			}
		} else if (seriesDocumentation.getClient().getPatientStatus() != null) {
			seriesDocumentation.getClient().setPatientStatus(null);
			seriesDocumentation.getClient().setPatientStatusDate(null);
			clientService.updateEntity(seriesDocumentation.getClient());
		}
		return managedSeriesDocumentations;
	}

	@Override
	public void deleteEntity(final SeriesDocumentation seriesDocumentation) {
		seriesDocumentation.setDeletedDate(new Date());
		seriesDocumentation.setDeletedUser(getCurrentUsername());
		entityManager.merge(seriesDocumentation);
		if (!seriesDocumentation.isDone()) {
			seriesDocumentation.getClient().setPatientStatus(null);
			seriesDocumentation.getClient().setPatientStatusDate(null);
			clientService.updateEntity(seriesDocumentation.getClient());
		}
	}

}
