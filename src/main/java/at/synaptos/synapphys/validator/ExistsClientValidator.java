package at.synaptos.synapphys.validator;

import java.time.LocalDate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import at.synaptos.synapphys.domain.I18nContext;
import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.utils.GlobalI18n;

/**
 * Check DB for possible existing Client with this Validator.
 * 
 * @author Philipp
 *
 */

@FacesValidator("doubledClientValidator")
public class ExistsClientValidator implements Validator {

	@Inject
	private ClientFacade clientFacade;

	@Inject
	private FacesContext facesContext;

	@Inject
	@GlobalI18n
	protected I18nContext i18nContext;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		// if (value == null) {
		// return;
		// }
		//
		// LocalDate bday = LocalDate.now();
		//
		// if (bday == null) {
		// return;
		// }
		//
		// if (!validateSin((int) value, bday)) {
		// setMessage(context, "validator.insuranceNo.INVALID");
		// }
		//
		// List<Client> doubledClients =
		// clientService.getSameSinClients((int)value);
		//
		// if (!doubledClients.isEmpty()) {
		// setMessage(context, "validator.insuranceNo.EXISTS");
		// }

		UIInput lastNameInput = (UIInput) component.findComponent("lastnameInput_client");
		System.out.println(lastNameInput.getLocalValue());
		System.out.println(lastNameInput.getValue());
		System.out.println(lastNameInput.getSubmittedValue());

		// TODO: PS validierung nn+vn+bday
	}

	public void setMessage(String i18nMessage) {
		facesContext.addMessage("sinInput",
				(new FacesMessage(i18nContext.getString("warning.label"), i18nContext.getString(i18nMessage))));
	}

	public boolean validateSin(int sinNr, LocalDate birthDate) {
		return clientFacade.validateHealthInsuranceNo(sinNr, birthDate);
	}

}
