package at.synaptos.synapphys.validator;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import at.synaptos.synapphys.controller.SettingsScheduleController;
import at.synaptos.synapphys.domain.I18nContext;
import at.synaptos.synapphys.domain.OfficeHoursPerDay;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.GlobalI18n;

@FacesValidator("mornEndDateValidator")
public class MornEndDateValidator implements Validator {

	@Inject
	@GlobalI18n
	protected I18nContext i18nContext;

	@Inject
	SettingsScheduleController settingsScheduleController;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value == null) {
			return;
		}

		LocalDateTime mornEndLdt = DateUtils.asLdt(((Date) value));
		Integer id = Integer.parseInt(component.getId().replaceAll("[^0-9]", ""));
		if (id != null) {
			OfficeHoursPerDay officeHoursPerDay = settingsScheduleController.getOfficeHours().hoursPerDay(id);

			if (mornEndLdt.toLocalTime().isAfter(LocalTime.of(12, 1))) {
				throw new ValidatorException(new FacesMessage(i18nContext.getString("warning.label"),
						i18nContext.getString("setings.validDate.message")));
			}

			if (mornEndLdt.toLocalTime().isBefore(officeHoursPerDay.getMornStartTime())) {
				throw new ValidatorException(new FacesMessage(i18nContext.getString("warning.label"),
						i18nContext.getString("setings.validDate.message")));
			}

		}
		return;
	}

}
