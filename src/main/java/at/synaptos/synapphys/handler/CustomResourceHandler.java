package at.synaptos.synapphys.handler;

import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;
import javax.inject.Inject;

import org.omnifaces.resourcehandler.DefaultResourceHandler;
import org.omnifaces.resourcehandler.RemappedResource;

import at.synaptos.synapphys.controller.PublicViewController;
import at.synaptos.synapphys.utils.StringUtils;

public class CustomResourceHandler extends DefaultResourceHandler {

	@Inject
	private PublicViewController publicViewController;

	public CustomResourceHandler(ResourceHandler wrapped) {
		super(wrapped);
	}

	@Override
	public Resource decorateResource(Resource resource) {
		if (resource == null) {
			return resource;
		}
		if (StringUtils.isEmpty(resource.getLibraryName())) {
			return new RemappedResource(resource,
					resource.getRequestPath() + "?v=" + publicViewController.getVersion());
		}
		if ("poseidon-layout".equals(resource.getLibraryName())
				|| resource.getLibraryName().startsWith("primefaces-poseidon")) {
			return new RemappedResource(resource,
					resource.getRequestPath() + "&v=" + publicViewController.getVersion());
		}
		return resource;
	}

}
