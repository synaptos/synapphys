package at.synaptos.synapphys.renderer;

import java.io.IOException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.primefaces.component.texteditor.TextEditor;
import org.primefaces.component.texteditor.TextEditorRenderer;
import org.primefaces.util.ComponentUtils;

public class CustomTextEditorRenderer extends TextEditorRenderer {

	@Override
	protected void encodeMarkup(FacesContext context, TextEditor editor) throws IOException {
		ResponseWriter writer = context.getResponseWriter();
		String clientId = editor.getClientId(context);
		String valueToRender = ComponentUtils.getValueToRender(context, editor);
		String inputId = clientId + "_input";
		String editorId = clientId + "_editor";
		UIComponent toolbar = editor.getFacet("toolbar");

		String style = editor.getStyle();
		String styleClass = editor.getStyleClass();

		writer.startElement("div", editor);
		writer.writeAttribute("id", clientId, null);
		if (style != null)
			writer.writeAttribute("style", style, null);
		if (styleClass != null)
			writer.writeAttribute("class", editor.getStyleClass(), null);

		writer.startElement("div", editor);
		writer.writeAttribute("id", editorId, null);
		if (valueToRender != null) {
			writer.write(valueToRender);
		}
		writer.endElement("div");

		writer.startElement("input", null);
		writer.writeAttribute("type", "hidden", null);
		writer.writeAttribute("name", inputId, null);
		writer.endElement("input");

		if (toolbar != null) {
			writer.startElement("div", editor);
			writer.writeAttribute("id", clientId + "_toolbar", null);
			writer.writeAttribute("class", "ui-editor-toolbar", null);
			toolbar.encodeAll(context);
			writer.endElement("div");
		}

		writer.endElement("div");

	}

}
