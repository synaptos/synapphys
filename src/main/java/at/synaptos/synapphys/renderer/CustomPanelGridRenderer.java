package at.synaptos.synapphys.renderer;

import java.io.IOException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.primefaces.component.panelgrid.PanelGridRenderer;

public class CustomPanelGridRenderer extends PanelGridRenderer {
	
	@Inject
	private Logger logger;	

	@Override
	public void encodeBegin(FacesContext context, UIComponent component)
			throws IOException {
   		logger.trace("component.getId()="+component.getId());		
		super.encodeBegin(context, component);
	}

}
