package at.synaptos.synapphys.renderer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.primefaces.component.schedule.Schedule;
import org.primefaces.component.schedule.ScheduleRenderer;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import at.synaptos.synapphys.domain.ScheduleEntry;

public class CustomScheduleRenderer extends ScheduleRenderer {
	
    private TimeZone appropriateTimeZone;

    private java.util.TimeZone calculateTimeZone(Schedule schedule) {
		if(appropriateTimeZone == null) {
			Object usertimeZone = schedule.getTimeZone();
			if(usertimeZone != null) {
				if(usertimeZone instanceof String)
					appropriateTimeZone =  TimeZone.getTimeZone((String) usertimeZone);
				else if(usertimeZone instanceof java.util.TimeZone)
					appropriateTimeZone = (TimeZone) usertimeZone;
				else
					throw new IllegalArgumentException("TimeZone could be either String or java.util.TimeZone");
			} else {
				appropriateTimeZone = TimeZone.getTimeZone("UTC");
			}
		}
		
		return appropriateTimeZone;
	}
	
	@Override
	protected void encodeEventsAsJSON(FacesContext context, Schedule schedule, ScheduleModel model) throws IOException {
		ResponseWriter writer = context.getResponseWriter();
        TimeZone timeZone = calculateTimeZone(schedule);

        SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        iso.setTimeZone(timeZone);
        writer.write("{");
        writer.write("\"events\" : [");
		
        if(model != null) {
            for(Iterator<ScheduleEvent> iterator = model.getEvents().iterator(); iterator.hasNext();) {
            	ScheduleEvent event = (ScheduleEntry)iterator.next();
                String className = event.getStyleClass();
                String description = event.getDescription();
                String rendering = null;
                if (event instanceof ScheduleEntry) {
                	rendering = ((ScheduleEntry)event).getRendering();
                }
               
                writer.write("{");
                writer.write("\"id\": \"" + event.getId() + "\"");	
                writer.write(",\"title\": \"" + escapeText(event.getTitle()) + "\"");
                writer.write(",\"start\": \"" + iso.format(event.getStartDate()) + "\"");	
                writer.write(",\"end\": \"" + iso.format(event.getEndDate()) + "\"");
                writer.write(",\"allDay\":" + event.isAllDay());
                writer.write(",\"editable\":" + event.isEditable());
                if(className != null) {
                    writer.write(",\"className\":\"" + className + "\"");
                }
                if(description != null) {
                    writer.write(",\"description\":\"" + escapeText(description) + "\"");
                }
                if(rendering != null) {
                    writer.write(",\"rendering\":\"" + rendering + "\"");
                }
                
                writer.write("}");

                if(iterator.hasNext()) {
                    writer.write(",");
                }
            }
        }
		
		writer.write("]}");
	}

}
