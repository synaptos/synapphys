package at.synaptos.synapphys.listener;

import java.security.Principal;
import java.util.Date;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.jboss.logging.Logger;
import org.jboss.security.CacheableManager;
import org.jboss.security.SecurityConstants;

@WebListener
public class CustomHttpSessionListener implements HttpSessionListener {

	@Inject
	private Logger logger;

	@Inject
	private Principal principal;

	@Resource(name = SecurityConstants.JAAS_CONTEXT_ROOT + "SynapphysApplicationSD/authenticationMgr")
	private CacheableManager<?, Principal> authenticationManager;

	private static int sessionCount = 0;

	@Override
	public void sessionCreated(HttpSessionEvent httpSessionEvent) {
		sessionCount++;
		logger.info("sessionCount=" + sessionCount + ", sessionId=" + httpSessionEvent.getSession().getId()
				+ ", createdTime=" + (new Date(httpSessionEvent.getSession().getCreationTime())));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
		authenticationManager.flushCache(principal);
		logger.info("sessionCount=" + sessionCount + ", sessionId=" + httpSessionEvent.getSession().getId()
				+ ", lastAccessedTime=" + (new Date(httpSessionEvent.getSession().getLastAccessedTime())));
		sessionCount--;
	}

}