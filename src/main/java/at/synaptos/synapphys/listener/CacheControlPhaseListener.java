package at.synaptos.synapphys.listener;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.inject.Inject;

import org.jboss.logging.Logger;

public class CacheControlPhaseListener implements PhaseListener {

	private static final long serialVersionUID = 7625158791506002529L;

	@Inject
	private Logger logger;
		
	@Override
	public void afterPhase(PhaseEvent arg0) {
	}

	@Override
	public void beforePhase(PhaseEvent arg0) {
		logger.trace("arg0.getPhaseId()="+arg0.getPhaseId());

		/*
		FacesContext facesContext = arg0.getFacesContext();
       
		HttpServletResponse response = (HttpServletResponse)facesContext
                .getExternalContext().getResponse();

		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.addHeader("Cache-Control", "no-store");
		response.addHeader("Cache-Control", "must-revalidate");
		response.setHeader("Expires", "Mon, 8 Aug 2006 10:00:00 GMT");
		*/
	}

	@Override
	public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
	}
		
}
