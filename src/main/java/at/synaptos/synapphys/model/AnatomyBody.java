package at.synaptos.synapphys.model;

public enum AnatomyBody {
   	BONE,
   	MUSCLE,
    SKIN
}
