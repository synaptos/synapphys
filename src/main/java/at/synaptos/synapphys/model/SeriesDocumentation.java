package at.synaptos.synapphys.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;

@NamedQueries({ @NamedQuery(name = SeriesDocumentation.FIND_ALL, query = "select d from SeriesDocumentation d"
		+ " where d.deletedDate is null" + " order by d.approvalDate desc, d.referralDate desc, d.createdDate desc") })
@Table(name = "series_documentation")
@Entity
public class SeriesDocumentation extends AbstractEntity {

	private static final long serialVersionUID = -5906538712735651785L;

	public static final String FIND_ALL = "SeriesDocumentation.findAll";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "series_documentation_id")
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name = "referral_date")
	private Date referralDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "approval_date")
	private Date approvalDate;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "client", referencedColumnName = "person_id", nullable = false)
	private Client client;

	@ManyToOne
	@JoinColumn(name = "physician", referencedColumnName = "person_id")
	private Physician physician;

	@Column(name = "medical_diagnosis")
	private String medicalDiagnosis;

	@Column(name = "session_duration")
	private Integer sessionDuration;

	@Column(name = "session_count")
	private Integer sessionCount;

	@Column(name = "findingNote")
	private String findingNote;

	@Column(name = "longterm_outcome")
	private String longtermOutcome;

	@Column(name = "shortterm_outcome")
	private String shorttermOutcome;

	@OneToMany(mappedBy = "seriesDocumentation")
	@Where(clause = "deleted_date is null")
	private List<ServiceTemplate> serviceTemplates;

	@OneToMany(mappedBy = "seriesDocumentation")
	@OrderBy("session_start_date desc")
	@Where(clause = "deleted_date is null")
	private List<SessionDocumentation> sessionDocumentations;

	@OneToMany(mappedBy = "seriesDocumentation")
	@Where(clause = "deleted_date is null")
	private List<ServiceAttachment> serviceAttachments;

	// TODO WQ umstellen auf schwaches Entity
	@OneToOne(mappedBy = "seriesDocumentation", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE }, fetch = FetchType.EAGER)
	private TherapeuticalAnamnesis therapeuticalAnamnesis;

	// TODO WQ umstellen auf schwaches Entity
	@OneToOne(mappedBy = "seriesDocumentation", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE }, fetch = FetchType.EAGER)
	private TherapeuticalDiagnosis therapeuticalDiagnosis;

	@OneToOne(mappedBy = "seriesDocumentation")
	private FindingsSheet findingsSheet;

	@OneToOne(mappedBy = "seriesDocumentation")
	private FindingsLsp findingsLsp;

	@OneToMany(mappedBy = "seriesDocumentation")
	@Where(clause = "deleted_date is null")
	private List<BodymapItem> bodymapItems;

	@Temporal(TemporalType.DATE)
	@Column(name = "canceled_date")
	private Date canceledDate;

	@OneToMany(mappedBy = "seriesDocumentation")
	@OrderBy("billing_type, billing_date desc")
	@Where(clause = "deleted_date is null")
	private List<Billing> billings;

	public SeriesDocumentation() {
		setSessionDuration(30);
		setSessionCount(10);
	}

	public static SeriesDocumentation getInstance() {
		return new SeriesDocumentation();
	}

	public static SeriesDocumentation getInstance(@NotNull final Client client) {
		return getInstance().withClient(client);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getReferralDate() {
		return referralDate;
	}

	public void setReferralDate(Date referralDate) {
		this.referralDate = referralDate;
	}

	public LocalDate getReferralLd() {
		if (getReferralDate() == null) {
			return null;
		}
		return DateUtils.asLdt(getReferralDate()).toLocalDate();
	}

	public void setReferralLd(LocalDate referralLd) {
		setReferralDate(DateUtils.asDate(referralLd));
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public LocalDate getApprovalLd() {
		if (getApprovalDate() == null) {
			return null;
		}
		return DateUtils.asLdt(getApprovalDate()).toLocalDate();
	}

	public void setApprovalLd(LocalDate approvalLd) {
		setApprovalDate(DateUtils.asDate(approvalLd));
	}

	public Client getClient() {
		return client;
	}

	public SeriesDocumentation withClient(Client client) {
		setClient(client);
		return this;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Physician getPhysician() {
		return physician;
	}

	public void setPhysician(Physician physician) {
		this.physician = physician;
	}

	public String getMedicalDiagnosis() {
		return medicalDiagnosis;
	}

	public void setMedicalDiagnosis(String medicalDiagnosis) {
		this.medicalDiagnosis = medicalDiagnosis;
	}

	public Integer getSessionDuration() {
		return sessionDuration;
	}

	public void setSessionDuration(Integer sessionDuration) {
		this.sessionDuration = sessionDuration;
	}

	public SeriesDocumentation withSessionDuration(Integer sessionDuration) {
		setSessionDuration(sessionDuration);
		return this;
	}

	public Integer getSessionCount() {
		return sessionCount;
	}

	public void setSessionCount(Integer sessionCount) {
		this.sessionCount = sessionCount;
	}

	public SeriesDocumentation withSessionCount(Integer sessionCount) {
		setSessionCount(sessionCount);
		return this;
	}

	public String getFindingNote() {
		return findingNote;
	}

	public void setFindingNote(String findingNote) {
		this.findingNote = findingNote;
	}

	public String getLongtermOutcome() {
		return longtermOutcome;
	}

	public void setLongtermOutcome(String longtermOutcome) {
		this.longtermOutcome = longtermOutcome;
	}

	public String getShorttermOutcome() {
		return shorttermOutcome;
	}

	public void setShorttermOutcome(String shorttermOutcome) {
		this.shorttermOutcome = shorttermOutcome;
	}

	public List<ServiceTemplate> getServiceTemplates() {
		if (serviceTemplates == null) {
			serviceTemplates = new ArrayList<ServiceTemplate>();
		}
		return serviceTemplates;
	}

	public List<SessionDocumentation> getSessionDocumentations() {
		if (sessionDocumentations == null) {
			sessionDocumentations = new ArrayList<SessionDocumentation>();
		}
		return sessionDocumentations;
	}

	public List<ServiceAttachment> getServiceAttachments() {
		if (serviceAttachments == null) {
			serviceAttachments = new ArrayList<ServiceAttachment>();
		}
		return serviceAttachments;
	}

	public void setServiceAttachments(List<ServiceAttachment> serviceAttachments) {
		this.serviceAttachments = serviceAttachments;
	}

	// FIXME WQ
	public TherapeuticalAnamnesis getTherapeuticalAnamnesis() {
		if (therapeuticalAnamnesis == null) {
			therapeuticalAnamnesis = new TherapeuticalAnamnesis();
		}
		return therapeuticalAnamnesis;
	}

	public void setTherapeuticalAnamnesis(TherapeuticalAnamnesis therapeuticalAnamnesis) {
		this.therapeuticalAnamnesis = therapeuticalAnamnesis;
	}

	// FIXME WQ
	public TherapeuticalDiagnosis getTherapeuticalDiagnosis() {
		if (therapeuticalDiagnosis == null) {
			therapeuticalDiagnosis = new TherapeuticalDiagnosis();
		}
		return therapeuticalDiagnosis;
	}

	public void setTherapeuticalDiagnosis(TherapeuticalDiagnosis therapeuticalDiagnosis) {
		this.therapeuticalDiagnosis = therapeuticalDiagnosis;
	}

	public FindingsSheet getFindingsSheet() {
		return findingsSheet;
	}

	public void setFindingsSheet(FindingsSheet findingsSheet) {
		this.findingsSheet = findingsSheet;
	}

	public FindingsLsp getFindingsLsp() {
		return findingsLsp;
	}

	public void setFindingsLsp(FindingsLsp findingsLsp) {
		this.findingsLsp = findingsLsp;
	}

	public List<BodymapItem> getBodymapItems() {
		if (bodymapItems == null) {
			bodymapItems = new ArrayList<BodymapItem>();
		}
		return bodymapItems;
	}

	public void setBodymapItems(List<BodymapItem> bodymapItems) {
		this.bodymapItems = bodymapItems;
	}

	public Date getCanceledDate() {
		return canceledDate;
	}

	public void setCanceledDate(Date canceledDate) {
		this.canceledDate = canceledDate;
	}

	public List<Billing> getBillings() {
		if (billings == null) {
			billings = new ArrayList<Billing>();
		}
		return billings;
	}

	public void setBillings(List<Billing> billings) {
		this.billings = billings;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof SeriesDocumentation && id != null) ? id.equals(((SeriesDocumentation) other).id)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{ super=" + super.toString() + ", id=" + id + ", referralDate="
				+ referralDate + ", approvalDate=" + approvalDate + ", client=" + client + ", sessionCount="
				+ sessionCount + ", sessionDuration=" + sessionDuration + ", canceledDate=" + canceledDate + " }";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	@Override
	public SeriesDocumentation withBackUrl(String backUrl) {
		setBackUrl(backUrl);
		return this;
	}

	public SessionDocumentation getFirstSessionDocumentation() {
		if (CollectionUtils.isEmpty(getSessionDocumentations())) {
			return null;
		}
		return getSessionDocumentations().get(getSessionDocumentations().size() - 1);
	}

	public SessionDocumentation getLastSessionDocumentation() {
		if (CollectionUtils.isEmpty(getSessionDocumentations())) {
			return null;
		}
		return getSessionDocumentations().get(0);
	}

	public List<SessionDocumentation> getSessionDocumentationsReversed() {
		List<SessionDocumentation> sessionDocumentationsReversed = new ArrayList<SessionDocumentation>();
		sessionDocumentationsReversed.addAll(getSessionDocumentations());
		Collections.reverse(sessionDocumentationsReversed);
		return sessionDocumentationsReversed;
	}

	public int getRemainingSessionCount() {
		if (getSessionCount() == null) {
			return 0;
		}
		return getSessionCount() - getSessionDocumentations().size();
	}

	public Date getAddedDate() {
		if (getApprovalDate() != null) {
			return getApprovalDate();
		}
		if (getReferralDate() != null) {
			return getReferralDate();
		}
		return getCreatedDate();
	}

	public Date getDoneDate() {
		Billing invoice = getInvoice();
		if (invoice != null) {
			return invoice.getBillingDate();
		}
		return getCanceledDate();
	}

	public boolean isDone() {
		return getDoneDate() != null;
	}

	public Billing getInvoice() {
		if (CollectionUtils.isEmpty(getBillings())) {
			return null;
		}
		for (Billing billing : getBillings()) {
			if (BillingType.INVOICE == billing.getBillingType()) {
				return billing;
			}
		}
		return null;
	}

	public boolean isInvoiced() {
		return AbstractEntity.isValid(getInvoice());
	}

	public double getServiceTotal() {
		if (CollectionUtils.isEmpty(getSessionDocumentations())) {
			return 0d;
		}
		double total = 0d;
		for (SessionDocumentation currSessionDocumentation : getSessionDocumentations()) {
			if (!CollectionUtils.isEmpty(currSessionDocumentation.getServiceDocumentations())) {
				total += currSessionDocumentation.getServiceDocumentations().stream()
						.mapToDouble(serviceDocumentation -> serviceDocumentation.getTotal()).sum();
			}
		}
		return total;
	}

	public List<KpiItem> getKpiItems(final KpiType kpiType) {
		final List<KpiItem> kpiItems = new ArrayList<KpiItem>();
		for (SessionDocumentation sessionDocumentation : this.getSessionDocumentationsReversed()) {
			if (CollectionUtils.isEmpty(sessionDocumentation.getKpiItems())) {
				kpiItems.add(KpiItem.getInstance(kpiType));
			} else {
				sessionDocumentation.getKpiItems().stream().filter(kpi -> kpi.getKpiType() == kpiType)
						.forEach(kpi -> kpiItems.add(kpi));
			}
		}
		return kpiItems;
	}

	public LocalDate getDeadlineLd() {
		if (getApprovalLd() == null) {
			return null;
		}
		return getApprovalLd().plusMonths(3);
	}

}
