package at.synaptos.synapphys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

import at.synaptos.synapphys.utils.StringUtils;

@Embeddable
public class Contact implements Serializable {

	private static final long serialVersionUID = 1886422496028239273L;

	@Column(name = "contact_phone_1")
	private String phone1;

	@Column(name = "contact_phone_2")
	private String phone2;

	@Column(name = "contact_fax")
	private String fax;

	@Pattern(regexp = ".+@.+", message = "{contact.email.INVALID}")
	@Column(name = "contact_email")
	private String email;

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone) {
		this.phone1 = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone) {
		this.phone2 = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		String phone = getPhone1();
		if (StringUtils.isEmpty(phone)) {
			phone = getPhone2();
		}
		return phone;
	}

}
