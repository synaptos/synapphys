package at.synaptos.synapphys.model;

public enum KpiType {
    ADL,
    PAIN,
    MOBILITY
}
