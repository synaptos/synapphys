package at.synaptos.synapphys.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries({ @NamedQuery(name = HealthInsurance.FIND_ALL, query = "select i from HealthInsurance i"
		+ " where i.deletedDate is null order by i.name") })
@Table(name = "health_insurance")
@Entity
public class HealthInsurance extends AbstractEntity {

	private static final long serialVersionUID = 7763347443902919597L;

	public static final String FIND_ALL = "Insurance.findAll";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "health_insurance_id")
	private Long id;

	@NotNull
	@Column(name = "health_insurance_name")
	private String name;

	@Embedded
	private Address address;

	@Embedded
	private Contact contact;

	@Column(name = "contact_note")
	private String contactNote;

	@NotNull
	@Column(name = "health_insurance_code")
	private String code;

	public HealthInsurance() {
	}

	public static HealthInsurance getInstance() {
		return new HealthInsurance();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		if (address == null) {
			address = new Address();
		}
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Contact getContact() {
		if (contact == null) {
			contact = new Contact();
		}
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getContactNote() {
		return contactNote;
	}

	public void setContactNote(String contactNote) {
		this.contactNote = contactNote;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof HealthInsurance && id != null) ? id.equals(((HealthInsurance) other).id)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", name=" + name + ", code=" + code + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	@Override
	public HealthInsurance withBackUrl(String backUrl) {
		setBackUrl(backUrl);
		return this;
	}

}
