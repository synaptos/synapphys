package at.synaptos.synapphys.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries({
		@NamedQuery(name = ServiceDocumentation.FIND_ALL, query = "select s from ServiceDocumentation s where s.deletedDate is null order by s.id") })
@Table(name = "service_documentation")
@Entity
public class ServiceDocumentation extends AbstractEntity {

	private static final long serialVersionUID = 8905499628817067695L;

	public static final String FIND_ALL = "ServiceDocumentation.findAll";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "service_documentation_id")
	private Long id;

	@NotNull
	@OneToOne
	@JoinColumn(name = "service_type", referencedColumnName = "service_type_id")
	private ServiceType serviceType;

	@NotNull
	@Column(name = "service_quantity")
	private Integer quantity;

	@OneToMany(mappedBy = "serviceDocumentation")
	private List<BillingItem> billingItems;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "session_documentation", referencedColumnName = "session_documentation_id", nullable = false)
	private SessionDocumentation sessionDocumentation;

	public ServiceDocumentation() {
		setQuantity(1);
	}

	public static ServiceDocumentation getInstance(@NotNull final ServiceType serviceType) {
		final ServiceDocumentation instance = new ServiceDocumentation();
		instance.setServiceType(serviceType);
		return instance;
	}

	public static ServiceDocumentation getInstance(@NotNull final ServiceTemplate serviceTemplate) {
		final ServiceDocumentation instance = getInstance(serviceTemplate.getServiceType());
		instance.setQuantity(serviceTemplate.getQuantity());
		return instance;
	}

	public static ServiceDocumentation getInstance(@NotNull final ServiceDocumentation serviceDocumentation) {
		final ServiceDocumentation instance = getInstance(serviceDocumentation.getServiceType());
		instance.setQuantity(serviceDocumentation.getQuantity());
		return instance;
	}

	@PrePersist
	public void prePersist() {
		if (getQuantity() == null) {
			setQuantity(1);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public ServiceDocumentation withQuantity(Integer quantity) {
		setQuantity(quantity);
		return this;
	}

	public List<BillingItem> getBillingItems() {
		if (billingItems == null) {
			setBillingItems(new ArrayList<BillingItem>());
		}
		return billingItems;
	}

	public void setBillingItems(List<BillingItem> billingItems) {
		this.billingItems = billingItems;
	}

	public SessionDocumentation getSessionDocumentation() {
		return sessionDocumentation;
	}

	public void setSessionDocumentation(SessionDocumentation sessionDocumentation) {
		this.sessionDocumentation = sessionDocumentation;
	}

	public ServiceDocumentation withSessionDocumentation(SessionDocumentation sessionDocumentation) {
		setSessionDocumentation(sessionDocumentation);
		return this;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", serviceType=" + serviceType + ", quantity=" + quantity
				+ ", sessionDocumentation=" + sessionDocumentation + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	public double getTotal() {
		BigDecimal vat = getServiceType().getCategory().getVat() == null ? BigDecimal.ZERO
				: getServiceType().getCategory().getVat();
		return getServiceType().getAmount().multiply(BigDecimal.valueOf(getQuantity()))
				.multiply(BigDecimal.valueOf(100).add(vat).divide(BigDecimal.valueOf(100))).doubleValue();
	}

	public boolean isBilled(BillingType billingType) {
		for (BillingItem billingItem : getBillingItems()) {
			if (billingItem.getBilling().isDeleted()) {
				continue;
			}
			if (billingItem.getBilling().getBillingType() == billingType) {
				return true;
			}
		}
		return false;
	}

	public boolean isReceipted() {
		return isBilled(BillingType.RECEIPT);
	}

	public boolean isInvoiced() {
		return isBilled(BillingType.INVOICE);
	}

}
