package at.synaptos.synapphys.model;

public enum PaymentMethod {
	CASH,
    MC,
    CC,
    BANK
}
