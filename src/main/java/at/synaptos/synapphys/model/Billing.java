package at.synaptos.synapphys.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import at.synaptos.synapphys.domain.BankTransfer;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;

@SqlResultSetMapping(name = "Billing.mappingNative", entities = { @EntityResult(entityClass = Billing.class, fields = {
		@FieldResult(name = "id", column = "billing_id"), @FieldResult(name = "billingType", column = "billing_type"),
		@FieldResult(name = "billingDate", column = "billing_date"),
		@FieldResult(name = "billingNote", column = "billing_note"),
		@FieldResult(name = "headerNote", column = "header_note"),
		@FieldResult(name = "footerNote", column = "footer_note"), @FieldResult(name = "dueDate", column = "due_date"),
		@FieldResult(name = "paymentMethod", column = "payment_method"),
		@FieldResult(name = "paymentDate", column = "payment_date"),
		@FieldResult(name = "paymentNo", column = "payment_no"),
		@FieldResult(name = "paymentNote", column = "payment_note"),
		@FieldResult(name = "referenceYear", column = "reference_year"),
		@FieldResult(name = "referenceSequence", column = "reference_sequence"),
		@FieldResult(name = "ftAccessToken", column = "ft_access_token"),
		@FieldResult(name = "ftCashboxId", column = "ft_cashbox_id"),
		@FieldResult(name = "cbTerminalId", column = "cb_terminal_id"),
		@FieldResult(name = "seriesDocumentation", column = "series_documentation"),
		@FieldResult(name = "createdDate", column = "created_date"),
		@FieldResult(name = "createdUser", column = "created_user"),
		@FieldResult(name = "deletedDate", column = "deleted_date"),
		@FieldResult(name = "deletedUser", column = "deleted_user"),
		@FieldResult(name = "updatedDate", column = "updated_date"),
		@FieldResult(name = "updatedUser", column = "updated_user") }) }, columns = {
				@ColumnResult(name = "billing_total", type = Double.class) })
@NamedNativeQueries({
		@NamedNativeQuery(name = Billing.FIND_BY_REFERENCE_NATIVE, query = "select ifnull(max(reference_sequence),0)+1 from billing"
				+ " where billing_type = ?1 and billing_date >= makedate(?2, 1) and billing_date < makedate(?2+1, 1)"),
		@NamedNativeQuery(name = Billing.FIND_BETWEEN_NATIVE, query = "select b.*,"
				+ " sum(i.amount*(1+ifnull(i.vat,0)/100)) as billing_total"
				+ " from billing b inner join billing_item i on i.billing = b.billing_id"
				+ " where b.billing_type = ?1 and b.billing_date between ?2 and ?3"
				+ " group by b.billing_id order by b.billing_date desc", resultSetMapping = "Billing.mappingNative"),
		@NamedNativeQuery(name = Billing.FIND_UNPAID_NATIVE, query = "select b.*,"
				+ " sum(i.amount*(1+ifnull(i.vat,0)/100)) as billing_total"
				+ " from billing b inner join billing_item i on i.billing = b.billing_id"
				+ " where b.billing_type = ?1 and b.deleted_date is null and b.payment_date is null"
				+ " group by b.billing_id order by b.billing_date desc", resultSetMapping = "Billing.mappingNative"),
		@NamedNativeQuery(name = Billing.FIND_BY_CLIENT_NATIVE, query = "select b.*,"
				+ " sum(i.amount*(1+ifnull(i.vat,0)/100)) as billing_total" + " from series_documentation s"
				+ " inner join billing b on s.series_documentation_id = b.series_documentation"
				+ " inner join billing_item i on i.billing = b.billing_id"
				+ " where b.billing_type = ?1 and b.deleted_date is null and s.client = ?2"
				+ " group by b.billing_id order by b.billing_date desc", resultSetMapping = "Billing.mappingNative"),
		@NamedNativeQuery(name = Billing.FIND_BY_TRANSFER_NATIVE, query = "select billing_id from billing"
				+ " where billing_type = ?1 and deleted_date is null and payment_date = ?2 and payment_no = ?3") })
@NamedQueries({
		@NamedQuery(name = Billing.FIND_ALL, query = "select b from Billing b where b.billingType = :billingType"
				+ " order by b.billingDate desc"),
		@NamedQuery(name = Billing.FIND_BETWEEN, query = "select b from Billing b"
				+ " where b.billingType = :billingType and b.billingDate between :startDate and :endDate"
				+ " order by b.billingDate desc") })
@Table(name = "billing", indexes = { @Index(columnList = "billing_type, billing_date desc", unique = false),
		@Index(columnList = "billing_type, payment_date desc, payment_no desc", unique = false) }, uniqueConstraints = {
				@UniqueConstraint(columnNames = { "reference_sequence", "reference_year", "billing_type" }) })
@Entity
public class Billing extends AbstractEntity {

	private static final long serialVersionUID = -3609717935141690473L;

	public static final String FIND_ALL = "Billing.findAll";
	public static final String FIND_BETWEEN = "Billing.findBetween";

	public static final String FIND_BETWEEN_NATIVE = "Billing.findBetweenNative";
	public static final String FIND_UNPAID_NATIVE = "Billing.findUnpaidNative";
	public static final String FIND_BY_CLIENT_NATIVE = "Billing.findByClientNative";
	public static final String FIND_BY_TRANSFER_NATIVE = "Billing.findByTransferNative";
	public static final String FIND_BY_REFERENCE_NATIVE = "Billing.findByReferenceNative";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "billing_id")
	private Long id;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "billing_type")
	private BillingType billingType;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "billing_date")
	private Date billingDate;

	@Column(name = "billing_note")
	private String billingNote;

	@Column(name = "header_note")
	private String headerNote;

	@Column(name = "footer_note")
	private String footerNote;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "due_date")
	private Date dueDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "payment_method")
	private PaymentMethod paymentMethod;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "payment_no")
	private String paymentNo;

	@Size(max = 2000)
	@Column(name = "payment_note")
	private String paymentNote;

	@Column(name = "reference_year", nullable = false)
	private Integer referenceYear;

	@Column(name = "reference_sequence", nullable = false)
	private Integer referenceSequence;

	@Column(name = "ft_access_token")
	private String ftAccessToken;

	@Column(name = "ft_cashbox_id")
	private String ftCashboxId;

	@Column(name = "cb_terminal_id")
	private String cbTerminalId;

	@OneToOne(mappedBy = "billing")
	private BillingSignature billingSignature;

	@Size(min = 1)
	@OneToMany(mappedBy = "billing", cascade = { CascadeType.PERSIST })
	private List<BillingItem> billingItems;

	@ManyToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id")
	private SeriesDocumentation seriesDocumentation;

	public Billing() {
	}

	public static Billing getInstance() {
		return new Billing();
	}

	@PrePersist
	private void prePersist() {
		if (getBillingLdt() == null) {
			setBillingLdt(LocalDateTime.now());
		}
		if (getDueLdt() == null) {
			if (getBillingType() == BillingType.INVOICE) {
				setDueLdt(getBillingLdt().plusMonths(1));
			} else {
				setDueLdt(getBillingLdt());
			}
		}
		if (getPaymentMethod() == null) {
			if (getBillingType() == BillingType.INVOICE) {
				setPaymentMethod(PaymentMethod.BANK);
			} else {
				setPaymentMethod(PaymentMethod.CASH);
			}
		}
		if (getPaymentLdt() == null) {
			if (getBillingType() == BillingType.INVOICE) {
				setPaymentLdt(null);
			} else {
				setPaymentLdt(getBillingLdt());
			}
		}
		if (getReferenceYear() == null) {
			setReferenceYear(0);
		}
		if (getReferenceSequence() == null) {
			setReferenceSequence(0);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BillingType getBillingType() {
		return billingType;
	}

	public void setBillingType(BillingType billingType) {
		this.billingType = billingType;
		if (getBillingType() == null) {
			setPaymentMethod(null);
		} else if (getBillingType() == BillingType.INVOICE) {
			setPaymentMethod(PaymentMethod.BANK);
		} else {
			setPaymentMethod(PaymentMethod.CASH);
		}
	}

	public Billing withBillingType(BillingType billingType) {
		setBillingType(billingType);
		return this;
	}

	public Date getBillingDate() {
		return billingDate;
	}

	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}

	public Billing withBillingDate(Date billingDate) {
		setBillingDate(billingDate);
		return this;
	}

	public String getBillingNote() {
		return billingNote;
	}

	public void setBillingNote(String billingNote) {
		this.billingNote = billingNote;
	}

	public String getHeaderNote() {
		return headerNote;
	}

	public void setHeaderNote(String headerNote) {
		this.headerNote = headerNote;
	}

	public String getFooterNote() {
		return footerNote;
	}

	public void setFooterNote(String footerNote) {
		this.footerNote = footerNote;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Billing withPaymentMethod(PaymentMethod paymentMethod) {
		setPaymentMethod(paymentMethod);
		return this;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}

	public String getPaymentNote() {
		return paymentNote;
	}

	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}

	public Integer getReferenceYear() {
		return referenceYear;
	}

	public void setReferenceYear(Integer referenceYear) {
		this.referenceYear = referenceYear;
	}

	public Integer getReferenceSequence() {
		return referenceSequence;
	}

	public void setReferenceSequence(Integer referenceSequence) {
		this.referenceSequence = referenceSequence;
	}

	public BillingSignature getBillingSignature() {
		return billingSignature;
	}

	public String getFtAccessToken() {
		return ftAccessToken;
	}

	public void setFtAccessToken(String ftAccessToken) {
		this.ftAccessToken = ftAccessToken;
	}

	public String getFtCashboxId() {
		return ftCashboxId;
	}

	public void setFtCashboxId(String ftCashboxId) {
		this.ftCashboxId = ftCashboxId;
	}

	public String getCbTerminalId() {
		return cbTerminalId;
	}

	public void setCbTerminalId(String cbTerminalId) {
		this.cbTerminalId = cbTerminalId;
	}

	public void setBillingSignature(BillingSignature billingSignature) {
		this.billingSignature = billingSignature;
	}

	public List<BillingItem> getBillingItems() {
		if (billingItems == null) {
			billingItems = new ArrayList<BillingItem>();
		}
		return billingItems;
	}

	public void setBillingItems(List<BillingItem> billingItems) {
		this.billingItems = billingItems;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	public Billing withSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		setSeriesDocumentation(seriesDocumentation);
		return this;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof Billing && id != null) ? id.equals(((Billing) other).id) : (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{ id=" + id + ", billingType=" + billingType + ", billingDate="
				+ billingDate + ", dueDate=" + dueDate + ", paymentMethod=" + paymentMethod + ", paymentDate="
				+ paymentDate + ", paymentNo=" + paymentNo + ", referenceYear=" + referenceYear + ", referenceSequence="
				+ referenceSequence + ", seriesDocumentation=" + seriesDocumentation + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	public boolean isReceipt() {
		return getBillingType() == BillingType.RECEIPT;

	}

	public boolean isInvoice() {
		return getBillingType() == BillingType.INVOICE;
	}

	public String getReferenceNo() {
		if (getReferenceYear() == null) {
			return "";
		}
		if (getReferenceSequence() == null) {
			return "";
		}
		return getReferenceYear() + "-" + getReferenceSequence();
	}

	@Transient
	private BigDecimal total;

	public double getTotal() {
		if (total != null) {
			return total.doubleValue();
		}
		if (CollectionUtils.isEmpty(getBillingItems())) {
			return 0d;
		}
		return getBillingItems().stream().mapToDouble(billingItem -> billingItem.getTotal()).sum();
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Billing withTotal(BigDecimal total) {
		setTotal(total);
		return this;
	}

	public double getSumVat20() {
		if (CollectionUtils.isEmpty(getBillingItems())) {
			return 0d;
		}
		return getBillingItems().stream()
				.filter(billingItem -> billingItem.getVat() != null && billingItem.getVat().doubleValue() == 20d)
				.mapToDouble(billingItem -> billingItem.getTotal()).sum();
	}

	@Transient
	private BillingStatus billingStatus;

	public BillingStatus getBillingStatus() {
		if (isCanceled()) {
			return BillingStatus.CANCELED;
		} else if (isPaid()) {
			return BillingStatus.PAID;
		} else if (isOverdue()) {
			return BillingStatus.OVERDUE;
		}
		if (billingStatus != null) {
			return billingStatus;
		}
		return BillingStatus.OPEN;
	}

	public void setBillingStatus(BillingStatus billingStatus) {
		this.billingStatus = billingStatus;
	}

	public Billing withBillingStatus(BillingStatus billingStatus) {
		setBillingStatus(billingStatus);
		return this;
	}

	public boolean isCanceled() {
		return getDeletedDate() != null;
	}

	public boolean isPaid() {
		return getPaymentDate() != null;
	}

	public boolean isOverdue() {
		return getDueDate() != null && getDueDate().getTime() < System.currentTimeMillis();
	}

	public LocalDateTime getBillingLdt() {
		return DateUtils.asLdt(getBillingDate());
	}

	public void setBillingLdt(LocalDateTime billingLdt) {
		setBillingDate(DateUtils.asDate(billingLdt));
	}

	public LocalDateTime getDueLdt() {
		return DateUtils.asLdt(getDueDate());
	}

	public void setDueLdt(LocalDateTime dueLdt) {
		setDueDate(DateUtils.asDate(dueLdt));
	}

	public LocalDateTime getPaymentLdt() {
		return DateUtils.asLdt(getPaymentDate());
	}

	public void setPaymentLdt(LocalDateTime paymentLdt) {
		setPaymentDate(DateUtils.asDate(paymentLdt));
	}

	public void addBillingItem(BillingItem billingItem) {
		getBillingItems().add(billingItem);
	}

	/**
	 * 
	 * @param sequenceNo
	 * @param subject
	 * @param quantity
	 * @param amount
	 */
	public void addBillingItemValues(Integer sequenceNo, String subject, Integer quantity, BigDecimal amount) {
		addBillingItemValues(sequenceNo, subject, quantity, amount, null);
	}

	/**
	 * 
	 * @param sequenceNo
	 * @param subject
	 * @param quantity
	 * @param amount
	 * @param vat
	 */
	public void addBillingItemValues(Integer sequenceNo, String subject, Integer quantity, BigDecimal amount,
			BigDecimal vat) {
		BillingItem billingItem = BillingItem.getInstance();
		billingItem.setSequenceNo(sequenceNo);
		billingItem.setSubject(subject);
		billingItem.setQuantity(quantity);
		billingItem.setAmount(amount);
		billingItem.setVat(vat);
		addBillingItem(billingItem);
	}

	/**
	 * 
	 * @param seriesDocumentation
	 */
	public void setBillingItems(SeriesDocumentation seriesDocumentation) {
		if (seriesDocumentation == null) {
			return;
		}
		if (CollectionUtils.isEmpty(seriesDocumentation.getSessionDocumentations())) {
			return;
		}
		if (!CollectionUtils.isEmpty(getBillingItems())) {
			return;
		}
		List<BillingItem> billingItems = new ArrayList<BillingItem>();
		seriesDocumentation.getSessionDocumentations().stream().filter(
				sessionDocumentation -> !CollectionUtils.isEmpty(sessionDocumentation.getServiceDocumentations()))
				.forEach(sessionDocumentation -> {
					sessionDocumentation.getServiceDocumentations().stream()
							.filter(serviceDocumentation -> !serviceDocumentation.isBilled(getBillingType()))
							.forEach(serviceDocumentation -> {
								billingItems.add(0, BillingItem.getInstance(serviceDocumentation));
							});
				});

		getBillingItems().addAll(billingItems);
	}

	public Billing withBillingItems(SeriesDocumentation seriesDocumentation) {
		setBillingItems(seriesDocumentation);
		return this;
	}

	@Transient
	private BankTransfer bankTransfer;

	public BankTransfer getBankTransfer() {
		return bankTransfer;
	}

	public void setBankTransfer(BankTransfer bankTransfer) {
		this.bankTransfer = bankTransfer;
	}

}
