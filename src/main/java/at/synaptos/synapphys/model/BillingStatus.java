package at.synaptos.synapphys.model;

public enum BillingStatus {

	OPEN("green"), PAID("blue"), OVERDUE("red"), CANCELED("gray");

	private String flagStyleClass;

	private BillingStatus() {
	}

	private BillingStatus(String flagColor) {
		setFlagStyleClass(flagColor);
	}

	public String getFlagStyleClass() {
		return flagStyleClass;
	}

	private void setFlagStyleClass(String flagStyleClass) {
		this.flagStyleClass = flagStyleClass;
	}

}
