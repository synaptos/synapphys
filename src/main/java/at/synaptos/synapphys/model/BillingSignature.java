package at.synaptos.synapphys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Table(name = "billing_signature")
@Entity
public class BillingSignature implements Serializable {

	private static final long serialVersionUID = -4175788705522614334L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false)
	private Date createdDate;

	@Column(name = "created_user", nullable = false)
	private String createdUser;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "billing_signature_id")
	private Long id;

	@Column(name = "ft_cashbox_id")
	private String ftCashboxId;

	@Column(name = "cb_terminal_id")
	private String cbTerminalId;

	@Column(name = "ft_cashbox_name")
	private String ftCashboxName;

	@Column(name = "ft_receipt_id")
	private String ftReceiptId;

	@Column(name = "ft_receipt_moment")
	private Date ftReceiptMoment;

	@Column(name = "ft_state_flag")
	private Long ftStateFlag;

	@Column(name = "ft_state_no")
	private Integer ftStateNo;

	@Column(name = "ft_state_exception")
	private String ftStateException;

	@Column(name = "ft_state_signing")
	private Boolean ftStateSigning;

	@Column(name = "ft_state_counting")
	private Boolean ftStateCounting;

	@Column(name = "ft_state_zero_receipt")
	private Boolean ftStateZeroReceipt;

	@Column(name = "ft_receipt_signature", columnDefinition = "TEXT")
	private String ftReceiptSignature;

	@Column(name = "ft_receipt_response_log", columnDefinition = "TEXT")
	private String ftReceiptResponseLog;

	@NotNull
	@OneToOne
	@JoinColumn(name = "billing", referencedColumnName = "billing_id", nullable = false)
	private Billing billing;

	public BillingSignature() {
	}

	public static BillingSignature getInstance() {
		final BillingSignature instance = new BillingSignature();
		return instance;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFtCashboxId() {
		return ftCashboxId;
	}

	public void setFtCashboxId(String ftCashboxId) {
		this.ftCashboxId = ftCashboxId;
	}

	public String getCbTerminalId() {
		return cbTerminalId;
	}

	public void setCbTerminalId(String cbTerminalId) {
		this.cbTerminalId = cbTerminalId;
	}

	public String getFtCashboxName() {
		return ftCashboxName;
	}

	public void setFtCashboxName(String ftCashboxName) {
		this.ftCashboxName = ftCashboxName;
	}

	public String getFtReceiptId() {
		return ftReceiptId;
	}

	public void setFtReceiptId(String ftReceiptId) {
		this.ftReceiptId = ftReceiptId;
	}

	public Date getFtReceiptMoment() {
		return ftReceiptMoment;
	}

	public void setFtReceiptMoment(Date ftReceiptMoment) {
		this.ftReceiptMoment = ftReceiptMoment;
	}

	public Long getFtStateFlag() {
		return ftStateFlag;
	}

	public void setFtStateFlag(Long ftStateFlag) {
		this.ftStateFlag = ftStateFlag;
	}

	public Integer getFtStateNo() {
		return ftStateNo;
	}

	public void setFtStateNo(Integer ftStateNo) {
		this.ftStateNo = ftStateNo;
	}

	public String getFtStateException() {
		return ftStateException;
	}

	public void setFtStateException(String ftStateException) {
		this.ftStateException = ftStateException;
	}

	public Boolean getFtStateSigning() {
		return ftStateSigning;
	}

	public void setFtStateSigning(Boolean ftStateSigning) {
		this.ftStateSigning = ftStateSigning;
	}

	public Boolean getFtStateCounting() {
		return ftStateCounting;
	}

	public void setFtStateCounting(Boolean ftStateCounting) {
		this.ftStateCounting = ftStateCounting;
	}

	public Boolean getFtStateZeroReceipt() {
		return ftStateZeroReceipt;
	}

	public void setFtStateZeroReceipt(Boolean ftStateZeroReceipt) {
		this.ftStateZeroReceipt = ftStateZeroReceipt;
	}

	public String getFtReceiptSignature() {
		return ftReceiptSignature;
	}

	public void setFtReceiptSignature(String ftReceiptSignature) {
		this.ftReceiptSignature = ftReceiptSignature;
	}

	public String getFtReceiptResponseLog() {
		return ftReceiptResponseLog;
	}

	public void setFtReceiptResponseLog(String ftReceiptResponseLog) {
		this.ftReceiptResponseLog = ftReceiptResponseLog;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof BillingSignature && id != null) ? id.equals(((BillingSignature) other).id)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + "}";
	}

	public boolean isNew() {
		return id == null;
	}

}
