package at.synaptos.synapphys.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@NamedQueries({
		@NamedQuery(name = Physician.FIND_ALL, query = "select p from Physician p where p.deletedDate is null order by p.lastname") })
@Table(name = "physician", indexes = { @Index(columnList = "person_lastname, person_firstname", unique = false) })
@Entity
public class Physician extends AbstractPerson {

	private static final long serialVersionUID = 6199060322835032038L;

	public static final String FIND_ALL = "Physician.findAll";

	private String speciality;

	@Column(name = "office_hours")
	private String officeHours;

	public Physician() {
		setTitle("Dr.");
	}

	public static Physician getInstance() {
		return new Physician();
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getOfficeHours() {
		return officeHours;
	}

	public void setOfficeHours(String officeHours) {
		this.officeHours = officeHours;
	}

	@Override
	public int hashCode() {
		return (getId() != null) ? this.getClass().hashCode() + getId().hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof Physician && getId() != null) ? getId().equals(((Physician) other).getId())
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{ super=" + super.toString() + ", speciality=" + speciality + " }";
	}

	@Override
	public Physician withBackUrl(String backUrl) {
		return (Physician) super.withBackUrl(backUrl);
	}

	@Transient
	private PhysicianOwner physicianOwner;

	public PhysicianOwner getPhysicianOwner() {
		return physicianOwner;
	}

	public void setPhysicianOwner(PhysicianOwner physicianOwner) {
		this.physicianOwner = physicianOwner;
	}

}
