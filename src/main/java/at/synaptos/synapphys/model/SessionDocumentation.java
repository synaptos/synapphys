package at.synaptos.synapphys.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Where;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.StringUtils;

@NamedQueries({ @NamedQuery(name = SessionDocumentation.findBetween, query = "select d from SessionDocumentation d "
		+ " where d.startDate <= :endDate and d.endDate >= :startDate"
		+ " and d.deletedDate is null order by d.startDate") })
@Table(name = "session_documentation", indexes = { @Index(columnList = "session_start_date, session_end_date") })
@Entity
public class SessionDocumentation extends AbstractEntity {

	private static final long serialVersionUID = -5309893364491201477L;

	public static final String findBetween = "SessionDocumentation.findBetween";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "session_documentation_id")
	private Long id;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "session_start_date")
	private Date startDate;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "session_end_date")
	private Date endDate;

	@Column(name = "session_subject")
	private String sessionSubject;

	@Size(max = 2000)
	@Column(name = "session_note")
	private String sessionNote;

	@Size(max = 2000)
	@Column(name = "session_text")
	private String sessionText;

	@Column(name = "schedule_subject")
	private String scheduleSubject;

	@Column(name = "schedule_note")
	private String scheduleNote;

	@Column(name = "remind_by_sms")
	private Boolean remindBySms;

	@OneToMany(mappedBy = "sessionDocumentation", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE }, fetch = FetchType.EAGER)
	@Where(clause = "deleted_date is null")
	private List<ServiceDocumentation> serviceDocumentations;

	// TODO WQ: kommen weg!
	@OneToMany(mappedBy = "sessionDocumentation", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@Where(clause = "deleted_date is null")
	private List<KpiItem> kpiItems;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	public static SessionDocumentation getInstance(@NotNull final ScheduleItem scheduleItem,
			@NotNull final SeriesDocumentation seriesDocumentation) {
		final SessionDocumentation instance = new SessionDocumentation();
		instance.setStartLdt(scheduleItem.getStartLdt());
		instance.setEndLdt(scheduleItem.getEndLdt());
		instance.setScheduleSubject(scheduleItem.getSubject());
		instance.setScheduleNote(scheduleItem.getNote());
		instance.setRemindBySms(scheduleItem.isRemindBySms());
		instance.setSeriesDocumentation(seriesDocumentation);
		return instance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = Optional.ofNullable(startDate).orElseGet(() -> (Date) startDate.clone());
	}

	public LocalDateTime getStartLdt() {
		return DateUtils.asLdt(getStartDate());
	}

	public void setStartLdt(LocalDateTime startLdt) {
		setStartDate(DateUtils.asDate(startLdt));
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = Optional.ofNullable(endDate).orElseGet(() -> (Date) endDate.clone());
	}

	public LocalDateTime getEndLdt() {
		return DateUtils.asLdt(getEndDate());
	}

	public void setEndLdt(LocalDateTime endLdt) {
		setEndDate(DateUtils.asDate(endLdt));
	}

	public String getSessionNote() {
		return sessionNote;
	}

	public void setSessionNote(String sessionNote) {
		this.sessionNote = sessionNote;
	}

	public String getSessionText() {
		return sessionText;
	}

	public void setSessionText(String sessionText) {
		this.sessionText = sessionText;
	}

	public String getScheduleSubject() {
		return scheduleSubject;
	}

	public void setScheduleSubject(String scheduleSubject) {
		this.scheduleSubject = scheduleSubject;
	}

	public String getScheduleNote() {
		return scheduleNote;
	}

	public void setScheduleNote(String scheduleNote) {
		this.scheduleNote = scheduleNote;
	}

	public Boolean getRemindBySms() {
		return remindBySms;
	}

	public Boolean isRemindBySms() {
		return getRemindBySms();
	}

	public void setRemindBySms(Boolean remindBySms) {
		this.remindBySms = remindBySms;
	}

	public List<ServiceDocumentation> getServiceDocumentations() {
		if (serviceDocumentations == null) {
			serviceDocumentations = new ArrayList<ServiceDocumentation>();
		}
		return serviceDocumentations;
	}

	public void setServiceDocumentations(List<ServiceDocumentation> serviceDocumentations) {
		this.serviceDocumentations = serviceDocumentations;
	}

	public SessionDocumentation withServiceDocumentations(List<ServiceDocumentation> serviceDocumentations) {
		setServiceDocumentations(serviceDocumentations);
		return this;
	}

	public List<KpiItem> getKpiItems() {
		if (kpiItems == null) {
			kpiItems = new ArrayList<KpiItem>();
		}
		return kpiItems;
	}

	public void setKpiItems(List<KpiItem> kpiItems) {
		this.kpiItems = kpiItems;
	}

	public SessionDocumentation withKpiItems(List<KpiItem> kpiItems) {
		setKpiItems(kpiItems);
		return this;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof SessionDocumentation && id != null) ? id.equals(((SessionDocumentation) other).id)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", seriesDocumentation=" + seriesDocumentation + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	@Override
	public SessionDocumentation withBackUrl(String backUrl) {
		setBackUrl(backUrl);
		return this;
	}

	public LocalDate getStartLd() {
		return DateUtils.asLd(getStartDate());
	}

	public LocalDate getEndLd() {
		return DateUtils.asLd(getEndDate());
	}

	public String getPrimaryServiceDocumentations() {
		String services = "";
		for (ServiceDocumentation serviceDocumentation : getServiceDocumentations()) {
			if (serviceDocumentation.getServiceType().getCategory() == ServiceTypeCategory.C10_THERAPY) {
				if (!StringUtils.isEmpty(services)) {
					services += ", ";
				}
				services += serviceDocumentation.getServiceType().getCode();
			}
		}
		return services;
	}

	public String getPlainSessionText() {
		if (StringUtils.isEmpty(getSessionText())) {
			return "";
		}
		Document doc = Jsoup.parse(getSessionText());
		String text = doc.body().text();
		if (text.length() > 100) {
			text = text.substring(0, 100) + "...";
		}
		return text;

	}

}
