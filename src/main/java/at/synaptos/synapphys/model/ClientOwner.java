package at.synaptos.synapphys.model;

public interface ClientOwner {

	public Client getClient();

	public void setClient(Client client);

}
