package at.synaptos.synapphys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

@Embeddable
public class BankAccount implements Serializable {

	private static final long serialVersionUID = 6707201817525854962L;

	@Pattern(regexp = "[A-Z]{2}[0-9]{2}[0-9a-zA-Z ]{16,37}", message = "{bankAccount.iban.INVALID}")
	@Column(name = "bank_account_iban")
	private String iban;

	@Column(name = "bank_account_bic")
	private String bic;

	@Column(name = "bank_account_bankname")
	private String bankname;

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

}
