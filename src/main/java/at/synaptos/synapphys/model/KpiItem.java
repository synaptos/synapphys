package at.synaptos.synapphys.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@NamedQueries({
		@NamedQuery(name = KpiItem.FIND_ALL, query = "select kpi from KpiItem kpi where kpi.deletedDate is null order by kpi.id") })
@Table(name = "kpi_item")
@Entity
public class KpiItem extends AbstractEntity {

	private static final long serialVersionUID = 1627564548704990837L;

	public static final String FIND_ALL = "KeyPerformanceIndicator.findAll";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "kpi_item_id")
	private Long id;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "kpi_type")
	private KpiType kpiType;

	@NotNull
	@Min(value = 0, message = "kpiValue min value is 0")
	@Max(value = 10, message = "kpiValue max value is 10")
	@Column(name = "kpi_value")
	private Integer kpiValue;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "session_documentation", referencedColumnName = "session_documentation_id", nullable = false)
	private SessionDocumentation sessionDocumentation;

	public KpiItem() {
		setKpiValue(0);
	}

	public static KpiItem getInstance(KpiType kpiType) {
		KpiItem instance = new KpiItem();
		instance.setKpiType(kpiType);
		return instance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public KpiType getKpiType() {
		return kpiType;
	}

	public void setKpiType(KpiType kpiType) {
		this.kpiType = kpiType;
	}

	public Integer getKpiValue() {
		return kpiValue;
	}

	public void setKpiValue(Integer kpiValue) {
		this.kpiValue = kpiValue;
	}

	public KpiItem withKpiValue(Integer kpiValue) {
		setKpiValue(kpiValue);
		return this;
	}

	public SessionDocumentation getSessionDocumentation() {
		return sessionDocumentation;
	}

	public void setSessionDocumentation(SessionDocumentation sessionDocumentation) {
		this.sessionDocumentation = sessionDocumentation;
	}

	public KpiItem withSessionDocumentation(SessionDocumentation sessionDocumentation) {
		setSessionDocumentation(sessionDocumentation);
		return this;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", kpiType=" + kpiType + ", kpiValue=" + kpiValue
				+ ", sessionDocumentation=" + sessionDocumentation + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

}
