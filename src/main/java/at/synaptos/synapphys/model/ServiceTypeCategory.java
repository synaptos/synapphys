package at.synaptos.synapphys.model;

import java.math.BigDecimal;

public enum ServiceTypeCategory {
	C10_THERAPY, C20_ADMIN, C30_PRODUCT(BigDecimal.valueOf(20)), C50_FEE();

	private BigDecimal vat;

	private ServiceTypeCategory() {
	}

	private ServiceTypeCategory(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getVat() {
		return vat;
	}

}
