package at.synaptos.synapphys.model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import at.synaptos.synapphys.utils.DateUtils;

@NamedQueries({
		@NamedQuery(name = ScheduleItem.FIND_ALL, query = "select i from ScheduleItem i where i.deletedDate is null"),
		@NamedQuery(name = ScheduleItem.FIND_BETWEEN, query = "select i from ScheduleItem i where i.startDate <= :endDate and i.endDate >= :startDate and"
				+ " i.deletedDate is null order by i.startDate") })
@Table(name = "schedule_item", indexes = { @Index(columnList = "schedule_start_date, schedule_end_date") })
@Entity
public class ScheduleItem extends AbstractEntity {

	private static final long serialVersionUID = -1700451807673251684L;

	public static final String FIND_ALL = "ScheduleItem.findAll";
	public static final String FIND_BETWEEN = "ScheduleItem.findBetween";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "schedule_item_id")
	private Long id;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "schedule_start_date")
	private Date startDate;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "schedule_end_date")
	private Date endDate;

	@Column(name = "schedule_subject")
	private String subject;

	@Column(name = "schedule_note")
	private String note;

	@Column(name = "phone")
	private String phone;

	@Column(name = "remind_by_sms")
	private Boolean remindBySms;

	@Column(name = "confirmed_date")
	private Date confirmedDate;

	@ManyToOne
	@JoinColumn(name = "client", referencedColumnName = "person_id")
	private Client client;

	public ScheduleItem() {
		setRemindBySms(false);
	}

	public static ScheduleItem getInstance() {
		return new ScheduleItem();
	}

	public static ScheduleItem getInstance(@NotNull final Date startDate, @NotNull final Date endDate) {
		return getInstance().withStartDate(startDate).withEndDate(endDate);
	}

	public static ScheduleItem getInstance(@NotNull final LocalDateTime startLdt, @NotNull final LocalDateTime endLdt) {
		return getInstance().withStartLdt(startLdt).withEndLdt(endLdt);
	}

	@PrePersist
	public void prePersist() {
		if (getRemindBySms() == null) {
			setRemindBySms(false);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = Optional.ofNullable(startDate).orElseGet(() -> (Date) startDate.clone());
	}

	public ScheduleItem withStartDate(Date startDate) {
		setStartDate(startDate);
		return this;
	}

	public LocalDateTime getStartLdt() {
		return DateUtils.asLdt(getStartDate());
	}

	public void setStartLdt(LocalDateTime startLdt) {
		setStartDate(DateUtils.asDate(startLdt));
	}

	public ScheduleItem withStartLdt(LocalDateTime startLdt) {
		setStartDate(DateUtils.asDate(startLdt));
		return this;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = Optional.ofNullable(endDate).orElseGet(() -> (Date) endDate.clone());
	}

	public ScheduleItem withEndDate(Date endDate) {
		setEndDate(endDate);
		return this;
	}

	public LocalDateTime getEndLdt() {
		return DateUtils.asLdt(getEndDate());
	}

	public void setEndLdt(LocalDateTime endLdt) {
		setEndDate(DateUtils.asDate(endLdt));
	}

	public ScheduleItem withEndLdt(LocalDateTime endLdt) {
		setEndDate(DateUtils.asDate(endLdt));
		return this;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getRemindBySms() {
		return remindBySms;
	}

	public Boolean isRemindBySms() {
		return getRemindBySms();
	}

	public void setRemindBySms(Boolean remindBySms) {
		this.remindBySms = remindBySms;
	}

	public ScheduleItem withRemindBySms(Boolean remindBySms) {
		setRemindBySms(remindBySms);
		return this;
	}

	public Date getConfirmedDate() {
		return confirmedDate;
	}

	public void setConfirmedDate(Date confirmedDate) {
		this.confirmedDate = confirmedDate;
	}

	public ScheduleItem withConfirmedDate(Date confirmedDate) {
		setConfirmedDate(confirmedDate);
		return this;
	}

	public LocalDateTime getConfirmedLdt() {
		return DateUtils.asLdt(getConfirmedDate());
	}

	public void setConfirmedLdt(LocalDateTime confirmedLdt) {
		setConfirmedDate(DateUtils.asDate(confirmedLdt));
	}

	public ScheduleItem withConfirmedLdt(LocalDateTime confirmedLdt) {
		setConfirmedLdt(confirmedLdt);
		return this;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public ScheduleItem withClient(Client client) {
		setClient(client);
		return this;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof ScheduleItem && id != null) ? id.equals(((ScheduleItem) other).id) : (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ",startDate=" + startDate + ",endDate=" + endDate + ",subject="
				+ subject + ",note=" + note + ",phone=" + phone + ",note=" + note + ",remindBySms=" + remindBySms
				+ ",confirmedDate=" + confirmedDate + ",client=" + client + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	@Override
	public boolean isDeleted() {
		return super.isDeleted() && getConfirmedDate() != null;
	}

}
