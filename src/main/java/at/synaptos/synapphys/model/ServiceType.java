package at.synaptos.synapphys.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries({
		@NamedQuery(name = ServiceType.FIND_ALL, query = "select s from ServiceType s where s.deletedDate is null order by s.category, s.name") })
@Table(name = "service_type")
@Entity
public class ServiceType extends AbstractEntity {

	private static final long serialVersionUID = -7881139496448175642L;

	public static final String FIND_ALL = "ServiceType.findByAll";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "service_type_id")
	private Long id;

	@NotNull
	@Column(name = "service_type_name")
	private String name;

	@NotNull
	@Enumerated(EnumType.STRING)
	private ServiceTypeCategory category;

	@NotNull
	@Column(name = "amount")
	private BigDecimal amount;

	@NotNull
	@Column(name = "service_type_code")
	private String code;

	public ServiceType() {
		setCategory(ServiceTypeCategory.C10_THERAPY);
		setAmount(BigDecimal.ZERO);
	}

	public static ServiceType getInstance() {
		return new ServiceType();
	}

	@PrePersist
	public void prePersist() {
		if (getAmount() == null) {
			setAmount(BigDecimal.ZERO);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ServiceTypeCategory getCategory() {
		return category;
	}

	public void setCategory(ServiceTypeCategory category) {
		this.category = category;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof ServiceType && id != null) ? id.equals(((ServiceType) other).id) : (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", name=" + name + ", category=" + category + ", amount="
				+ amount + ", code=" + code + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	@Override
	public ServiceType withBackUrl(String backUrl) {
		setBackUrl(backUrl);
		return this;
	}

}
