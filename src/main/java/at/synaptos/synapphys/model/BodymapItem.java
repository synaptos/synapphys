package at.synaptos.synapphys.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NamedNativeQueries({
		@NamedNativeQuery(name = BodymapItem.NEXT_SEQUENCE_NO_NATIVE, query = "select max(sequence_no) as next_sequence_no"
				+ " from bodymap_item i where i.deleted_date is null and i.series_documentation = ?1") })
@Table(name = "bodymap_item")
@Entity
public class BodymapItem extends AbstractEntity {

	private static final long serialVersionUID = 7271521989529851995L;

	public static final String NEXT_SEQUENCE_NO_NATIVE = "BodymapItem.nextSequenceNoNative";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bodymap_item_id")
	private Long id;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "finding_type")
	private FindingType findingType;

	// TODO WQ
	// @Enumerated(EnumType.STRING)
	// @Column(name = "finding_type_modifier")
	@Transient
	//
	private FindingType findingTypeModifier;

	@NotNull
	@Column(name = "finding_position_x")
	private Double findingPositionX;

	@NotNull
	@Column(name = "finding_position_y")
	private Double findingPositionY;

	// TODO WQ
	// @Column(name = "body_segment")
	@Transient
	private String bodySegment;

	@Column(name = "value_from")
	private Integer valueF;

	@Column(name = "value_to")
	private Integer valueT;

	@Size(max = 2000)
	@Column(name = "finding_note")
	private String findingNote;

	@Size(max = 2000)
	@Column(name = "finding_text")
	private String findingText;

	@Column(name = "tip_position_x")
	private Double tipPositionX;

	@Column(name = "tip_position_y")
	private Double tipPositionY;

	@NotNull
	@Column(name = "sequence_no")
	private Integer sequenceNo;

	@ManyToOne
	@JoinColumn(name = "parent_item", referencedColumnName = "bodymap_item_id")
	private BodymapItem parentItem;

	@ManyToOne
	@JoinColumn(name = "session_documentation", referencedColumnName = "session_documentation_id")
	private SessionDocumentation sessionDocumentation;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	@PrePersist
	public void prePersist() {
		if (getSequenceNo() == null) {
			setSequenceNo(0);
		}
		if (getSessionDocumentation() != null) {
			setSeriesDocumentation(getSessionDocumentation().getSeriesDocumentation());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FindingType getFindingType() {
		return findingType;
	}

	public void setFindingType(FindingType findingType) {
		this.findingType = findingType;
	}

	public FindingType getFindingTypeModifier() {
		return findingTypeModifier;
	}

	public void setFindingTypeModifier(FindingType findingTypeModifier) {
		this.findingTypeModifier = findingTypeModifier;
	}

	public Double getFindingPositionX() {
		return findingPositionX;
	}

	public void setFindingPositionX(Double findingPositionX) {
		this.findingPositionX = findingPositionX;
	}

	public Double getFindingPositionY() {
		return findingPositionY;
	}

	public void setFindingPositionY(Double findingPositionY) {
		this.findingPositionY = findingPositionY;
	}

	public String getBodySegment() {
		return bodySegment;
	}

	public void setBodySegment(String bodySegment) {
		this.bodySegment = bodySegment;
	}

	public Integer getValueF() {
		return valueF;
	}

	public void setValueF(Integer valueF) {
		this.valueF = valueF;
	}

	public Integer getValueT() {
		return valueT;
	}

	public void setValueT(Integer valueT) {
		this.valueT = valueT;
	}

	public String getFindingNote() {
		return findingNote;
	}

	public void setFindingNote(String findingNote) {
		this.findingNote = findingNote;
	}

	public String getFindingText() {
		return findingText;
	}

	public void setFindingText(String findingText) {
		this.findingText = findingText;
	}

	public Double getTipPositionX() {
		return tipPositionX;
	}

	public void setTipPositionX(Double tipPositionX) {
		this.tipPositionX = tipPositionX;
	}

	public Double getTipPositionY() {
		return tipPositionY;
	}

	public void setTipPositionY(Double tipPositionX) {
		this.tipPositionY = tipPositionX;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public BodymapItem getParentItem() {
		return parentItem;
	}

	public void setParentItem(BodymapItem parentItem) {
		this.parentItem = parentItem;
	}

	public SessionDocumentation getSessionDocumentation() {
		return sessionDocumentation;
	}

	public void setSessionDocumentation(SessionDocumentation sessionDocumentation) {
		this.sessionDocumentation = sessionDocumentation;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", findingType=" + findingType + ", valueF=" + valueF
				+ ", valueT=" + valueT + ", findingText=" + findingText + ", seriesDocumentation=" + seriesDocumentation
				+ "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

}
