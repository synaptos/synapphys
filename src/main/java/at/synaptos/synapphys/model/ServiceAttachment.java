package at.synaptos.synapphys.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Table(name = "service_attachment")
@Entity
public class ServiceAttachment extends AbstractEntity {

	private static final long serialVersionUID = -7216180410795598505L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "service_attachment_id")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "service_attachment_category")
	private ServiceAttachmentCategory category;

	private String subject;

	@Size(max = 2000)
	@Column(name = "content_note")
	private String contentNote;

	@NotNull
	@Column(name = "content_type")
	private String contentType;

	@NotNull
	@Column(name = "source_filename")
	private String sourceFilename;

	@NotNull
	@Column(name = "target_filename")
	private String targetFilename;

	@ManyToOne
	@JoinColumn(name = "bodymap_item", referencedColumnName = "bodymap_item_id")
	private BodymapItem bodymapItem;

	@ManyToOne
	@JoinColumn(name = "session_documentation", referencedColumnName = "session_documentation_id")
	private SessionDocumentation sessionDocumentation;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	public static ServiceAttachment getInstance() {
		return new ServiceAttachment();
	}

	@PrePersist
	public void prePersist() {
		if (getBodymapItem() != null) {
			setSessionDocumentation(getBodymapItem().getSessionDocumentation());
			setSeriesDocumentation(getBodymapItem().getSeriesDocumentation());
		} else if (getSessionDocumentation() != null) {
			setSeriesDocumentation(getSessionDocumentation().getSeriesDocumentation());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServiceAttachmentCategory getCategory() {
		return category;
	}

	public void setCategory(ServiceAttachmentCategory category) {
		this.category = category;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContentNote() {
		return contentNote;
	}

	public void setContentNote(String contentNote) {
		this.contentNote = contentNote;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getSourceFilename() {
		return sourceFilename;
	}

	public void setSourceFilename(String sourceFilename) {
		this.sourceFilename = sourceFilename;
	}

	public String getTargetFilename() {
		return targetFilename;
	}

	public void setTargetFilename(String targetFilename) {
		this.targetFilename = targetFilename;
	}

	public BodymapItem getBodymapItem() {
		return bodymapItem;
	}

	public void setBodymapItem(BodymapItem bodymapItem) {
		this.bodymapItem = bodymapItem;
	}

	public SessionDocumentation getSessionDocumentation() {
		return sessionDocumentation;
	}

	public void setSessionDocumentation(SessionDocumentation sessionDocumentation) {
		this.sessionDocumentation = sessionDocumentation;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ",category=" + category + ",subject=" + subject
				+ ",contentNote=" + contentNote + ",contentType=" + contentType + ",sourceFilename=" + sourceFilename
				+ ",targetFilename=" + targetFilename + ", seriesDocumentation=" + seriesDocumentation + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

}
