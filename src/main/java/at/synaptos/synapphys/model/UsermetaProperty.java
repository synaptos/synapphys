package at.synaptos.synapphys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@NamedNativeQueries({
		@NamedNativeQuery(
				name = UsermetaProperty.FIND_BY_USERNAME,
				query = "select p.* from wp_usermeta p inner join wp_users u on p.user_id = u.ID where u.user_login = ?1", 
				resultClass = UsermetaProperty.class
		)
})
@Table(name = "wp_usermeta")
@Entity
public class UsermetaProperty implements Serializable {
	
	private static final long serialVersionUID = -2671568830323156959L;

	public static final String FIND_BY_USERNAME = "UsermetaProperty.findByUsername";
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "umeta_id")
    private Long id;

    @NotNull
    @Column(name = "meta_key")
    private String key;

    @Column(name = "meta_value")
    private String value;
        
    @NotNull
    @Column(name = "user_id")
    private Long userId;
    
	public UsermetaProperty() {
		super();
	}
	
	public UsermetaProperty(String key, String value) {
		this();
		setKey(key);
		setValue(value);
	}
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
    
    @Override
    public int hashCode() {
        return (id != null)
            ? this.getClass().hashCode()+id.hashCode()
            : super.hashCode();
    }        
	
    @Override
    public boolean equals(Object other) {
        return (other instanceof UsermetaProperty && id != null)
            ? id.equals(((UsermetaProperty)other).id)
            : (other == this);
    }
    
    @Override
    public String toString() {
	    return this.getClass().getName()+
	    		"{id="+id+
	    		",key="+key+
	    		",value="+value+
	    		",userId="+userId+"}";
    }
    	
	public boolean isNew() {
		return key == null;
	}
    
    @Transient
    private boolean changed = false;
    
    public boolean isChanged() {
    	return changed;
    }

    public void setChanged(boolean changed) {
    	this.changed = changed;
    }
	
}
