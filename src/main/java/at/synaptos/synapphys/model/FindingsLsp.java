package at.synaptos.synapphys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Table(name = "findings_lsp")
@Entity
public class FindingsLsp implements Serializable {

	private static final long serialVersionUID = -7343445522116091518L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false)
	private Date createdDate;

	@Column(name = "created_user", nullable = false)
	private String createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", nullable = false)
	private Date updatedDate;

	@Column(name = "updated_user", nullable = false)
	private String updatedUser;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "findings_lsp_id")
	private Long id;

	@Column(name = "active_test_extension")
	private Integer activeTestExtension = 0;

	@Column(name = "active_test_flexion")
	private Integer activeTestflexion = 0;

	@Column(name = "active_test_flex_hws_flexion")
	private Integer activeTestFlexHwsFlexion = 0;

	@Column(name = "active_test_flex_latara_right")
	private Integer activeTestFlexLataralRight = 0;

	@Column(name = "active_test_flex_latara_left")
	private Integer activeTestFlexLataralLeft = 0;

	@Column(name = "active_test_toe_stand_right")
	private Integer activeTestToeStandRight = 0;

	@Column(name = "active_test_toe_stand_left")
	private Integer activeTestToeStandLeft = 0;

	@Column(name = "active_test_heel_walk_right")
	private Integer activeTestHeelWalkRight = 0;

	@Column(name = "active_test_heel_walk_left")
	private Integer activeTestHeelWalkLeft = 0;

	@Column(name = "passiv_test_hip_flexion_right")
	private Integer passivTestHipFlexionRight = 0;

	@Column(name = "passiv_test_hip_flexion_left")
	private Integer passivTestHipFlexionLeft = 0;

	@Column(name = "passiv_test_hip_ir_right")
	private Integer passivTestHipIrRight = 0;

	@Column(name = "passiv_test_hip_ir_left")
	private Integer passivTestHipIrLeft = 0;

	@Column(name = "passiv_test_hip_ar_right")
	private Integer passivTestHipArRight = 0;

	@Column(name = "passiv_test_hip_ar_left")
	private Integer passivTestHipArLeft = 0;

	@Column(name = "passiv_test_sig_i_lia_dorsal_right")
	private Integer passivTestHipSigILiaDorsalRight = 0;

	@Column(name = "passiv_test_sig_i_lia_dorsal_left")
	private Integer passivTestHipSigILiaDorsalLeft = 0;

	@Column(name = "passiv_test_straight_leg_raise_right")
	private Integer passivTestStraightLegRaiseRight = 0;

	@Column(name = "passiv_test_straight_leg_raise_left")
	private Integer passivTestStraightLegRaiseLeft = 0;

	@Column(name = "resistance_test_m_psoas_right")
	private Boolean resistanceTestM_PsoasRight;

	@Column(name = "resistance_test_m_psoas_left")
	private Boolean resistanceTestM_PsoasLeft;

	@Column(name = "resistance_test_m_tibialis_anterior_right")
	private Boolean resistanceTestMT_ibialisAnteriorRight;

	@Column(name = "resistance_test_m_tibialis_anterior_left")
	private Boolean resistanceTestMT_ibialisAnteriorLeft;

	@Column(name = "resistance_test_m_ehl_right")
	private Boolean resistanceTestM_EhlRight;

	@Column(name = "resistance_test_m_ehl_left")
	private Boolean resistanceTestM_EhlLeft;

	@Column(name = "resistance_test_mm_peronei_right")
	private Boolean resistanceTestMmPeroneiRight;

	@Column(name = "resistance_test_mm_peronei_left")
	private Boolean resistanceTestMmPeroneiLeft;

	@NotNull
	@OneToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	public FindingsLsp() {
	}

	public static FindingsLsp getInstance() {
		return new FindingsLsp();
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getActiveTestExtension() {
		return activeTestExtension;
	}

	public void setActiveTestExtension(Integer activeTestExtension) {
		this.activeTestExtension = activeTestExtension;
	}

	public Integer getActiveTestflexion() {
		return activeTestflexion;
	}

	public void setActiveTestflexion(Integer activeTestflexion) {
		this.activeTestflexion = activeTestflexion;
	}

	public Integer getActiveTestFlexHwsFlexion() {
		return activeTestFlexHwsFlexion;
	}

	public void setActiveTestFlexHwsFlexion(Integer activeTestFlexHwsFlexion) {
		this.activeTestFlexHwsFlexion = activeTestFlexHwsFlexion;
	}

	public Integer getActiveTestFlexLataralRight() {
		return activeTestFlexLataralRight;
	}

	public void setActiveTestFlexLataralRight(Integer activeTestFlexLataralRight) {
		this.activeTestFlexLataralRight = activeTestFlexLataralRight;
	}

	public Integer getActiveTestFlexLataralLeft() {
		return activeTestFlexLataralLeft;
	}

	public void setActiveTestFlexLataralLeft(Integer activeTestFlexLataralLeft) {
		this.activeTestFlexLataralLeft = activeTestFlexLataralLeft;
	}

	public Integer getActiveTestToeStandRight() {
		return activeTestToeStandRight;
	}

	public void setActiveTestToeStandRight(Integer activeTestToeStandRight) {
		this.activeTestToeStandRight = activeTestToeStandRight;
	}

	public Integer getActiveTestToeStandLeft() {
		return activeTestToeStandLeft;
	}

	public void setActiveTestToeStandLeft(Integer activeTestToeStandLeft) {
		this.activeTestToeStandLeft = activeTestToeStandLeft;
	}

	public Integer getActiveTestHeelWalkRight() {
		return activeTestHeelWalkRight;
	}

	public void setActiveTestHeelWalkRight(Integer activeTestHeelWalkRight) {
		this.activeTestHeelWalkRight = activeTestHeelWalkRight;
	}

	public Integer getActiveTestHeelWalkLeft() {
		return activeTestHeelWalkLeft;
	}

	public void setActiveTestHeelWalkLeft(Integer activeTestHeelWalkLeft) {
		this.activeTestHeelWalkLeft = activeTestHeelWalkLeft;
	}

	public Integer getPassivTestHipFlexionRight() {
		return passivTestHipFlexionRight;
	}

	public void setPassivTestHipFlexionRight(Integer passivTestHipFlexionRight) {
		this.passivTestHipFlexionRight = passivTestHipFlexionRight;
	}

	public Integer getPassivTestHipFlexionLeft() {
		return passivTestHipFlexionLeft;
	}

	public void setPassivTestHipFlexionLeft(Integer passivTestHipFlexionLeft) {
		this.passivTestHipFlexionLeft = passivTestHipFlexionLeft;
	}

	public Integer getPassivTestHipIrRight() {
		return passivTestHipIrRight;
	}

	public void setPassivTestHipIrRight(Integer passivTestHipIrRight) {
		this.passivTestHipIrRight = passivTestHipIrRight;
	}

	public Integer getPassivTestHipIrLeft() {
		return passivTestHipIrLeft;
	}

	public void setPassivTestHipIrLeft(Integer passivTestHipIrLeft) {
		this.passivTestHipIrLeft = passivTestHipIrLeft;
	}

	public Integer getPassivTestHipArRight() {
		return passivTestHipArRight;
	}

	public void setPassivTestHipArRight(Integer passivTestHipArRight) {
		this.passivTestHipArRight = passivTestHipArRight;
	}

	public Integer getPassivTestHipArLeft() {
		return passivTestHipArLeft;
	}

	public void setPassivTestHipArLeft(Integer passivTestHipArLeft) {
		this.passivTestHipArLeft = passivTestHipArLeft;
	}

	public Integer getPassivTestHipSigILiaDorsalRight() {
		return passivTestHipSigILiaDorsalRight;
	}

	public void setPassivTestHipSigILiaDorsalRight(Integer passivTestHipSigILiaDorsalRight) {
		this.passivTestHipSigILiaDorsalRight = passivTestHipSigILiaDorsalRight;
	}

	public Integer getPassivTestHipSigILiaDorsalLeft() {
		return passivTestHipSigILiaDorsalLeft;
	}

	public void setPassivTestHipSigILiaDorsalLeft(Integer passivTestHipSigILiaDorsalLeft) {
		this.passivTestHipSigILiaDorsalLeft = passivTestHipSigILiaDorsalLeft;
	}

	public Integer getPassivTestStraightLegRaiseRight() {
		return passivTestStraightLegRaiseRight;
	}

	public void setPassivTestStraightLegRaiseRight(Integer passivTestStraightLegRaiseRight) {
		this.passivTestStraightLegRaiseRight = passivTestStraightLegRaiseRight;
	}

	public Integer getPassivTestStraightLegRaiseLeft() {
		return passivTestStraightLegRaiseLeft;
	}

	public void setPassivTestStraightLegRaiseLeft(Integer passivTestStraightLegRaiseLeft) {
		this.passivTestStraightLegRaiseLeft = passivTestStraightLegRaiseLeft;
	}

	public Boolean getResistanceTestM_PsoasRight() {
		return resistanceTestM_PsoasRight;
	}

	public void setResistanceTestM_PsoasRight(Boolean resistanceTestM_PsoasRight) {
		this.resistanceTestM_PsoasRight = resistanceTestM_PsoasRight;
	}

	public Boolean getResistanceTestM_PsoasLeft() {
		return resistanceTestM_PsoasLeft;
	}

	public void setResistanceTestM_PsoasLeft(Boolean resistanceTestM_PsoasLeft) {
		this.resistanceTestM_PsoasLeft = resistanceTestM_PsoasLeft;
	}

	public Boolean getResistanceTestMT_ibialisAnteriorRight() {
		return resistanceTestMT_ibialisAnteriorRight;
	}

	public void setResistanceTestMT_ibialisAnteriorRight(Boolean resistanceTestMT_ibialisAnteriorRight) {
		this.resistanceTestMT_ibialisAnteriorRight = resistanceTestMT_ibialisAnteriorRight;
	}

	public Boolean getResistanceTestMT_ibialisAnteriorLeft() {
		return resistanceTestMT_ibialisAnteriorLeft;
	}

	public void setResistanceTestMT_ibialisAnteriorLeft(Boolean resistanceTestMT_ibialisAnteriorLeft) {
		this.resistanceTestMT_ibialisAnteriorLeft = resistanceTestMT_ibialisAnteriorLeft;
	}

	public Boolean getResistanceTestM_EhlRight() {
		return resistanceTestM_EhlRight;
	}

	public void setResistanceTestM_EhlRight(Boolean resistanceTestM_EhlRight) {
		this.resistanceTestM_EhlRight = resistanceTestM_EhlRight;
	}

	public Boolean getResistanceTestM_EhlLeft() {
		return resistanceTestM_EhlLeft;
	}

	public void setResistanceTestM_EhlLeft(Boolean resistanceTestM_EhlLeft) {
		this.resistanceTestM_EhlLeft = resistanceTestM_EhlLeft;
	}

	public Boolean getResistanceTestMmPeroneiRight() {
		return resistanceTestMmPeroneiRight;
	}

	public void setResistanceTestMmPeroneiRight(Boolean resistanceTestMmPeroneiRight) {
		this.resistanceTestMmPeroneiRight = resistanceTestMmPeroneiRight;
	}

	public Boolean getResistanceTestMmPeroneiLeft() {
		return resistanceTestMmPeroneiLeft;
	}

	public void setResistanceTestMmPeroneiLeft(Boolean resistanceTestMmPeroneiLeft) {
		this.resistanceTestMmPeroneiLeft = resistanceTestMmPeroneiLeft;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof FindingsLsp && id != null) ? id.equals(((FindingsLsp) other).id) : (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + "}";
	}

}
