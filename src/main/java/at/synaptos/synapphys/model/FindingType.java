package at.synaptos.synapphys.model;

import java.util.ArrayList;
import java.util.List;

public enum FindingType {

	// Bone
	/*
	 * BONE_FRACTURE, BONE_IMPLANT,
	 * BONE_MOBILITY_X("bodymapItem.valueF.label.default", 0, -180, 180, 5,
	 * "bodymapItem.valueT.label.default", 0, -180, 180, 5),
	 * BONE_MOBILITY_Y("bodymapItem.valueF.label.default", 0, -180, 180, 5,
	 * "bodymapItem.valueT.label.default", 0, -180, 180, 5),
	 * BONE_MOBILITY_Z("bodymapItem.valueF.label.default", 0, -180, 180, 5,
	 * "bodymapItem.valueT.label.default", 0, -180, 180, 5), BONE_OSTEOPOROSIS,
	 * 
	 * // Muscle MUSCLE_ANAEMIC_INFARCT, MUSCLE_HYPERTONUS,
	 * MUSCLE_PARALYSIS_STROKE, MUSCLE_SPASTICITY, MUSCLE_STIFFNESS,
	 * 
	 * // Skin SKIN_ATROPHY("bodymapItem.valueF.label.value", 1, 1, 5, 1),
	 * SKIN_BURN, SKIN_PAIN_CHRONIC, SKIN_PAIN(
	 * "bodymapItem.valueF.label.level", 1, 1, 10, 1), SKIN_PIT, SKIN_REDNESS,
	 * SKIN_SWELLING, SKIN_VERTIGO,
	 */

	// *
	SKIN_LEAD_PAIN("bodymapItem.valueF.label.level", 1, 1, 10,
			1), SKIN_LEAD_MOBILITY, SKIN_LEAD_TEXT, SKIN_LEAD_AREA, SKIN_LEAD_VECTOR;

	private String labelF;
	private Integer defaultValueF;
	private Integer minValueF;
	private Integer maxValueF;
	private Integer stepF;
	private String labelT;
	private Integer defaultValueT;
	private Integer minValueT;
	private Integer maxValueT;
	private Integer stepT;

	private FindingType() {
	}

	private FindingType(String label, int defaultValue, int minValue, int maxValue, int step) {
		this(label, defaultValue, minValue, maxValue, step, null, null, null, null, null);
	}

	private FindingType(String labelF, Integer defaultValueF, Integer minValueF, Integer maxValueF, Integer stepF,
			String labelT, Integer defaultValueT, Integer minValueT, Integer maxValueT, Integer stepT) {
		this.labelF = labelF;
		this.defaultValueF = defaultValueF;
		this.minValueF = minValueF;
		this.maxValueF = maxValueF;
		this.stepF = stepF;
		this.labelT = labelT;
		this.defaultValueT = defaultValueT;
		this.minValueT = minValueT;
		this.maxValueT = maxValueT;
		this.stepT = stepT;
	}

	public String getLabelF() {
		return labelF;
	}

	public Integer getDefaultValueF() {
		return defaultValueF;
	}

	public Integer getMinValueF() {
		return minValueF;
	}

	public Integer getMaxValueF() {
		return maxValueF;
	}

	public Integer getStepF() {
		return stepF;
	}

	public String getLabelT() {
		return labelT;
	}

	public Integer getDefaultValueT() {
		return defaultValueT;
	}

	public Integer getMinValueT() {
		return minValueT;
	}

	public Integer getMaxValueT() {
		return maxValueT;
	}

	public Integer getStepT() {
		return stepT;
	}

	public static String toJson() {
		StringBuilder sb = new StringBuilder("[");
		for (FindingType findingType : FindingType.values()) {
			sb.append("\"" + findingType.name() + "\",");
		}
		if (sb.length() > 1) {
			sb.deleteCharAt(sb.length() - 1);
		}
		sb.append("]");
		return sb.toString();
	}

	public static List<FindingType> toList() {
		return toList(null);
	}

	public static List<FindingType> toList(AnatomyBody anatomyBody) {
		List<FindingType> findingTypes = new ArrayList<FindingType>();
		for (FindingType findingType : FindingType.values()) {
			if (anatomyBody == null || findingType.name().startsWith(anatomyBody.name())) {
				findingTypes.add(findingType);
			}
		}
		return findingTypes;
	}

}
