package at.synaptos.synapphys.model;

public enum Gender {
    FEMALE,
    MALE
}
