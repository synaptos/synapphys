package at.synaptos.synapphys.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import at.synaptos.synapphys.utils.StringUtils;

@MappedSuperclass
public abstract class AbstractPerson extends AbstractEntity {

	private static final long serialVersionUID = -5897780763510635316L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "person_id")
	private Long id;

	@NotNull
	@Column(name = "person_lastname")
	private String lastname;

	@Column(name = "person_firstname")
	private String firstname;

	@Column(name = "person_title")
	private String title;

	@Embedded
	private Address address;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "phone1", column = @Column(name = "contact_home_phone_1")),
			@AttributeOverride(name = "phone2", column = @Column(name = "contact_home_phone_2")),
			@AttributeOverride(name = "fax", column = @Column(name = "contact_home_fax")),
			@AttributeOverride(name = "email", column = @Column(name = "contact_home_email")) })
	private Contact homeContact;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "phone1", column = @Column(name = "contact_work_phone_1")),
			@AttributeOverride(name = "phone2", column = @Column(name = "contact_work_phone_2")),
			@AttributeOverride(name = "fax", column = @Column(name = "contact_work_fax")),
			@AttributeOverride(name = "email", column = @Column(name = "contact_work_email")) })
	private Contact workContact;

	@Column(name = "contact_note")
	private String contactNote;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Address getAddress() {
		if (address == null) {
			address = new Address();
		}
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Contact getHomeContact() {
		if (homeContact == null) {
			homeContact = new Contact();
		}
		return homeContact;
	}

	public void setHomeContact(Contact homeContact) {
		this.homeContact = homeContact;
	}

	public Contact getWorkContact() {
		if (workContact == null) {
			workContact = new Contact();
		}
		return workContact;
	}

	public void setWorkContact(Contact workContact) {
		this.workContact = workContact;
	}

	public String getContactNote() {
		return contactNote;
	}

	public void setContactNote(String contactNote) {
		this.contactNote = contactNote;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof AbstractPerson && id != null) ? id.equals(((AbstractPerson) other).id)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", title=" + title + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

	public String getFullname() {
		if (getLastname() == null) {
			return "";
		}
		return getLastname() + (!StringUtils.isEmpty(getFirstname()) ? ", " + getFirstname() : "");
	}

	public String getPhone() {
		return getHomeContact().getPhone();
	}

	public String getAddressAsString() {
		String address = "";

		if (!StringUtils.isEmpty(getAddress().getStreet())) {
			address += getAddress().getStreet() + ",";
		}

		if (getAddress().getPostcode() != null) {
			if (!StringUtils.isEmpty(address)) {
				address += " ";
			}
			address += getAddress().getPostcode();
		}

		if (!StringUtils.isEmpty(getAddress().getCity())) {
			if (!StringUtils.isEmpty(address)) {
				address += " ";
			}
			address += getAddress().getCity();
		}

		return address;
	}

}
