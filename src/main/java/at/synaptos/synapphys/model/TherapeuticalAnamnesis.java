package at.synaptos.synapphys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "therapeutical_anamnesis")
@Entity
public class TherapeuticalAnamnesis implements Serializable {

	private static final long serialVersionUID = -514969569188616205L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "therapeutical_anamnesis_id")
	private Long id;

	@Column(name = "what_note")
	private String whatNote;

	@Column(name = "where_note")
	private String whereNote;

	@Column(name = "when_note")
	private String whenNote;

	@Column(name = "how_note")
	private String howNote;

	@Column(name = "previous_treatment_note")
	private String previousTreatmentNote;

	@Column(name = "what_makes_problems_easier_note")
	private String whatMakesProblemsEasierNote;

	@Column(name = "what_makes_problems_worser_note")
	private String whatMakesProblemsWorserNote;

	@Column(name = "medication_blood_coagulation")
	private Boolean medicationBloodCoagulation;

	@Column(name = "medication_pain_killer")
	private Boolean medicationPainKiller;

	@Column(name = "medication_sleeping_drug")
	private Boolean medicationSleepingDrug;

	@Column(name = "medication_note")
	private String medicationNote;

	@Column(name = "disease_diabetes")
	private Boolean diseaseDiabetes;

	@Column(name = "disease_aids")
	private Boolean diseaseAids;

	@Column(name = "disease_heart_pacemaker")
	private Boolean diseaseHeartPacemaker;

	@Column(name = "disease_note")
	private String diseaseNote;

	@Column(name = "women_pregnancy")
	private Boolean womenPregnancy;

	@Column(name = "women_coil")
	private Boolean womenCoil;

	@Column(name = "women_note")
	private String womenNote;

	@NotNull
	@OneToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	public TherapeuticalAnamnesis() {
	}

	public static TherapeuticalAnamnesis getInstance() {
		return new TherapeuticalAnamnesis();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWhatNote() {
		return whatNote;
	}

	public void setWhatNote(String whatNote) {
		this.whatNote = whatNote;
	}

	public String getWhereNote() {
		return whereNote;
	}

	public void setWhereNote(String whereNote) {
		this.whereNote = whereNote;
	}

	public String getWhenNote() {
		return whenNote;
	}

	public void setWhenNote(String whenNote) {
		this.whenNote = whenNote;
	}

	public String getHowNote() {
		return howNote;
	}

	public void setHowNote(String howNote) {
		this.howNote = howNote;
	}

	public String getPreviousTreatmentNote() {
		return previousTreatmentNote;
	}

	public void setPreviousTreatmentNote(String previousTreatmentNote) {
		this.previousTreatmentNote = previousTreatmentNote;
	}

	public String getWhatMakesProblemsEasierNote() {
		return whatMakesProblemsEasierNote;
	}

	public void setWhatMakesProblemsEasierNote(String whatMakesProblemsEasierNote) {
		this.whatMakesProblemsEasierNote = whatMakesProblemsEasierNote;
	}

	public String getWhatMakesProblemsWorserNote() {
		return whatMakesProblemsWorserNote;
	}

	public void setWhatMakesProblemsWorserNote(String whatMakesProblemsWorserNote) {
		this.whatMakesProblemsWorserNote = whatMakesProblemsWorserNote;
	}

	public Boolean getMedicationBloodCoagulation() {
		return medicationBloodCoagulation;
	}

	public void setMedicationBloodCoagulation(Boolean medicationBloodCoagulation) {
		this.medicationBloodCoagulation = medicationBloodCoagulation;
	}

	public Boolean getMedicationPainKiller() {
		return medicationPainKiller;
	}

	public void setMedicationPainKiller(Boolean medicationPainKiller) {
		this.medicationPainKiller = medicationPainKiller;
	}

	public Boolean getMedicationSleepingDrug() {
		return medicationSleepingDrug;
	}

	public void setMedicationSleepingDrug(Boolean medicationSleepingDrug) {
		this.medicationSleepingDrug = medicationSleepingDrug;
	}

	public String getMedicationNote() {
		return medicationNote;
	}

	public void setMedicationNote(String medicationNote) {
		this.medicationNote = medicationNote;
	}

	public Boolean getDiseaseDiabetes() {
		return diseaseDiabetes;
	}

	public void setDiseaseDiabetes(Boolean diseaseDiabetes) {
		this.diseaseDiabetes = diseaseDiabetes;
	}

	public Boolean getDiseaseAids() {
		return diseaseAids;
	}

	public void setDiseaseAids(Boolean diseaseAids) {
		this.diseaseAids = diseaseAids;
	}

	public Boolean getDiseaseHeartPacemaker() {
		return diseaseHeartPacemaker;
	}

	public void setDiseaseHeartPacemaker(Boolean diseaseHeartPacemaker) {
		this.diseaseHeartPacemaker = diseaseHeartPacemaker;
	}

	public String getDiseaseNote() {
		return diseaseNote;
	}

	public void setDiseaseNote(String diseaseNote) {
		this.diseaseNote = diseaseNote;
	}

	public Boolean getWomenPregnancy() {
		return womenPregnancy;
	}

	public void setWomenPregnancy(Boolean womenPregnancy) {
		this.womenPregnancy = womenPregnancy;
	}

	public Boolean getWomenCoil() {
		return womenCoil;
	}

	public void setWomenCoil(Boolean womenCoil) {
		this.womenCoil = womenCoil;
	}

	public String getWomenNote() {
		return womenNote;
	}

	public void setWomenNote(String womenNote) {
		this.womenNote = womenNote;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof TherapeuticalAnamnesis && id != null) ? id.equals(((TherapeuticalAnamnesis) other).id)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ",whatNote=" + whatNote + ",whereNote=" + whereNote
				+ ",whenNote=" + whenNote + ",seriesDocumentation=" + seriesDocumentation + "}";
	}

}
