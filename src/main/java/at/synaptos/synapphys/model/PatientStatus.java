package at.synaptos.synapphys.model;

public enum PatientStatus {
	CURRENT, BOOKMARKED
}
