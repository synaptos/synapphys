package at.synaptos.synapphys.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Table(name = "billing_item")
@Entity
public class BillingItem implements Serializable {

	private static final long serialVersionUID = -7832487719362633662L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "billing_item_id")
	private Long id;

	@NotNull
	private String subject;

	@NotNull
	private Integer quantity;

	@NotNull
	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "vat")
	private BigDecimal vat;

	@NotNull
	@Column(name = "sequence_no")
	private Integer sequenceNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "service_date")
	private Date serviceDate;

	@ManyToOne
	@JoinColumn(name = "service_documentation", referencedColumnName = "service_documentation_id")
	private ServiceDocumentation serviceDocumentation;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "billing", referencedColumnName = "billing_id", nullable = false)
	private Billing billing;

	public BillingItem() {
		setQuantity(1);
		setAmount(BigDecimal.ZERO);
	}

	public static BillingItem getInstance() {
		final BillingItem instance = new BillingItem();
		return instance;
	}

	public static BillingItem getInstance(@NotNull final ServiceDocumentation serviceDocumentation) {
		final BillingItem instance = getInstance();
		instance.setSubject(serviceDocumentation.getServiceType().getName());
		instance.setAmount(serviceDocumentation.getServiceType().getAmount());
		instance.setQuantity(serviceDocumentation.getQuantity());
		instance.setServiceDate((Date) serviceDocumentation.getSessionDocumentation().getStartDate().clone());
		instance.setServiceDocumentation(serviceDocumentation);
		return instance;
	}

	@PrePersist
	private void prePersist() {
		if (getQuantity() == null) {
			setQuantity(1);
		}
		if (getAmount() == null) {
			setAmount(BigDecimal.ZERO);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public BillingItem withServiceDate(Date serviceDate) {
		setServiceDate(serviceDate);
		return this;
	}

	public ServiceDocumentation getServiceDocumentation() {
		return serviceDocumentation;
	}

	public void setServiceDocumentation(ServiceDocumentation serviceDocumentation) {
		this.serviceDocumentation = serviceDocumentation;
	}

	public BillingItem withServiceDocumentation(ServiceDocumentation serviceDocumentation) {
		setServiceDocumentation(serviceDocumentation);
		return this;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof BillingItem && id != null) ? id.equals(((BillingItem) other).id) : (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ", subject=" + subject + ", quantity=" + quantity + ", amount="
				+ amount + ", vat=" + vat + ", sequenceNo=" + sequenceNo + ", serviceDate=" + serviceDate
				+ ", serviceDocumentation=" + serviceDocumentation + ", billing=" + billing + "}";
	}

	public boolean isNew() {
		return id == null;
	}

	public double getTotal() {
		BigDecimal vat = getVat() == null ? BigDecimal.ZERO : getVat();
		return getAmount().multiply(BigDecimal.valueOf(getQuantity()))
				.multiply(BigDecimal.valueOf(100).add(vat).divide(BigDecimal.valueOf(100))).doubleValue();
	}

}
