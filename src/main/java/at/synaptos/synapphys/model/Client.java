package at.synaptos.synapphys.model;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;

import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;

@NamedNativeQueries({ @NamedNativeQuery(name = Client.FIND_SEARCH_ITEMS_NATIVE, query = "select"
		+ " distinct(substring(c.person_lastname, 1, 1)) as search_key, count(c.person_id) as search_key_count"
		+ " from client c where c.deleted_date is null group by search_key order by search_key") })
@NamedQueries({
		@NamedQuery(name = Client.FIND_ALL, query = "select c from Client c where c.deletedDate is null"
				+ " order by c.lastname"),
		@NamedQuery(name = Client.FIND_MAIN_INSURED, query = "select c from Client c where c.deletedDate is null"
				+ " and c.mainInsuredClient is null order by c.lastname"),
		@NamedQuery(name = Client.FIND_BY_SEARCH_KEY, query = "select c from Client c where c.deletedDate is null"
				+ " and substring(c.lastname, 1, 1) = :searchKey order by c.lastname"),
		@NamedQuery(name = Client.FIND_BY_PATIENT_STATUS, query = "select c from Client c where c.deletedDate is null"
				+ " and c.patientStatus = :patientStatus order by c.lastname"),
		@NamedQuery(name = Client.FIND_BY_HEALTH_INSURANCE, query = "select c from Client c where c.deletedDate is null"
				+ " and c.healthInsuranceNo = :healthInsuranceNo and c.healthInsurance = :healthInsurance order by c.lastname"),
		@NamedQuery(name = Client.FIND_BY_HEALTH_INSURANCE_NO, query = "select c from Client c where c.deletedDate is null"
				+ " and c.healthInsuranceNo = :healthInsuranceNo order by c.lastname") })
@Table(name = "client", indexes = {
		@Index(columnList = "person_lastname, person_firstname, birth_date", unique = false),
		@Index(columnList = "health_insurance_no", unique = false),
		@Index(columnList = "patient_status", unique = false) })
@Entity
public class Client extends AbstractPerson implements ClientOwner, PhysicianOwner {

	private static final long serialVersionUID = -4552186650349057217L;

	public static final String FIND_ALL = "Client.findAll";
	public static final String FIND_MAIN_INSURED = "Client.findMainInsured";
	public static final String FIND_BY_SEARCH_KEY = "Client.findBySearchKey";
	public static final String FIND_BY_PATIENT_STATUS = "Client.findByPatientStatus";
	public static final String FIND_BY_HEALTH_INSURANCE = "Client.findByHealthInsurance";
	public static final String FIND_BY_HEALTH_INSURANCE_NO = "Client.findByHealthInsuranceNo";

	public static final String FIND_SEARCH_ITEMS_NATIVE = "Client.findSearchItemsNative";

	@Enumerated(EnumType.STRING)
	@Column(name = "gender")
	private Gender gender;

	@Temporal(TemporalType.DATE)
	@Column(name = "birth_date")
	private Date birthDate;

	@Column(name = "birth_place")
	private String birthPlace;

	@ManyToOne
	@JoinColumn(name = "main_insured_client", referencedColumnName = "person_id")
	private Client mainInsuredClient;

	@OneToMany(mappedBy = "mainInsuredClient")
	@Where(clause = "deleted_date is null")
	private List<Client> coInsuredClients;

	@ManyToOne
	@JoinColumn(name = "health_insurance", referencedColumnName = "health_insurance_id")
	private HealthInsurance healthInsurance;

	@Column(name = "health_insurance_no")
	private String healthInsuranceNo;

	@ManyToOne
	@JoinColumn(name = "physician", referencedColumnName = "person_id")
	private Physician physician;

	private String profession;

	@Embedded
	private BankAccount bankAccount;

	@Column(name = "vip_date")
	private Date vipDate;

	@Column(name = "vip_note")
	private String vipNote;

	@Enumerated(EnumType.STRING)
	@Column(name = "patient_status")
	private PatientStatus patientStatus;

	@Column(name = "patient_status_date")
	private Date patientStatusDate;

	@Column(name = "patient_status_note")
	private String patientStatusNote;

	@OneToMany(mappedBy = "client")
	@OrderBy("schedule_start_date desc")
	@Where(clause = "deleted_date is null")
	private List<ScheduleItem> scheduleItems;

	@OneToMany(mappedBy = "client")
	@OrderBy("approval_date desc, referral_date desc, created_date desc")
	@Where(clause = "deleted_date is null")
	private List<SeriesDocumentation> seriesDocumentations;

	public static Client getInstance() {
		return new Client();
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public Client getMainInsuredClient() {
		return mainInsuredClient;
	}

	public void setMainInsuredClient(Client mainInsuredClient) {
		this.mainInsuredClient = mainInsuredClient;
	}

	public List<Client> getCoInsuredClients() {
		if (coInsuredClients == null) {
			coInsuredClients = new ArrayList<Client>();
		}
		return coInsuredClients;
	}

	public void setCoInsuredClients(List<Client> coInsuredClients) {
		this.coInsuredClients = coInsuredClients;
	}

	public HealthInsurance getHealthInsurance() {
		return healthInsurance;
	}

	public void setHealthInsurance(HealthInsurance healthInsurance) {
		this.healthInsurance = healthInsurance;
	}

	public String getHealthInsuranceNo() {
		return healthInsuranceNo;
	}

	public void setHealthInsuranceNo(String healthInsuranceNo) {
		this.healthInsuranceNo = healthInsuranceNo;
	}

	@Override
	public Physician getPhysician() {
		return physician;
	}

	@Override
	public void setPhysician(Physician physician) {
		this.physician = physician;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public BankAccount getBankAccount() {
		if (bankAccount == null) {
			bankAccount = new BankAccount();
		}
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public Date getVipDate() {
		return vipDate;
	}

	public void setVipDate(Date vipDate) {
		this.vipDate = vipDate;
	}

	public String getVipNote() {
		return vipNote;
	}

	public void setVipNote(String vipNote) {
		this.vipNote = vipNote;
	}

	public PatientStatus getPatientStatus() {
		return patientStatus;
	}

	public void setPatientStatus(PatientStatus patientStatus) {
		this.patientStatus = patientStatus;
	}

	public Date getPatientStatusDate() {
		return patientStatusDate;
	}

	public void setPatientStatusDate(Date patientStatusDate) {
		this.patientStatusDate = patientStatusDate;
	}

	public String getPatientStatusNote() {
		return patientStatusNote;
	}

	public void setPatientStatusNote(String patientStatusNote) {
		this.patientStatusNote = patientStatusNote;
	}

	public List<ScheduleItem> getScheduleItems() {
		if (scheduleItems == null) {
			scheduleItems = new ArrayList<ScheduleItem>();
		}
		return scheduleItems;
	}

	public void setScheduleItems(List<ScheduleItem> scheduleItems) {
		this.scheduleItems = scheduleItems;
	}

	public List<SeriesDocumentation> getSeriesDocumentations() {
		if (seriesDocumentations == null) {
			seriesDocumentations = new ArrayList<SeriesDocumentation>();
		}
		return seriesDocumentations;
	}

	public void setSeriesDocumentations(List<SeriesDocumentation> seriesDocumentations) {
		this.seriesDocumentations = seriesDocumentations;
	}

	@Override
	public int hashCode() {
		return (getId() != null) ? this.getClass().hashCode() + getId().hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof Client && getId() != null) ? getId().equals(((Client) other).getId())
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{ super=" + super.toString() + ", gender=" + gender + ", birthDate="
				+ birthDate + ", birthPlace=" + birthPlace + ", mainInsuredClient=" + mainInsuredClient
				+ ", healthInsurance=" + healthInsurance + ", healthInsuranceNo=" + healthInsuranceNo + ", physician="
				+ physician + ", profession=" + profession + ", bankAccount=" + bankAccount + ", vipDate=" + vipDate
				+ ", patientStatus=" + patientStatus + ", patientStatusDate=" + patientStatusDate + " }";
	}

	@Override
	public Client withBackUrl(String backUrl) {
		return (Client) super.withBackUrl(backUrl);
	}

	@Override
	public Client getClient() {
		return getMainInsuredClient();
	}

	@Override
	public void setClient(Client client) {
		setMainInsuredClient(client);
	}

	@Transient
	private ClientOwner clientOwner;

	public ClientOwner getClientOwner() {
		return clientOwner;
	}

	public void setClientOwner(ClientOwner clientOwner) {
		this.clientOwner = clientOwner;
	}

	@Transient
	private ScheduleItem selectedScheduleItem;

	public ScheduleItem getSelectedScheduleItem() {
		return selectedScheduleItem;
	}

	public void setSelectedScheduleItem(ScheduleItem selectedScheduleItem) {
		this.selectedScheduleItem = selectedScheduleItem;
	}

	public Client withSelectedScheduleItem(ScheduleItem selectedScheduleItem) {
		setSelectedScheduleItem(selectedScheduleItem);
		return this;
	}

	@Transient
	private SeriesDocumentation selectedSeriesDocumentation;

	public SeriesDocumentation getSelectedSeriesDocumentation() {
		return selectedSeriesDocumentation;
	}

	public void setSelectedSeriesDocumentation(SeriesDocumentation selectedSeriesDocumentation) {
		this.selectedSeriesDocumentation = selectedSeriesDocumentation;
	}

	public Client withSelectedSeriesDocumentation(SeriesDocumentation selectedSeriesDocumentation) {
		setSelectedSeriesDocumentation(selectedSeriesDocumentation);
		return this;
	}

	public SeriesDocumentation getLastSeriesDocumentation() {
		if (CollectionUtils.isEmpty(getSeriesDocumentations())) {
			return null;
		}
		for (SeriesDocumentation seriesDocumentation : getSeriesDocumentations()) {
			if (!seriesDocumentation.isDone()) {
				return seriesDocumentation;
			}
		}
		return getSeriesDocumentations().get(0);
	}

	@Transient
	private SessionDocumentation selectedSessionDocumentation;

	public SessionDocumentation getSelectedSessionDocumentation() {
		return selectedSessionDocumentation;
	}

	public void setSelectedSessionDocumentation(SessionDocumentation selectedSessionDocumentation) {
		this.selectedSessionDocumentation = selectedSessionDocumentation;
	}

	public Client withSelectedSessionDocumentation(SessionDocumentation selectedSessionDocumentation) {
		setSelectedSessionDocumentation(selectedSessionDocumentation);
		return this;
	}

	public Integer getAge() {
		if (getBirthDate() == null) {
			return null;
		}
		return Period.between(DateUtils.asLdt(getBirthDate()).toLocalDate(), LocalDate.now()).getYears();
	}

	public boolean isVip() {
		return getVipDate() != null;
	}

	public void setVip(boolean vip) {
		setVipDate(vip ? new Date() : null);
	}

	public long getVipSinceDays() {
		if (getVipDate() == null) {
			return 0l;
		}
		LocalDate vipLd = DateUtils.asLd(getVipDate());
		long daysBetween = ChronoUnit.DAYS.between(vipLd, LocalDate.now());
		return daysBetween;
	}

	public long getPatientStatusSinceDays() {
		if (getPatientStatusDate() == null) {
			return 0l;
		}
		LocalDate statusLd = DateUtils.asLd(getPatientStatusDate());
		long daysBetween = ChronoUnit.DAYS.between(statusLd, LocalDate.now());
		return daysBetween;
	}

	public boolean isCurrent() {
		if (getPatientStatus() == null) {
			return false;
		}
		return getPatientStatus() == PatientStatus.CURRENT;
	}

	public void setCurrent(boolean current) {
		if (current) {
			setPatientStatus(PatientStatus.CURRENT);
			setPatientStatusDate(new Date());
		} else {
			setPatientStatus(null);
			setPatientStatusDate(null);
		}
	}

	public boolean isBookmarked() {
		if (getPatientStatus() == null) {
			return false;
		}
		return getPatientStatus() == PatientStatus.BOOKMARKED;
	}

	public void setBookmarked(boolean bookmarked) {
		if (bookmarked) {
			setPatientStatus(PatientStatus.BOOKMARKED);
			setPatientStatusDate(new Date());
		} else {
			setPatientStatus(null);
			setPatientStatusDate(null);
		}
	}

	public String getTextKeyForGender() {
		return textKeyForGender(".name");
	}

	public String textKeyForGender(String suffix) {
		return textKeyForGender("client.gender.range.", suffix);
	}

	public String textKeyForGender(String prefix, String suffix) {
		return prefix + (getGender() != null ? getGender().name() : "NULL") + suffix;
	}

}
