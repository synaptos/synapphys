package at.synaptos.synapphys.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "service_template")
@Entity
public class ServiceTemplate extends AbstractEntity {

	private static final long serialVersionUID = -2331975991671581243L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "service_template_id")
	private Long id;

	@NotNull
	@OneToOne
	@JoinColumn(name = "service_type", referencedColumnName = "service_type_id")
	private ServiceType serviceType;

	@NotNull
	@Column(name = "service_quantity")
	private Integer quantity;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	public ServiceTemplate() {
		setQuantity(1);
	}

	public static ServiceTemplate getInstance() {
		final ServiceTemplate instance = new ServiceTemplate();
		return instance;
	}

	public static ServiceTemplate getInstance(@NotNull final ServiceType serviceType) {
		final ServiceTemplate instance = getInstance();
		instance.setServiceType(serviceType);
		return instance;
	}

	@PrePersist
	public void prePersist() {
		if (getQuantity() == null) {
			setQuantity(1);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ",serviceType=" + serviceType + ",quantity=" + quantity
				+ ",seriesDocumentation=" + seriesDocumentation + "}";
	}

	@Override
	public boolean isNew() {
		return id == null;
	}

}
