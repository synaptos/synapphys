package at.synaptos.synapphys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "therapeutical_diagnosis")
@Entity
public class TherapeuticalDiagnosis implements Serializable {

	private static final long serialVersionUID = -1109588074723833206L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "therapeutical_diagnosis_id")
	private Long id;

	@Column(name = "location")
	private String location;

	@Column(name = "method")
	private String method;

	@Column(name = "direction")
	private String direction;

	@Column(name = "quality")
	private String quality;

	@Column(name = "level")
	private String level;

	@Column(name = "activity")
	private String activity;

	@Column(name = "participation")
	private String participation;

	@NotNull
	@OneToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	public TherapeuticalDiagnosis() {
	}

	public static TherapeuticalDiagnosis getInstance() {
		return new TherapeuticalDiagnosis();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getParticipation() {
		return participation;
	}

	public void setParticipation(String participation) {
		this.participation = participation;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof TherapeuticalDiagnosis && id != null) ? id.equals(((TherapeuticalDiagnosis) other).id)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + ",location=" + location + ",method=" + method + ",direction="
				+ direction + ",seriesDocumentation=" + seriesDocumentation + "}";
	}

}
