package at.synaptos.synapphys.model;

public interface PhysicianOwner {

	public Physician getPhysician();

	public void setPhysician(Physician physician);

}
