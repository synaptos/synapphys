package at.synaptos.synapphys.model;

public enum Priority {
	P4_LOW, P3_MEDIUM, P2_HIGH, P1_URGENT
}
