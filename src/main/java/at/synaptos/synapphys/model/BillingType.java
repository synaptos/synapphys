package at.synaptos.synapphys.model;

public enum BillingType {
    RECEIPT,
    INVOICE
}
