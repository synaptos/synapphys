package at.synaptos.synapphys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import at.synaptos.synapphys.utils.StringUtils;

@Embeddable
public class Address implements Serializable {

	private static final long serialVersionUID = 6556318770933199585L;

	@Column(name = "address_street")
	private String street;

	@Column(name = "address_postcode")
	private String postcode;

	@Column(name = "address_city")
	private String city;

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostcity() {
		if (StringUtils.isEmpty(getPostcode())) {
			return getCity();
		}
		if (StringUtils.isEmpty(getCity())) {
			return getPostcode();
		}
		return getPostcode() + " " + getCity();
	}

}
