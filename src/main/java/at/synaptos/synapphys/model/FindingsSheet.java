package at.synaptos.synapphys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Table(name = "findings_sheet")
@Entity
public class FindingsSheet implements Serializable {

	private static final long serialVersionUID = -5445055666909288694L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false)
	private Date createdDate;

	@Column(name = "created_user", nullable = false)
	private String createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", nullable = false)
	private Date updatedDate;

	@Column(name = "updated_user", nullable = false)
	private String updatedUser;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "findings_sheet_id")
	private Long id;

	@Column(name = "pain_anamnesis", columnDefinition = "TEXT")
	private String painAnamnesis;

	@Column(name = "special_issues", columnDefinition = "TEXT")
	private String specialIssues;

	@Column(name = "anamnesis_history", columnDefinition = "TEXT")
	private String anamnesisHistory;

	@Column(name = "present_pain", columnDefinition = "TEXT")
	private String presentPain;

	@Column(name = "inspection", columnDefinition = "TEXT")
	private String inspection;

	@Column(name = "palpation", columnDefinition = "TEXT")
	private String palpation;

	@Column(name = "functional_demo", columnDefinition = "TEXT")
	private String functionalDemo;

	@Column(name = "functional_examination", columnDefinition = "TEXT")
	private String functionalExamination;

	@Column(name = "neurodynamicial_test", columnDefinition = "TEXT")
	private String neurodynamicTest;

	@Column(name = "specific_measuring", columnDefinition = "TEXT")
	private String specificMeasuring;

	@Column(name = "exclusionary_test", columnDefinition = "TEXT")
	private String exclusionaryTest;

	@Column(name = "neurological_examination", columnDefinition = "TEXT")
	private String neurologicalExamination;

	@Column(name = "fbl_constitution", columnDefinition = "TEXT")
	private String fblConstitution;

	@Column(name = "fbl_statics_of_site", columnDefinition = "TEXT")
	private String fblStaticsOfSite;

	@Column(name = "fbl_statics_of_front_and_back", columnDefinition = "TEXT")
	private String fblStatusStaticsOfFrontAndBack;

	@Column(name = "fbl_thrust_load", columnDefinition = "TEXT")
	private String fblThrustLoad;

	@Column(name = "fbl_reactive_hypertonus", columnDefinition = "TEXT")
	private String fblReactiveHypertons;

	@Column(name = "fbl_motion_behaviour", columnDefinition = "TEXT")
	private String fblMotionBehaviour;

	@Column(name = "interpretation_hypothesis", columnDefinition = "TEXT")
	private String interpretationHypothesis;

	@Column(name = "planned_therapeutical_functional_outcome", columnDefinition = "TEXT")
	private String plannedTherapeuticalFunctionalOutcome;

	@Column(name = "planned_therapeutical_outcome_and_measures", columnDefinition = "TEXT")
	private String plannedTherapeuticalOutcomeAndMeasures;

	@NotNull
	@OneToOne
	@JoinColumn(name = "series_documentation", referencedColumnName = "series_documentation_id", nullable = false)
	private SeriesDocumentation seriesDocumentation;

	public FindingsSheet() {
	}

	public static FindingsSheet getInstance() {
		return new FindingsSheet();
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPainAnamnesis() {
		return painAnamnesis;
	}

	public void setPainAnamnesis(String painAnamnesis) {
		this.painAnamnesis = painAnamnesis;
	}

	public String getSpecialIssues() {
		return specialIssues;
	}

	public void setSpecialIssues(String specialIssues) {
		this.specialIssues = specialIssues;
	}

	public SeriesDocumentation getSeriesDocumentation() {
		return seriesDocumentation;
	}

	public void setSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		this.seriesDocumentation = seriesDocumentation;
	}

	public String getAnamnesisHistory() {
		return anamnesisHistory;
	}

	public void setAnamnesisHistory(String anamnesisHistory) {
		this.anamnesisHistory = anamnesisHistory;
	}

	public String getPresentPain() {
		return presentPain;
	}

	public void setPresentPain(String presentPain) {
		this.presentPain = presentPain;
	}

	public String getInspection() {
		return inspection;
	}

	public void setInspection(String inspection) {
		this.inspection = inspection;
	}

	public String getPalpation() {
		return palpation;
	}

	public void setPalpation(String palpation) {
		this.palpation = palpation;
	}

	public String getFunctionalDemo() {
		return functionalDemo;
	}

	public void setFunctionalDemo(String functionalDemo) {
		this.functionalDemo = functionalDemo;
	}

	public String getFunctionalExamination() {
		return functionalExamination;
	}

	public void setFunctionalExamination(String functionalExamination) {
		this.functionalExamination = functionalExamination;
	}

	public String getNeurodynamicTest() {
		return neurodynamicTest;
	}

	public void setNeurodynamicTest(String neurodynamicTest) {
		this.neurodynamicTest = neurodynamicTest;
	}

	public String getSpecificMeasuring() {
		return specificMeasuring;
	}

	public void setSpecificMeasuring(String specificMeasuring) {
		this.specificMeasuring = specificMeasuring;
	}

	public String getExclusionaryTest() {
		return exclusionaryTest;
	}

	public void setExclusionaryTest(String exclusionaryTest) {
		this.exclusionaryTest = exclusionaryTest;
	}

	public String getNeurologicalExamination() {
		return neurologicalExamination;
	}

	public void setNeurologicalExamination(String neurologicalExamination) {
		this.neurologicalExamination = neurologicalExamination;
	}

	public String getFblConstitution() {
		return fblConstitution;
	}

	public void setFblConstitution(String fblConstitution) {
		this.fblConstitution = fblConstitution;
	}

	public String getFblStaticsOfSite() {
		return fblStaticsOfSite;
	}

	public void setFblStaticsOfSite(String fblStaticsOfSite) {
		this.fblStaticsOfSite = fblStaticsOfSite;
	}

	public String getFblStatusStaticsOfFrontAndBack() {
		return fblStatusStaticsOfFrontAndBack;
	}

	public void setFblStatusStaticsOfFrontAndBack(String fblStatusStaticsOfFrontAndBack) {
		this.fblStatusStaticsOfFrontAndBack = fblStatusStaticsOfFrontAndBack;
	}

	public String getFblThrustLoad() {
		return fblThrustLoad;
	}

	public void setFblThrustLoad(String fblThrustLoad) {
		this.fblThrustLoad = fblThrustLoad;
	}

	public String getFblReactiveHypertons() {
		return fblReactiveHypertons;
	}

	public void setFblReactiveHypertons(String fblReactiveHypertons) {
		this.fblReactiveHypertons = fblReactiveHypertons;
	}

	public String getFblMotionBehaviour() {
		return fblMotionBehaviour;
	}

	public void setFblMotionBehaviour(String fblMotionBehaviour) {
		this.fblMotionBehaviour = fblMotionBehaviour;
	}

	public String getInterpretationHypothesis() {
		return interpretationHypothesis;
	}

	public void setInterpretationHypothesis(String interpretationHypothesis) {
		this.interpretationHypothesis = interpretationHypothesis;
	}

	public String getPlannedTherapeuticalFunctionalOutcome() {
		return plannedTherapeuticalFunctionalOutcome;
	}

	public void setPlannedTherapeuticalFunctionalOutcome(String plannedTherapeuticalFunctionalOutcome) {
		this.plannedTherapeuticalFunctionalOutcome = plannedTherapeuticalFunctionalOutcome;
	}

	public String getPlannedTherapeuticalOutcomeAndMeasures() {
		return plannedTherapeuticalOutcomeAndMeasures;
	}

	public void setPlannedTherapeuticalOutcomeAndMeasures(String plannedTherapeuticalOutcomeAndMeasures) {
		this.plannedTherapeuticalOutcomeAndMeasures = plannedTherapeuticalOutcomeAndMeasures;
	}

	@Override
	public int hashCode() {
		return (id != null) ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof FindingsSheet && id != null) ? id.equals(((FindingsSheet) other).id) : (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{id=" + id + "}";
	}

}
