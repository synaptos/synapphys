package at.synaptos.synapphys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

@NamedQueries({
		@NamedQuery(name = InstanceProperty.FIND_ALL, query = "select p from InstanceProperty p order by p.key") })
@Table(name = "instance_property")
@Entity
public class InstanceProperty implements Serializable {

	private static final long serialVersionUID = 795057755230517247L;

	public static final String FIND_ALL = "InstanceProperty.findAll";

	@Id
	@Column(name = "instance_property_key")
	private String key;

	@Size(max = 2000)
	@Column(name = "instance_property_value")
	private String value;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", nullable = false)
	private Date updatedDate;

	@Column(name = "updated_user", nullable = false)
	private String updatedUser;

	public InstanceProperty() {
		super();
	}

	public InstanceProperty(String key, String value) {
		this();
		setKey(key);
		setValue(value);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public void setUpdatedDate(long updatedDate) {
		setUpdatedDate(new Date(updatedDate));
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	@Override
	public int hashCode() {
		return (key != null) ? this.getClass().hashCode() + key.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof InstanceProperty && key != null) ? key.equals(((InstanceProperty) other).key)
				: (other == this);
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{ key=" + key + ", value=" + value + " }";
	}

	public boolean isNew() {
		return key == null;
	}

	@Transient
	private boolean changed = false;

	public boolean isChanged() {
		return changed;
	}

	public void setChanged(boolean changed) {
		this.changed = changed;
	}

}
