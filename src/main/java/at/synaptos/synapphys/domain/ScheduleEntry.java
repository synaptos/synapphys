package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.StringUtils;

public class ScheduleEntry implements CustomScheduleEvent, Serializable {

	private static final long serialVersionUID = 6749091641218346851L;

	private String id;
	private boolean allDay = false;
	private String styleClass;
	private AbstractEntity data;
	private boolean editable = true;
	private String rendering;
	private int weighting;
	private List<ScheduleEntry> previewEntries;

	private ScheduleEntry() {
		setWeighting(0);
	}

	private ScheduleEntry(ScheduleItem data) {
		this();
		this.data = data;
	}

	public static ScheduleEntry getInstance(Date startDate, Date endDate) {
		return getInstance(startDate, endDate, 0);
	}

	public static ScheduleEntry getInstance(Date startDate, Date endDate, int weighting) {
		ScheduleEntry scheduleEntry = getInstance(ScheduleItem.getInstance(startDate, endDate));
		scheduleEntry.setStyleClass("fc-nonbusiness");
		scheduleEntry.setRendering("background");
		scheduleEntry.setWeighting(weighting);
		return scheduleEntry;
	}

	public static ScheduleEntry getInstance(LocalDateTime startLdt, LocalDateTime endLdt) {
		return getInstance(startLdt, endLdt, 0);
	}

	public static ScheduleEntry getInstance(LocalDateTime startLdt, LocalDateTime endLdt, int weighting) {
		return getInstance(DateUtils.asDate(startLdt), DateUtils.asDate(endLdt), weighting);
	}

	public static ScheduleEntry getInstance(ScheduleItem data) {
		return new ScheduleEntry(data);
	}

	private ScheduleEntry(SessionDocumentation data) {
		this();
		this.data = data;
	}

	public static ScheduleEntry getInstance(SessionDocumentation data) {
		return new ScheduleEntry(data);
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getTitle() {
		String title = Optional.ofNullable(getSubject()).orElse("");
		Client client = getClient();
		if (client != null) {
			if (!StringUtils.isEmpty(title)) {
				title = client.getFullname() + ": " + title;
			} else {
				title = client.getFullname();
			}
		}
		return title;
	}

	public String getSubject() {
		String subject = "";
		if (getData() instanceof ScheduleItem) {
			subject = ((ScheduleItem) getData()).getSubject();
		} else if (getData() instanceof SessionDocumentation) {
			subject = ((SessionDocumentation) getData()).getScheduleSubject();
		}
		return subject;
	}

	public void setSubject(String subject) {
		if (getData() instanceof ScheduleItem) {
			((ScheduleItem) getData()).setSubject(subject);
		} else if (getData() instanceof SessionDocumentation) {
			((SessionDocumentation) getData()).setScheduleSubject(subject);
		}
	}

	@Override
	public Date getStartDate() {
		if (getData() instanceof ScheduleItem) {
			return ((ScheduleItem) getData()).getStartDate();
		} else if (getData() instanceof SessionDocumentation) {
			return ((SessionDocumentation) getData()).getStartDate();
		}
		return null;
	}

	public void setStartDate(Date startDate) {
		if (getData() instanceof ScheduleItem) {
			((ScheduleItem) getData()).setStartDate(startDate);
		} else if (getData() instanceof SessionDocumentation) {
			((SessionDocumentation) getData()).setStartDate(startDate);
		}
	}

	@Override
	public Date getEndDate() {
		if (getData() instanceof ScheduleItem) {
			return ((ScheduleItem) getData()).getEndDate();
		} else if (getData() instanceof SessionDocumentation) {
			return ((SessionDocumentation) getData()).getEndDate();
		}
		return null;
	}

	public void setEndDate(Date endDate) {
		if (getData() instanceof ScheduleItem) {
			((ScheduleItem) getData()).setEndDate(endDate);
		} else if (getData() instanceof SessionDocumentation) {
			((SessionDocumentation) getData()).setEndDate(endDate);
		}
	}

	@Override
	public boolean isAllDay() {
		return allDay;
	}

	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	@Override
	public String getStyleClass() {
		return styleClass;
	}

	@Override
	public AbstractEntity getData() {
		return data;
	}

	@Override
	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	@Override
	public String getDescription() {
		if (getData() instanceof ScheduleItem) {
			return ((ScheduleItem) getData()).getNote();
		} else if (getData() instanceof SessionDocumentation) {
			return ((SessionDocumentation) getData()).getScheduleNote();
		}
		return null;
	}

	public void setDescription(String description) {
		if (getData() instanceof ScheduleItem) {
			((ScheduleItem) getData()).setNote(description);
		} else if (getData() instanceof SessionDocumentation) {
			((SessionDocumentation) getData()).setScheduleNote(description);
		}
	}

	public String getPhone() {
		if (getData() instanceof ScheduleItem) {
			return ((ScheduleItem) getData()).getPhone();
		} else if (getData() instanceof SessionDocumentation) {
			SessionDocumentation sessionDocumentation = (SessionDocumentation) getData();
			if (sessionDocumentation.getSeriesDocumentation() == null) {
				return null;
			}
			if (sessionDocumentation.getSeriesDocumentation().getClient() == null) {
				return null;
			}
			return sessionDocumentation.getSeriesDocumentation().getClient().getPhone();
		}
		return null;
	}

	public void setPhone(String phone) {
		if (getData() instanceof ScheduleItem) {
			((ScheduleItem) getData()).setPhone(phone);
		} else if (getData() instanceof SessionDocumentation) {
		}
	}

	public Boolean getRemindBySms() {
		if (getData() instanceof ScheduleItem) {
			return ((ScheduleItem) getData()).isRemindBySms();
		} else if (getData() instanceof SessionDocumentation) {
			return ((SessionDocumentation) getData()).isRemindBySms();
		}
		return false;
	}

	public Boolean isRemindBySms() {
		return getRemindBySms();
	}

	public void setRemindBySms(Boolean remindBySms) {
		if (getData() instanceof ScheduleItem) {
			((ScheduleItem) getData()).setRemindBySms(remindBySms);
		} else if (getData() instanceof SessionDocumentation) {
			((SessionDocumentation) getData()).setRemindBySms(remindBySms);
		}
	}

	public Client getClient() {
		if (getData() instanceof ScheduleItem) {
			return ((ScheduleItem) getData()).getClient();
		} else if (getData() instanceof SessionDocumentation) {
			return ((SessionDocumentation) getData()).getSeriesDocumentation().getClient();
		}
		return null;
	}

	public void setClient(Client client) {
		if (getData() instanceof ScheduleItem) {
			((ScheduleItem) getData()).setClient(client);
		} else if (getData() instanceof SessionDocumentation) {
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ScheduleEntry other = (ScheduleEntry) obj;
		if ((this.getTitle() == null) ? (other.getTitle() != null) : !this.getTitle().equals(other.getTitle())) {
			return false;
		}
		if (this.getStartDate() != other.getStartDate()
				&& (this.getStartDate() == null || !this.getStartDate().equals(other.getStartDate()))) {
			return false;
		}
		if (this.getEndDate() != other.getEndDate()
				&& (this.getEndDate() == null || !this.getEndDate().equals(other.getEndDate()))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 61 * hash + (this.getTitle() != null ? this.getTitle().hashCode() : 0);
		hash = 61 * hash + (this.getStartDate() != null ? this.getStartDate().hashCode() : 0);
		hash = 61 * hash + (this.getEndDate() != null ? this.getEndDate().hashCode() : 0);
		return hash;
	}

	@Override
	public String toString() {
		return getClass().getName() + "{title=" + getTitle() + ",phone=" + getPhone() + ",startDate=" + getStartDate()
				+ ",endDate=" + getEndDate() + ",client=" + getClient() + "}";
	}

	@Override
	public String getRendering() {
		return rendering;
	}

	public void setRendering(String rendering) {
		this.rendering = rendering;
	}

	public int getWeighting() {
		return weighting;
	}

	public void setWeighting(int weighting) {
		this.weighting = weighting;
	}

	public List<ScheduleEntry> getPreviewEntries() {
		return previewEntries;
	}

	public void setPreviewEntries(List<ScheduleEntry> previewEntries) {
		this.previewEntries = previewEntries;
	}

	public Date getStartDayOfMonth() {
		return DateUtils.resetTimeForDate(getStartDate());
	}

	public Date getEndDayOfMonth() {
		return DateUtils.resetTimeForDate(getEndDate());
	}

	public void plusWeighting(int count) {
		setWeighting(getWeighting() + count);
	}

	public LocalDateTime getStartLdt() {
		return DateUtils.asLdt(getStartDate());
	}

	public void setStartLdt(LocalDateTime startLdt) {
		setStartDate(DateUtils.asDate(startLdt));
	}

	public LocalDateTime getEndLdt() {
		return DateUtils.asLdt(getEndDate());
	}

	public void setEndLdt(LocalDateTime endLdt) {
		setEndDate(DateUtils.asDate(endLdt));
	}

	public String getPreviewGroupLabel() {
		final int[] groups = { 0, 0 };
		if (CollectionUtils.isEmpty(getPreviewEntries())) {
			return "";
		}
		LocalTime noon = LocalTime.of(12, 0);
		getPreviewEntries().forEach(scheduleEntry -> {
			if (scheduleEntry.getStartLdt().toLocalTime().isBefore(noon)) {
				groups[0]++;
			} else {
				groups[1]++;
			}
		});
		String label = "";
		if (groups[0] > 0) {
			label += "VM " + groups[0];
		}
		if (groups[1] > 0) {
			if (!StringUtils.isEmpty(label)) {
				label += " / ";
			}
			label += "NM " + groups[1];
		}
		return label;
	}

}