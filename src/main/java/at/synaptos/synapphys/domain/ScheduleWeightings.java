package at.synaptos.synapphys.domain;

import java.io.Serializable;

import com.google.gson.Gson;

import at.synaptos.synapphys.utils.StringUtils;

public class ScheduleWeightings implements Serializable {

	private static final long serialVersionUID = 3968404467757341390L;

	private Integer sameTimeOfDay;

	private Integer sameDayOfWeek;

	private Integer followPrevious;

	private ScheduleWeightings() {
		super();
		setSameDayOfWeek(1);
		setSameTimeOfDay(1);
		setFollowPrevious(1);
	}

	public static ScheduleWeightings getInstance(String weightingsSerial) {
		if (StringUtils.isEmpty(weightingsSerial)) {
			return new ScheduleWeightings();
		}
		Gson gson = new Gson();
		return gson.fromJson(weightingsSerial, ScheduleWeightings.class);
	}

	public String asWeightingsSerial() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public Integer getSameTimeOfDay() {
		return sameTimeOfDay;
	}

	public void setSameTimeOfDay(Integer sameTimeOfDay) {
		this.sameTimeOfDay = sameTimeOfDay;
	}

	public Integer getSameDayOfWeek() {
		return sameDayOfWeek;
	}

	public void setSameDayOfWeek(Integer sameDayOfWeek) {
		this.sameDayOfWeek = sameDayOfWeek;
	}

	public Integer getFollowPrevious() {
		return followPrevious;
	}

	public void setFollowPrevious(Integer followPrevious) {
		this.followPrevious = followPrevious;
	}

	public void decreaseSameDayOfWeek() {
		if (sameDayOfWeek > 0) {
			sameDayOfWeek--;
		}
	}

	public void increaseSameDayOfWeek() {
		if (sameDayOfWeek < 5) {
			sameDayOfWeek++;
		}
	}

	public void decreaseSameTimeOfDay() {
		if (sameTimeOfDay > 0) {
			sameTimeOfDay--;
		}
	}

	public void increaseSameTimeOfDay() {
		if (sameTimeOfDay < 5) {
			sameTimeOfDay++;
		}
	}

	public void decreaseFollowPrevious() {
		if (followPrevious > 0) {
			followPrevious--;
		}
	}

	public void increaseFollowPrevious() {
		if (followPrevious < 5) {
			followPrevious++;
		}
	}

}
