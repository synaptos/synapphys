package at.synaptos.synapphys.domain;

import at.synaptos.synapphys.model.AbstractEntity;

public abstract class AbstractTimelineEntry {

	private AbstractEntity entity;

	public AbstractEntity getEntity() {
		return entity;
	}

	public void setEntity(AbstractEntity entity) {
		this.entity = entity;
	}

	public AbstractTimelineEntry withEntity(AbstractEntity entity) {
		setEntity(entity);
		return this;
	}

}