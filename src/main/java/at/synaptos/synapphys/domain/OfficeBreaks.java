package at.synaptos.synapphys.domain;

import java.io.Serializable;

import com.google.gson.Gson;

import at.synaptos.synapphys.utils.StringUtils;

public class OfficeBreaks implements Serializable {

	private static final long serialVersionUID = -375503153613789957L;

	private Integer breakMins;

	private Integer afterPeriodOfMins;

	private OfficeBreaks() {
		super();
		setBreakMins(15);
		setAfterPeriodOfHours(2);
	}

	public static OfficeBreaks getInstance(String breaksSerial) {
		if (StringUtils.isEmpty(breaksSerial)) {
			return new OfficeBreaks();
		}
		Gson gson = new Gson();
		return gson.fromJson(breaksSerial, OfficeBreaks.class);
	}

	public String asBreaksSerial() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public Integer getBreakMins() {
		return breakMins;
	}

	public void setBreakMins(Integer breakMins) {
		this.breakMins = breakMins;
	}

	public Integer getAfterPeriodOfMins() {
		return afterPeriodOfMins;
	}

	public void setAfterPeriodOfMins(Integer afterPeriodOfMins) {
		this.afterPeriodOfMins = afterPeriodOfMins;
	}

	public Integer getAfterPeriodOfHours() {
		return getAfterPeriodOfMins() / 60;
	}

	public void setAfterPeriodOfHours(Integer afterPeriodOfHours) {
		setAfterPeriodOfMins(afterPeriodOfHours * 60);
	}

	public void decreaseAfterPeriodOfHours() {
		if (getAfterPeriodOfHours() > 0) {
			setAfterPeriodOfHours(getAfterPeriodOfHours() - 1);
		}
	}

	public void increaseAfterPeriodOfHours() {
		if (getAfterPeriodOfHours() < 5) {
			setAfterPeriodOfHours(getAfterPeriodOfHours() + 1);
		}
	}

}
