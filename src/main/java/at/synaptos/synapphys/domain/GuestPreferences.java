/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
@Named
public class GuestPreferences implements Serializable {

	private static final long serialVersionUID = -682512115935974499L;

	@Inject
	ConfigContext configContext;

	private String themeColor;

	private String menuLayout;

	private Map<String, String> themeColors;

	public String init() {
		themeColors = new HashMap<String, String>();
		themeColors.put("turquoise", "#00acac");
		themeColors.put("blue", "#2f8ee5");
		themeColors.put("orange", "#efa64c");
		themeColors.put("purple", "#6c76af");
		themeColors.put("pink", "#f16383");
		themeColors.put("light-blue", "#63c9f1");
		themeColors.put("green", "#57c279");
		themeColors.put("deep-purple", "#7e57c2");
		setThemeColor(configContext.getThemeColor());
		setMenuLayout(configContext.getMenuLayout());
		return null;
	}

	public String getThemeColor() {
		if (themeColor == null) {
			setThemeColor("green");
		}
		return themeColor;
	}

	public void setThemeColor(String themeColor) {
		this.themeColor = themeColor;
	}

	public String getMenuLayout() {
		if ("static".equals(menuLayout))
			return "menu-layout-static";
		else if ("overlay".equals(menuLayout))
			return "menu-layout-overlay";
		else if ("horizontal".equals(menuLayout))
			return "menu-layout-static menu-layout-horizontal";
		else
			return "menu-layout-static";
	}

	public void setMenuLayout(String menuLayout) {
		this.menuLayout = menuLayout;
	}

	public Map<String, String> getThemeColors() {
		return themeColors;
	}

	public void setThemeColors(Map<String, String> themeColors) {
		this.themeColors = themeColors;
	}

}
