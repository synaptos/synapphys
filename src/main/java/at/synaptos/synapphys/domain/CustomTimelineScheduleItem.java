package at.synaptos.synapphys.domain;

import java.io.Serializable;

import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.utils.DateUtils;

//TODO WQ Deprecated
public class CustomTimelineScheduleItem extends AbstractTimelineEntry implements Serializable {

	private static final long serialVersionUID = -4049072522904115520L;

	private CustomTimelineScheduleItem() {
	}

	public static CustomTimelineScheduleItem getInstance(ScheduleItem scheduleItem) {
		return (new CustomTimelineScheduleItem()).withEntity(scheduleItem);
	}

	@Override
	public ScheduleItem getEntity() {
		return (ScheduleItem) super.getEntity();
	}

	@Override
	public CustomTimelineScheduleItem withEntity(AbstractEntity entity) {
		return (CustomTimelineScheduleItem) super.withEntity(entity);
	}

	@Override
	public String toString() {
		return DateUtils.getDateAsDDMM(((ScheduleItem) super.getEntity()).getStartDate());
	}

}