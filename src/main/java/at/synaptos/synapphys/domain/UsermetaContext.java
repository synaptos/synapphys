package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.model.UsermetaProperty;
import at.synaptos.synapphys.service.WPUsermetaPropertyService;
import at.synaptos.synapphys.utils.GlobalI18n;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.StringUtils;

@Named
@SessionScoped
public class UsermetaContext implements Serializable {

	private static final long serialVersionUID = -8455764560520940321L;

	private static final String USERMETA_FIRSTNAME = "first_name";
	private static final String USERMETA_LASTNAME = "last_name";
	private static final String USERMETA_BILLING_FIRSTNAME = "billing_first_name";
	private static final String USERMETA_BILLING_LASTNAME = "billing_last_name";
	private static final String USERMETA_BILLING_COMPANY = "billing_company";
	private static final String USERMETA_BILLING_ADDRESS_STREET_1 = "billing_address_1";
	private static final String USERMETA_BILLING_ADDRESS_STREET_2 = "billing_address_2";
	private static final String USERMETA_BILLING_ADDRESS_POSTCODE = "billing_postcode";
	private static final String USERMETA_BILLING_ADDRESS_CITY = "billing_city";
	private static final String USERMETA_BILLING_EMAIL = "billing_email";
	private static final String USERMETA_BILLING_PHONE = "billing_phone";

	@Inject
	@GlobalI18n
	private I18nContext i18nContext;

	@Inject
	private WPUsermetaPropertyService wpUsermetaPropertyService;

	private Map<String, UsermetaProperty> usermetaPropertyMap;

	public UsermetaContext() {
		usermetaPropertyMap = new HashMap<String, UsermetaProperty>();
	}

	@PostConstruct
	private void init() {
		putAll(wpUsermetaPropertyService.getUsermetaProperties());
	}

	private List<UsermetaProperty> getUsermetaProperties() {
		return usermetaPropertyMap.values().stream().collect(Collectors.toList());
	}

	private void put(UsermetaProperty usermetaProperty) {
		usermetaPropertyMap.put(usermetaProperty.getKey(), usermetaProperty);
	}

	private void putAll(List<UsermetaProperty> usermetaProperties) {
		usermetaPropertyMap = new HashMap<String, UsermetaProperty>();
		usermetaProperties.forEach(usermetaProperty -> put(usermetaProperty));
	}

	private String getString(String key) {
		return getString(key, null);
	}

	private String getString(String key, String defaultValue) {
		if (key == null) {
			return null;
		}
		UsermetaProperty wpUsermeta = usermetaPropertyMap.get(key);
		if (wpUsermeta == null) {
			return defaultValue;
		}
		if (wpUsermeta.getValue() == null) {
			return "";
		}
		return wpUsermeta.getValue();
	}

	private void setString(String key, String value) {
		if (key == null) {
			return;
		}
		UsermetaProperty wpUsermeta = usermetaPropertyMap.get(key);
		if (wpUsermeta == null) {
			wpUsermeta = new UsermetaProperty(key, value);
			wpUsermeta.setChanged(true);
		} else if (wpUsermeta.getValue() == null) {
			if (value != null) {
				wpUsermeta.setValue(value);
				wpUsermeta.setChanged(true);
			}
		} else if (!wpUsermeta.getValue().equals(value)) {
			wpUsermeta.setValue(value);
			wpUsermeta.setChanged(true);
		}
		put(wpUsermeta);
	}

	public void updateProperties() {
		wpUsermetaPropertyService.updateUsermetaProperties(getUsermetaProperties());
	}

	public String getFirstname() {
		return getString(USERMETA_FIRSTNAME, "Demo");
	}

	public void setFirstname(String firstname) {
		setString(USERMETA_FIRSTNAME, firstname);
	}

	public String getLastname() {
		return getString(USERMETA_LASTNAME, "Demo");
	}

	public void setLastname(String lastname) {
		setString(USERMETA_LASTNAME, lastname);
	}

	public String getFullname() {
		if (getLastname() == null) {
			return "";
		}
		return (!StringUtils.isEmpty(getFirstname()) ? getFirstname() + " " : "") + getLastname();
	}

	public String getBillingFirstname() {
		return getString(USERMETA_BILLING_FIRSTNAME, "Demo");
	}

	public void setBillingFirstname(String firstname) {
		setString(USERMETA_BILLING_FIRSTNAME, firstname);
	}

	public String getBillingLastname() {
		return getString(USERMETA_BILLING_LASTNAME, "Demo");
	}

	public void setBillingLastname(String lastname) {
		setString(USERMETA_BILLING_LASTNAME, lastname);
	}

	public String getBillingFullname() {
		if (getBillingLastname() == null) {
			return "";
		}
		return (!StringUtils.isEmpty(getBillingFirstname()) ? getBillingFirstname() + " " : "") + getBillingLastname();
	}

	public String getBillingCompany() {
		return getString(USERMETA_BILLING_COMPANY,
				i18nContext.getString(GlobalNames.MESSAGE_KEY_USERMETA_BILLING_COMPANY));
	}

	public void setBillingCompany(String company) {
		setString(USERMETA_BILLING_COMPANY, company);
	}

	public String getBillingAddressStreet1() {
		return getString(USERMETA_BILLING_ADDRESS_STREET_1);
	}

	public void setBillingAddressStreet1(String street) {
		setString(USERMETA_BILLING_ADDRESS_STREET_1, street);
	}

	public String getBillingAddressStreet2() {
		return getString(USERMETA_BILLING_ADDRESS_STREET_2);
	}

	public void setBillingAddressStreet2(String street) {
		setString(USERMETA_BILLING_ADDRESS_STREET_2, street);
	}

	public String getBillingAddressPostcode() {
		return getString(USERMETA_BILLING_ADDRESS_POSTCODE);
	}

	public void setBillingAddressPostcode(String postcode) {
		setString(USERMETA_BILLING_ADDRESS_POSTCODE, postcode);
	}

	public String getBillingAddressCity() {
		return getString(USERMETA_BILLING_ADDRESS_CITY);
	}

	public void setBillingAddressCity(String city) {
		setString(USERMETA_BILLING_ADDRESS_CITY, city);
	}

	public String getBillingEmail() {
		return getString(USERMETA_BILLING_EMAIL);
	}

	public void setBillingEmail(String email) {
		setString(USERMETA_BILLING_EMAIL, email);
	}

	public String getBillingPhone() {
		return getString(USERMETA_BILLING_PHONE);
	}

	public void setBillingPhone(String phone) {
		setString(USERMETA_BILLING_PHONE, phone);
	}

}
