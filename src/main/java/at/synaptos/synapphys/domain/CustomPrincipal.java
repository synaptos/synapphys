package at.synaptos.synapphys.domain;

import org.jboss.security.SimplePrincipal;

public class CustomPrincipal extends SimplePrincipal {

	private static final long serialVersionUID = -1699708530434818777L;
	
	private String homeUrl;

	private String logoutUrl;

	private String accountUrl;

	public CustomPrincipal(String name) {
		super(name);
	}

	public String getHomeUrl() {
		return homeUrl;
	}

	public void setHomeUrl(String homeUrl) {
		this.homeUrl = homeUrl;
	}

	public String getLogoutUrl() {
		return logoutUrl;
	}

	public void setLogoutUrl(String logoutUrl) {
		this.logoutUrl = logoutUrl;
	}
	
	public String getAccountUrl() {
		return accountUrl;
	}

	public void setAccountUrl(String accountUrl) {
		this.accountUrl = accountUrl;
	}

	@Override
	public String toString() {
	    return this.getClass().getName()+
	    		"{name="+getName()+
	    		",homeUrl="+homeUrl+
	    		",logoutUrl="+logoutUrl+
	    		",accountUrl="+accountUrl+"}";
	}

}