package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import at.synaptos.synapphys.model.BodymapItem;
import at.synaptos.synapphys.model.FindingType;

public class FindingNode implements Serializable {

	private static final long serialVersionUID = -302254304608272384L;

	private transient I18nContext i18n;

	private Long bodymapItemId;

	private FindingType findingType;

	private FindingType findingTypeModifier;

	private Double findingPositionX;

	private Double findingPositionY;

	private String bodySegment;

	private Integer valueF;

	private Integer valueT;

	private String note;

	private String text;

	private Double tipPositionX;

	private Double tipPositionY;

	private Integer sequenceNo;

	private FindingNode() {
	}

	private FindingNode(BodymapItem bodymapItem, I18nContext i18n) {
		setI18n(i18n);
		setBodymapItemId(bodymapItem.getId());
		setFindingType(bodymapItem.getFindingType());
		setFindingTypeModifier(bodymapItem.getFindingTypeModifier());
		setFindingPositionX(bodymapItem.getFindingPositionX());
		setFindingPositionY(bodymapItem.getFindingPositionY());
		setBodySegment(bodymapItem.getBodySegment());
		setValueF(bodymapItem.getValueF());
		setValueT(bodymapItem.getValueT());
		setNote(bodymapItem.getFindingNote());
		setText(bodymapItem.getFindingText());
		setTipPositionX(bodymapItem.getTipPositionX());
		setTipPositionY(bodymapItem.getTipPositionY());
		setSequenceNo(bodymapItem.getSequenceNo());
	}

	public static FindingNode getInstance() {
		return new FindingNode();
	}

	public static FindingNode getInstance(BodymapItem bodymapItem) {
		return new FindingNode(bodymapItem, null);
	}

	public static FindingNode getInstance(BodymapItem bodymapItem, I18nContext i18n) {
		return new FindingNode(bodymapItem, i18n);
	}

	private I18nContext getI18n() {
		return i18n;
	}

	private void setI18n(I18nContext i18n) {
		this.i18n = i18n;
	}

	public Long getBodymapItemId() {
		return bodymapItemId;
	}

	public void setBodymapItemId(Long bodymapItemId) {
		this.bodymapItemId = bodymapItemId;
	}

	public FindingType getFindingType() {
		return findingType;
	}

	public void setFindingType(FindingType findingType) {
		this.findingType = findingType;
	}

	public FindingType getFindingTypeModifier() {
		return findingTypeModifier;
	}

	public void setFindingTypeModifier(FindingType findingTypeModifier) {
		this.findingTypeModifier = findingTypeModifier;
	}

	public Double getFindingPositionX() {
		return findingPositionX;
	}

	public void setFindingPositionX(Double findingPositionX) {
		this.findingPositionX = findingPositionX;
	}

	public Double getFindingPositionY() {
		return findingPositionY;
	}

	public void setFindingPositionY(Double findingPositionY) {
		this.findingPositionY = findingPositionY;
	}

	public String getBodySegment() {
		return bodySegment;
	}

	public void setBodySegment(String bodySegment) {
		this.bodySegment = bodySegment;
	}

	public Integer getValueF() {
		return valueF;
	}

	public void setValueF(Integer valueF) {
		this.valueF = valueF;
	}

	public String getLabelF() {
		if (getFindingTypeEffective() == null) {
			return "?";
		}
		if (getFindingTypeEffective().getLabelF() == null) {
			return "?";
		}
		if (getI18n() == null) {
			return "?";
		}
		return getI18n().getString(getFindingTypeEffective().getLabelF());
	}

	public Integer getMinValueF() {
		if (getFindingTypeEffective() == null) {
			return 0;
		}
		return getFindingTypeEffective().getMinValueF();
	}

	public Integer getMaxValueF() {
		if (getFindingTypeEffective() == null) {
			return 0;
		}
		return getFindingTypeEffective().getMaxValueF();
	}

	public Integer getStepF() {
		if (getFindingTypeEffective() == null) {
			return 0;
		}
		return getFindingTypeEffective().getStepF();
	}

	public Integer getValueT() {
		return valueT;
	}

	public void setValueT(Integer valueT) {
		this.valueT = valueT;
	}

	public String getLabelT() {
		if (getFindingTypeEffective() == null) {
			return "?";
		}
		if (getFindingTypeEffective().getLabelT() == null) {
			return "?";
		}
		if (getI18n() == null) {
			return "?";
		}
		return getI18n().getString(getFindingTypeEffective().getLabelT());
	}

	public Integer getMinValueT() {
		if (getFindingTypeEffective() == null) {
			return 0;
		}
		return getFindingTypeEffective().getMinValueT();
	}

	public Integer getMaxValueT() {
		if (getFindingTypeEffective() == null) {
			return 0;
		}
		return getFindingTypeEffective().getMaxValueT();
	}

	public Integer getStepT() {
		if (getFindingTypeEffective() == null) {
			return 0;
		}
		return getFindingTypeEffective().getStepT();
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		if (note != null && note.isEmpty()) {
			note = null;
		}
		this.note = note;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		if (text != null && text.isEmpty()) {
			text = null;
		}
		this.text = text;
	}

	public Double getTipPositionX() {
		return tipPositionX;
	}

	public void setTipPositionX(Double tipPositionX) {
		this.tipPositionX = tipPositionX;
	}

	public Double getTipPositionY() {
		return tipPositionY;
	}

	public void setTipPositionY(Double tipPositionX) {
		this.tipPositionY = tipPositionX;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{ bodymapItemId=" + bodymapItemId + ", findingType=" + findingType + " }";
	}

	public boolean isEmpty() {
		return getValueF() == null && getValueT() == null && getNote() == null;
	}

	public Integer getValueOrDefaultF() {
		return (valueF != null) ? valueF
				: (getFindingTypeEffective() != null) ? getFindingTypeEffective().getDefaultValueF() : null;
	}

	public void setValueOrDefaultF(Integer valueF) {
		setValueF(valueF);
	}

	public Integer getValueOrDefaultT() {
		return (valueT != null) ? valueT
				: (getFindingTypeEffective() != null) ? getFindingTypeEffective().getDefaultValueT() : null;
	}

	public void setValueOrDefaultT(Integer valueT) {
		setValueT(valueT);
	}

	public Double getTipPositionOrDefaultX() {
		if (isEmpty()) {
			return null;
		}
		return (tipPositionX != null) ? tipPositionX : 0d;
	}

	public void setTipPositionOrDefaultX(Double tipPositionX) {
		setTipPositionX(tipPositionX);
	}

	public Double getTipPositionOrDefaultY() {
		if (isEmpty()) {
			return null;
		}
		return (tipPositionY != null) ? tipPositionY : 0d;
	}

	public void setTipPositionOrDefaultY(Double tipPositionY) {
		setTipPositionY(tipPositionY);
	}

	public FindingType getFindingTypeEffective() {
		return getFindingTypeModifier() != null ? getFindingTypeModifier() : getFindingType();
	}

	public List<FindingType> getFindingTypeModifiers() {
		List<FindingType> findingTypes = new ArrayList<FindingType>();
		if (getFindingType() != null
				&& (getFindingType().name().endsWith("_AREA") || getFindingType().name().endsWith("_VECTOR"))) {
			for (FindingType findingType : FindingType.values()) {
				if (!(findingType.name().endsWith("_AREA") || findingType.name().endsWith("_VECTOR"))) {
					findingTypes.add(findingType);
				}
			}
		}
		return findingTypes;

	}

}
