package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.model.InstanceProperty;
import at.synaptos.synapphys.model.PaymentMethod;
import at.synaptos.synapphys.service.InstancePropertyService;
import at.synaptos.synapphys.utils.GlobalI18n;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.StringUtils;

@Named
@SessionScoped
public class ConfigContext implements Serializable {

	private static final long serialVersionUID = -8455764560520940321L;

	private static final String INSTANCE_PROPERTY_LOGO_FILENAME = "logo.filename";
	private static final String INSTANCE_PROPERTY_USER_LASTLOGIN = "user.lastlogin";

	private static final String INSTANCE_PROPERTY_PAYMENT_SHOW_METHODS = "payment.showMethods";

	private static final String INSTANCE_PROPERTY_BILLING_HEADER = "billing.header";
	private static final String INSTANCE_PROPERTY_BILLING_FOOTER = "billing.footer";

	private static final String INSTANCE_PROPERTY_BILLING_SHOW_PREVIEW = "billing.showPreview";

	private static final String INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_IBAN = "billing.bankAccount.iban";
	private static final String INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_BIC = "billing.bankAccount.bic";
	private static final String INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_BANKNAME = "billing.bankAccount.bankname";

	private static final String INSTANCE_PROPERTY_OFFICE_HOURS = "schedule.officeHours";
	private static final String INSTANCE_PROPERTY_OFFICE_BREAKS = "schedule.officeBreaks";
	private static final String INSTANCE_PROPERTY_SCHEDULE_WEIGHTINGS = "schedule.weightings";

	private static final String INSTANCE_PROPERTY_MENU_LAYOUT = "menu.layout";
	private static final String INSTANCE_PROPERTY_THEME_COLOR = "theme.color";

	// Cashbox (CB)
	//
	private static final String INSTANCE_PROPERTY_FT_CASHBOX_ID = "fiskaltrust.cashboxId";
	private static final String INSTANCE_PROPERTY_FT_ACCESS_TOKEN = "fiskaltrust.accessToken";
	private static final String INSTANCE_PROPERTY_CB_TERMINAL_ID = "fiskaltrust.terminalId";
	//

	@Inject
	@GlobalI18n
	private I18nContext i18nContext;

	@Inject
	private InstancePropertyService instancePropertyService;

	@Inject
	private GuestPreferences guestPreferences;

	private Map<String, InstanceProperty> instancePropertyMap;

	private Date lastloginDate = null;

	private LocalTime scheduleMinTime;

	private LocalTime scheduleMaxTime;

	@PostConstruct
	private void init() {
		putAll(instancePropertyService.getInstanceProperties());
		Long userLastLogin = getUserLastlogin();
		if (userLastLogin != null) {
			setLastloginDate(userLastLogin);
		}
		setUserLastlogin(System.currentTimeMillis());
		setScheduleMinTime(LocalTime.of(5, 0));
		setScheduleMaxTime(LocalTime.of(19, 0));
		updateProperties();
		guestPreferences.setThemeColor(getThemeColor());
		guestPreferences.setMenuLayout(getMenuLayout());
	}

	public ConfigContext() {
		instancePropertyMap = new HashMap<String, InstanceProperty>();
	}

	private List<InstanceProperty> getInstanceProperties() {
		return instancePropertyMap.values().stream().collect(Collectors.toList());
	}

	private void put(InstanceProperty instanceProperty) {
		instancePropertyMap.put(instanceProperty.getKey(), instanceProperty);
	}

	private void putAll(List<InstanceProperty> instanceProperties) {
		instancePropertyMap = new HashMap<String, InstanceProperty>();
		instanceProperties.forEach(instanceProperty -> put(instanceProperty));
	}

	private String getString(String key) {
		return getString(key, null);
	}

	private String getString(String key, String defaultValue) {
		if (key == null) {
			return null;
		}
		InstanceProperty instanceProperty = instancePropertyMap.get(key);
		if (instanceProperty == null) {
			return defaultValue;
		}
		if (instanceProperty.getValue() == null) {
			return "";
		}
		return instanceProperty.getValue();
	}

	private void setString(String key, String value) {
		if (key == null) {
			return;
		}
		InstanceProperty instanceProperty = instancePropertyMap.get(key);
		if (instanceProperty == null) {
			instanceProperty = new InstanceProperty(key, value);
			instanceProperty.setChanged(true);
		} else if (instanceProperty.getValue() == null) {
			if (value != null) {
				instanceProperty.setValue(value);
				instanceProperty.setChanged(true);
			}
		} else if (!instanceProperty.getValue().equals(value)) {
			instanceProperty.setValue(value);
			instanceProperty.setChanged(true);
		}
		put(instanceProperty);
	}

	public void updateProperties() {
		instancePropertyService.updateInstanceProperties(getInstanceProperties());
	}

	public Date getLastloginDate() {
		return lastloginDate;
	}

	private void setLastloginDate(Date lastloginDate) {
		this.lastloginDate = lastloginDate;
	}

	private void setLastloginDate(long lastloginDate) {
		setLastloginDate(new Date(lastloginDate));
	}

	public LocalTime getScheduleMinTime() {
		return scheduleMinTime;
	}

	public void setScheduleMinTime(LocalTime scheduleMinTime) {
		this.scheduleMinTime = scheduleMinTime;
	}

	public LocalTime getScheduleMaxTime() {
		return scheduleMaxTime;
	}

	public void setScheduleMaxTime(LocalTime scheduleMaxTime) {
		this.scheduleMaxTime = scheduleMaxTime;
	}

	public String getLogoFilename() {
		return getString(INSTANCE_PROPERTY_LOGO_FILENAME);
	}

	public void setLogoFilename(String filename) {
		setString(INSTANCE_PROPERTY_LOGO_FILENAME, filename);
	}

	public String getLogoUrl() {
		String filename = getLogoFilename();
		if (StringUtils.isEmpty(filename)) {
			return null;
		}
		String logoUrl = GlobalNames.EXTERNAL_RESOURCE_URL + "/" + filename;
		return logoUrl;
	}

	public Long getUserLastlogin() {
		String lastlogin = getString(INSTANCE_PROPERTY_USER_LASTLOGIN);
		if (StringUtils.isEmpty(lastlogin)) {
			return null;
		}
		return Long.valueOf(lastlogin);
	}

	public void setUserLastlogin(Long lastlogin) {
		setString(INSTANCE_PROPERTY_USER_LASTLOGIN, lastlogin != null ? String.valueOf(lastlogin) : null);
	}

	public String[] getShowPaymentMethods() {
		String value = getString(INSTANCE_PROPERTY_PAYMENT_SHOW_METHODS, PaymentMethod.MC + "," + PaymentMethod.CC);
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return value.split(",");
	}

	public void setShowPaymentMethods(String[] showPaymentMethods) {
		StringBuilder sb = new StringBuilder();
		for (String t : showPaymentMethods) {
			sb.append(t).append(",");
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 1);
		}
		setString(INSTANCE_PROPERTY_PAYMENT_SHOW_METHODS, sb.toString());
	}

	public String getBillingHeader() {
		return getString(INSTANCE_PROPERTY_BILLING_HEADER,
				i18nContext.getString(GlobalNames.MESSAGE_KEY_INSTANCE_PROPERTY_BILLING_HEADER));
	}

	public void setBillingHeader(String header) {
		setString(INSTANCE_PROPERTY_BILLING_HEADER, header);
	}

	public String getBillingFooter() {
		return getString(INSTANCE_PROPERTY_BILLING_FOOTER,
				i18nContext.getString(GlobalNames.MESSAGE_KEY_INSTANCE_PROPERTY_BILLING_FOOTER));
	}

	public void setBillingFooter(String footer) {
		setString(INSTANCE_PROPERTY_BILLING_FOOTER, footer);
	}

	public boolean getBillingShowPreview() {
		return Boolean.valueOf(getString(INSTANCE_PROPERTY_BILLING_SHOW_PREVIEW, "true"));
	}

	public void setBillingShowPreview(boolean show) {
		setString(INSTANCE_PROPERTY_BILLING_SHOW_PREVIEW, String.valueOf(show));
	}

	public OfficeHours getInstancePropertyOfficeHours() {
		return OfficeHours.getInstance(getString(INSTANCE_PROPERTY_OFFICE_HOURS));
	}

	public void setInstancePropertyOfficeHours(OfficeHours officeHours) {
		if (officeHours == null) {
			return;
		}
		String hoursSerial = officeHours.asHoursSerial();
		setString(INSTANCE_PROPERTY_OFFICE_HOURS,
				!StringUtils.isEmpty(hoursSerial) ? String.valueOf(hoursSerial) : null);
	}

	public OfficeBreaks getInstancePropertyOfficeBreaks() {
		return OfficeBreaks.getInstance(getString(INSTANCE_PROPERTY_OFFICE_BREAKS));
	}

	public void setInstancePropertyOfficeBreaks(OfficeBreaks officeBreaks) {
		if (officeBreaks == null) {
			return;
		}
		String breaksSerial = officeBreaks.asBreaksSerial();
		setString(INSTANCE_PROPERTY_OFFICE_BREAKS,
				!StringUtils.isEmpty(breaksSerial) ? String.valueOf(breaksSerial) : null);
	}

	public ScheduleWeightings getInstancePropertyScheduleWeightings() {
		return ScheduleWeightings.getInstance(getString(INSTANCE_PROPERTY_SCHEDULE_WEIGHTINGS));
	}

	public void setInstancePropertyScheduleWeightings(ScheduleWeightings scheduleWeightings) {
		if (scheduleWeightings == null) {
			return;
		}
		String weightingsSerial = scheduleWeightings.asWeightingsSerial();
		setString(INSTANCE_PROPERTY_SCHEDULE_WEIGHTINGS,
				!StringUtils.isEmpty(weightingsSerial) ? String.valueOf(weightingsSerial) : null);
	}

	public String getBillingBankAccountIban() {
		return getString(INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_IBAN);
	}

	public void setBillingBankAccountIban(String iban) {
		setString(INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_IBAN, iban);
	}

	public String getBillingBankAccountBic() {
		return getString(INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_BIC);
	}

	public void setBillingBankAccountBic(String bic) {
		setString(INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_BIC, bic);
	}

	public String getBillingBankAccountBankname() {
		return getString(INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_BANKNAME);
	}

	public void setBillingBankAccountBankname(String bankname) {
		setString(INSTANCE_PROPERTY_BILLING_BANK_ACCOUNT_BANKNAME, bankname);
	}

	public String getMenuLayout() {
		return getString(INSTANCE_PROPERTY_MENU_LAYOUT, "static");
	}

	public void setMenuLayout(String menuLayout) {
		setString(INSTANCE_PROPERTY_MENU_LAYOUT, menuLayout);
		guestPreferences.setMenuLayout(menuLayout);
	}

	public String getThemeColor() {
		return getString(INSTANCE_PROPERTY_THEME_COLOR, "green");
	}

	public void setThemeColor(String themeColor) {
		setString(INSTANCE_PROPERTY_THEME_COLOR, themeColor);
		guestPreferences.setThemeColor(themeColor);
	}

	public String getInstancePropertyFtCashboxId() {
		return getString(INSTANCE_PROPERTY_FT_CASHBOX_ID);
	}

	public void setInstancePropertyFtCashboxId(String cashboxId) {
		setString(INSTANCE_PROPERTY_FT_CASHBOX_ID, cashboxId);
	}

	public String getInstancePropertyFtAccessToken() {
		return getString(INSTANCE_PROPERTY_FT_ACCESS_TOKEN);
	}

	public void setInstancePropertyFtAccessToken(String accessToken) {
		setString(INSTANCE_PROPERTY_FT_ACCESS_TOKEN, accessToken);
	}

	public String getInstancePropertyCbTerminalId() {
		return getString(INSTANCE_PROPERTY_CB_TERMINAL_ID);
	}

	public void setInstancePropertyCbTerminalId(String terminalId) {
		setString(INSTANCE_PROPERTY_CB_TERMINAL_ID, terminalId);
	}

}
