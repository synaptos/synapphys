package at.synaptos.synapphys.domain;

import org.primefaces.model.ScheduleEvent;

public interface CustomScheduleEvent extends ScheduleEvent {
	
    public String getRendering();

}