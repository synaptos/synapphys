package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

import at.synaptos.synapphys.utils.DateUtils;

public class OfficeHoursPerDay implements Serializable {

	private static final long serialVersionUID = 7493982210700235754L;

	private LocalTime mornStartTime;

	private LocalTime mornEndTime;

	private Integer mornWeighting;

	private LocalTime aftnStartTime;

	private LocalTime aftnEndTime;

	private Integer aftnWeighting;

	private OfficeHoursPerDay() {
		super();
	}

	public static OfficeHoursPerDay getInstance() {
		return new OfficeHoursPerDay();
	}

	public static OfficeHoursPerDay getInstance(LocalTime mornStartTime, LocalTime mornEndTime, int mornWeighting,
			LocalTime aftnStartTime, LocalTime aftnEndTime, int aftnWeighting) {
		OfficeHoursPerDay officeHoursPerDay = OfficeHoursPerDay.getInstance();
		officeHoursPerDay.setMornStartTime(mornStartTime);
		officeHoursPerDay.setMornEndTime(mornEndTime);
		officeHoursPerDay.setMornWeighting(mornWeighting);
		officeHoursPerDay.setAftnStartTime(aftnStartTime);
		officeHoursPerDay.setAftnEndTime(aftnEndTime);
		officeHoursPerDay.setAftnWeighting(aftnWeighting);
		return officeHoursPerDay;
	}

	public LocalTime getMornStartTime() {
		return mornStartTime;
	}

	public void setMornStartTime(LocalTime mornStartTime) {
		this.mornStartTime = mornStartTime;
	}

	public LocalTime getMornEndTime() {
		return mornEndTime;
	}

	public void setMornEndTime(LocalTime mornEndTime) {
		this.mornEndTime = mornEndTime;
	}

	public Integer getMornWeighting() {
		return mornWeighting;
	}

	public void setMornWeighting(Integer mornWeighting) {
		this.mornWeighting = mornWeighting;
	}

	public LocalTime getAftnStartTime() {
		return aftnStartTime;
	}

	public void setAftnStartTime(LocalTime aftnStartTime) {
		this.aftnStartTime = aftnStartTime;
	}

	public LocalTime getAftnEndTime() {
		return aftnEndTime;
	}

	public void setAftnEndTime(LocalTime aftnEndTime) {
		this.aftnEndTime = aftnEndTime;
	}

	public Integer getAftnWeighting() {
		return aftnWeighting;
	}

	public void setAftnWeighting(Integer aftnWeighting) {
		this.aftnWeighting = aftnWeighting;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{mornStartTime=" + mornStartTime + ",mornEndTime=" + mornEndTime
				+ ",mornWeighting=" + mornWeighting + ",aftnStartTime=" + aftnStartTime + ",aftnEndTime=" + aftnEndTime
				+ "}";
	}

	public Date getMornStartDate() {
		return DateUtils.asDate(getMornStartTime());
	}

	public void setMornStartDate(Date mornStartDate) {
		setMornStartTime(mornStartDate != null ? DateUtils.asLdt(mornStartDate).toLocalTime() : null);
	}

	public Date getMornEndDate() {
		return DateUtils.asDate(getMornEndTime());
	}

	public void setMornEndDate(Date mornEndDate) {
		setMornEndTime(mornEndDate != null ? DateUtils.asLdt(mornEndDate).toLocalTime() : null);
	}

	public Date getAftnStartDate() {
		return DateUtils.asDate(getAftnStartTime());
	}

	public void setAftnStartDate(Date aftnStartDate) {
		setAftnStartTime(aftnStartDate != null ? DateUtils.asLdt(aftnStartDate).toLocalTime() : null);
	}

	public Date getAftnEndDate() {
		return DateUtils.asDate(getAftnEndTime());
	}

	public void setAftnEndDate(Date aftnEndDate) {
		setAftnEndTime(aftnEndDate != null ? DateUtils.asLdt(aftnEndDate).toLocalTime() : null);
	}

	public void increaseMornWeighting() {
		if (mornWeighting < 5) {
			mornWeighting++;
		}
	}

	public void decreaseMornWeighting() {
		if (mornWeighting != 0) {
			mornWeighting--;
		}
	}

	public void increaseAftnWeighting() {
		if (aftnWeighting < 5) {
			aftnWeighting++;
		}
	}

	public void decreaseAftnWeighting() {
		if (aftnWeighting != 0) {
			aftnWeighting--;
		}
	}

}
