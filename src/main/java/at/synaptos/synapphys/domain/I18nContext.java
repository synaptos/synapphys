package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ResourceBundle;

public class I18nContext implements Serializable {

	private static final long serialVersionUID = 1546809784805059303L;

	private ResourceBundle resourceBundle;

	public I18nContext(ResourceBundle resourceBundle) {
		this.resourceBundle = resourceBundle;
	}

	public String getString(String key) {
		return resourceBundle.getString(key);
	}

	public String getString(String key, Object... arguments) {
		return MessageFormat.format(resourceBundle.getString(key), arguments);
	}

}
