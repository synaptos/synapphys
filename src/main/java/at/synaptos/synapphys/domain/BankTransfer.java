package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.StringUtils;

public class BankTransfer implements Serializable {

	private static final long serialVersionUID = -2368436365393932776L;

	private String iban = "";

	private String bookingText = "";

	private LocalDate bookingLd = LocalDate.MIN;

	private LocalDate valueLd = LocalDate.MIN;

	private Double amount = 0d;

	private String currency = "";

	private String bookingNo = "";

	private String referenceNo = "";

	private BankTransfer() {
		super();
	}

	public static BankTransfer getInstance(String line) {
		BankTransfer instance = new BankTransfer();

		if (!StringUtils.isEmpty(line)) {
			// AT671400096510367305;SVNR:4001240266 LEISTUNG 16-08:1.557,7 5
			// VD/000004391 BUNDATWWXXX AT320100000005690006 AMS
			// KAERNTEN;06.09.2016;06.09.2016;+1.557,75;EUR
			String[] items = line.split("\\;");

			if (items.length > 0) {
				instance.setIban(items[0]);
			}
			if (items.length > 1) {
				instance.setBookingText(items[1]);
				Pattern pattern = Pattern.compile("[A-Z]{2}/[0-9]{9}");
				Matcher matcher = pattern.matcher(instance.getBookingText());
				if (matcher.find()) {
					instance.setBookingNo(matcher.group());
					pattern = Pattern.compile("20[0-9]{2}-[0-9]{1,10}");
					matcher = pattern.matcher(instance.getBookingText().substring(0, matcher.start()));
					if (matcher.find()) {
						instance.setReferenceNo(matcher.group());
					}
				}
			}
			if (items.length > 2) {
				instance.setBookingLd(LocalDate.parse(items[2], DateTimeFormatter.ofPattern("dd.MM.yyyy")));
			}
			if (items.length > 3) {
				instance.setValueLd(LocalDate.parse(items[3], DateTimeFormatter.ofPattern("dd.MM.yyyy")));
			}
			if (items.length > 4) {
				NumberFormat format = NumberFormat.getInstance(Locale.getDefault());
				try {
					Number number = format.parse(items[4].replace("+", ""));
					instance.setAmount(number.doubleValue());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if (items.length > 5) {
				instance.setCurrency(items[5]);
			}
		}

		return instance;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Date getBookingDate() {
		return DateUtils.asDate(getBookingLd());
	}

	public void setBookingDate(Date bookingDate) {
		setBookingLd(DateUtils.asLdt(bookingDate).toLocalDate());
	}

	public LocalDate getBookingLd() {
		return bookingLd;
	}

	public void setBookingLd(LocalDate bookingLd) {
		this.bookingLd = bookingLd;
	}

	public String getBookingText() {
		return bookingText;
	}

	public void setBookingText(String bookingText) {
		this.bookingText = bookingText;
	}

	public LocalDate getValueLd() {
		return valueLd;
	}

	public void setValueLd(LocalDate valueLd) {
		this.valueLd = valueLd;
	}

	public Date getValueDate() {
		return DateUtils.asDate(getValueLd());
	}

	public void setValueDate(Date valueDate) {
		setValueLd(DateUtils.asLdt(valueDate).toLocalDate());
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

}
