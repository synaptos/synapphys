package at.synaptos.synapphys.domain;

import java.io.Serializable;

import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.utils.DateUtils;

//TODO WQ Deprecated
public class CustomTimelineSessionDocumentation extends AbstractTimelineEntry implements Serializable {

	private static final long serialVersionUID = -2264750473833470008L;

	private CustomTimelineSessionDocumentation() {
	}

	public static CustomTimelineSessionDocumentation getInstance(SessionDocumentation sessionDocumentation) {
		return (new CustomTimelineSessionDocumentation()).withEntity(sessionDocumentation);
	}

	@Override
	public SessionDocumentation getEntity() {
		return (SessionDocumentation) super.getEntity();
	}

	@Override
	public CustomTimelineSessionDocumentation withEntity(AbstractEntity entity) {
		return (CustomTimelineSessionDocumentation) super.withEntity(entity);
	}

	@Override
	public String toString() {
		return DateUtils.getDateAsDDMM(((SessionDocumentation) super.getEntity()).getStartDate());
	}

}