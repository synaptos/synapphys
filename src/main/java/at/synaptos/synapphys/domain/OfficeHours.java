package at.synaptos.synapphys.domain;

import java.io.Serializable;
import java.time.LocalTime;

import com.google.gson.Gson;

import at.synaptos.synapphys.utils.StringUtils;

public class OfficeHours implements Serializable {
	
	private static final long serialVersionUID = 3480182933026393048L;

	private OfficeHoursPerDay[] hours = new OfficeHoursPerDay[] {
			OfficeHoursPerDay.getInstance(LocalTime.of(7, 0), LocalTime.of(12, 0), 1, LocalTime.of(13, 0), LocalTime.of(17, 0), 1),
			OfficeHoursPerDay.getInstance(LocalTime.of(7, 0), LocalTime.of(12, 0), 1, LocalTime.of(13, 0), LocalTime.of(17, 0), 1),
			OfficeHoursPerDay.getInstance(LocalTime.of(7, 0), LocalTime.of(12, 0), 1, LocalTime.of(13, 0), LocalTime.of(17, 0), 1),
			OfficeHoursPerDay.getInstance(LocalTime.of(7, 0), LocalTime.of(12, 0), 1, LocalTime.of(13, 0), LocalTime.of(17, 0), 1),
			OfficeHoursPerDay.getInstance(LocalTime.of(7, 0), LocalTime.of(12, 0), 1, LocalTime.of(13, 0), LocalTime.of(17, 0), 1),
			OfficeHoursPerDay.getInstance(),
			OfficeHoursPerDay.getInstance(),
	};
	
	private OfficeHours() {
		super();
	}
	
	public static OfficeHours getInstance() {
		return new OfficeHours();		
	}
	
	public static OfficeHours getInstance(String hoursSerial) {
		if (StringUtils.isEmpty(hoursSerial)) {
			return new OfficeHours();
		}
		Gson gson = new Gson();
		return gson.fromJson(hoursSerial, OfficeHours.class);
	}

	public String asHoursSerial() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public OfficeHoursPerDay hoursPerDay(int day) {
		return hours[day];
	}

}
