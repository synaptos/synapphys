package at.synaptos.synapphys.exception;

import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;

import org.omnifaces.exceptionhandler.FacesMessageExceptionHandler;

import at.synaptos.synapphys.utils.TagAttribute;

public class ViewMessageExceptionHandler extends FacesMessageExceptionHandler {

	public ViewMessageExceptionHandler(ExceptionHandler wrapped) {
		super(wrapped);
	}

	@Override
	public void handle() throws FacesException {
		for (Iterator<ExceptionQueuedEvent> iter = getUnhandledExceptionQueuedEvents().iterator(); iter.hasNext();) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fehler: so geht's nicht!", createFatalMessage(iter.next().getContext().getException()));
			FacesContext.getCurrentInstance().addMessage(TagAttribute.MESSAGE_FOR_GLOBAL_PANEL, message);
			iter.remove();
		}

		getWrapped().handle();		
	}

}
