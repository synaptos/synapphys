package at.synaptos.synapphys.exception;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

import org.omnifaces.exceptionhandler.FacesMessageExceptionHandlerFactory;

public class ViewMessageExceptionHandlerFactory extends FacesMessageExceptionHandlerFactory {

	public ViewMessageExceptionHandlerFactory(ExceptionHandlerFactory wrapped) {
		super(wrapped);
	}

	@Override
	public ExceptionHandler getExceptionHandler() {
		return new ViewMessageExceptionHandler(getWrapped().getExceptionHandler());
	}

}
