package at.synaptos.synapphys.fiskaltrust;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.jboss.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingSignature;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.HttpUtils;
import at.synaptos.synapphys.utils.StringUtils;

public class ReceiptSigner {

	private static final Logger logger = Logger.getLogger(ReceiptSigner.class);

	private static final Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class,
			(JsonSerializer<LocalDateTime>) (ldt, type, jsonSerializationContext) -> {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				ZonedDateTime ldtZoned = ldt.atZone(ZoneId.systemDefault());
				ZonedDateTime utcZoned = ldtZoned.withZoneSameInstant(ZoneId.of("UTC"));
				return new JsonPrimitive(utcZoned.format(formatter));
			})
			.registerTypeAdapter(LocalDateTime.class,
					(JsonDeserializer<LocalDateTime>) (json, type, jsonDeserializationContext) -> ZonedDateTime
							.parse(json.getAsJsonPrimitive().getAsString()).toLocalDateTime())
			.registerTypeAdapter(SignatureItem.Format.class,
					(JsonDeserializer<SignatureItem.Format>) (json, type,
							jsonDeserializationContext) -> SignatureItem.Format.fromKey(json.getAsLong()))
			.registerTypeAdapter(SignatureItem.Type.class, (JsonDeserializer<SignatureItem.Type>) (json, type,
					jsonDeserializationContext) -> SignatureItem.Type.fromKey(json.getAsLong()))
			.create();

	private static final String FT_ENDPOINT = "https://signaturcloud-sandbox.fiskaltrust.at/json/sign";

	public static BillingSignature sign(Billing billing) {
		logger.trace("billing=" + billing);

		if (billing == null) {
			logger.info("There's no billing given.");
			return null;
		} else if (StringUtils.isEmpty(billing.getFtCashboxId())) {
			logger.info("Fiskaltrust cashbox identification is missing.");
			return null;
		} else if (StringUtils.isEmpty(billing.getFtAccessToken())) {
			logger.info("Fiskaltrust Access token is missing.");
			return null;
		} else if (StringUtils.isEmpty(billing.getCbTerminalId())) {
			logger.info("Cashbox terminal identification is missing.");
			return null;
		} else if (StringUtils.isEmpty(billing.getReferenceNo())) {
			logger.info("Cashbox receipt reference is missing.");
			return null;
		} else if (billing.getBillingLdt() == null) {
			logger.info("Cashbox receipt moment is missing.");
			return null;
		}

		ReceiptRequest receiptRequest = ReceiptRequest.getInstance(billing);

		if (receiptRequest == null) {
			logger.info("Couldn't create receipt request.");
			return null;
		}

		String reqdata = gson.toJson(receiptRequest);

		if (StringUtils.isEmpty(reqdata)) {
			logger.info("Couldn't get receipt request data.");
			return null;
		}

		String respdata = HttpUtils.postJson(FT_ENDPOINT, reqdata, billing.getFtCashboxId(),
				billing.getFtAccessToken());

		if (StringUtils.isEmpty(respdata)) {
			logger.info("Couldn't get receipt response data.");
			return null;
		}

		ReceiptResponse receiptResponse = gson.fromJson(respdata, ReceiptResponse.class);

		if (receiptResponse == null || StringUtils.isEmpty(receiptResponse.getFtCashBoxID())) {
			logger.info("Couldn't create receipt response.");
			return null;
		} else if (receiptResponse.getCbTerminalID() == null
				&& !receiptResponse.getCbTerminalID().equals(receiptRequest.getCbTerminalID())) {
			logger.info("Cashbox terminal identification received doesn't match requested.");
			return null;
		} else if (receiptResponse.getCbReceiptReference() == null
				&& !receiptResponse.getCbReceiptReference().equals(receiptRequest.getCbReceiptReference())) {
			logger.info("Cashbox receipt reference received id doesn't match requested.");
			return null;
		}

		BillingSignature signature = BillingSignature.getInstance();
		signature.setFtCashboxId(receiptResponse.getFtCashBoxID());
		signature.setCbTerminalId(receiptResponse.getCbTerminalID());
		signature.setFtCashboxName(receiptResponse.getFtCashBoxIdentification());
		signature.setFtReceiptId(receiptResponse.getFtReceiptIdentification());
		signature.setFtReceiptMoment(DateUtils.asDate(receiptResponse.getFtReceiptMoment()));
		signature.setFtStateFlag(receiptResponse.getFtState());
		if (!StringUtils.isEmpty(receiptResponse.getFtStateData())) {
			StateData stateData = gson.fromJson(receiptResponse.getFtStateData(), StateData.class);
			signature.setFtStateNo(stateData.getNumber());
			signature.setFtStateSigning(stateData.getSigning());
			signature.setFtStateCounting(stateData.getCounting());
			signature.setFtStateException(stateData.getException());
			signature.setFtStateZeroReceipt(stateData.getZeroReceipt());
		}
		for (SignatureItem signatureItem : receiptResponse.getFtSignatures()) {
			if (signatureItem.getFtSignatureFormat() == SignatureItem.Format.QR_CODE) {
				signature.setFtReceiptSignature(signatureItem.getData());
				break;
			}
		}
		signature.setFtReceiptResponseLog(respdata);

		logger.trace("signature=" + signature);
		return signature;
	}

}
