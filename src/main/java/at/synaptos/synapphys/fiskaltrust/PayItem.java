package at.synaptos.synapphys.fiskaltrust;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PayItem implements Serializable {

	private static final long serialVersionUID = -3075941725640823676L;

	// [DataMember(Order = 5, EmitDefaultValue = false, IsRequired = false)]
	private Long Position = 0l;
	// [DataMember(Order = 10, EmitDefaultValue = true, IsRequired = true)]
	private BigDecimal Quantity = BigDecimal.valueOf(1d);
	// [DataMember(Order = 20, EmitDefaultValue = true, IsRequired = true)]
	private String Description = "";
	// [DataMember(Order = 30, EmitDefaultValue = true, IsRequired = true)]
	private BigDecimal Amount = BigDecimal.valueOf(0d);
	// [DataMember(Order = 40, EmitDefaultValue = true, IsRequired = true)]
	private Long ftPayItemCase = 0l;
	// [DataMember(Order = 50, EmitDefaultValue = false, IsRequired = false)]
	private String ftPayItemCaseData;
	// [DataMember(Order = 60, EmitDefaultValue = false, IsRequired = false)]
	private String AccountNumber;
	// [DataMember(Order = 70, EmitDefaultValue = false, IsRequired = false)]
	private String CostCenter;
	// [DataMember(Order = 80, EmitDefaultValue = false, IsRequired = false)]
	private String MoneyGroup;
	// [DataMember(Order = 90, EmitDefaultValue = false, IsRequired = false)]
	private String MoneyNumber;
	// [DataMember(Order = 100, EmitDefaultValue = false, IsRequired = false)]
	private LocalDateTime Moment;

	public Long getPosition() {
		return Position;
	}

	public void setPosition(Long position) {
		Position = position;
	}

	public BigDecimal getQuantity() {
		return Quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		Quantity = quantity;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public BigDecimal getAmount() {
		return Amount;
	}

	public void setAmount(BigDecimal amount) {
		Amount = amount;
	}

	public Long getFtPayItemCase() {
		return ftPayItemCase;
	}

	public void setFtPayItemCase(Long ftPayItemCase) {
		this.ftPayItemCase = ftPayItemCase;
	}

	public String getFtPayItemCaseData() {
		return ftPayItemCaseData;
	}

	public void setFtPayItemCaseData(String ftPayItemCaseData) {
		this.ftPayItemCaseData = ftPayItemCaseData;
	}

	public String getAccountNumber() {
		return AccountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}

	public String getCostCenter() {
		return CostCenter;
	}

	public void setCostCenter(String costCenter) {
		CostCenter = costCenter;
	}

	public String getMoneyGroup() {
		return MoneyGroup;
	}

	public void setMoneyGroup(String moneyGroup) {
		MoneyGroup = moneyGroup;
	}

	public String getMoneyNumber() {
		return MoneyNumber;
	}

	public void setMoneyNumber(String moneyNumber) {
		MoneyNumber = moneyNumber;
	}

	public LocalDateTime getMoment() {
		return Moment;
	}

	public void setMoment(LocalDateTime moment) {
		Moment = moment;
	}

}
