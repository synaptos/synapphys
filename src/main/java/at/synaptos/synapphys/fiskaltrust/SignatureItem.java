package at.synaptos.synapphys.fiskaltrust;

import java.io.Serializable;

public class SignatureItem implements Serializable {

	private static final long serialVersionUID = -161947513364160589L;

	public enum Format {
		UNKOWN(0x00), TEXT(0x01), LINK(0x02), QR_CODE(0x03), CODE128(0x04), OCR_A(0x05), PDF417(0x06), DATAMATRIX(
				0x07), AZTEC(0x08), EAN_8(0x09), EAN_13(0x0A), UPC_A(0x0B), CODE39(0x0C);

		private long key;

		private Format(long key) {
			this.setKey(key);
		}

		public long getKey() {
			return key;
		}

		public void setKey(long key) {
			this.key = key;
		}

		public static Format fromKey(long key) {
			for (Format format : Format.values()) {
				if (format.getKey() == key) {
					return format;
				}
			}
			return UNKOWN;
		}
	}

	public enum Type {
		UNKNOWN(0x0000), INFORMATION(0x1000), WARNING(0x2000), ERROR(0x3000), AT_UNKNOWN(0x4154000000000000l), AT_RKSV(
				0x4154000000000001l), AT_STORAGE_OBLIGATION(0x4154000000000002l), AT_FINANZ_ONLINE(
						0x4154000000000003l), DE_UNKOWN(0x4445000000000000l), DE_HASH(
								0x4445000000000001l), DE_STORAGE_OBLIGATION(0x4445000000000002l);

		private long key;

		private Type(long key) {
			this.setKey(key);
		}

		public long getKey() {
			return key;
		}

		public void setKey(long key) {
			this.key = key;
		}

		public static Type fromKey(long key) {
			for (Type type : Type.values()) {
				if (type.getKey() == key) {
					return type;
				}
			}
			return UNKNOWN;
		}
	}

	// [DataMember(Order = 10, EmitDefaultValue = true, IsRequired = true)]
	private Format ftSignatureFormat;
	// [DataMember(Order = 20, EmitDefaultValue = true, IsRequired = true)]
	private Type ftSignatureType;
	// [DataMember(Order = 30, EmitDefaultValue = false, IsRequired = false)]
	private String caption;
	// [DataMember(Order = 40, EmitDefaultValue = true, IsRequired = true)]
	private String data;

	public Format getFtSignatureFormat() {
		return ftSignatureFormat;
	}

	public Type getFtSignatureType() {
		return ftSignatureType;
	}

	public String getCaption() {
		return caption;
	}

	public String getData() {
		return data;
	}

}
