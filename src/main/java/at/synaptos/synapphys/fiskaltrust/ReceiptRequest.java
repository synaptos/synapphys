package at.synaptos.synapphys.fiskaltrust;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import at.synaptos.synapphys.model.Billing;

public class ReceiptRequest implements Serializable {

	private static final long serialVersionUID = 1657929523691033598L;

	public enum ReceiptCase {
		DEFAULT(0x00, "Vorgabewert"), AT_UNKNOWN(0x4154000000000000l, "Unbekannte Belegart für AT"), AT_RKSV(
				0x4154000000000001l,
				"Barumsatz mit RKSV-Pflicht für AT"), AT_NULL(0x4154000000000002l, "Null-Beleg"), AT_START(
						0x4154000000000003l, "Inbetriebnahme-Beleg"), AT_STOP(0x4154000000000004l,
								"Außerbetriebnahme-Beleg"), AT_MONTH(0x4154000000000005l, "Monats-Beleg"), AT_YEAR(
										0x4154000000000006l, "Jahres-Beleg"), AT_CANCEL(0x0000000000040000l,
												"Storno-Beleg"), AT_POST_RECORDING_FAIL(0x0000000000010000l,
														"Ausfall-Nacherfassung"), AT_POST_RECORDING_MANUAL(
																0x0000000000080000l, "Handschriftbeleg-Nacherfassung");

		private long key;
		private String name;

		private ReceiptCase(long key, String name) {
			this.setKey(key);
			this.setName(name);
		}

		public long getKey() {
			return key;
		}

		public void setKey(long key) {
			this.key = key;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public static ReceiptCase fromKey(long key) {
			for (ReceiptCase receiptCase : ReceiptCase.values()) {
				if (receiptCase.getKey() == key) {
					return receiptCase;
				}
			}
			return DEFAULT;
		}
	}

	// [DataMember(Order = 10, EmitDefaultValue = true, IsRequired = true)]
	private String ftCashBoxID = "";
	// [DataMember(Order = 15, EmitDefaultValue = false, IsRequired = false)]
	private String ftQueueID;
	// [DataMember(Order = 16, EmitDefaultValue = false, IsRequired = false)]
	private String ftPosSystemId;
	// [DataMember(Order = 20, EmitDefaultValue = true, IsRequired = true)]
	private String cbTerminalID = "";
	// [DataMember(Order = 30, EmitDefaultValue = true, IsRequired = true)]
	private String cbReceiptReference = "";
	// [DataMember(Order = 40, EmitDefaultValue = true, IsRequired = true)]
	private LocalDateTime cbReceiptMoment;
	// [DataMember(Order = 50, EmitDefaultValue = true, IsRequired = true)]
	private List<ChargeItem> cbChargeItems = new ArrayList<ChargeItem>();
	// [DataMember(Order = 60, EmitDefaultValue = true, IsRequired = true)]
	private List<PayItem> cbPayItems = new ArrayList<PayItem>();
	// [DataMember(Order = 70, EmitDefaultValue = true, IsRequired = true)]
	private Long ftReceiptCase = 0l;
	// [DataMember(Order = 80, EmitDefaultValue = false, IsRequired = false)]
	private String ftReceiptCaseData;
	// [DataMember(Order = 90, EmitDefaultValue = false, IsRequired = false)]
	private BigDecimal cbReceiptAmount;
	// [DataMember(Order = 100, EmitDefaultValue = false, IsRequired = false)]
	private String cbUser;
	// [DataMember(Order = 110, EmitDefaultValue = false, IsRequired = false)]
	private String cbArea;
	// [DataMember(Order = 120, EmitDefaultValue = false, IsRequired = false)]
	private String cbCustomer;
	// [DataMember(Order = 130, EmitDefaultValue = false, IsRequired = false)]
	private String cbSettlement;
	// [DataMember(Order = 140, EmitDefaultValue = false, IsRequired = false)]
	private String cbPreviousReceiptReference;

	public static ReceiptRequest getInstance(Billing billing) {
		ReceiptRequest receiptRequest = new ReceiptRequest();
		receiptRequest.setFtCashBoxID(billing.getFtCashboxId());
		receiptRequest.setCbTerminalID(billing.getCbTerminalId());
		receiptRequest.setCbUser(billing.getCreatedUser());
		receiptRequest.setCbReceiptReference(billing.getReferenceNo());
		receiptRequest.setCbReceiptMoment(billing.getBillingLdt());
		receiptRequest.setFtReceiptCase(0x4154000000000000l);
		billing.getBillingItems()
				.forEach(billingItem -> receiptRequest.addCbChargeItemValues(billingItem.getSequenceNo().longValue(),
						BigDecimal.valueOf(billingItem.getQuantity().doubleValue()),
						BigDecimal.valueOf(billingItem.getTotal()), billingItem.getSubject(), billingItem.getVat(),
						0x4154000000000000l));
		receiptRequest.addCbPayItemValues(1l, BigDecimal.ONE, BigDecimal.valueOf(billing.getTotal()),
				billing.getPaymentMethod().name(), 0x4154000000000000l);
		return receiptRequest;
	}

	public String getFtCashBoxID() {
		return ftCashBoxID;
	}

	public void setFtCashBoxID(String ftCashBoxID) {
		this.ftCashBoxID = ftCashBoxID;
	}

	public String getFtQueueID() {
		return ftQueueID;
	}

	public void setFtQueueID(String ftQueueID) {
		this.ftQueueID = ftQueueID;
	}

	public String getFtPosSystemId() {
		return ftPosSystemId;
	}

	public void setFtPosSystemId(String ftPosSystemId) {
		this.ftPosSystemId = ftPosSystemId;
	}

	public String getCbTerminalID() {
		return cbTerminalID;
	}

	public void setCbTerminalID(String cbTerminalID) {
		this.cbTerminalID = cbTerminalID;
	}

	public String getCbReceiptReference() {
		return cbReceiptReference;
	}

	public void setCbReceiptReference(String cbReceiptReference) {
		this.cbReceiptReference = cbReceiptReference;
	}

	public LocalDateTime getCbReceiptMoment() {
		return cbReceiptMoment;
	}

	public void setCbReceiptMoment(LocalDateTime cbReceiptMoment) {
		this.cbReceiptMoment = cbReceiptMoment;
	}

	public List<ChargeItem> getCbChargeItems() {
		return cbChargeItems;
	}

	public void setCbChargeItems(List<ChargeItem> cbChargeItems) {
		this.cbChargeItems = cbChargeItems;
	}

	public List<PayItem> getCbPayItems() {
		return cbPayItems;
	}

	public void setCbPayItems(List<PayItem> cbPayItems) {
		this.cbPayItems = cbPayItems;
	}

	public Long getFtReceiptCase() {
		return ftReceiptCase;
	}

	public void setFtReceiptCase(Long ftReceiptCase) {
		this.ftReceiptCase = ftReceiptCase;
	}

	public String getFtReceiptCaseData() {
		return ftReceiptCaseData;
	}

	public void setFtReceiptCaseData(String ftReceiptCaseData) {
		this.ftReceiptCaseData = ftReceiptCaseData;
	}

	public BigDecimal getCbReceiptAmount() {
		return cbReceiptAmount;
	}

	public void setCbReceiptAmount(BigDecimal cbReceiptAmount) {
		this.cbReceiptAmount = cbReceiptAmount;
	}

	public String getCbUser() {
		return cbUser;
	}

	public void setCbUser(String cbUser) {
		this.cbUser = cbUser;
	}

	public String getCbArea() {
		return cbArea;
	}

	public void setCbArea(String cbArea) {
		this.cbArea = cbArea;
	}

	public String getCbCustomer() {
		return cbCustomer;
	}

	public void setCbCustomer(String cbCustomer) {
		this.cbCustomer = cbCustomer;
	}

	public String getCbSettlement() {
		return cbSettlement;
	}

	public void setCbSettlement(String cbSettlement) {
		this.cbSettlement = cbSettlement;
	}

	public String getCbPreviousReceiptReference() {
		return cbPreviousReceiptReference;
	}

	public void setCbPreviousReceiptReference(String cbPreviousReceiptReference) {
		this.cbPreviousReceiptReference = cbPreviousReceiptReference;
	}

	public void addCbChargeItem(ChargeItem item) {
		this.getCbChargeItems().add(item);
	}

	public void addCbChargeItemValues(Long Position, BigDecimal Quantity, BigDecimal Amount, String Description,
			BigDecimal VATRate, Long ftChargeItemCase) {
		ChargeItem item = new ChargeItem();
		if (Position != null) {
			item.setPosition(Position);
		}
		if (Quantity != null) {
			item.setQuantity(Quantity);
		}
		if (Amount != null) {
			item.setAmount(Amount);
		}
		if (Description != null) {
			item.setDescription(Description);
		}
		if (VATRate != null) {
			item.setVatRate(VATRate);
		}
		item.setFtChargeItemCase(ftChargeItemCase);
		addCbChargeItem(item);
	}

	public void addCbPayItem(PayItem item) {
		this.getCbPayItems().add(item);
	}

	public void addCbPayItemValues(Long Position, BigDecimal Quantity, BigDecimal Amount, String Description,
			Long ftPayItemCase) {
		PayItem item = new PayItem();
		if (Position != null) {
			item.setPosition(Position);
		}
		if (Quantity != null) {
			item.setQuantity(Quantity);
		}
		if (Amount != null) {
			item.setAmount(Amount);
		}
		if (Description != null) {
			item.setDescription(Description);
		}
		item.setFtPayItemCase(ftPayItemCase);
		addCbPayItem(item);
	}

}
