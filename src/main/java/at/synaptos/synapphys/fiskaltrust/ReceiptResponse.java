package at.synaptos.synapphys.fiskaltrust;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ReceiptResponse implements Serializable {

	private static final long serialVersionUID = -1449831190869731747L;

	// [DataMember(Order = 10, EmitDefaultValue = true, IsRequired = true)]
	private String ftCashBoxID;
	// [DataMember(Order = 14, EmitDefaultValue = true, IsRequired = true)]
	private String ftQueueID;
	// [DataMember(Order = 15, EmitDefaultValue = true, IsRequired = true)]
	private String ftQueueItemID;
	// [DataMember(Order = 16, EmitDefaultValue = true, IsRequired = true)]
	private Long ftQueueRow;
	// [DataMember(Order = 20, EmitDefaultValue = true, IsRequired = true)]
	private String cbTerminalID;
	// [DataMember(Order = 30, EmitDefaultValue = true, IsRequired = true)]
	private String cbReceiptReference;
	// [DataMember(Order = 35, EmitDefaultValue = true, IsRequired = true)]
	private String ftCashBoxIdentification;
	// [DataMember(Order = 40, EmitDefaultValue = true, IsRequired = true)]
	private String ftReceiptIdentification;
	// [DataMember(Order = 50, EmitDefaultValue = true, IsRequired = true)]
	private LocalDateTime ftReceiptMoment;
	// [DataMember(Order = 60, EmitDefaultValue = false, IsRequired = false)]
	private String[] ftReceiptHeader;
	// [DataMember(Order = 70, EmitDefaultValue = false, IsRequired = false)]
	private ChargeItem[] ftChargeItems;
	// [DataMember(Order = 80, EmitDefaultValue = false, IsRequired = false)]
	private String[] ftChargeLines;
	// [DataMember(Order = 90, EmitDefaultValue = false, IsRequired = false)]
	private PayItem[] ftPayItems;
	// [DataMember(Order = 100, EmitDefaultValue = false, IsRequired = false)]
	private String[] ftPayLines;
	// [DataMember(Order = 110, EmitDefaultValue = true, IsRequired = true)]
	private SignatureItem[] ftSignatures;
	// [DataMember(Order = 120, EmitDefaultValue = false, IsRequired = false)]
	private String[] ftReceiptFooter;
	// [DataMember(Order = 130, EmitDefaultValue = true, IsRequired = true)]
	private Long ftState;
	// [DataMember(Order = 140, EmitDefaultValue = false, IsRequired = false)]
	private String ftStateData;

	public String getFtCashBoxID() {
		return ftCashBoxID;
	}

	public String getFtQueueID() {
		return ftQueueID;
	}

	public String getFtQueueItemID() {
		return ftQueueItemID;
	}

	public Long getFtQueueRow() {
		return ftQueueRow;
	}

	public String getCbTerminalID() {
		return cbTerminalID;
	}

	public String getCbReceiptReference() {
		return cbReceiptReference;
	}

	public String getFtCashBoxIdentification() {
		return ftCashBoxIdentification;
	}

	public String getFtReceiptIdentification() {
		return ftReceiptIdentification;
	}

	public LocalDateTime getFtReceiptMoment() {
		return ftReceiptMoment;
	}

	public String[] getFtReceiptHeader() {
		return ftReceiptHeader;
	}

	public ChargeItem[] getFtChargeItems() {
		return ftChargeItems;
	}

	public String[] getFtChargeLines() {
		return ftChargeLines;
	}

	public PayItem[] getFtPayItems() {
		return ftPayItems;
	}

	public String[] getFtPayLines() {
		return ftPayLines;
	}

	public SignatureItem[] getFtSignatures() {
		return ftSignatures;
	}

	public String[] getFtReceiptFooter() {
		return ftReceiptFooter;
	}

	public Long getFtState() {
		return ftState;
	}

	public String getFtStateData() {
		return ftStateData;
	}

}
