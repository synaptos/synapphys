package at.synaptos.synapphys.fiskaltrust;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ChargeItem implements Serializable {

	private static final long serialVersionUID = -6465165255077197175L;

	// [DataMember(Order = 5, EmitDefaultValue = false, IsRequired = false)]
	private Long Position = 0l;
	// [DataMember(Order = 10, EmitDefaultValue = true, IsRequired = true)]
	private BigDecimal Quantity = BigDecimal.valueOf(1d);
	// [DataMember(Order = 20, EmitDefaultValue = true, IsRequired = true)]
	private String Description = "";
	// [DataMember(Order = 30, EmitDefaultValue = true, IsRequired = true)]
	private BigDecimal Amount = BigDecimal.valueOf(0d);
	// [DataMember(Order = 40, EmitDefaultValue = true, IsRequired = true)]
	private BigDecimal VATRate = BigDecimal.valueOf(0d);
	// [DataMember(Order = 50, EmitDefaultValue = true, IsRequired = true)]
	private Long ftChargeItemCase = 0l;
	// [DataMember(Order = 60, EmitDefaultValue = false, IsRequired = false)]
	private String ftChargeItemCaseData;
	// [DataMember(Order = 70, EmitDefaultValue = false, IsRequired = false)]
	private BigDecimal VATAmount;
	// [DataMember(Order = 80, EmitDefaultValue = false, IsRequired = false)]
	private String AccountNumber;
	// [DataMember(Order = 90, EmitDefaultValue = false, IsRequired = false)]
	private String CostCenter;
	// [DataMember(Order = 100, EmitDefaultValue = false, IsRequired = false)]
	private String ProductGroup;
	// [DataMember(Order = 110, EmitDefaultValue = false, IsRequired = false)]
	private String ProductNumber;
	// [DataMember(Order = 120, EmitDefaultValue = false, IsRequired = false)]
	private String ProductBarcode;
	// [DataMember(Order = 130, EmitDefaultValue = false, IsRequired = false)]
	private String Unit;
	// [DataMember(Order = 140, EmitDefaultValue = false, IsRequired = false)]
	private BigDecimal UnitQuantity;
	// [DataMember(Order = 150, EmitDefaultValue = false, IsRequired = false)]
	private BigDecimal UnitPrice;
	// [DataMember(Order = 160, EmitDefaultValue = false, IsRequired = false)]
	private LocalDateTime Moment;

	public Long getPosition() {
		return Position;
	}

	public void setPosition(Long position) {
		Position = position;
	}

	public BigDecimal getQuantity() {
		return Quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		Quantity = quantity;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public BigDecimal getAmount() {
		return Amount;
	}

	public void setAmount(BigDecimal amount) {
		Amount = amount;
	}

	public BigDecimal getVatRate() {
		return VATRate;
	}

	public void setVatRate(BigDecimal vatRate) {
		VATRate = vatRate;
	}

	public Long getFtChargeItemCase() {
		return ftChargeItemCase;
	}

	public void setFtChargeItemCase(Long ftChargeItemCase) {
		this.ftChargeItemCase = ftChargeItemCase;
	}

	public String getFtChargeItemCaseData() {
		return ftChargeItemCaseData;
	}

	public void setFtChargeItemCaseData(String ftChargeItemCaseData) {
		this.ftChargeItemCaseData = ftChargeItemCaseData;
	}

	public BigDecimal getVATAmount() {
		return VATAmount;
	}

	public void setVATAmount(BigDecimal vATAmount) {
		VATAmount = vATAmount;
	}

	public String getAccountNumber() {
		return AccountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}

	public String getCostCenter() {
		return CostCenter;
	}

	public void setCostCenter(String costCenter) {
		CostCenter = costCenter;
	}

	public String getProductGroup() {
		return ProductGroup;
	}

	public void setProductGroup(String productGroup) {
		ProductGroup = productGroup;
	}

	public String getProductNumber() {
		return ProductNumber;
	}

	public void setProductNumber(String productNumber) {
		ProductNumber = productNumber;
	}

	public String getProductBarcode() {
		return ProductBarcode;
	}

	public void setProductBarcode(String productBarcode) {
		ProductBarcode = productBarcode;
	}

	public String getUnit() {
		return Unit;
	}

	public void setUnit(String unit) {
		Unit = unit;
	}

	public BigDecimal getUnitQuantity() {
		return UnitQuantity;
	}

	public void setUnitQuantity(BigDecimal unitQuantity) {
		UnitQuantity = unitQuantity;
	}

	public BigDecimal getUnitPrice() {
		return UnitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		UnitPrice = unitPrice;
	}

	public LocalDateTime getMoment() {
		return Moment;
	}

	public void setMoment(LocalDateTime moment) {
		Moment = moment;
	}

}
