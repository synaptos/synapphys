package at.synaptos.synapphys.fiskaltrust;

import java.io.Serializable;

public class StateData implements Serializable {

	private static final long serialVersionUID = -5792514833544783206L;

	private Integer Number;

	private String Exception;

	private Boolean Signing;

	private Boolean Counting;

	private Boolean ZeroReceipt;

	public Integer getNumber() {
		return Number;
	}

	public void setNumber(Integer number) {
		Number = number;
	}

	public String getException() {
		return Exception;
	}

	public void setException(String exception) {
		Exception = exception;
	}

	public Boolean getSigning() {
		return Signing;
	}

	public void setSigning(Boolean signing) {
		Signing = signing;
	}

	public Boolean getCounting() {
		return Counting;
	}

	public void setCounting(Boolean counting) {
		Counting = counting;
	}

	public Boolean getZeroReceipt() {
		return ZeroReceipt;
	}

	public void setZeroReceipt(Boolean zeroReceipt) {
		ZeroReceipt = zeroReceipt;
	}

}
