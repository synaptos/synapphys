package at.synaptos.synapphys.security;

import java.security.Principal;
import java.security.acl.Group;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.jboss.logging.Logger;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

import at.synaptos.synapphys.domain.CustomPrincipal;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.StringUtils;
import at.synaptos.synapphys.utils.XmlRpcUtils;

public class CustomUsernamePasswordLoginModule extends UsernamePasswordLoginModule {

	private static final Logger logger = Logger.getLogger(CustomUsernamePasswordLoginModule.class);

	@SuppressWarnings("serial")
	public static final Map<String, String> XMLRPC_PACKAGES = new HashMap<String, String>() {
		{
			this.put("Starter", "user");
			this.put("Standard", "user");
			this.put("Professional", "user");
			this.put("Premium", "user");
			this.put("Test", "user");
		}
	};

	private CustomPrincipal identity = null;

	private String packId = null;
	private String roleId = null;

	@Override
	protected boolean validatePassword(String inputPassword, String expectedPassword) {
		logger.debug("username=" + getUsername());

		if (StringUtils.isEmpty(getUsername())) {
			logger.info("### Login failed: username=" + getUsername() + ", packId=" + packId + ", rolenId=" + roleId
					+ ", identity=" + identity);
			return false;
		}

		final String[] data;
		if (getUsername().startsWith("\t_" + GlobalNames.APPLICATION_URL_PARAM_AUTHTOKEN)) {
			data = XmlRpcUtils.getSessionData(inputPassword);
		} else {
			inputPassword = inputPassword.replace("Â§", "§");
			data = XmlRpcUtils.getSessionData(getUsername(), inputPassword);
		}

		if (XMLRPC_PACKAGES.containsKey(data[0])) {
			packId = data[0];
			roleId = XMLRPC_PACKAGES.get(packId);
		}
		if (data[1] != null) {
			identity = new CustomPrincipal(data[1]);
			if (data[2] != null) {
				identity.setHomeUrl(data[2]);
			}
			if (data[3] != null) {
				identity.setLogoutUrl(data[3]);
			}
			if (data[4] != null) {
				identity.setAccountUrl(data[4]);
			}
		}

		logger.info("### Login " + (roleId != null ? "successful" : "failed") + ": username=" + getUsername()
				+ ", packId=" + packId + ", roleId=" + roleId + ", identity=" + identity);
		return roleId != null;
	}

	@Override
	protected Group[] getRoleSets() throws LoginException {
		SimpleGroup group = new SimpleGroup("Roles");
		try {
			group.addMember(new SimplePrincipal(roleId));
		} catch (Exception e) {
			throw new LoginException("Failed to add member [" + roleId + "] to group [" + group + "]");
		}
		return new Group[] { group };
	}

	@Override
	protected Principal getIdentity() {
		if (identity == null) {
			return super.getIdentity();
		}
		return identity;
	}

	@Override
	protected String getUsersPassword() throws LoginException {
		return null;
	}

}
