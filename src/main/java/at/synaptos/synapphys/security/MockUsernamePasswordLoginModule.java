package at.synaptos.synapphys.security;

import java.security.Principal;
import java.security.acl.Group;

import javax.security.auth.login.LoginException;

import org.jboss.logging.Logger;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

import at.synaptos.synapphys.domain.CustomPrincipal;

public class MockUsernamePasswordLoginModule extends UsernamePasswordLoginModule {

	private static final Logger logger = Logger.getLogger(CustomUsernamePasswordLoginModule.class);

	private static final String MOCK_IDENTITY = "a";
	private static final String MOCK_ROLE = "user";

	@Override
	protected boolean validatePassword(String inputPassword, String expectedPassword) {
		logger.info("rolename=" + MOCK_ROLE + ", identity=" + MOCK_IDENTITY);
		return true;
	}

	@Override
	protected Group[] getRoleSets() throws LoginException {
		SimpleGroup group = new SimpleGroup("Roles");
		try {
			group.addMember(new SimplePrincipal(MOCK_ROLE));
		} catch (Exception e) {
			throw new LoginException("Failed to add member [user] to group [" + group + "]");
		}
		return new Group[] { group };
	}

	@Override
	protected Principal getIdentity() {
		Principal customPrincipal = new CustomPrincipal(MOCK_IDENTITY);
		return customPrincipal;
	}

	@Override
	protected String getUsersPassword() throws LoginException {
		return null;
	}

}
