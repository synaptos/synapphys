package at.synaptos.synapphys.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import at.synaptos.synapphys.model.BillingStatus;
import at.synaptos.synapphys.utils.StringUtils;

@FacesConverter(value = "billingStatusConverter", forClass = BillingStatus.class)
public class BillingStatusConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return BillingStatus.valueOf(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		return String.valueOf(value);
	}

}
