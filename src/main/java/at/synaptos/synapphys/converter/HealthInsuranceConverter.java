package at.synaptos.synapphys.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import at.synaptos.synapphys.facade.HealthInsuranceFacade;
import at.synaptos.synapphys.model.HealthInsurance;

@FacesConverter(forClass = HealthInsurance.class)
public class HealthInsuranceConverter implements Converter {

	@Inject
	private HealthInsuranceFacade insuranceProducer;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		try {
			return insuranceProducer.getHealthInsurance(Long.valueOf(value));
		} catch (NumberFormatException e) {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		if (((HealthInsurance) value).getId() == null) {
			return null;
		}
		return ((HealthInsurance) value).getId().toString();
	}

}
