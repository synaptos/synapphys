package at.synaptos.synapphys.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import at.synaptos.synapphys.model.ServiceTypeCategory;
import at.synaptos.synapphys.utils.StringUtils;

@FacesConverter(forClass = ServiceTypeCategory.class)
public class ServiceCategoryConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return ServiceTypeCategory.valueOf(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		return String.valueOf(value);
	}

}
