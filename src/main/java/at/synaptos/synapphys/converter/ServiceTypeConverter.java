package at.synaptos.synapphys.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import at.synaptos.synapphys.facade.ServiceTypeFacade;
import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.utils.StringUtils;

@FacesConverter(forClass = ServiceType.class)
public class ServiceTypeConverter implements Converter {

	@Inject
	private ServiceTypeFacade serviceTypeFacade;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return serviceTypeFacade.getServiceType(Long.valueOf(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		if (((ServiceType) value).getId() == null) {
			return null;
		}
		return ((ServiceType) value).getId().toString();
	}

}
