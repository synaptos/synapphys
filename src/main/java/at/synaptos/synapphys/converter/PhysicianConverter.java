package at.synaptos.synapphys.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import at.synaptos.synapphys.facade.PhysicianFacade;
import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.utils.StringUtils;

@FacesConverter(value = "physicianConverter", forClass = Physician.class)
public class PhysicianConverter implements Converter {

	@Inject
	private PhysicianFacade physicianFacade;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		try {
			return physicianFacade.getPhysician(Long.valueOf(value));
		} catch (NumberFormatException e) {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		if (((Physician) value).getId() == null) {
			return null;
		}
		return ((Physician) value).getId().toString();
	}

}
