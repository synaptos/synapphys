package at.synaptos.synapphys.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.utils.StringUtils;

@FacesConverter(value = "clientConverter", forClass = Client.class)
public class ClientConverter implements Converter {

	@Inject
	private ClientFacade clientFacade;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		try {
			return clientFacade.getClient(Long.valueOf(value));
		} catch (NumberFormatException e) {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		if (((Client) value).getId() == null) {
			return null;
		}
		return ((Client) value).getId().toString();
	}

}
