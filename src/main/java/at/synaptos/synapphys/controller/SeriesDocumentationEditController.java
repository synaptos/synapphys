package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.SelectEvent;

import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.facade.PhysicianFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.producer.SeriesDocumentationProducer;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.StringUtils;

@ViewScoped
@Named
public class SeriesDocumentationEditController extends AbstractEditController implements Serializable {

	private static final long serialVersionUID = -691270361243945046L;

	@Inject
	private Logger logger;

	@Inject
	@Added
	private Event<SeriesDocumentation> seriesDocumentationAddEvent;
	@Inject
	@Updated
	private Event<SeriesDocumentation> seriesDocumentationUpdateEvent;

	@Inject
	private SeriesDocumentationProducer seriesDocumentationProducer;

	@Inject
	private ClientFacade clientFacade;

	@Inject
	private PhysicianFacade physicianFacade;

	private List<Client> clients;

	private List<Physician> physicians;

	@PostConstruct
	private void init() {
		setClients(clientFacade.getClients());
		setPhysicians(physicianFacade.getPhysicians());
	}

	public List<Client> getClients() {
		return clients;
	}

	private void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public List<Physician> getPhysicians() {
		return physicians;
	}

	private void setPhysicians(List<Physician> physicians) {
		this.physicians = physicians;
	}

	@Override
	public String doSave() {
		if (seriesDocumentationProducer.getPreparedSeriesDocumentation().isNew()) {
			seriesDocumentationAddEvent.fire(seriesDocumentationProducer.getPreparedSeriesDocumentation());
		} else {
			seriesDocumentationUpdateEvent.fire(seriesDocumentationProducer.getPreparedSeriesDocumentation());
		}
		return doCancel();
	}

	@Override
	public String doCancel() {
		return PageUrlHelper.redirect(seriesDocumentationProducer.getPreparedSeriesDocumentation().getBackUrl());
	}

	public List<Client> queryClients(String query) {
		logger.debug("query=" + query);

		if (seriesDocumentationProducer.getPreparedSeriesDocumentation().getClient() != null
				&& !seriesDocumentationProducer.getPreparedSeriesDocumentation().getClient().getFullname()
						.equals(query)) {
			seriesDocumentationProducer.getPreparedSeriesDocumentation().setClient(null);
		}

		if (StringUtils.isEmpty(query)) {
			return new ArrayList<Client>();
		}

		List<Client> filteredClients = getClients().stream()
				.filter(client -> client.getFullname().toLowerCase().startsWith(query.toLowerCase()))
				.collect(Collectors.toList());

		logger.debug("filteredClients.size()=" + CollectionUtils.size(filteredClients));
		return filteredClients;
	}

	public void onItemSelectClient(SelectEvent selectEvent) {
		if (seriesDocumentationProducer.getPreparedSeriesDocumentation().getPhysician() == null
				&& AbstractEntity.isValid(seriesDocumentationProducer.getPreparedSeriesDocumentation().getClient())) {
			seriesDocumentationProducer.getPreparedSeriesDocumentation().setPhysician(
					seriesDocumentationProducer.getPreparedSeriesDocumentation().getClient().getPhysician());
		}
	}

	public List<Physician> queryPhysicians(String query) {
		logger.debug("query=" + query);

		if (seriesDocumentationProducer.getPreparedSeriesDocumentation().getPhysician() != null
				&& !seriesDocumentationProducer.getPreparedSeriesDocumentation().getPhysician().getFullname()
						.equals(query)) {
			seriesDocumentationProducer.getPreparedSeriesDocumentation().setPhysician(null);
		}

		if (StringUtils.isEmpty(query)) {
			return new ArrayList<Physician>();
		}

		List<Physician> filteredPyhsicians = getPhysicians().stream()
				.filter(physician -> physician.getFullname().toLowerCase().startsWith(query.toLowerCase()))
				.collect(Collectors.toList());

		logger.debug("filteredPyhsicians.size()=" + CollectionUtils.size(filteredPyhsicians));
		return filteredPyhsicians;
	}

}
