package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.producer.ServiceTypeProducer;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.TagAttribute;

@ViewScoped
@Named
public class ServiceTypeEditController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 8871890524183021724L;

	@Inject
	protected Logger logger;

	@Inject
	@Added
	private Event<ServiceType> serviceTypeAddEvent;
	@Inject
	@Updated
	private Event<ServiceType> serviceTypeUpdateEvent;

	@Inject
	private ServiceTypeProducer serviceTypeProducer;

	public void doSave() {
		if (serviceTypeProducer.getPreparedServiceType().isNew()) {
			serviceTypeAddEvent.fire(serviceTypeProducer.getPreparedServiceType());
		} else {
			serviceTypeUpdateEvent.fire(serviceTypeProducer.getPreparedServiceType());
		}
		String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_INFO,
				GlobalNames.MESSAGE_KEY_ACTION_SAVE_SUCCESS);
		logger.debug(msgText);
	}

	public String doBack() {
		return PageUrlHelper.redirect(serviceTypeProducer.getPreparedServiceType().getBackUrl());
	}

}
