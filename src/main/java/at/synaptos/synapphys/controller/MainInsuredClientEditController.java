package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import at.synaptos.synapphys.model.Client;

@ViewScoped
@Named
public class MainInsuredClientEditController extends AbstractClientEditController implements Serializable {

	private static final long serialVersionUID = -691270361243945046L;

	@Override
	protected Client getPreparedClient() {
		return clientProducer.getPreparedMainInsuredClient();
	}

}
