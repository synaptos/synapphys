package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.PatientStatus;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.StringUtils;

@ViewScoped
@Named
public class ClientEditController extends AbstractClientEditController implements Serializable {

	private static final long serialVersionUID = -691270361243945046L;

	private List<Client> mainInsuredClients;

	@Override
	protected Client getPreparedClient() {
		return clientProducer.getPreparedClient();
	}

	public List<Client> getMainInsuredClients() {
		if (mainInsuredClients == null) {
			setMainInsuredClients(clientFacade.getMainInsuredClients());
		}
		return mainInsuredClients;
	}

	protected void setMainInsuredClients(List<Client> mainInsuredClients) {
		this.mainInsuredClients = mainInsuredClients;
	}

	public List<Client> queryMainInsuredClients(String query) {
		logger.debug("query=" + query);

		if (getPreparedClient().getMainInsuredClient() != null
				&& !getPreparedClient().getMainInsuredClient().getFullname().equals(query)) {
			getPreparedClient().setMainInsuredClient(null);
		}

		if (StringUtils.isEmpty(query)) {
			return new ArrayList<Client>();
		}

		List<Client> filteredClients = getMainInsuredClients().stream()
				.filter(client -> !client.equals(getPreparedClient())
						&& client.getFullname().toLowerCase().startsWith(query.toLowerCase()))
				.collect(Collectors.toList());

		logger.debug("filteredClients.size()=" + CollectionUtils.size(filteredClients));
		return filteredClients;
	}

	public void onItemSelectMainInsuredClient(SelectEvent selectEvent) {
		if (getPreparedClient().getHealthInsurance() == null
				&& AbstractEntity.isValid(getPreparedClient().getMainInsuredClient())) {
			getPreparedClient().setHealthInsurance(getPreparedClient().getMainInsuredClient().getHealthInsurance());
		}
		if (getPreparedClient().getPhysician() == null
				&& AbstractEntity.isValid(getPreparedClient().getMainInsuredClient())) {
			getPreparedClient().setPhysician(getPreparedClient().getMainInsuredClient().getPhysician());
		}
	}

	public String doAddMainInsuredClient() {
		Client newMainInsuredClient = Client.getInstance();
		newMainInsuredClient.setClientOwner(getPreparedClient());
		newMainInsuredClient.setBackUrl(PageUrl.CLIENT_EDIT);
		clientProducer.setPreparedMainInsuredClient(newMainInsuredClient);
		return PageUrlHelper.redirect(PageUrl.MAIN_INSURED_CLIENT_EDIT);
	}

	public String doEditMainInsuredClient() {
		getPreparedClient().getMainInsuredClient().setBackUrl(PageUrl.CLIENT_EDIT);
		clientProducer.setPreparedMainInsuredClient(getPreparedClient().getMainInsuredClient());
		return PageUrlHelper.redirect(PageUrl.MAIN_INSURED_CLIENT_EDIT);
	}

	public String doEditSessionDocumentation() {
		save();
		selectedContextKeeper.setSelectedClient(getPreparedClient());
		return PageUrlHelper.redirect(PageUrl.SERIES_DASHBOARD);
	}

	public String doAddBookmarkClient() {
		getPreparedClient().setPatientStatus(PatientStatus.BOOKMARKED);
		getPreparedClient().setPatientStatusDate(new Date());
		save();
		return PageUrlHelper.redirect(PageUrl.CLIENT_SELECT);
	}

	public void doAddClient() {
		save();
		Client newClient = Client.getInstance();
		newClient.setBackUrl(getPreparedClient().getBackUrl());
		clientProducer.setPreparedClient(newClient);
	}

}
