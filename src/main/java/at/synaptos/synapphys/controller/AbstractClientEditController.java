package at.synaptos.synapphys.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.event.Event;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.primefaces.event.CaptureEvent;

import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.facade.HealthInsuranceFacade;
import at.synaptos.synapphys.facade.PhysicianFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.producer.ClientProducer;
import at.synaptos.synapphys.producer.PhysicianProducer;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.tenancy.CustomTenantIdentifierResolverResolver;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.StringUtils;
import at.synaptos.synapphys.utils.TagAttribute;
import at.synaptos.synapphys.utils.UploadUtils;

public abstract class AbstractClientEditController extends AbstractViewController {

	@Inject
	protected Logger logger;

	@Inject
	@Added
	protected Event<Client> clientAddEvent;
	@Inject
	@Updated
	protected Event<Client> clientUpdateEvent;

	@Inject
	protected ClientFacade clientFacade;

	@Inject
	protected ClientProducer clientProducer;

	@Inject
	protected PhysicianFacade physicianFacade;

	@Inject
	protected PhysicianProducer physicianProducer;

	@Inject
	protected HealthInsuranceFacade healthInsuranceFacade;

	@Inject
	protected SelectedContextKeeper selectedContextKeeper;

	private List<HealthInsurance> healthInsurances;

	private List<Physician> physicians;

	private String imageTmpFilename;

	public List<HealthInsurance> getHealthInsurances() {
		if (healthInsurances == null) {
			setHealthInsurances(healthInsuranceFacade.getHealthInsurances());
		}
		return healthInsurances;
	}

	protected void setHealthInsurances(List<HealthInsurance> healthInsurances) {
		this.healthInsurances = healthInsurances;
	}

	public List<Physician> getPhysicians() {
		if (physicians == null) {
			setPhysicians(physicianFacade.getPhysicians());
		}
		return physicians;
	}

	protected void setPhysicians(List<Physician> physicians) {
		this.physicians = physicians;
	}

	protected String getImageTmpFilename() {
		return imageTmpFilename;
	}

	protected void setImageTmpFilename(String imageTmpFilename) {
		this.imageTmpFilename = imageTmpFilename;
	}

	protected abstract Client getPreparedClient();

	public String getPhotoUrl() {
		if (!StringUtils.isEmpty(getImageTmpFilename())) {
			return GlobalNames.EXTERNAL_RESOURCE_URL + "/" + getImageTmpFilename();
		} else if (AbstractEntity.isValid(getPreparedClient())) {
			return GlobalNames.EXTERNAL_RESOURCE_URL + "/" + "client_" + getPreparedClient().getId() + "_photo.jpeg";
		}
		return null;
	}

	protected void save() {
		if (getPreparedClient().isNew()) {
			clientAddEvent.fire(getPreparedClient());
		} else {
			clientUpdateEvent.fire(getPreparedClient());
		}
		if (getImageTmpFilename() != null) {
			String imageTmpDir = System.getProperty("java.io.tmpdir");
			try {
				UploadUtils.dumpFile(new FileInputStream(new File(imageTmpDir, getImageTmpFilename())),
						UploadUtils.getUploadsDir(CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()),
						"client_" + getPreparedClient().getId() + "_photo.jpeg");
				setImageTmpFilename(null);
			} catch (IOException e) {
				throw new FacesException("Error in saving captured image", e);
			}
		}
		String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_INFO,
				GlobalNames.MESSAGE_KEY_ACTION_SAVE_SUCCESS);
		logger.debug(msgText);
	}

	public void doSave() {
		save();
	}

	public String doBack() {
		return PageUrlHelper.redirect(getPreparedClient().getBackUrl());
	}

	public List<Physician> queryPhysicians(String query) {
		logger.debug("query=" + query);

		if (getPreparedClient().getPhysician() != null
				&& !getPreparedClient().getPhysician().getFullname().equals(query)) {
			getPreparedClient().setPhysician(null);
		}

		if (StringUtils.isEmpty(query)) {
			return new ArrayList<Physician>();
		}

		List<Physician> filteredPyhsicians = getPhysicians().stream()
				.filter(physician -> physician.getFullname().toLowerCase().startsWith(query.toLowerCase()))
				.collect(Collectors.toList());

		logger.debug("filteredPyhsicians.size()=" + CollectionUtils.size(filteredPyhsicians));
		return filteredPyhsicians;
	}

	public void onPhotoCapture(CaptureEvent captureEvent) {
		String imageTmpFilename = "tmp_" + (Math.random() * 10000000) + ".jpeg";
		String imageTmpDir = System.getProperty("java.io.tmpdir");

		try {
			UploadUtils.dumpFile(captureEvent.getData(), imageTmpDir, imageTmpFilename);
			setImageTmpFilename(imageTmpFilename);
		} catch (IOException e) {
			throw new FacesException("Error in writing captured image", e);
		}
	}

	public String doAddPhysician() {
		Physician newPhysician = Physician.getInstance();
		newPhysician.setPhysicianOwner(getPreparedClient());
		newPhysician.setBackUrl(PageUrl.CLIENT_EDIT);
		physicianProducer.setPreparedPhysician(newPhysician);
		return PageUrlHelper.redirect(PageUrl.PHYSICIAN_EDIT);
	}

	public String doEditPhysician() {
		getPreparedClient().getPhysician().setBackUrl(PageUrl.CLIENT_EDIT);
		physicianProducer.setPreparedPhysician(getPreparedClient().getPhysician());
		return PageUrlHelper.redirect(PageUrl.PHYSICIAN_EDIT);
	}

}
