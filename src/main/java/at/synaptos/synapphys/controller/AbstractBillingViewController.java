package at.synaptos.synapphys.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.omnifaces.util.Ajax;
import org.primefaces.component.datatable.DataTable;

import at.synaptos.synapphys.facade.BillingFacade;
import at.synaptos.synapphys.facade.ServiceTypeFacade;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingItem;
import at.synaptos.synapphys.model.PaymentMethod;
import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.producer.BillingProducer;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.StringUtils;

public abstract class AbstractBillingViewController extends AbstractViewController {

	private static final int DATATABLE_ROWS = 10;

	@Inject
	private Logger logger;

	@Inject
	@Added
	private Event<Billing> billingAddEvent;

	@Inject
	private BillingFacade billingFacade;

	@Inject
	private BillingProducer billingProducer;

	@Inject
	protected SelectedContextKeeper selectedContextKeeper;

	@Inject
	private ServiceTypeFacade serviceTypeFacade;

	private Billing preparedBilling;

	private BillingItem preparedBillingItem;

	private List<BillingItem> billingItems0;

	private List<ServiceType> serviceTypes;

	public String init() {
		setServiceTypes(serviceTypeFacade.getServiceTypes());
		getServiceTypes().add(ServiceType.getInstance());
		setBillingItems0(new ArrayList<BillingItem>());
		getBillingItems0().add(BillingItem.getInstance());
		doAddBilling();
		return null;
	}

	public Billing getPreparedBilling() {
		return preparedBilling;
	}

	protected void setPreparedBilling(Billing preparedBilling) {
		this.preparedBilling = preparedBilling;
	}

	public BillingItem getPreparedBillingItem() {
		return preparedBillingItem;
	}

	private void setPreparedBillingItem(BillingItem preparedBillingItem) {
		this.preparedBillingItem = preparedBillingItem;
	}

	public List<BillingItem> getBillingItems0() {
		return billingItems0;
	}

	private void setBillingItems0(List<BillingItem> billingItems0) {
		this.billingItems0 = billingItems0;
	}

	public List<ServiceType> getServiceTypes() {
		return serviceTypes;
	}

	private void setServiceTypes(List<ServiceType> serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	public String getTextKeyForBillingType() {
		return textKeyForBillingType(".label");
	}

	public String textKeyForBillingType(String suffix) {
		return textKeyForBillingType("billing.", suffix);
	}

	public String textKeyForBillingType(String prefix, String suffix) {
		return prefix + (getPreparedBilling().getBillingType() != null
				? getPreparedBilling().getBillingType().name().toLowerCase() : "") + suffix;
	}

	public List<PaymentMethod> getShowPaymentMethods() {
		String[] showPaymentMethods = configContext.getShowPaymentMethods();
		List<PaymentMethod> currPaymentMethods = new ArrayList<PaymentMethod>();
		currPaymentMethods.add(PaymentMethod.CASH);
		if (showPaymentMethods != null) {
			for (int i = 0; i < showPaymentMethods.length; i++) {
				currPaymentMethods.add(PaymentMethod.valueOf(showPaymentMethods[i]));
			}
		}
		return currPaymentMethods;
	}

	public int getMaxRows() {
		return DATATABLE_ROWS;
	}

	public int getFirstRowIndex() {
		int rowIndex = preparedBilling.getBillingItems().size() - 1;
		int pageIndex = rowIndex / getMaxRows();
		int firstIndex = pageIndex * getMaxRows();
		return firstIndex;
	}

	public abstract void doAddBilling();

	public void doPrintBilling() {
		billingProducer.setSelectedBilling(preparedBilling);
		if (billingProducer.getSelectedBilling().isReceipt()) {
			doSaveBilling();
		} else if (billingProducer.getSelectedBilling().isInvoice()) {
			preparedBilling.setReferenceYear(LocalDate.now().getYear());
			preparedBilling.setReferenceSequence(billingFacade.getReferenceSequence(preparedBilling.getBillingType(),
					preparedBilling.getReferenceYear()));
		}
	}

	public void doSaveBilling() {
		billingAddEvent.fire(billingProducer.getSelectedBilling());
		selectedContextKeeper.refreshSelectedClient();
		doAddBilling();
	}

	public void doAddBillingItem() {
		Map<String, String> params = facesContext.getExternalContext().getRequestParameterMap();
		BillingItem billingItem = BillingItem.getInstance();
		billingItem.setId(System.currentTimeMillis() * -1);
		billingItem.setSubject(params.get("subject"));
		billingItem.setQuantity(Integer.parseInt(params.get("quantity")));
		billingItem.setAmount(BigDecimal.valueOf(Double.parseDouble(params.get("amount"))));
		if (!StringUtils.isEmpty(params.get("vat"))) {
			billingItem.setVat(BigDecimal.valueOf(Double.parseDouble(params.get("vat"))));
		}
		preparedBilling.getBillingItems().add(billingItem);
		String updateId = params.get("updateId");
		if (!StringUtils.isEmpty(updateId)) {
			DataTable billingItems = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
					.findComponent(updateId);
			billingItems.setFirst(getFirstRowIndex());
			Ajax.update(updateId);
		}
	}

	public void doAddBillingItemForEdit() {
		setPreparedBillingItem(BillingItem.getInstance());
	}

	public void doChangeBillingItemQuantity() {
		Map<String, String> params = facesContext.getExternalContext().getRequestParameterMap();
		BillingItem billingItem = preparedBilling.getBillingItems().get(Integer.parseInt(params.get("rowIndex")));
		billingItem.setQuantity(billingItem.getQuantity() + Integer.parseInt(params.get("count")));
		logger.trace("billingItem=" + billingItem);
	}

	public void doEditBillingItem(int rowIndex) {
		setPreparedBillingItem(preparedBilling.getBillingItems().get(rowIndex));
	}

	public void doSaveBillingItem() {
		if (getPreparedBillingItem().isNew()) {
			BillingItem billingItem = getPreparedBillingItem();
			billingItem.setId(System.currentTimeMillis() * -1);
			preparedBilling.getBillingItems().add(billingItem);
		}
	}

	public void doDeleteBillingItem(int rowIndex) {
		preparedBilling.getBillingItems().remove(rowIndex);
	}

}
