package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class BillingInvoiceViewController extends AbstractBillingViewController implements Serializable {

	private static final long serialVersionUID = -1133508366372409792L;

	@Override
	public String init() {
		// Da muss unbedingt ein Klient ausgewählt sein
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return PageUrlHelper.redirect(PageUrl.CLIENT_SELECT);
		}
		selectedContextKeeper.refreshSelectedClient();
		return super.init();
	}

	@Override
	public void doAddBilling() {
		setPreparedBilling(Billing.getInstance().withBillingType(BillingType.INVOICE));
		getPreparedBilling()
				.setBillingItems(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
		getPreparedBilling()
				.setSeriesDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
	}

}
