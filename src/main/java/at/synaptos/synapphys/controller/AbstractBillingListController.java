package at.synaptos.synapphys.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import at.synaptos.synapphys.facade.BillingFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.producer.BillingProducer;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

public abstract class AbstractBillingListController extends AbstractListController {

	@Inject
	@Updated
	private Event<Billing> billingUpdateEvent;
	@Inject
	@Deleted
	private Event<Billing> billingDeleteEvent;

	@Inject
	private BillingFacade billingFacade;

	@Inject
	private BillingProducer billingProducer;

	private List<Billing> filteredBillings;

	private LocalDateTime filteredStartLdt;

	private List<Billing> billings;

	@PostConstruct
	private void init() {
		setFilteredStartLdt(LocalDateTime.of(LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()), LocalTime.MIN));
		refreshBillings();
	}

	public abstract BillingType getBillingType();

	public String getTextKeyForBillingType() {
		return textKeyForBillingType(".label");
	}

	public String textKeyForBillingType(String suffix) {
		return textKeyForBillingType("billing.", suffix);
	}

	public String textKeyForBillingType(String prefix, String suffix) {
		return prefix + (getBillingType() != null ? getBillingType().name().toLowerCase() : "") + suffix;
	}

	public List<Billing> getBillings() {
		return billings;
	}

	private void setBillings(List<Billing> billings) {
		this.billings = billings;
	}

	private void refreshBillings() {
		setBillings(
				billingFacade.getBillings(getBillingType(), DateUtils.asDate(getFilteredStartLdt()),
						DateUtils.asDate(LocalDateTime.of(
								getFilteredStartLdt().toLocalDate().with(TemporalAdjusters.lastDayOfMonth()),
								LocalTime.MAX))));
	}

	public List<Billing> getFilteredBillings() {
		return filteredBillings;
	}

	public void setFilteredBillings(List<Billing> filteredBillings) {
		this.filteredBillings = filteredBillings;
	}

	public Date getFilteredStartDate() {
		return DateUtils.asDate(getFilteredStartLdt());
	}

	public LocalDateTime getFilteredStartLdt() {
		return filteredStartLdt;
	}

	public void setFilteredStartLdt(LocalDateTime filteredStartLdt) {
		this.filteredStartLdt = filteredStartLdt;
	}

	public void doSelect(Billing billing) {
		billingProducer.setSelectedBilling(billing);
	}

	public void doSave() {
		if (!AbstractEntity.isValid(billingProducer.getSelectedBilling())) {
			return;
		}
		billingUpdateEvent.fire(billingProducer.getSelectedBilling());
	}

	public void doPrint(Billing billing) {
		billingProducer.setSelectedBilling(billing);
	}

	public void doCancel(Billing billing) {
		billingDeleteEvent.fire(billing);
	}

	public void doBillingDateDecr() {
		setFilteredStartLdt(getFilteredStartLdt().minusMonths(1));
		refreshBillings();
	}

	public void doBillingDateIncr() {
		setFilteredStartLdt(getFilteredStartLdt().plusMonths(1));
		refreshBillings();
	}

}
