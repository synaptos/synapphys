package at.synaptos.synapphys.controller;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import at.synaptos.synapphys.domain.ConfigContext;
import at.synaptos.synapphys.domain.CustomPrincipal;
import at.synaptos.synapphys.domain.I18nContext;
import at.synaptos.synapphys.utils.GlobalI18n;

public abstract class AbstractViewController {

	@Inject
	protected RequestContext requestContext;

	@Inject
	protected FacesContext facesContext;

	@Inject
	protected ConfigContext configContext;

	@Inject
	@GlobalI18n
	protected I18nContext i18nContext;

	protected void addMessage(String clientId, Severity severity, String detail) {
		addMessage(clientId, severity, detail, null);
	}

	protected void addMessage(String clientId, Severity severity, String summary, String detail) {
		facesContext.addMessage(clientId, new FacesMessage(severity, summary, detail));
	}

	protected String addMessageI18n(String clientId, Severity severity, String key, Object... arguments) {
		String detail = i18nContext.getString(key, arguments);
		addMessage(clientId, severity, detail);
		return detail;
	}

	protected String getCurrentUsername() {
		ExternalContext externalContext = facesContext.getExternalContext();
		return ((CustomPrincipal) externalContext.getUserPrincipal()).getName();
	}

}
