package at.synaptos.synapphys.controller.phipse;

import java.time.LocalDate;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import at.synaptos.synapphys.facade.ClientFacade;

@ViewScoped
public class HealthInsuranceNoController {

	@Inject
	private ClientFacade clientFacade;

	private LocalDate birthDate = null;
	private Integer sinNr = null;

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getSinNr() {
		return sinNr;
	}

	public void setSinNr(Integer sinNr) {
		this.sinNr = sinNr;
	}

	public boolean validateSin() {
		return clientFacade.validateHealthInsuranceNo(getSinNr(), getBirthDate());
	}

}
