package at.synaptos.synapphys.controller.phipse;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@ViewScoped
@Named
public class TopbarClientController {

	public boolean rightView() {
		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		if ("/pages/billing/receiptView.xhtml".equals(viewId) || "/pages/billing/invoiceView.xhtml".equals(viewId)
				|| "/pages/session/findings.xhtml".equals(viewId)) {
			return true;
		}
		return false;
	}

}
