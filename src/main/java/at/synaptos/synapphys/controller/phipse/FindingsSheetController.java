package at.synaptos.synapphys.controller.phipse;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.controller.AbstractViewController;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.FindingsSheet;
import at.synaptos.synapphys.producer.FindingsSheetProducer;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class FindingsSheetController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 6109116700870714600L;

	@Inject
	@Added
	private Event<FindingsSheet> findingsSheetAddEvent;
	@Inject
	@Updated
	private Event<FindingsSheet> findingsSheetUpdateEvent;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	@Inject
	private FindingsSheetProducer findingsSheetProducer;

	public String init() {
		// Es muss ein Klient ausgewählt sein!!!
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return PageUrlHelper.redirect(PageUrl.CLIENT_SELECT);
		}
		// Es muss ein Serie ausgewählt sein!!!
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return PageUrlHelper.redirect(PageUrl.CLIENT_SELECT);
		}
		// Nimm das aktuelle Sheet aus der Serie oder erzeuge ein frisches
		findingsSheetProducer.setPreparedFindingsSheet(
				selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getFindingsSheet());
		if (findingsSheetProducer.getPreparedFindingsSheet() == null) {
			findingsSheetProducer.setPreparedFindingsSheet(FindingsSheet.getInstance());
		}
		return null;
	}

	public void doSave() {
		if (findingsSheetProducer.getPreparedFindingsSheet().getId() == null) {
			findingsSheetProducer.getPreparedFindingsSheet()
					.setSeriesDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
			findingsSheetAddEvent.fire(findingsSheetProducer.getPreparedFindingsSheet());
		} else {
			findingsSheetUpdateEvent.fire(findingsSheetProducer.getPreparedFindingsSheet());
		}
	}

	public String doBack() {
		return PageUrlHelper.redirect(PageUrl.SERIES_DASHBOARD);
	}

}
