package at.synaptos.synapphys.controller.phipse;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.controller.AbstractViewController;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.FindingsLsp;
import at.synaptos.synapphys.producer.FindingsLspProducer;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class FindingsLspController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 6109116700870714600L;

	@Inject
	@Added
	private Event<FindingsLsp> findingsLspAddEvent;
	@Inject
	@Updated
	private Event<FindingsLsp> findingsLspUpdateEvent;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	@Inject
	private FindingsLspProducer findingsLspProducer;

	public String init() {
		// Es muss ein Klient ausgewählt sein!!!
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return PageUrlHelper.redirect(PageUrl.CLIENT_SELECT);
		}
		// Es muss ein Serie ausgewählt sein!!!
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return PageUrlHelper.redirect(PageUrl.CLIENT_SELECT);
		}
		// Nimm das aktuelle Sheet aus der Serie oder erzeuge ein frisches
		findingsLspProducer.setPreparedFindingsLsp(
				selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getFindingsLsp());
		if (findingsLspProducer.getPreparedFindingsLsp() == null) {
			findingsLspProducer.setPreparedFindingsLsp(FindingsLsp.getInstance());
		}
		return null;
	}

	public void doSave() {
		if (findingsLspProducer.getPreparedFindingsLsp().getId() == null) {
			findingsLspProducer.getPreparedFindingsLsp()
					.setSeriesDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
			findingsLspAddEvent.fire(findingsLspProducer.getPreparedFindingsLsp());
		} else {
			findingsLspUpdateEvent.fire(findingsLspProducer.getPreparedFindingsLsp());
		}
	}

	public String doBack() {
		return PageUrlHelper.redirect(PageUrl.SERIES_DASHBOARD);
	}

}
