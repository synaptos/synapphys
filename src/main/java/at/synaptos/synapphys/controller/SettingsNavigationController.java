package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named
public class SettingsNavigationController implements Serializable {

	private static final long serialVersionUID = -1039488344376743116L;

	private final String APPOINTMENT_EDIT = "/includes/scheduleEntryAdviceEdit.xhtml";
	private final String PAUSE_EDIT = "/includes/officeBreakEdit.xhtml";
	private final String SCHEDULE_VIEW_EDIT = "/includes/scheduleViewEdit.xhtml";
	private final String WORK_TIME_EDIT = "/includes/officeHourEdit.xhtml";

	private int panelPage = 1;

	public String getPageSrc() {
		if (getPanelPage() == 1) {
			return WORK_TIME_EDIT;
		}
		if (getPanelPage() == 2) {
			return PAUSE_EDIT;
		}
		if (getPanelPage() == 3) {
			return APPOINTMENT_EDIT;
		}
		if (getPanelPage() == 4) {
			return SCHEDULE_VIEW_EDIT;
		}
		return null;
	}

	public int getPanelPage() {
		return panelPage;
	}

	public void setPanelPage(int panelPage) {
		this.panelPage = panelPage;
	}

}
