package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.StringUtils;

@SessionScoped
@Named
public class PublicViewController implements Serializable {

	private static final long serialVersionUID = 5974374135632978824L;

	public static final String SYSTEM_PROPERTY_KEY_LOGOUT_REDIRECT = "synapphys.logout.redirect";

	@Inject
	private FacesContext facesContext;

	private String version;

	private Locale locale;

	private String currencySymbol;

	private String currencyCode;

	@PostConstruct
	public void init() {
		setLocale("de");
		Properties prop = new Properties();
		try {
			prop.load(facesContext.getExternalContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		version = prop.getProperty("Implementation-Version", "");
	}

	public String getEnvironment() {
		return System.getProperty(GlobalNames.APPLICATION_NAMESPACE + "." + "environment", "");
	}

	public String getVersion() {
		return version;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(String language) {
		setLocale(language, null);
	}

	public void setLocale(String language, String country) {
		if (StringUtils.isEmpty(language)) {
			return;
		}
		if (StringUtils.isEmpty(country)) {
			locale = new Locale(language);
			if (new Locale("de").getLanguage().equals(locale.getLanguage())) {
				currencySymbol = "€";
				currencyCode = "EUR";
			}
		} else {
			locale = new Locale(language, country);
			currencySymbol = Currency.getInstance(locale).getSymbol(locale);
			currencyCode = Currency.getInstance(locale).getCurrencyCode();
		}
		Locale.setDefault(locale);
	}

	public String getLanguage() {
		return locale.getLanguage();
	}

	public String getCountry() {
		return locale.getCountry();
	}

	public String getLocaleId() {
		return getLocaleId("-");
	}

	public String getLocaleId(String separator) {
		String localeId = getLanguage();
		if (!StringUtils.isEmpty(getCountry())) {
			localeId += separator + getCountry();
		}
		return localeId;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String asString(Date date) {
		return asString(date, DateFormat.SHORT);
	}

	public String asString(Date date, int dateStyle) {
		if (date == null) {
			return "";
		}
		DateFormat df = DateFormat.getDateInstance(dateStyle, getLocale());
		return df.format(date);
	}

	public String asMedium(Date date) {
		if (date == null) {
			return "";
		}
		return DateUtils.getDateTimeAsShort(date);
	}

	public Integer asInteger(String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return Integer.valueOf(value);
	}

	public String asUpper(String source) {
		if (source == null) {
			return "";
		}
		return source.toUpperCase(getLocale());
	}

}
