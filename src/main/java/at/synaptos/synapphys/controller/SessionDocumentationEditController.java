package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import at.synaptos.synapphys.facade.ServiceTypeFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.KpiItem;
import at.synaptos.synapphys.model.KpiType;
import at.synaptos.synapphys.model.ServiceDocumentation;
import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class SessionDocumentationEditController extends AbstractEditController implements Serializable {

	private static final long serialVersionUID = -894469440973431828L;

	@Inject
	@Updated
	private Event<SessionDocumentation> sessionDocumentationUpdateEvent;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	@Inject
	private ServiceTypeFacade serviceTypeFacade;

	private LineChartModel kpiModel;

	private Map<KpiType, KpiItem> kpiItemByType;

	private List<ServiceType> serviceTypes;

	@PostConstruct
	private void init() {
		createKpiModel();
		setServiceTypes(serviceTypeFacade.getServiceTypes());
	}

	public LineChartModel getkpiModel() {
		return kpiModel;
	}

	private void createKpiModel() {
		kpiModel = initKpiModel();
		kpiModel.setLegendPosition("e");
		kpiModel.setShowPointLabels(true);
		kpiModel.setExtender("skinChart");
		kpiModel.setAnimate(true);
		Axis xAxis = kpiModel.getAxis(AxisType.X);
		xAxis.setMin(1);
		xAxis.setMax(10);
		xAxis.setTickInterval("1");
		Axis yAxis = kpiModel.getAxis(AxisType.Y);
		yAxis.setMin(0);
		yAxis.setMax(10);
		yAxis.setTickInterval("1");
		kpiItemByType = new HashMap<KpiType, KpiItem>();
		for (int i = 0; i < KpiType.values().length; i++) {
			kpiItemByType.put(KpiType.values()[i], KpiItem.getInstance(KpiType.values()[i]));
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return;
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation())) {
			return;
		}
		selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation().getKpiItems().forEach(kpi -> {
			kpiItemByType.replace(kpi.getKpiType(), kpi);
		});
	}

	private LineChartModel initKpiModel() {
		final LineChartModel model = new LineChartModel();

		final LineChartSeries adlSeries = new LineChartSeries();
		adlSeries.setLabel("ATL");
		adlSeries.set(0, 0);

		final LineChartSeries painSeries = new LineChartSeries();
		painSeries.setLabel("Schmerz");
		painSeries.set(0, 0);

		final LineChartSeries mobilitySeries = new LineChartSeries();
		mobilitySeries.setLabel("Beweglichkeit");
		mobilitySeries.set(0, 0);

		if (AbstractEntity.isValid(selectedContextKeeper.getSelectedClient()) && AbstractEntity
				.isValid(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation())) {
			int i;

			i = 0;
			for (KpiItem kpi : selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation()
					.getSeriesDocumentation().getKpiItems(KpiType.ADL)) {
				i++;
				if (AbstractEntity.isValid(kpi)) {
					adlSeries.set(i, kpi.getKpiValue());
				}
			}

			i = 0;
			for (KpiItem kpi : selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation()
					.getSeriesDocumentation().getKpiItems(KpiType.PAIN)) {
				i++;
				if (AbstractEntity.isValid(kpi)) {
					painSeries.set(i, kpi.getKpiValue());
				}
			}

			i = 0;
			for (KpiItem kpi : selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation()
					.getSeriesDocumentation().getKpiItems(KpiType.MOBILITY)) {
				i++;
				if (AbstractEntity.isValid(kpi)) {
					mobilitySeries.set(i, kpi.getKpiValue());
				}
			}
		}

		model.addSeries(adlSeries);
		model.addSeries(painSeries);
		model.addSeries(mobilitySeries);

		return model;
	}

	public Map<KpiType, KpiItem> getKpiItemByType() {
		return kpiItemByType;
	}

	public List<SessionDocumentation> getTop2SessionDocumentations() {
		List<SessionDocumentation> sessionDocumentations = new ArrayList<SessionDocumentation>();
		if (AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation())) {
			sessionDocumentations.add(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation());
		}
		return sessionDocumentations;
	}

	public List<ServiceType> getServiceTypes() {
		return serviceTypes;
	}

	private void setServiceTypes(List<ServiceType> serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	@Override
	public String doSave() {
		if (AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation())) {
			SessionDocumentation sessionDocumentation = selectedContextKeeper.getSelectedClient()
					.getSelectedSessionDocumentation();
			if (CollectionUtils.isEmpty(sessionDocumentation.getKpiItems())) {
				for (int i = 0; i < KpiType.values().length; i++) {
					sessionDocumentation.getKpiItems().add(kpiItemByType.get(KpiType.values()[i]));
				}
			}
			sessionDocumentationUpdateEvent.fire(sessionDocumentation);
			selectedContextKeeper.refreshSelectedClient();
			createKpiModel();
		}
		return doCancel();
	}

	@Override
	public String doCancel() {
		return PageUrlHelper
				.redirect(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation().getBackUrl());
	}

	// TODO WQ
	public String doBack() {
		return PageUrlHelper.redirect(PageUrl.SERIES_DASHBOARD);
	}

	public void doSaveServiceDocumentation(ServiceType serviceType) {
		if (AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation())) {
			SessionDocumentation sessionDocumentation = selectedContextKeeper.getSelectedClient()
					.getSelectedSessionDocumentation();
			sessionDocumentation.getServiceDocumentations().add(ServiceDocumentation.getInstance(serviceType));
			sessionDocumentationUpdateEvent.fire(sessionDocumentation);
		}
	}

	public void doDeleteServiceDocumentation(ServiceDocumentation serviceDocumentation) {
		serviceDocumentation.setDeletedDate(new Date());
		serviceDocumentation.setDeletedUser(getCurrentUsername());
		sessionDocumentationUpdateEvent
				.fire(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation());
		selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation().getServiceDocumentations()
				.remove(serviceDocumentation);
	}

	public String doEditBodymap() {
		selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation()
				.setBackUrl(PageUrl.SESSION_DOCUMENTATION_EDIT);
		return PageUrlHelper.redirect(PageUrl.BODYMAP_EDIT);
	}

}
