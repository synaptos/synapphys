package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.producer.ClientProducer;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class ClientListController extends AbstractListController implements Serializable {

	private static final long serialVersionUID = -1594177744535327427L;

	@Inject
	@Deleted
	private Event<Client> clientDeleteEvent;

	@Inject
	private ClientProducer clientProducer;

	@Inject
	private ClientFacade clientFacade;

	private List<Client> clients;

	@PostConstruct
	private void init() {
		setClients(clientFacade.getClients());
	}

	public List<Client> getClients() {
		return clients;
	}

	private void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public String doAdd() {
		return doEdit(Client.getInstance());
	}

	public String doEdit(Client client) {
		clientProducer.setPreparedClient(client.withBackUrl(PageUrl.CLIENT_LIST));
		return PageUrlHelper.redirect(PageUrl.CLIENT_EDIT);
	}

	public void doDelete(Client client) {
		clientDeleteEvent.fire(client);
		getClients().remove(client);
	}

}
