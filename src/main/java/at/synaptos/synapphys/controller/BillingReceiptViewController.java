package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingType;

@ViewScoped
@Named
public class BillingReceiptViewController extends AbstractBillingViewController implements Serializable {

	private static final long serialVersionUID = -2073361854360282095L;

	@Override
	public void doAddBilling() {
		setPreparedBilling(Billing.getInstance().withBillingType(BillingType.RECEIPT));
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return;
		}
		getPreparedBilling()
				.setBillingItems(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
	}

}
