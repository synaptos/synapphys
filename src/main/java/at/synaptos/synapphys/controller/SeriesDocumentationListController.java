package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.facade.SeriesDocumentationFacade;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.producer.SeriesDocumentationProducer;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class SeriesDocumentationListController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = -1594177744535327427L;

	@Inject
	@Deleted
	private Event<SeriesDocumentation> seriesDocumentationDeleteEvent;

	@Inject
	private SeriesDocumentationFacade seriesDocumentationFacade;

	@Inject
	private SeriesDocumentationProducer seriesDocumentationProducer;

	private List<SeriesDocumentation> seriesDocumentations;

	@PostConstruct
	private void init() {
		setSeriesDocumentations(seriesDocumentationFacade.getSeriesDocumentations());
	}

	public List<SeriesDocumentation> getSeriesDocumentations() {
		return seriesDocumentations;
	}

	private void setSeriesDocumentations(List<SeriesDocumentation> seriesDocumentations) {
		this.seriesDocumentations = seriesDocumentations;
	}

	public String doAdd() {
		doEdit(SeriesDocumentation.getInstance());
		return PageUrlHelper.redirect(PageUrl.SERIES_DASHBOARD);
	}

	public String doEdit(SeriesDocumentation seriesDocumentation) {
		seriesDocumentationProducer
				.setPreparedSeriesDocumentation(seriesDocumentation.withBackUrl(PageUrl.SERIES_DOCUMENTATION_LIST));
		return PageUrlHelper.redirect(PageUrl.SERIES_DASHBOARD);
	}

	public void doDelete(SeriesDocumentation seriesDocumentation) {
		seriesDocumentationDeleteEvent.fire(seriesDocumentation);
		getSeriesDocumentations().remove(seriesDocumentation);
	}

}
