package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.producer.PhysicianProducer;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.TagAttribute;

@ViewScoped
@Named
public class PhysicianEditController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 5630658649414113111L;

	@Inject
	protected Logger logger;

	@Inject
	@Added
	private Event<Physician> physicianAddEvent;
	@Inject
	@Updated
	private Event<Physician> physicianUpdateEvent;

	@Inject
	private PhysicianProducer physicianProducer;

	public void doSave() {
		if (physicianProducer.getPreparedPhysician().isNew()) {
			physicianAddEvent.fire(physicianProducer.getPreparedPhysician());
		} else {
			physicianUpdateEvent.fire(physicianProducer.getPreparedPhysician());
		}
		String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_INFO,
				GlobalNames.MESSAGE_KEY_ACTION_SAVE_SUCCESS);
		logger.debug(msgText);
	}

	public String doBack() {
		return PageUrlHelper.redirect(physicianProducer.getPreparedPhysician().getBackUrl());
	}

}
