package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.SelectEvent;

import at.synaptos.synapphys.domain.ScheduleEntry;
import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.facade.ScheduleItemFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.StringUtils;
import at.synaptos.synapphys.utils.TagAttribute;

@ViewScoped
@Named
public class ScheduleEntryController extends AbstractEditController implements Serializable {

	private static final long serialVersionUID = 3121722929506108022L;

	@Inject
	private Logger logger;

	@Inject
	@Added
	private Event<ScheduleItem> scheduleItemAddEvent;

	@Inject
	private ScheduleItemFacade scheduleItemFacade;

	@Inject
	private ClientFacade clientFacade;

	private List<Client> clients;

	private ScheduleItem preparedScheduleItem;

	private ScheduleEntry previewScheduleEntry;

	private LocalDateTime nextScheduleEntriesStartLdt;

	private int nextScheduleEntriesMins;

	private List<ScheduleEntry> nextScheduleEntries;

	public List<Client> getClients() {
		return clients;
	}

	private void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public ScheduleItem getPreparedScheduleItem() {
		return preparedScheduleItem;
	}

	public void setPreparedScheduleItem(ScheduleItem preparedScheduleItem) {
		this.preparedScheduleItem = preparedScheduleItem;
	}

	public ScheduleEntry getPreviewScheduleEntry() {
		return previewScheduleEntry;
	}

	private void setPreviewScheduleEntry(ScheduleEntry previewScheduleEntry) {
		this.previewScheduleEntry = previewScheduleEntry;
	}

	private LocalDateTime getNextScheduleEntriesStartLdt() {
		return nextScheduleEntriesStartLdt;
	}

	private void setNextScheduleEntriesStartLdt(LocalDateTime nextScheduleEntriesStartLdt) {
		this.nextScheduleEntriesStartLdt = nextScheduleEntriesStartLdt;
	}

	public int getNextScheduleEntriesWeekOfYear() {
		return DateUtils.weekOfYear(getNextScheduleEntriesStartLdt());
	}

	public int getNextScheduleEntriesMins() {
		return nextScheduleEntriesMins;
	}

	private void setNextScheduleEntriesMins(int nextScheduleEntriesMins) {
		this.nextScheduleEntriesMins = nextScheduleEntriesMins;
	}

	public List<ScheduleEntry> getNextScheduleEntries() {
		if (getPreviewScheduleEntry() == null) {
			return nextScheduleEntries;
		}
		return getPreviewScheduleEntry().getPreviewEntries();
	}

	private void setNextScheduleEntries(List<ScheduleEntry> nextScheduleEntries) {
		this.nextScheduleEntries = nextScheduleEntries;
	}

	public boolean isShowPreviewScheduleEntries() {
		return getPreviewScheduleEntry() != null;
	}

	public void doPrepareScheduleItem() {
		setClients(clientFacade.getClients());
		setPreparedScheduleItem(ScheduleItem.getInstance());
		setNextScheduleEntriesStartLdt(DateUtils.adjustWeekStartLdt(LocalDateTime.now()));
		setNextScheduleEntriesMins(0);
	}

	public List<Client> queryClients(String query) {
		logger.debug("query=" + query);

		if (AbstractEntity.isValid(getPreparedScheduleItem().getClient())
				&& !getPreparedScheduleItem().getClient().getFullname().equals(query)) {
			getPreparedScheduleItem().setClient(null);
			setNextScheduleEntriesMins(0);
			prepareNextScheduleEntries();
			requestContext.execute("updateEventPreview();");
		}

		if (StringUtils.isEmpty(query)) {
			return new ArrayList<Client>();
		}

		List<Client> filteredClients = getClients().stream()
				.filter(client -> client.getFullname().toLowerCase().startsWith(query.toLowerCase()))
				.collect(Collectors.toList());

		logger.debug("filteredClients.size()=" + CollectionUtils.size(filteredClients));
		return filteredClients;
	}

	public void onItemSelectClient(SelectEvent selectEvent) {
		getPreparedScheduleItem().setClient((Client) selectEvent.getObject());
		setNextScheduleEntriesMins(0);
		prepareNextScheduleEntries();
	}

	private void prepareNextScheduleEntries() {
		setNextScheduleEntries(null);
		setPreviewScheduleEntry(null);

		if (getNextScheduleEntriesStartLdt() == null) {
			return;
		}
		if (getNextScheduleEntriesMins() == 0) {
			return;
		}

		Date startDate = DateUtils.asDate(getNextScheduleEntriesStartLdt());
		Date endDate = DateUtils.asDate(DateUtils.adjustWeekEndLdt(getNextScheduleEntriesStartLdt()));

		final List<SessionDocumentation> sessionDocumentations = new ArrayList<SessionDocumentation>();
		if (AbstractEntity.isValid(getPreparedScheduleItem().getClient())
				&& AbstractEntity.isValid(getPreparedScheduleItem().getClient().getLastSeriesDocumentation())) {
			sessionDocumentations.addAll(
					getPreparedScheduleItem().getClient().getLastSeriesDocumentation().getSessionDocumentations());
		}

		setNextScheduleEntries(
				scheduleItemFacade.createNextScheduleEntries(startDate, endDate, getNextScheduleEntriesMins(),
						configContext.getInstancePropertyOfficeHours(), configContext.getInstancePropertyOfficeBreaks(),
						configContext.getInstancePropertyScheduleWeightings(), sessionDocumentations));
	}

	public void doShowNextScheduleEntries(int mins) {
		setNextScheduleEntriesStartLdt(DateUtils.adjustWeekStartLdt(LocalDateTime.now()));
		setNextScheduleEntriesMins(mins);
		prepareNextScheduleEntries();
	}

	public void doHideNextScheduleEntries() {
		setNextScheduleEntriesMins(0);
		prepareNextScheduleEntries();
	}

	public void doShowPreviewScheduleEntries(ScheduleEntry scheduleEntry) {
		setPreviewScheduleEntry(scheduleEntry);
	}

	public void doHidePreviewScheduleEntries() {
		setPreviewScheduleEntry(null);
	}

	public void doDecrNextScheduleEntriesWeekOfYear() {
		setNextScheduleEntriesStartLdt(LocalDateTime.of(
				getNextScheduleEntriesStartLdt().with(TemporalAdjusters.previous(DayOfWeek.MONDAY)).toLocalDate(),
				LocalTime.of(0, 0)));
		prepareNextScheduleEntries();
	}

	public void doIncrNextScheduleEntriesWeekOfYear() {
		setNextScheduleEntriesStartLdt(LocalDateTime.of(
				getNextScheduleEntriesStartLdt().with(TemporalAdjusters.next(DayOfWeek.MONDAY)).toLocalDate(),
				LocalTime.of(0, 0)));
		prepareNextScheduleEntries();
	}

	public void doSelectPreviewScheduleEntry(ScheduleEntry previewScheduleEntry) {
		getPreparedScheduleItem().setStartLdt(previewScheduleEntry.getStartLdt());
		getPreparedScheduleItem().setEndLdt(previewScheduleEntry.getEndLdt());
		doHideNextScheduleEntries();
	}

	@Override
	public String doSave() {
		if (getPreparedScheduleItem().getStartLdt() == null) {
			requestContext.addCallbackParam("validationFailed", true);
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EVENT_DIALOG, FacesMessage.SEVERITY_ERROR,
					GlobalNames.MESSAGE_KEY_SCHEDULE_ITEM_START_DATE_INVALID);
			logger.debug(msgText);
			return null;
		}
		scheduleItemAddEvent.fire(getPreparedScheduleItem());
		return null;
	}

	@Override
	public String doCancel() {
		return null;
	}

}
