package at.synaptos.synapphys.controller;

import java.io.IOException;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.timeline.TimelineSelectEvent;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;

import at.synaptos.synapphys.domain.CustomTimelineScheduleItem;
import at.synaptos.synapphys.domain.CustomTimelineSessionDocumentation;
import at.synaptos.synapphys.domain.ScheduleEntry;
import at.synaptos.synapphys.facade.BillingFacade;
import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.facade.ScheduleItemFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingStatus;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.model.ServiceDocumentation;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.producer.ClientProducer;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.producer.SeriesDocumentationProducer;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.StringUtils;

@ViewScoped
@Named
public class DashboardViewController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 3121722929506108022L;

	@Inject
	private Logger logger;

	@Inject
	@Added
	private Event<SessionDocumentation> sessionDocumentationAddEvent;
	@Inject
	@Updated
	private Event<SessionDocumentation> sessionDocumentationUpdateEvent;

	@Inject
	@Added
	private Event<ScheduleItem> scheduleItemAddEvent;
	@Inject
	@Updated
	private Event<ScheduleItem> scheduleItemUpdateEvent;

	@Inject
	private ScheduleItemFacade scheduleItemFacade;

	@Inject
	private SeriesDocumentationProducer seriesDocumentationProducer;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	@Inject
	private ClientProducer clientProducer;

	@Inject
	private ClientFacade clientFacade;

	@Inject
	private BillingFacade billingFacade;

	private List<Client> clients;

	private TimelineModel timelineModel;

	private LocalDateTime timelineLdt;

	private Date timelineMinDate;
	private Date timelineMaxDate;

	private ScheduleEntry selectedScheduleEntry;

	private LocalDateTime nextScheduleEntriesStartLdt;

	private int nextScheduleEntriesMins;

	private List<ScheduleEntry> nextScheduleEntries;

	private List<Billing> invoices;

	@PostConstruct
	private void init() {
		setClients(clientFacade.getClients());
		setTimelineLdt(DateUtils.adjustWeekStartLdt(LocalDateTime.now()));
		prepareTimeline();
		prepareInvoices();
	}

	public List<Client> getClients() {
		return clients;
	}

	private void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public TimelineModel getTimelineModel() {
		return timelineModel;
	}

	private void setTimelineModel(TimelineModel timelineModel) {
		this.timelineModel = timelineModel;
	}

	private LocalDateTime getTimelineLdt() {
		return timelineLdt;
	}

	private void setTimelineLdt(LocalDateTime timelineLdt) {
		this.timelineLdt = timelineLdt;
	}

	public Date getTimelineDate() {
		return DateUtils.asDate(getTimelineLdt());
	}

	public Date getTimelineMinDate() {
		return timelineMinDate;
	}

	public void setTimelineMinDate(Date timelineMinDate) {
		this.timelineMinDate = timelineMinDate;
	}

	public Date getTimelineMaxDate() {
		return timelineMaxDate;
	}

	public void setTimelineMaxDate(Date timelineMaxDate) {
		this.timelineMaxDate = timelineMaxDate;
	}

	private LocalDateTime getNextScheduleEntriesStartLdt() {
		return nextScheduleEntriesStartLdt;
	}

	private void setNextScheduleEntriesStartLdt(LocalDateTime nextScheduleEntriesStartLdt) {
		this.nextScheduleEntriesStartLdt = nextScheduleEntriesStartLdt;
	}

	public int getNextScheduleEntriesWeekOfYear() {
		return DateUtils.weekOfYear(getNextScheduleEntriesStartLdt());
	}

	public int getNextScheduleEntriesMins() {
		return nextScheduleEntriesMins;
	}

	private void setNextScheduleEntriesMins(int nextScheduleEntriesMins) {
		this.nextScheduleEntriesMins = nextScheduleEntriesMins;
	}

	public ScheduleEntry getSelectedScheduleEntry() {
		return selectedScheduleEntry;
	}

	private void setSelectedScheduleEntry(ScheduleEntry selectedScheduleEntry) {
		this.selectedScheduleEntry = selectedScheduleEntry;
	}

	public List<ScheduleEntry> getNextScheduleEntries() {
		if (getSelectedScheduleEntry() == null) {
			return nextScheduleEntries;
		}
		return getSelectedScheduleEntry().getPreviewEntries();
	}

	private void setNextScheduleEntries(List<ScheduleEntry> nextScheduleEntries) {
		this.nextScheduleEntries = nextScheduleEntries;
	}

	public boolean isShowPreviewScheduleEntries() {
		return getSelectedScheduleEntry() != null;
	}

	public List<Billing> getInvoices() {
		return invoices;
	}

	private void setInvoices(List<Billing> invoices) {
		this.invoices = invoices;
	}

	public List<Client> queryClients(String query) {
		logger.debug("query=" + query);

		if (StringUtils.isEmpty(query)) {
			return new ArrayList<Client>();
		}

		List<Client> filteredClients = getClients().stream()
				.filter(client -> client.getFullname().toLowerCase().startsWith(query.toLowerCase()))
				.collect(Collectors.toList());

		logger.debug("filteredClients.size()=" + CollectionUtils.size(filteredClients));
		return filteredClients;
	}

	public void onItemSelectClient(SelectEvent selectEvent) {
		Client client = (Client) selectEvent.getObject();
		logger.debug("client=" + client);

		selectedContextKeeper.setSelectedClient(client);
		selectedContextKeeper.getSelectedClient()
				.setSelectedSeriesDocumentation(selectedContextKeeper.getSelectedClient().getLastSeriesDocumentation());

		prepareTimeline();
		prepareInvoices();

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.addCallbackParam("gender", client.getGender().name());
	}

	public void onTimelineSelectEvent(TimelineSelectEvent timelineSelectEvent) throws IOException {
		if (timelineSelectEvent.getTimelineEvent().getData() == null) {
			return;
		}
		if (timelineSelectEvent.getTimelineEvent().getData() instanceof CustomTimelineScheduleItem) {
			selectedContextKeeper.getSelectedClient().setSelectedScheduleItem(
					((CustomTimelineScheduleItem) timelineSelectEvent.getTimelineEvent().getData()).getEntity());
		} else if (timelineSelectEvent.getTimelineEvent().getData() instanceof CustomTimelineSessionDocumentation) {
			selectedContextKeeper.getSelectedClient().setSelectedSessionDocumentation(
					((CustomTimelineSessionDocumentation) timelineSelectEvent.getTimelineEvent().getData())
							.getEntity());
			facesContext.getExternalContext().redirect(facesContext.getExternalContext().getApplicationContextPath()
					+ prepareEditSessionDocumentation() + ".jsf");
		}
	}

	private void prepareTimeline() {
		setTimelineModel(new TimelineModel());

		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return;
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return;
		}

		List<TimelineEvent> timelineEvents = new ArrayList<TimelineEvent>();

		LocalDateTime timelineStartLdt = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
		LocalDateTime timelineEndLdt = timelineStartLdt.plusMonths(3);

		if (selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getApprovalDate() != null) {
			timelineStartLdt = LocalDateTime.of(DateUtils
					.asLdt(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getApprovalDate())
					.toLocalDate(), LocalTime.of(0, 0));
			timelineEvents
					.add(new TimelineEvent("Bewilligung " + DateUtils.getDateAsDDMM(DateUtils.asDate(timelineStartLdt)),
							DateUtils.asDate(timelineStartLdt), false, null, "timelineEventOuter"));
			timelineEvents
					.add(new TimelineEvent("Therapieende " + DateUtils.getDateAsDDMM(DateUtils.asDate(timelineEndLdt)),
							DateUtils.asDate(timelineEndLdt), false, null, "timelineEventOuter"));
		} else if (selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()
				.getReferralDate() != null) {
			timelineStartLdt = LocalDateTime.of(DateUtils
					.asLdt(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getReferralDate())
					.toLocalDate(), LocalTime.of(0, 0));
			timelineEvents
					.add(new TimelineEvent("Ausstellung " + DateUtils.getDateAsDDMM(DateUtils.asDate(timelineStartLdt)),
							DateUtils.asDate(timelineStartLdt), false, null, "timelineEventOuter"));
		}

		setTimelineMinDate(DateUtils.asDate(timelineStartLdt.minusMonths(1)));
		setTimelineMaxDate(DateUtils.asDate(timelineEndLdt.plusMonths(1)));

		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getSessionDocumentations()
				.forEach(sessionDocumentation -> {
					timelineEvents
							.add(new TimelineEvent(CustomTimelineSessionDocumentation.getInstance(sessionDocumentation),
									sessionDocumentation.getStartDate(), false, null, "timelineEventInner"));
				});
		selectedContextKeeper.getSelectedClient().getScheduleItems().forEach(scheduleItem -> {
			timelineEvents.add(new TimelineEvent(CustomTimelineScheduleItem.getInstance(scheduleItem),
					scheduleItem.getStartDate(), false, null, "timelineEventWhole"));
		});

		if (!CollectionUtils.isEmpty(timelineEvents)) {
			getTimelineModel().addAll(timelineEvents);
		}
	}

	private void prepareNextScheduleEntries() {
		setNextScheduleEntries(null);
		setSelectedScheduleEntry(null);

		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return;
		}

		Date startDate = DateUtils.asDate(getNextScheduleEntriesStartLdt());
		Date endDate = DateUtils.asDate(DateUtils.adjustWeekEndLdt(getNextScheduleEntriesStartLdt()));

		setNextScheduleEntries(scheduleItemFacade.createNextScheduleEntries(startDate, endDate,
				getNextScheduleEntriesMins(), configContext.getInstancePropertyOfficeHours(),
				configContext.getInstancePropertyOfficeBreaks(), configContext.getInstancePropertyScheduleWeightings(),
				selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getSessionDocumentations()));
	}

	private void prepareInvoices() {
		setInvoices(new ArrayList<Billing>());

		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return;
		}

		selectedContextKeeper.getSelectedClient().getSeriesDocumentations().stream()
				.filter(currSeriesDocumentation -> !currSeriesDocumentation.isDone())
				.forEach(currSeriesDocumentation -> {
					Billing invoice = Billing.getInstance().withBillingType(BillingType.INVOICE)
							.withBillingStatus(BillingStatus.OPEN).withBillingItems(currSeriesDocumentation);
					invoice.setSeriesDocumentation(currSeriesDocumentation);
					getInvoices().add(invoice);
				});

		getInvoices().addAll(billingFacade.getBillings(BillingType.INVOICE, selectedContextKeeper.getSelectedClient()));
	}

	public String doAddClient() {
		return doEditClient(Client.getInstance());
	}

	public String doEditClient() {
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return null;
		}
		return doEditClient(selectedContextKeeper.getSelectedClient());
	}

	public String doEditClient(Client client) {
		clientProducer.setPreparedClient(client.withBackUrl(PageUrl.DASHBOARD_VIEW));
		return PageUrlHelper.redirect(PageUrl.CLIENT_EDIT);
	}

	public String doAddSeriesDocumentation() {
		SeriesDocumentation seriesDocumentation = SeriesDocumentation.getInstance()
				.withClient(selectedContextKeeper.getSelectedClient());
		if (AbstractEntity.isValid(seriesDocumentation.getClient())) {
			seriesDocumentation.setPhysician(seriesDocumentation.getClient().getPhysician());
		}
		return doEditSeriesDocumentation(seriesDocumentation);
	}

	public String doEditSeriesDocumentation() {
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return null;
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return null;
		}
		return doEditSeriesDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
	}

	public String doEditSeriesDocumentation(SeriesDocumentation seriesDocumentation) {
		seriesDocumentationProducer
				.setPreparedSeriesDocumentation(seriesDocumentation.withBackUrl(PageUrl.DASHBOARD_VIEW));
		return PageUrlHelper.redirect(PageUrl.SERIES_DASHBOARD);
	}

	public String doEditServiceTemplates() {
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return null;
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return null;
		}
		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().setBackUrl(PageUrl.DASHBOARD_VIEW);
		return PageUrlHelper.redirect(PageUrl.SERVICE_TEMPLATES_EDIT);
	}

	public String doEditServiceAttachments() {
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return null;
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return null;
		}
		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().setBackUrl(PageUrl.DASHBOARD_VIEW);
		return PageUrlHelper.redirect(PageUrl.SERVICE_ATTACHMENTS_EDIT);
	}

	public String doAddSessionDocumentation() {
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return null;
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return null;
		}
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedScheduleItem())) {
			return null;
		}

		selectedContextKeeper.getSelectedClient()
				.setSelectedSessionDocumentation(SessionDocumentation.getInstance(
						selectedContextKeeper.getSelectedClient().getSelectedScheduleItem(),
						selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()));
		selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation().getSeriesDocumentation()
				.getServiceTemplates().forEach(serviceTemplate -> {
					selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation()
							.getServiceDocumentations().add(ServiceDocumentation.getInstance(serviceTemplate));
				});
		sessionDocumentationAddEvent.fire(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation());

		selectedContextKeeper.getSelectedClient().getSelectedScheduleItem().setConfirmedLdt(LocalDateTime.now());
		selectedContextKeeper.getSelectedClient().getSelectedScheduleItem().setDeletedDate(new Date());
		selectedContextKeeper.getSelectedClient().getSelectedScheduleItem().setDeletedUser(getCurrentUsername());
		scheduleItemUpdateEvent.fire(selectedContextKeeper.getSelectedClient().getSelectedScheduleItem());
		selectedContextKeeper.getSelectedClient().setSelectedScheduleItem(null);

		return PageUrlHelper.redirect(prepareEditSessionDocumentation());
	}

	public String doEditSessionDocumentation() {
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation())) {
			return null;
		}
		return PageUrlHelper.redirect(prepareEditSessionDocumentation());
	}

	public String prepareEditSessionDocumentation() {
		selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation().setBackUrl(PageUrl.DASHBOARD_VIEW);
		if (selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()
				.getTherapeuticalAnamnesis() == null) {
			return PageUrl.BODYMAP_EDIT;
		}
		return PageUrl.SESSION_DOCUMENTATION_EDIT;
	}

	public String doAddInvoice() {
		return PageUrlHelper.redirect(PageUrl.INVOICE_EDIT);
	}

	public void doShowNextScheduleEntries(int mins) {
		setNextScheduleEntriesMins(mins);
		setNextScheduleEntriesStartLdt(DateUtils.adjustWeekStartLdt(LocalDateTime.now()));
		prepareNextScheduleEntries();
	}

	public void doHideNextScheduleEntries() {
		setSelectedScheduleEntry(null);
		setNextScheduleEntries(null);
		setNextScheduleEntriesMins(0);
	}

	public void doShowPreviewScheduleEntries(ScheduleEntry scheduleEntry) {
		setSelectedScheduleEntry(scheduleEntry);
	}

	public void doHidePreviewScheduleEntries() {
		setSelectedScheduleEntry(null);
	}

	public void doDecrNextScheduleEntriesWeekOfYear() {
		setNextScheduleEntriesStartLdt(LocalDateTime.of(
				getNextScheduleEntriesStartLdt().with(TemporalAdjusters.previous(DayOfWeek.MONDAY)).toLocalDate(),
				LocalTime.of(0, 0)));
		prepareNextScheduleEntries();
	}

	public void doIncrNextScheduleEntriesWeekOfYear() {
		setNextScheduleEntriesStartLdt(LocalDateTime.of(
				getNextScheduleEntriesStartLdt().with(TemporalAdjusters.next(DayOfWeek.MONDAY)).toLocalDate(),
				LocalTime.of(0, 0)));
		prepareNextScheduleEntries();
	}

	public void doSaveScheduleEntry(ScheduleEntry scheduleEntry) {
		doHideNextScheduleEntries();
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return;
		}
		if (selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().isDone()) {
			return;
		}
		final ScheduleItem scheduleItem = ScheduleItem
				.getInstance(scheduleEntry.getStartLdt(), scheduleEntry.getEndLdt())
				.withClient(selectedContextKeeper.getSelectedClient());
		scheduleItemAddEvent.fire(scheduleItem);
		selectedContextKeeper.refreshSelectedClient();
		prepareTimeline();
	}

}
