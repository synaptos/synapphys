package at.synaptos.synapphys.controller;

public abstract class AbstractEditController extends AbstractViewController {

	public abstract String doSave();

	public abstract String doCancel();

}
