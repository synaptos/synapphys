package at.synaptos.synapphys.controller.walter;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.component.datatable.DataTable;

import at.synaptos.synapphys.controller.AbstractViewController;
import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.PatientStatus;
import at.synaptos.synapphys.producer.ClientProducer;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.TagAttribute;

@ViewScoped
@Named
public class ClientSelectController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = -7075481438353349600L;

	@Inject
	private Logger logger;

	@Inject
	@Updated
	private Event<Client> clientUpdateEvent;

	@Inject
	private ClientFacade clientFacade;

	@Inject
	private ClientProducer clientProducer;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	private List<Client> filteredClients;

	private PatientStatus filteredByPatientStatus;

	private List<Object[]> searchItems;

	private Client selectedClient;

	private String selectedSearchKey;

	@PostConstruct
	private void init() {
		setSearchItems(clientFacade.getSearchItems());
		doLoadCurrentClients();
		logger.info("View initialized, filteredClients.size()=" + CollectionUtils.size(getFilteredClients()));
	}

	public List<Client> getFilteredClients() {
		return filteredClients;
	}

	private void setFilteredClients(List<Client> filteredClients) {
		this.filteredClients = filteredClients;
	}

	public PatientStatus getFilteredByPatientStatus() {
		return filteredByPatientStatus;
	}

	private void setFilteredByPatientStatus(PatientStatus filteredByPatientStatus) {
		this.filteredByPatientStatus = filteredByPatientStatus;
	}

	public boolean isFilteredByPatientStatusCurrent() {
		return getFilteredByPatientStatus() == PatientStatus.CURRENT;
	}

	public boolean isFilteredByPatientStatusBookmarked() {
		return getFilteredByPatientStatus() == PatientStatus.BOOKMARKED;
	}

	/**
	 * Liefert alle ermittelten Search Items. Ein Search Item besteht aus einem
	 * Search Key und der Anzahl des Vorkommens.
	 * 
	 * @return
	 */
	public List<Object[]> getSearchItems() {
		return searchItems;
	}

	private void setSearchItems(List<Object[]> searchItems) {
		this.searchItems = searchItems;
	}

	public Client getSelectedClient() {
		return selectedClient;
	}

	public void setSelectedClient(Client selectedClient) {
		this.selectedClient = selectedClient;
	}

	/**
	 * Selektiert alle Klienten, deren Nachname mit dem Search Key beginnt.
	 * 
	 * @param searchKey
	 */
	public void doLoadClientsBySearchKey(String searchKey) {
		setFilteredClients(clientFacade.getClients(searchKey));
		setFilteredByPatientStatus(null);
	}

	/**
	 * Selektiert alle aktuellen Klienten. Das sind Klienten, die eine laufende
	 * Serie haben.
	 */
	public void doLoadCurrentClients() {
		setFilteredClients(clientFacade.getCurrentClients());
		setFilteredByPatientStatus(PatientStatus.CURRENT);
	}

	/**
	 * Selektiert alle wartenden Klienten. Das sind Klienten, die sich in der
	 * Warteliste befinden. Ein Klient ist in der Warteliste bis eine Serie
	 * gestartet oder wieder aus der Liste entfernt wird.
	 */
	public void doLoadBookmarkedClients() {
		setFilteredClients(clientFacade.getBookmarkedClients());
		setFilteredByPatientStatus(PatientStatus.BOOKMARKED);
	}

	public String doAddClient() {
		return doEditClient(Client.getInstance());
	}

	public String doEditClient(Client client) {
		clientProducer.setPreparedClient(client.withBackUrl(PageUrl.CLIENT_SELECT));
		return PageUrlHelper.redirect(PageUrl.CLIENT_EDIT);
	}

	/**
	 * Feuert, wenn ein Klient in der Liste gewählt wird. Klient wird in den
	 * Context Keeper gesetzt und damit auch für andere Views verfügbar.
	 * Anschließend wird das Dashboard für die Serie aufgerufen.
	 * 
	 * @throws IOException
	 */
	public void onRowSelectClient() throws IOException {
		selectedContextKeeper.setSelectedClient(getSelectedClient());
		facesContext.getExternalContext().redirect(
				facesContext.getExternalContext().getApplicationContextPath() + PageUrl.SERIES_DASHBOARD + ".jsf");
	}

	/**
	 * Setzt den gewählten Klienten auf die Warteliste oder entfernt diesen
	 * wieder.
	 * 
	 * @param client
	 */
	public void doSaveBookmarkClient(Client client) {
		clientUpdateEvent.fire(client);
	}

	/**
	 * Setzt den gewählten Klienten auf die Warteliste oder entfernt diesen
	 * wieder und aktualisert die Liste.
	 * 
	 * @param client
	 */
	public void doSaveBookmarkClientForBookmarked(Client client) {
		doSaveBookmarkClient(client);
		setFilteredClients(clientFacade.getBookmarkedClients());
	}

	public String getSelectedSearchKey() {
		return selectedSearchKey;
	}

	public void setSelectedSearchKey(String selectedSearchKey) {
		this.selectedSearchKey = selectedSearchKey;
	}

	public String doLoadClientsBySelectedSearchKey() {
		DataTable dataTable = (DataTable) facesContext.getViewRoot().findComponent("clientForm:clients");
		if (!dataTable.getFilters().isEmpty()) {
			dataTable.reset();
		}
		if (TagAttribute.CLIENT_SEARCH_KEY_CURRENT.equals(getSelectedSearchKey())) {
			doLoadCurrentClients();
		} else if (TagAttribute.CLIENT_SEARCH_KEY_BOOKMARKED.equals(getSelectedSearchKey())) {
			doLoadBookmarkedClients();
		} else {
			doLoadClientsBySearchKey(getSelectedSearchKey());
		}
		requestContext.update("clientForm:clients");
		return null;
	}

	// TODO: PS Avatar
	public String getPhotoUrl(Client client) {
		// bestimme pfad zum image
		// prüfe ob file im upload-verzeichnis
		// wenn ja url zurück geben
		return GlobalNames.EXTERNAL_RESOURCE_URL + "/" + "client_" + client.getId() + "_photo.jpeg";
		// sonst avatar
	}

}
