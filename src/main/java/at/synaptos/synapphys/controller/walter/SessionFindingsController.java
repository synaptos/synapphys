package at.synaptos.synapphys.controller.walter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;

import com.google.gson.Gson;

import at.synaptos.synapphys.controller.AbstractViewController;
import at.synaptos.synapphys.domain.FindingNode;
import at.synaptos.synapphys.model.AnatomyBody;
import at.synaptos.synapphys.model.BodymapItem;
import at.synaptos.synapphys.model.FindingType;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class SessionFindingsController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = -4061391370169208671L;

	@Inject
	private Logger logger;

	@Inject
	@Added
	private Event<BodymapItem> bodymapItemAddEvent;
	@Inject
	@Updated
	private Event<BodymapItem> bodymapItemUpdateEvent;
	@Inject
	@Deleted
	private Event<BodymapItem> bodymapItemDeleteEvent;

	@Inject
	@Updated
	private Event<SeriesDocumentation> seriesDocumentationUpdateEvent;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	private FindingNode findingNode;

	public String getFirstFindingType() {
		List<FindingType> findingTypes = FindingType.toList();
		return findingTypes.get(0).name();
	}

	public List<FindingType> findFindingTypes(AnatomyBody anatomyBody) {
		return FindingType.toList(anatomyBody);
	}

	public String getFindingTypeAsJson() {
		return FindingType.toJson();
	}

	public String getBodymapItemsAsJson() {
		logger.debug("selectedSeriesDocumentation.get().getBodymapItems().size()=" + CollectionUtils
				.size(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getBodymapItems()));
		final List<FindingNode> findingNodes = new ArrayList<FindingNode>();
		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getBodymapItems()
				.forEach(bodymapItem -> {
					findingNodes.add(FindingNode.getInstance(bodymapItem));
				});
		String json = new Gson().toJson(findingNodes);
		return json;
	}

	public String getTypeFindingNodes() {
		return "java.util.List<at.synaptos.synapphys.domain.FindingNode>";
	}

	public FindingNode getFindingNode() {
		return findingNode;
	}

	private void setFindingNode(FindingNode findingNode) {
		this.findingNode = findingNode;
	}

	public void doAddFindingNode(FindingNode findingNode) {
		logger.debug("findingNode=" + findingNode);

		BodymapItem bodymapItem = new BodymapItem();
		bodymapItem.setFindingType(findingNode.getFindingType());
		bodymapItem.setFindingPositionX(findingNode.getFindingPositionX());
		bodymapItem.setFindingPositionY(findingNode.getFindingPositionY());
		bodymapItem.setBodySegment(findingNode.getBodySegment());
		bodymapItem.setValueF(findingNode.getValueF());
		bodymapItem.setValueT(findingNode.getValueT());
		bodymapItem.setFindingNote(findingNode.getNote());
		bodymapItem.setFindingText(findingNode.getText());
		bodymapItem.setTipPositionX(findingNode.getTipPositionX());
		bodymapItem.setTipPositionY(findingNode.getTipPositionY());
		bodymapItem.setSeriesDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());

		bodymapItemAddEvent.fire(bodymapItem);

		// TODO WQ in die Facade übersiedeln!
		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getBodymapItems().add(bodymapItem);

		setFindingNode(FindingNode.getInstance(bodymapItem, i18nContext));

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.addCallbackParam("validationFailed", false);
		requestContext.addCallbackParam("findingType", bodymapItem.getFindingType().name());
		requestContext.addCallbackParam("findingPositionX", bodymapItem.getFindingPositionX());
		requestContext.addCallbackParam("findingPositionY", bodymapItem.getFindingPositionY());
		requestContext.addCallbackParam("bodymapItemId", bodymapItem.getId());
		if (bodymapItem.getBodySegment() != null) {
			requestContext.addCallbackParam("bodySegment", bodymapItem.getBodySegment());
		}
		if (bodymapItem.getValueF() != null) {
			requestContext.addCallbackParam("valueF", bodymapItem.getValueF());
		}
		if (bodymapItem.getValueT() != null) {
			requestContext.addCallbackParam("valueT", bodymapItem.getValueT());
		}
		if (bodymapItem.getFindingNote() != null) {
			requestContext.addCallbackParam("note", bodymapItem.getFindingNote());
		}
		if (bodymapItem.getFindingText() != null) {
			requestContext.addCallbackParam("text", bodymapItem.getFindingText());
		}
		if (bodymapItem.getTipPositionX() != null) {
			requestContext.addCallbackParam("tipPositionX", bodymapItem.getTipPositionX());
		}
		if (bodymapItem.getTipPositionY() != null) {
			requestContext.addCallbackParam("tipPositionY", bodymapItem.getTipPositionY());
		}
		if (bodymapItem.getSequenceNo() != null) {
			requestContext.addCallbackParam("sequenceNo", bodymapItem.getSequenceNo());
		}
		requestContext.addCallbackParam("tempFindingNodeId", findingNode.getBodymapItemId());
	}

	public void doEditFindingNodeValues(Long bodymapItemId) {
		logger.debug("bodymapItemId=" + bodymapItemId);

		setFindingNode(FindingNode.getInstance());

		BodymapItem bodymapItem = getBodymapItem(bodymapItemId);
		if (bodymapItem == null) {
			return;
		}

		setFindingNode(FindingNode.getInstance(bodymapItem, i18nContext));
	}

	public void doSelectFindingTypeModifier(FindingType findingTypeModifier) {
		getFindingNode().setFindingTypeModifier(findingTypeModifier);
		getFindingNode().setValueOrDefaultF(null);
		getFindingNode().setValueOrDefaultT(null);
		getFindingNode().setTipPositionOrDefaultX(null);
		getFindingNode().setTipPositionOrDefaultY(null);
	}

	public void doSaveFindingNodeValues() {
		if (getFindingNode() == null) {
			return;
		}

		BodymapItem bodymapItem = getBodymapItem(getFindingNode().getBodymapItemId());
		if (bodymapItem == null) {
			return;
		}

		bodymapItem.setFindingTypeModifier(getFindingNode().getFindingTypeModifier());
		bodymapItem.setValueF(getFindingNode().getValueOrDefaultF());
		bodymapItem.setValueT(getFindingNode().getValueOrDefaultT());
		bodymapItem.setFindingNote(getFindingNode().getNote());
		bodymapItem.setFindingText(getFindingNode().getText());
		bodymapItem.setTipPositionX(getFindingNode().getTipPositionOrDefaultX());
		bodymapItem.setTipPositionY(getFindingNode().getTipPositionOrDefaultY());

		bodymapItemUpdateEvent.fire(bodymapItem);

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.addCallbackParam("validationFailed", false);
		requestContext.addCallbackParam("findingType", bodymapItem.getFindingType().name());
		if (bodymapItem.getFindingTypeModifier() != null) {
			requestContext.addCallbackParam("findingTypeModifier", bodymapItem.getFindingTypeModifier().name());
		}
		requestContext.addCallbackParam("findingPositionX", bodymapItem.getFindingPositionX());
		requestContext.addCallbackParam("findingPositionY", bodymapItem.getFindingPositionY());
		requestContext.addCallbackParam("bodymapItemId", bodymapItem.getId());
		if (bodymapItem.getBodySegment() != null) {
			requestContext.addCallbackParam("bodySegment", bodymapItem.getBodySegment());
		}
		if (bodymapItem.getValueF() != null) {
			requestContext.addCallbackParam("valueF", bodymapItem.getValueF());
		}
		if (bodymapItem.getValueT() != null) {
			requestContext.addCallbackParam("valueT", bodymapItem.getValueT());
		}
		if (bodymapItem.getFindingNote() != null) {
			requestContext.addCallbackParam("note", bodymapItem.getFindingNote());
		}
		if (bodymapItem.getFindingText() != null) {
			requestContext.addCallbackParam("text", bodymapItem.getFindingText());
		}
		if (bodymapItem.getTipPositionX() != null) {
			requestContext.addCallbackParam("tipPositionX", bodymapItem.getTipPositionX());
		}
		if (bodymapItem.getTipPositionY() != null) {
			requestContext.addCallbackParam("tipPositionY", bodymapItem.getTipPositionY());
		}
		if (bodymapItem.getSequenceNo() != null) {
			requestContext.addCallbackParam("sequenceNo", bodymapItem.getSequenceNo());
		}
	}

	public void doUpdateFindingNodeTipPosition(FindingNode findingNode) {
		if (findingNode == null) {
			return;
		}

		BodymapItem bodymapItem = getBodymapItem(findingNode.getBodymapItemId());
		if (bodymapItem == null) {
			return;
		}

		bodymapItem.setTipPositionX(findingNode.getTipPositionX());
		bodymapItem.setTipPositionY(findingNode.getTipPositionY());

		bodymapItemUpdateEvent.fire(bodymapItem);

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.addCallbackParam("validationFailed", false);
	}

	public void doUpdateFindingNodesPosition(List<FindingNode> findingNodes) {
		if (findingNodes == null || findingNodes.size() == 0) {
			return;
		}
		findingNodes.forEach(findingNode -> {
			BodymapItem bodymapItem = getBodymapItem(findingNode.getBodymapItemId());
			if (bodymapItem != null) {
				bodymapItem.setFindingPositionX(findingNode.getFindingPositionX());
				bodymapItem.setFindingPositionY(findingNode.getFindingPositionY());
				bodymapItem.setBodySegment(findingNode.getBodySegment());
				bodymapItemUpdateEvent.fire(bodymapItem);
			}
		});
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.addCallbackParam("validationFailed", false);
	}

	public void doDeleteFindingNodes(List<FindingNode> findingNodes) {
		if (findingNodes == null || findingNodes.size() == 0) {
			return;
		}
		findingNodes.forEach(findingNode -> {
			BodymapItem bodymapItem = getBodymapItem(findingNode.getBodymapItemId());
			if (bodymapItem != null) {
				bodymapItemDeleteEvent.fire(bodymapItem);
			}
		});
	}

	private BodymapItem getBodymapItem(Long bodymapItemId) {
		if (bodymapItemId == null) {
			return null;
		}
		for (BodymapItem bodymapItem : selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()
				.getBodymapItems()) {
			if (bodymapItem.getId().equals(bodymapItemId)) {
				return bodymapItem;
			}
		}
		return null;
	}

	public String doBack() {
		return PageUrlHelper
				.redirect(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation().getBackUrl());
	}

}
