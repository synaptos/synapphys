package at.synaptos.synapphys.controller.walter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;

import at.synaptos.synapphys.controller.AbstractViewController;
import at.synaptos.synapphys.facade.ServiceTypeFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.model.ServiceAttachment;
import at.synaptos.synapphys.model.ServiceDocumentation;
import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.model.ServiceTypeCategory;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.producer.ClientProducer;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.tenancy.CustomTenantIdentifierResolverResolver;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.UploadUtils;

@ViewScoped
@Named
public class SeriesDashboardController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 6109116700870714600L;

	@Inject
	private Logger logger;

	@Inject
	@Updated
	private Event<Client> clientUpdateEvent;

	@Inject
	@Added
	private Event<SeriesDocumentation> seriesDocumentationAddEvent;
	@Inject
	@Updated
	private Event<SeriesDocumentation> seriesDocumentationUpdateEvent;
	@Inject
	@Deleted
	private Event<SeriesDocumentation> seriesDocumentationDeletedEvent;

	@Inject
	@Added
	private Event<SessionDocumentation> sessionDocumentationAddEvent;
	@Inject
	@Updated
	private Event<SessionDocumentation> sessionDocumentationUpdateEvent;
	@Inject
	@Deleted
	private Event<SessionDocumentation> sessionDocumentationDeleteEvent;

	@Inject
	@Added
	private Event<ServiceDocumentation> serviceDocumentationAddEvent;
	@Inject
	@Deleted
	private Event<ServiceDocumentation> serviceDocumentationDeleteEvent;

	@Inject
	@Added
	private Event<ServiceAttachment> serviceAttachmentAddEvent;
	@Inject
	@Deleted
	private Event<ServiceAttachment> serviceAttachmentDeleteEvent;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	@Inject
	private ServiceTypeFacade serviceTypeFacade;

	@Inject
	private ClientProducer clientProducer;

	private List<ServiceType> serviceTypes;

	private int sessionDocumentationActiveIndex;

	private TimelineModel timelineModel;

	private LocalDate timelineMinLd;
	private LocalDate timelineMaxLd;

	private String currentDialog;

	public String init() {
		// Da muss unbedingt ein Klient ausgewählt sein
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			return PageUrlHelper.redirect(PageUrl.CLIENT_SELECT);
		}
		// Wähle den Termin (und damit indirekt die Serie)
		selectedContextKeeper.getSelectedClient()
				.setSelectedSeriesDocumentation(selectedContextKeeper.getSelectedClient().getLastSeriesDocumentation());
		setSessionDocumentationActiveIndex(-1);
		return null;
	}

	public List<ServiceType> getServiceTypes() {
		if (serviceTypes == null) {
			setServiceTypes(serviceTypeFacade.getServiceTypes());
		}
		return serviceTypes;
	}

	public void setServiceTypes(List<ServiceType> serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	public int getSessionDocumentationActiveIndex() {
		return sessionDocumentationActiveIndex;
	}

	public void setSessionDocumentationActiveIndex(int sessionDocumentationActiveIndex) {
		this.sessionDocumentationActiveIndex = sessionDocumentationActiveIndex;
	}

	/**
	 * Prüft, ob das Beginndatum der Sitzung zwischem dem Bewilligungsdatum bzw.
	 * dem Bewilligungsdatum + 3 Monate liegt und liefert die entsprechende
	 * Style-Klasse für die Darstellung des Sitzungsdatums.
	 * 
	 * @param sessionDocumentation
	 * @param prefix
	 * @return
	 */
	public String styleClassForSessionOnTimeline(SessionDocumentation sessionDocumentation, String prefix) {
		if (sessionDocumentation.getSeriesDocumentation().getApprovalLd() == null) {
			return "";
		}
		if (sessionDocumentation.getStartLd().isBefore(sessionDocumentation.getSeriesDocumentation().getApprovalLd())) {
			return prefix + "red";
		}
		if (sessionDocumentation.getStartLd().isAfter(sessionDocumentation.getSeriesDocumentation().getDeadlineLd())) {
			return prefix + "red";
		}
		return "";
	}

	public TimelineModel getTimelineModel() {
		return timelineModel;
	}

	private void setTimelineModel(TimelineModel timelineModel) {
		this.timelineModel = timelineModel;
	}

	private LocalDate getTimelineMinLd() {
		return timelineMinLd;
	}

	private void setTimelineMinLd(LocalDate timelineMinLd) {
		this.timelineMinLd = timelineMinLd;
	}

	public Date getTimelineMinDate() {
		return DateUtils.asDate(getTimelineMinLd());
	}

	private LocalDate getTimelineMaxLd() {
		return timelineMaxLd;
	}

	private void setTimelineMaxLd(LocalDate timelineMaxLd) {
		this.timelineMaxLd = timelineMaxLd;
	}

	public Date getTimelineMaxDate() {
		return DateUtils.asDate(getTimelineMaxLd());
	}

	public String getCurrentDialog() {
		return currentDialog;
	}

	public void setCurrentDialog(String currentDialog) {
		this.currentDialog = currentDialog;
	}

	private void prepareTimeline() {
		setTimelineModel(new TimelineModel());

		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			return;
		}

		if (selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getApprovalLd() == null) {
			return;
		}

		List<TimelineEvent> timelineEvents = new ArrayList<TimelineEvent>();

		LocalDate timelineStartLd = selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()
				.getApprovalLd();
		LocalDate timelineEndLd = selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()
				.getDeadlineLd();

		timelineEvents
				.add(new TimelineEvent("Bewilligung " + DateUtils.getDateAsDDMM(DateUtils.asDate(timelineStartLd)),
						DateUtils.asDate(timelineStartLd), false, null, "timelineEventOuter"));
		timelineEvents.add(new TimelineEvent("Fristende " + DateUtils.getDateAsDDMM(DateUtils.asDate(timelineEndLd)),
				DateUtils.asDate(timelineEndLd), false, null, "timelineEventOuter"));

		setTimelineMinLd(timelineStartLd.minusMonths(1));
		setTimelineMaxLd(timelineEndLd.plusMonths(1));

		for (SessionDocumentation sessionDocumentation : selectedContextKeeper.getSelectedClient()
				.getSelectedSeriesDocumentation().getSessionDocumentations()) {
			timelineEvents.add(new TimelineEvent(DateUtils.getDateAsDDMM(sessionDocumentation.getStartDate()),
					sessionDocumentation.getStartDate(), false, null,
					"timelineEventInner" + styleClassForSessionOnTimeline(sessionDocumentation, "-")));
		}

		if (!CollectionUtils.isEmpty(timelineEvents)) {
			getTimelineModel().addAll(timelineEvents);
		}
	}

	/**
	 * Beim Wählen einer Serie den letzten Termin auswählen.
	 */
	public void onRowSelectSeriesDocumentation() {
		setSessionDocumentationActiveIndex(-1);
	}

	/**
	 * Beim Abwählen einer Serie wird automatisch wieder die letzte Serie
	 * ausgewählt.
	 */
	public void onRowUnselectSeriesDocumentation() {
		selectedContextKeeper.getSelectedClient()
				.setSelectedSeriesDocumentation(selectedContextKeeper.getSelectedClient().getLastSeriesDocumentation());
		onRowSelectSeriesDocumentation();
	}

	public String doEditClient() {
		clientProducer
				.setPreparedClient(selectedContextKeeper.getSelectedClient().withBackUrl(PageUrl.SERIES_DASHBOARD));
		return PageUrlHelper.redirect(PageUrl.CLIENT_EDIT);
	}

	public String doEditDiagnosis(SessionDocumentation sessionDocumentation) {
		selectedContextKeeper.getSelectedClient()
				.setSelectedSessionDocumentation(sessionDocumentation.withBackUrl(PageUrl.SERIES_DASHBOARD));
		return PageUrlHelper.redirect(PageUrl.BODYMAP_EDIT);
	}

	public String doEditKpi(SessionDocumentation sessionDocumentation) {
		selectedContextKeeper.getSelectedClient()
				.setSelectedSessionDocumentation(sessionDocumentation.withBackUrl(PageUrl.SERIES_DASHBOARD));
		return PageUrlHelper.redirect(PageUrl.SESSION_DOCUMENTATION_EDIT);
	}

	public String doAddInvoice() {
		return PageUrlHelper.redirect(PageUrl.INVOICE_EDIT);
	}

	/**
	 * Speichert den aktuellen Klienten.
	 */
	public void doSaveClient() {
		clientUpdateEvent.fire(selectedContextKeeper.getSelectedClient());
	}

	/**
	 * Speichert die aktuelle Serie.
	 */
	public void doSaveSeriesDocumentation() {
		seriesDocumentationUpdateEvent.fire(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
		selectedContextKeeper.refreshSelectedClient();
	}

	/**
	 * Erzeugt eine neue Sitzung.
	 */
	public void doAddSessionDocumentation() {
		// Sicherstellen, dass es immer eine offene Serie gibt
		if (!AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			// Keine Serie, also eine neue erzeugen
			SeriesDocumentation seriesDocumentation = SeriesDocumentation
					.getInstance(selectedContextKeeper.getSelectedClient());
			seriesDocumentationAddEvent.fire(seriesDocumentation);
			selectedContextKeeper.getSelectedClient().setSelectedSeriesDocumentation(seriesDocumentation);
		} else {
			// Gewählte Serie ist erledigt, also die letzte auswählen
			selectedContextKeeper.getSelectedClient().setSelectedSeriesDocumentation(
					selectedContextKeeper.getSelectedClient().getLastSeriesDocumentation());
			// Schauen, ob die letzte Serie bereits erledigt ist
			if (selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().isDone()) {
				// Wiederum neue Serie anlegen
				SeriesDocumentation seriesDocumentation = SeriesDocumentation
						.getInstance(selectedContextKeeper.getSelectedClient());
				seriesDocumentationAddEvent.fire(seriesDocumentation);
				selectedContextKeeper.getSelectedClient().setSelectedSeriesDocumentation(seriesDocumentation);
			}
		}

		// In der gewählten Serie die letzte Sitzung bestimmen
		SessionDocumentation lastSessionDocumentation = selectedContextKeeper.getSelectedClient()
				.getSelectedSeriesDocumentation().getLastSessionDocumentation();
		// Startdatum dieser Sitzung bestimmen
		LocalDateTime startLdt = DateUtils.nextOrSameQuarter(LocalDateTime.now());
		if (AbstractEntity.isValid(lastSessionDocumentation)) {
			if (!lastSessionDocumentation.getStartLdt().toLocalDate().isBefore(startLdt.toLocalDate())) {
				startLdt = DateUtils.nextOrSameQuarter(LocalDateTime
						.of(lastSessionDocumentation.getStartLdt().toLocalDate().plusDays(1), LocalTime.now()));
			}
		}
		// Jetzt die neue Sitzung erzeugen
		SessionDocumentation newSessionDocumentation = SessionDocumentation.getInstance(
				ScheduleItem.getInstance(startLdt, startLdt.plusMinutes(30)),
				selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
		// Die Hauptleistungen der letzten Sitzung in die neue Sitzung
		// übernehmen
		if (AbstractEntity.isValid(lastSessionDocumentation)) {
			lastSessionDocumentation.getServiceDocumentations().forEach(lastServiceDocumentation -> {
				if (lastServiceDocumentation.getServiceType().getCategory() == ServiceTypeCategory.C10_THERAPY) {
					newSessionDocumentation.getServiceDocumentations()
							.add(ServiceDocumentation.getInstance(lastServiceDocumentation));
				}
			});
		}
		// Neue Sitzung speichern
		sessionDocumentationAddEvent.fire(newSessionDocumentation);
		setSessionDocumentationActiveIndex(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()
				.getSessionDocumentations().indexOf(newSessionDocumentation));
	}

	/**
	 * Speichert die aktuelle Session.
	 */
	public void doSaveSessionDocumentation() {
		sessionDocumentationUpdateEvent
				.fire(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation());
	}

	/**
	 * Löscht die Sitzung und die Serie, falls es die letzte war.
	 */
	public void doDeleteSessionDocumentation(SessionDocumentation sessionDocumentation) {
		sessionDocumentationDeleteEvent.fire(sessionDocumentation);
		// Serie löschen, wenn sie keinen Termin mehr hat
		if (CollectionUtils.isEmpty(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation()
				.getSessionDocumentations())) {
			seriesDocumentationDeletedEvent
					.fire(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
		}
		// Wähle den Termin (und damit indirekt die Serie)
		selectedContextKeeper.getSelectedClient()
				.setSelectedSeriesDocumentation(selectedContextKeeper.getSelectedClient().getLastSeriesDocumentation());
		setSessionDocumentationActiveIndex(-1);
		if (AbstractEntity.isValid(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation())) {
			setSessionDocumentationActiveIndex(0);
		}
	}

	/**
	 * Erzeugt eine neues Service für den Typ.
	 * 
	 * @param serviceType
	 */
	public void doAddServiceDocumentation(ServiceType serviceType) {
		ServiceDocumentation serviceDocumentation = ServiceDocumentation.getInstance(serviceType);
		serviceDocumentation
				.setSessionDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation());
		serviceDocumentationAddEvent.fire(serviceDocumentation);
	}

	/**
	 * Löscht das Service.
	 * 
	 * @param serviceDocumentation
	 */
	public void doDeleteServiceDocumentation(ServiceDocumentation serviceDocumentation) {
		serviceDocumentationDeleteEvent.fire(serviceDocumentation);
	}

	public void handleFileUpload(FileUploadEvent event) {
		String sourceFilename = event.getFile().getFileName();
		logger.trace("sourceFilename=" + sourceFilename);

		String targetFilename = selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getId() + "_"
				+ System.currentTimeMillis() + "_" + sourceFilename;
		logger.trace("targetFilename=" + targetFilename);

		try {
			UploadUtils.dumpFile(event.getFile().getInputstream(),
					UploadUtils.getUploadsDir(CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()),
					targetFilename);

			ServiceAttachment serviceAttachment = ServiceAttachment.getInstance();
			serviceAttachment.setContentType(event.getFile().getContentType());
			serviceAttachment.setSourceFilename(sourceFilename);
			serviceAttachment.setTargetFilename(targetFilename);
			serviceAttachment
					.setSeriesDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
			serviceAttachmentAddEvent.fire(serviceAttachment);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StreamedContent handleFileDownload(ServiceAttachment serviceAttachment) {
		logger.debug("serviceAttachment=" + serviceAttachment);

		String sourceFilename = serviceAttachment.getSourceFilename();
		logger.trace("sourceFilename=" + sourceFilename);

		String targetFilename = serviceAttachment.getTargetFilename();
		logger.trace("targetfilename=" + targetFilename);

		String targetPath = UploadUtils.getUploadsDir(
				CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()) + "/" + targetFilename;
		logger.trace("targetPath=" + targetPath);

		try {
			InputStream stream = new FileInputStream(new File(targetPath));
			return new DefaultStreamedContent(stream, serviceAttachment.getContentType(), sourceFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void doDeleteServiceAttachment(ServiceAttachment serviceAttachment) {
		serviceAttachmentDeleteEvent.fire(serviceAttachment);
	}

	public void doToggleSessionsView() {
		if (getTimelineModel() == null) {
			prepareTimeline();
		} else {
			setTimelineModel(null);
		}
	}

	public String doEditBodymap(SessionDocumentation sessionDocumentation) {
		selectedContextKeeper.getSelectedClient().setSelectedSessionDocumentation(sessionDocumentation);
		selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation()
				.setBackUrl(PageUrl.SERIES_DASHBOARD);
		return PageUrlHelper.redirect(PageUrl.SESSION_FINDINGS_MAP);
	}

	public String doEditFindingsSheet() {
		return PageUrlHelper.redirect(PageUrl.SERIES_FINDINGS_SHEET);
	}

	public String doEditFindingsSheetLsp() {
		return PageUrlHelper.redirect(PageUrl.SERIES_FINDINGS_LSP);
	}

}
