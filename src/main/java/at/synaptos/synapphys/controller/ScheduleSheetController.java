package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleModel;

import at.synaptos.synapphys.domain.ScheduleEntry;
import at.synaptos.synapphys.facade.ClientFacade;
import at.synaptos.synapphys.facade.ScheduleItemFacade;
import at.synaptos.synapphys.facade.SessionDocumentationFacade;
import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.StringUtils;

@ViewScoped
@Named
public class ScheduleSheetController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 510341623420211156L;

	@Inject
	private Logger logger;

	@Inject
	@Added
	private Event<ScheduleItem> scheduleItemAddEvent;
	@Inject
	@Updated
	private Event<ScheduleItem> scheduleItemUpdateEvent;
	@Inject
	@Deleted
	private Event<ScheduleItem> scheduleItemDeleteEvent;

	@Inject
	@Updated
	private Event<SessionDocumentation> sessionDocumentationUpdateEvent;
	@Inject
	@Deleted
	private Event<SessionDocumentation> sessionDocumentationDeleteEvent;

	@Inject
	private ScheduleItemFacade scheduleItemFacade;

	@Inject
	private SessionDocumentationFacade sessionDocumentationFacade;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	@Inject
	private ClientFacade clientFacade;

	private List<Client> clients;

	private ScheduleModel scheduleModel;

	private ScheduleEntry scheduleEntry;

	@PostConstruct
	public void init() {
		scheduleModel = new LazyScheduleModel() {

			private static final long serialVersionUID = 8559474913341803027L;

			@Override
			public void loadEvents(Date startDate, Date endDate) {
				clear();
				final List<ScheduleEntry> officeHoursEntries = scheduleItemFacade.invertScheduleEntries(startDate,
						endDate, scheduleItemFacade.createScheduleEntries(startDate,
								configContext.getInstancePropertyOfficeHours()));
				officeHoursEntries.forEach(scheduleEntry -> addEvent(scheduleEntry));
				final List<ScheduleEntry> currScheduleEntries = createScheduleEntries(startDate, endDate);
				currScheduleEntries.forEach(scheduleEntry -> {
					addEvent(scheduleEntry);
				});
			}

			private List<ScheduleEntry> createScheduleEntries(final Date startDate, final Date endDate) {
				logger.debug("startDate=" + startDate + ", endDate=" + endDate);
				final List<ScheduleEntry> scheduleEntries = new ArrayList<ScheduleEntry>();
				scheduleEntries.addAll(scheduleItemFacade.createScheduleEntries(startDate, endDate));
				scheduleEntries.addAll(sessionDocumentationFacade.createScheduleEntries(startDate, endDate));
				logger.trace("scheduleEntries.size()=" + CollectionUtils.size(scheduleEntries));
				return scheduleEntries;
			}

		};
	}

	public List<Client> getClients() {
		return clients;
	}

	private void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public String getScheduleMinTimeHHMM() {
		int hour = configContext.getScheduleMinTime().getHour();
		int minute = configContext.getScheduleMinTime().getMinute();
		return String.format("%02d:%02d", hour, minute);
	}

	public String getScheduleMaxTimeHHMM() {
		int hour = configContext.getScheduleMaxTime().getHour();
		int minute = configContext.getScheduleMaxTime().getMinute();
		return String.format("%02d:%02d", hour, minute);
	}

	public ScheduleModel getScheduleModel() {
		return scheduleModel;
	}

	public ScheduleEntry getScheduleEntry() {
		return scheduleEntry;
	}

	private void setScheduleEntry(ScheduleEntry scheduleEntry) {
		this.scheduleEntry = scheduleEntry;
	}

	public List<Client> queryClients(String query) {
		logger.debug("query=" + query);

		if (StringUtils.isEmpty(query)) {
			return new ArrayList<Client>();
		}

		List<Client> filteredClients = getClients().stream()
				.filter(client -> client.getFullname().toLowerCase().startsWith(query.toLowerCase()))
				.collect(Collectors.toList());

		logger.debug("filteredClients.size()=" + CollectionUtils.size(filteredClients));
		return filteredClients;
	}

	public void onItemSelectClient(SelectEvent selectEvent) {
		Client currClient = (Client) selectEvent.getObject();
		logger.debug("currClient=" + currClient);
		getScheduleEntry().setClient(currClient);
	}

	public void onDateSelect(SelectEvent selectEvent) {
		setClients(clientFacade.getClients());
		LocalDateTime startLdt = DateUtils.asLdt((Date) selectEvent.getObject());
		LocalDateTime endLdt = startLdt.plusMinutes(30);
		setScheduleEntry(ScheduleEntry.getInstance(
				ScheduleItem.getInstance(startLdt, endLdt).withClient(selectedContextKeeper.getSelectedClient())));
	}

	public void onEventSelect(SelectEvent selectEvent) {
		setClients(clientFacade.getClients());
		setScheduleEntry((ScheduleEntry) selectEvent.getObject());
	}

	public void onEventMove(ScheduleEntryMoveEvent moveEvent) {
		setScheduleEntry((ScheduleEntry) moveEvent.getScheduleEvent());
		doSave();
	}

	public void onEventResize(ScheduleEntryResizeEvent resizeEvent) {
		setScheduleEntry((ScheduleEntry) resizeEvent.getScheduleEvent());
		doSave();
	}

	public void doSave() {
		logger.debug("scheduleEntry=" + getScheduleEntry());
		if (getScheduleEntry().getData() instanceof ScheduleItem) {
			ScheduleItem scheduleItem = (ScheduleItem) getScheduleEntry().getData();
			if (scheduleItem.isNew()) {
				scheduleItemAddEvent.fire(scheduleItem);
			} else {
				scheduleItemUpdateEvent.fire(scheduleItem);
			}
		} else if (getScheduleEntry().getData() instanceof SessionDocumentation) {
			SessionDocumentation sessionDocumentation = (SessionDocumentation) getScheduleEntry().getData();
			sessionDocumentationUpdateEvent.fire(sessionDocumentation);
		}
	}

	public void doDelete() {
		logger.debug("scheduleEntry=" + getScheduleEntry());
		if (!getScheduleEntry().getData().isNew()) {
			if (getScheduleEntry().getData() instanceof ScheduleItem) {
				scheduleItemDeleteEvent.fire((ScheduleItem) getScheduleEntry().getData());
			} else if (getScheduleEntry().getData() instanceof SessionDocumentation) {
				sessionDocumentationDeleteEvent.fire((SessionDocumentation) getScheduleEntry().getData());
			}
		}
		getScheduleModel().deleteEvent(getScheduleEntry());
	}

	public void doCancel() {
		logger.debug("scheduleEntry=" + getScheduleEntry());
		if (getScheduleEntry().getData().isNew()) {
			getScheduleModel().deleteEvent(getScheduleEntry());
		}
	}

	public String doForwardToDashboard() {
		logger.debug("scheduleEntry=" + getScheduleEntry());
		doSave();
		selectedContextKeeper.setSelectedClient(getScheduleEntry().getClient());
		if (AbstractEntity.isValid(selectedContextKeeper.getSelectedClient())) {
			selectedContextKeeper.getSelectedClient().setSelectedSeriesDocumentation(
					selectedContextKeeper.getSelectedClient().getLastSeriesDocumentation());
			if (getScheduleEntry().getData() instanceof ScheduleItem) {
				selectedContextKeeper.getSelectedClient()
						.setSelectedScheduleItem((ScheduleItem) getScheduleEntry().getData());
			} else if (getScheduleEntry().getData() instanceof SessionDocumentation) {
				selectedContextKeeper.getSelectedClient()
						.setSelectedSessionDocumentation((SessionDocumentation) getScheduleEntry().getData());
			}
		}
		return PageUrlHelper.redirect(PageUrl.DASHBOARD_VIEW);
	}

}
