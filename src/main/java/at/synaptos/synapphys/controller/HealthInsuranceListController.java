package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.facade.HealthInsuranceFacade;
import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.producer.HealthInsuranceProducer;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class HealthInsuranceListController implements Serializable {

	private static final long serialVersionUID = -8250737735530791086L;

	@Inject
	@Deleted
	private Event<HealthInsurance> healthInsuranceDeleteEvent;

	@Inject
	private HealthInsuranceProducer healthInsuranceProducer;

	@Inject
	private HealthInsuranceFacade healthInsuranceFacade;

	private List<HealthInsurance> healthInsurances;

	@PostConstruct
	private void init() {
		setHealthInsurances(healthInsuranceFacade.getHealthInsurances());
	}

	public List<HealthInsurance> getHealthInsurances() {
		return healthInsurances;
	}

	private void setHealthInsurances(List<HealthInsurance> healthInsurances) {
		this.healthInsurances = healthInsurances;
	}

	public String doAdd() {
		return doEdit(HealthInsurance.getInstance());
	}

	public String doEdit(HealthInsurance healthInsurance) {
		healthInsuranceProducer.setPreparedHealthInsurance(healthInsurance.withBackUrl(PageUrl.HEALTH_INSURANCE_LIST));
		return PageUrlHelper.redirect(PageUrl.HEALTH_INSURANCE_EDIT);
	}

	public void doDelete(HealthInsurance healthInsurance) {
		healthInsuranceDeleteEvent.fire(healthInsurance);
		getHealthInsurances().remove(healthInsurance);
	}

}
