package at.synaptos.synapphys.controller;

import java.io.File;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.FileUploadEvent;

import at.synaptos.synapphys.domain.UsermetaContext;
import at.synaptos.synapphys.tenancy.CustomTenantIdentifierResolverResolver;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.StringUtils;
import at.synaptos.synapphys.utils.TagAttribute;
import at.synaptos.synapphys.utils.UploadUtils;

@ViewScoped
@Named
public class SettingsBillingController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = -5595890893882669571L;

	@Inject
	private Logger logger;

	@Inject
	private UsermetaContext usermetaContext;

	public void doSave() {
		configContext.updateProperties();
		usermetaContext.updateProperties();
		addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_INFO, "action.save.SUCCESS");
	}

	public void handleLogoUpload(FileUploadEvent event) {
		String sourceFilename = event.getFile().getFileName();
		logger.trace("sourceFilename=" + sourceFilename);

		String targetFilename = "logo_" + System.currentTimeMillis() + "_" + sourceFilename;
		logger.trace("targetFilename=" + targetFilename);

		try {
			UploadUtils.dumpFile(event.getFile().getInputstream(),
					UploadUtils.getUploadsDir(CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()),
					targetFilename);

			String previousFilename = configContext.getLogoFilename();
			logger.trace("previousFilename=" + previousFilename);

			configContext.setLogoFilename(targetFilename);
			configContext.updateProperties();

			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_GLOBAL_PANEL, FacesMessage.SEVERITY_INFO,
					GlobalNames.MESSAGE_KEY_HANDLE_FILE_UPLOAD_SUCCESS, sourceFilename);
			logger.debug(msgText);

			if (!StringUtils.isEmpty(previousFilename)) {
				String previousPath = UploadUtils.getUploadsDir(
						CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()) + "/" + previousFilename;
				logger.trace("previousPath=" + previousPath);
				if (!(new File(previousPath)).delete()) {
					logger.warn("Couldn't delete file [" + previousPath + "]");
				}
			}
		} catch (Exception e) {
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_GLOBAL_PANEL, FacesMessage.SEVERITY_INFO,
					GlobalNames.MESSAGE_KEY_HANDLE_FILE_UPLOAD_ERROR, sourceFilename);
			logger.error(msgText, e);
		}
	}

	public void doDeleteLogo() {
		String previousFilename = configContext.getLogoFilename();
		logger.trace("previousFilename=" + previousFilename);

		configContext.setLogoFilename(null);
		configContext.updateProperties();

		if (!StringUtils.isEmpty(previousFilename)) {
			String previousPath = UploadUtils.getUploadsDir(
					CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()) + "/" + previousFilename;
			logger.trace("previousPath=" + previousPath);
			if (!(new File(previousPath)).delete()) {
				logger.warn("Couldn't delete file [" + previousPath + "]");
			}
		}
	}

}
