package at.synaptos.synapphys.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import at.synaptos.synapphys.model.ServiceAttachment;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.tenancy.CustomTenantIdentifierResolverResolver;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.TagAttribute;
import at.synaptos.synapphys.utils.UploadUtils;

@ViewScoped
@Named
public class ServiceAttachmentsEditController extends AbstractEditController implements Serializable {

	private static final long serialVersionUID = -1065720333503869616L;

	@Inject
	private Logger logger;

	@Inject
	@Added
	private Event<ServiceAttachment> serviceAttachmentAddEvent;
	@Inject
	@Deleted
	private Event<ServiceAttachment> serviceAttachmentDeleteEvent;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	public void handleFileUpload(FileUploadEvent event) {
		String sourceFilename = event.getFile().getFileName();
		logger.trace("sourceFilename=" + sourceFilename);

		String targetFilename = selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getId() + "_"
				+ System.currentTimeMillis() + "_" + sourceFilename;
		logger.trace("targetFilename=" + targetFilename);

		try {
			UploadUtils.dumpFile(event.getFile().getInputstream(),
					UploadUtils.getUploadsDir(CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()),
					targetFilename);

			ServiceAttachment serviceAttachment = ServiceAttachment.getInstance();
			serviceAttachment.setContentType(event.getFile().getContentType());
			serviceAttachment.setSourceFilename(sourceFilename);
			serviceAttachment.setTargetFilename(targetFilename);
			serviceAttachment.setSessionDocumentation(
					selectedContextKeeper.getSelectedClient().getSelectedSessionDocumentation());
			serviceAttachmentAddEvent.fire(serviceAttachment);
			selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getServiceAttachments()
					.add(serviceAttachment);

			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_GLOBAL_PANEL, FacesMessage.SEVERITY_INFO,
					GlobalNames.MESSAGE_KEY_HANDLE_FILE_UPLOAD_SUCCESS, sourceFilename);
			logger.debug(msgText);
		} catch (Exception e) {
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_GLOBAL_PANEL, FacesMessage.SEVERITY_INFO,
					GlobalNames.MESSAGE_KEY_HANDLE_FILE_UPLOAD_ERROR, sourceFilename);
			logger.error(msgText, e);
		}
	}

	public StreamedContent handleFileDownload(ServiceAttachment serviceAttachment) {
		logger.debug("serviceAttachment=" + serviceAttachment);

		String sourceFilename = serviceAttachment.getSourceFilename();
		logger.trace("sourceFilename=" + sourceFilename);

		String targetFilename = serviceAttachment.getTargetFilename();
		logger.trace("targetfilename=" + targetFilename);

		String targetPath = UploadUtils.getUploadsDir(
				CustomTenantIdentifierResolverResolver.getCurrentTenantIdentifier()) + "/" + targetFilename;
		logger.trace("targetPath=" + targetPath);

		try {
			InputStream stream = new FileInputStream(new File(targetPath));
			return new DefaultStreamedContent(stream, serviceAttachment.getContentType(), sourceFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String doSave() {
		return doCancel();
	}

	@Override
	public String doCancel() {
		return PageUrlHelper
				.redirect(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getBackUrl());
	}

	public void doDelete(ServiceAttachment serviceAttachment) {
		serviceAttachmentDeleteEvent.fire(serviceAttachment);
		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getServiceAttachments()
				.remove(serviceAttachment);

	}

}
