package at.synaptos.synapphys.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.domain.CustomPrincipal;
import at.synaptos.synapphys.domain.I18nContext;
import at.synaptos.synapphys.utils.GlobalI18n;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.StringUtils;

@ViewScoped
@Named
public class UserViewController implements Serializable {

	private static final long serialVersionUID = -8510816596870712509L;

	@Inject
	private FacesContext facesContext;

	@Inject
	@GlobalI18n
	private I18nContext i18nContext;

	public String getTopbarSrc() {
		ExternalContext externalContext = facesContext.getExternalContext();
		return externalContext.isUserInRole("user") ? "topbarDefault.xhtml" : "topbarReceipt.xhtml";
	}

	public String getHomeUrl() {
		ExternalContext externalContext = facesContext.getExternalContext();
		String homeUrl = ((CustomPrincipal) externalContext.getUserPrincipal()).getHomeUrl();
		if (StringUtils.isEmpty(homeUrl)) {
			homeUrl = externalContext.getApplicationContextPath();
		}
		return homeUrl;
	}

	public String getLogoutUrl() {
		boolean fromRemoteSession = Boolean
				.valueOf(System.getProperty(GlobalNames.SYSTEM_PROPERTY_KEY_LOGOUT_FROM_REMOTE_SESSION, "true"));
		ExternalContext externalContext = facesContext.getExternalContext();
		String logoutUrl = null;
		if (fromRemoteSession) {
			logoutUrl = ((CustomPrincipal) externalContext.getUserPrincipal()).getLogoutUrl();
		}
		if (StringUtils.isEmpty(logoutUrl)) {
			logoutUrl = externalContext.getApplicationContextPath();
		}
		return logoutUrl;
	}

	public String getAccountUrl() {
		ExternalContext externalContext = facesContext.getExternalContext();
		String accountUrl = ((CustomPrincipal) externalContext.getUserPrincipal()).getAccountUrl();
		return accountUrl;
	}

	public String getUsername() {
		ExternalContext externalContext = facesContext.getExternalContext();
		String username = ((CustomPrincipal) externalContext.getUserPrincipal()).getName();
		return username;
	}

	public boolean isSpecialUser() {
		ExternalContext externalContext = facesContext.getExternalContext();
		String username = ((CustomPrincipal) externalContext.getUserPrincipal()).getName();
		if ("walter.quendler".equals(username) || "alex".equals(username) || "m.friede".equals(username)
				|| "phipse".equals(username) || "a".equals(username)) {
			return true;
		}
		return false;
	}

	public void logout() throws IOException {
		ExternalContext externalContext = facesContext.getExternalContext();
		externalContext.invalidateSession();
		externalContext.redirect(getLogoutUrl());
	}

}
