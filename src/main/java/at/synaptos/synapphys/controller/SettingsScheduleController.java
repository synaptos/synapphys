package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import at.synaptos.synapphys.domain.OfficeBreaks;
import at.synaptos.synapphys.domain.OfficeHours;
import at.synaptos.synapphys.domain.ScheduleWeightings;
import at.synaptos.synapphys.utils.DateUtils;

@ViewScoped
@Named
public class SettingsScheduleController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 2527461221065067578L;

	private OfficeHours officeHours;

	private OfficeBreaks officeBreaks;

	private ScheduleWeightings scheduleWeightings;

	private Date scheduleMinTime;

	private Date scheduleMaxTime;

	@PostConstruct
	public void init() {
		setOfficeHours(configContext.getInstancePropertyOfficeHours());
		setOfficeBreaks(configContext.getInstancePropertyOfficeBreaks());
		setScheduleWeightings(configContext.getInstancePropertyScheduleWeightings());
		setScheduleMinTime(DateUtils.asDate(LocalDateTime.of(LocalDate.now(), configContext.getScheduleMinTime())));
		setScheduleMaxTime(DateUtils.asDate(LocalDateTime.of(LocalDate.now(), configContext.getScheduleMaxTime())));
	}

	public OfficeHours getOfficeHours() {
		return officeHours;
	}

	private void setOfficeHours(OfficeHours officeHours) {
		this.officeHours = officeHours;
	}

	public OfficeBreaks getOfficeBreaks() {
		return officeBreaks;
	}

	public void setOfficeBreaks(OfficeBreaks officeBreaks) {
		this.officeBreaks = officeBreaks;
	}

	public ScheduleWeightings getScheduleWeightings() {
		return scheduleWeightings;
	}

	private void setScheduleWeightings(ScheduleWeightings scheduleWeightings) {
		this.scheduleWeightings = scheduleWeightings;
	}

	public Date getScheduleMinTime() {
		return scheduleMinTime;
	}

	public void setScheduleMinTime(Date scheduleMinTime) {
		this.scheduleMinTime = scheduleMinTime;
	}

	public Date getScheduleMaxTime() {
		return scheduleMaxTime;
	}

	public void setScheduleMaxTime(Date scheduleMaxTime) {
		this.scheduleMaxTime = scheduleMaxTime;
	}

	public void doSave() {
		configContext.setInstancePropertyOfficeHours(getOfficeHours());
		configContext.setInstancePropertyOfficeBreaks(getOfficeBreaks());
		configContext.setInstancePropertyScheduleWeightings(getScheduleWeightings());
		configContext.setScheduleMinTime(DateUtils.asLdt(getScheduleMinTime()).toLocalTime());
		configContext.setScheduleMaxTime(DateUtils.asLdt(getScheduleMaxTime()).toLocalTime());
		configContext.updateProperties();
	}

	public String dayOfWeekName(int dayOfWeek) {
		String dayOfWeekName = "";
		if (dayOfWeek == 0) {
			dayOfWeekName = "Mo";
		} else if (dayOfWeek == 1) {
			dayOfWeekName = "Di";
		} else if (dayOfWeek == 2) {
			dayOfWeekName = "Mi";
		} else if (dayOfWeek == 3) {
			dayOfWeekName = "Do";
		} else if (dayOfWeek == 4) {
			dayOfWeekName = "Fr";
		} else if (dayOfWeek == 5) {
			dayOfWeekName = "Sa";
		} else if (dayOfWeek == 6) {
			dayOfWeekName = "So";
		}
		return dayOfWeekName;
	}

}
