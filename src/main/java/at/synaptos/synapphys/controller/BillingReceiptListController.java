package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import at.synaptos.synapphys.model.BillingType;

@ViewScoped
@Named
public class BillingReceiptListController extends AbstractBillingListController implements Serializable {

	private static final long serialVersionUID = 191821242176868883L;

	@Override
	public BillingType getBillingType() {
		return BillingType.RECEIPT;
	}

}
