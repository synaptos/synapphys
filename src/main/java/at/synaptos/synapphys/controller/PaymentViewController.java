package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.FileUploadEvent;

import at.synaptos.synapphys.domain.BankTransfer;
import at.synaptos.synapphys.facade.BillingFacade;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.TagAttribute;

@ViewScoped
@Named
public class PaymentViewController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = -6793970679461309568L;

	@Inject
	private Logger logger;

	@Inject
	@Updated
	private Event<List<Billing>> billingsUpdateEvent;

	@Inject
	private BillingFacade billingFacade;

	private List<Billing> billings;

	private Billing selectedBilling;

	private List<BankTransfer> unmatchedBankTransfers;

	@PostConstruct
	private void init() {
		refreshBillings();
	}

	public List<Billing> getBillings() {
		return billings;
	}

	private void setBillings(List<Billing> billings) {
		this.billings = billings;
	}

	public Billing getSelectedBilling() {
		return selectedBilling;
	}

	public void setSelectedBilling(Billing selectedBilling) {
		this.selectedBilling = selectedBilling;
	}

	public List<BankTransfer> getUnmatchedBankTransfers() {
		return unmatchedBankTransfers;
	}

	private void setUnmatchedBankTransfers(List<BankTransfer> unmatchedBankTransfers) {
		this.unmatchedBankTransfers = unmatchedBankTransfers;
	}

	private void refreshBillings() {
		setBillings(billingFacade.getUnpaidBillings(BillingType.INVOICE));
	}

	public void onRowSelectBilling() {
		if (getSelectedBilling().getPaymentLdt() != null) {
			setSelectedBilling(null);
		}
	}

	public void doSave() {
		billingsUpdateEvent.fire(getBillings());
		setSelectedBilling(null);
		refreshBillings();
	}

	public void doMatch(BankTransfer bankTransfer) {
		if (getSelectedBilling() == null) {
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_ERROR,
					GlobalNames.MESSAGE_KEY_BANK_TRANSFER_NO_INVOICE_SELECTED_ERROR);
			logger.debug(msgText);
			return;
		}
		if (getSelectedBilling().getPaymentLdt() != null) {
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_ERROR,
					GlobalNames.MESSAGE_KEY_BANK_TRANSFER_INVOICE_HAS_BEEN_PAID_ERROR);
			logger.debug(msgText);
			return;
		}
		if (bankTransfer.getBookingLd().isBefore(getSelectedBilling().getBillingLdt().toLocalDate())) {
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_ERROR,
					GlobalNames.MESSAGE_KEY_BANK_TRANSFER_BOOKING_DATE_TO_OLD_ERROR);
			logger.debug(msgText);
			return;
		}
		if (!bankTransfer.getAmount().equals(getSelectedBilling().getTotal())) {
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_WARN,
					GlobalNames.MESSAGE_KEY_BANK_TRANSFER_AMOUNT_MISMATCH_WARN);
			logger.debug(msgText);
		}
		getUnmatchedBankTransfers().remove(bankTransfer);
		getSelectedBilling().setPaymentDate(DateUtils.asDate(bankTransfer.getBookingLd()));
		getSelectedBilling().setPaymentNo(bankTransfer.getBookingNo());
		getSelectedBilling().setPaymentNote(bankTransfer.getBookingText());
		getSelectedBilling().setBankTransfer(bankTransfer);
		getSelectedBilling().setChanged(true);
	}

	public void doUnmatch(Billing billing) {
		setSelectedBilling(null);
		getUnmatchedBankTransfers().add(billing.getBankTransfer());
		getUnmatchedBankTransfers()
				.sort((BankTransfer l1, BankTransfer l2) -> l2.getBookingLd().compareTo(l1.getBookingLd()));
		billing.setBankTransfer(null);
		billing.setPaymentDate(null);
		billing.setPaymentNo(null);
		billing.setPaymentNote(null);
		billing.setChanged(true);
	}

	public void handleFileUpload(FileUploadEvent event) {
		String sourceFilename = event.getFile().getFileName();
		logger.trace("sourceFilename=" + sourceFilename);

		setSelectedBilling(null);

		setUnmatchedBankTransfers(new ArrayList<BankTransfer>());

		LocalDate oldestBillingLd = LocalDate.now();
		if (!CollectionUtils.isEmpty(getBillings())) {
			oldestBillingLd = getBillings().get(getBillings().size() - 1).getBillingLdt().toLocalDate();
		}

		Map<String, Billing> billingByReferenceNo = CollectionUtils.toMap(getBillings(), Billing::getReferenceNo);

		try {
			Scanner scanner = new Scanner(event.getFile().getInputstream(), "ISO-8859-1");

			while (scanner.hasNextLine()) {
				String isoLineSerial = scanner.nextLine();
				String utfLineSerial = new String(isoLineSerial.getBytes(), "UTF-8");
				BankTransfer bankTransfer = BankTransfer.getInstance(utfLineSerial);
				if (bankTransfer.getReferenceNo() == null) {
					continue;
				}
				if (bankTransfer.getBookingLd().isBefore(oldestBillingLd)) {
					continue;
				}
				if (bankTransfer.getAmount() <= 0) {
					continue;
				}
				if (billingFacade.existsBilling(BillingType.INVOICE, bankTransfer.getBookingDate(),
						bankTransfer.getBookingNo())) {
					continue;
				}
				Billing billing = billingByReferenceNo.get(bankTransfer.getReferenceNo());
				if (billing == null) {
					getUnmatchedBankTransfers().add(bankTransfer);
					continue;
				}
				if (billing.getPaymentLdt() != null) {
					getUnmatchedBankTransfers().add(bankTransfer);
					continue;
				}
				if (bankTransfer.getBookingLd().isBefore(billing.getBillingLdt().toLocalDate())) {
					getUnmatchedBankTransfers().add(bankTransfer);
					continue;
				}
				if (!bankTransfer.getAmount().equals(billing.getTotal())) {
					getUnmatchedBankTransfers().add(bankTransfer);
					continue;
				}
				billing.setPaymentDate(DateUtils.asDate(bankTransfer.getBookingLd()));
				billing.setPaymentNo(bankTransfer.getBookingNo());
				billing.setPaymentNote(bankTransfer.getBookingText());
				billing.setBankTransfer(bankTransfer);
				billing.setChanged(true);
			}

			scanner.close();

			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_INFO,
					GlobalNames.MESSAGE_KEY_HANDLE_FILE_UPLOAD_SUCCESS, sourceFilename);
			logger.debug(msgText);
		} catch (Exception e) {
			String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_INFO,
					GlobalNames.MESSAGE_KEY_HANDLE_FILE_UPLOAD_ERROR, sourceFilename);
			logger.error(msgText, e);
		}
	}

}
