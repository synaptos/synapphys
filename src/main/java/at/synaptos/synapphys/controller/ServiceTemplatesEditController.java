package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.facade.ServiceTypeFacade;
import at.synaptos.synapphys.model.ServiceTemplate;
import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.producer.SelectedContextKeeper;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class ServiceTemplatesEditController extends AbstractEditController implements Serializable {

	private static final long serialVersionUID = 8871890524183021724L;

	@Inject
	@Added
	private Event<ServiceTemplate> serviceTemplateAddEvent;
	@Inject
	@Deleted
	private Event<ServiceTemplate> serviceTemplateDeleteEvent;

	@Inject
	private ServiceTypeFacade serviceTypeFacade;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	private List<ServiceType> serviceTypes;

	private ServiceTemplate preparedServiceTemplate;

	@PostConstruct
	public void init() {
		setServiceTypes(serviceTypeFacade.getServiceTypes());
		setPreparedServiceTemplate(ServiceTemplate.getInstance());
	}

	public List<ServiceType> getServiceTypes() {
		return serviceTypes;
	}

	private void setServiceTypes(List<ServiceType> serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	public ServiceTemplate getPreparedServiceTemplate() {
		return preparedServiceTemplate;
	}

	public void setPreparedServiceTemplate(ServiceTemplate preparedServiceTemplate) {
		this.preparedServiceTemplate = preparedServiceTemplate;
	}

	@Override
	public String doSave() {
		getPreparedServiceTemplate()
				.setSeriesDocumentation(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation());
		serviceTemplateAddEvent.fire(getPreparedServiceTemplate());
		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getServiceTemplates()
				.add(getPreparedServiceTemplate());
		setPreparedServiceTemplate(ServiceTemplate.getInstance());
		return null;
	}

	@Override
	public String doCancel() {
		return PageUrlHelper
				.redirect(selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getBackUrl());
	}

	public void doDelete(ServiceTemplate serviceTemplate) {
		serviceTemplateDeleteEvent.fire(serviceTemplate);
		selectedContextKeeper.getSelectedClient().getSelectedSeriesDocumentation().getServiceTemplates()
				.remove(serviceTemplate);
	}

}
