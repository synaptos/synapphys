package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.xhtmlrenderer.pdf.ITextRenderer;

@ViewScoped
@Named
public class BillingPrintController implements Serializable {

	private static final long serialVersionUID = -2830159131823264072L;

	public void doCreatePdf() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		String url = "http://localhost:8081/smarttherapy/pages/billing/billingPdf.jsf;jsessionid=" + session.getId();
		try {
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(url);
			renderer.layout();
			response.reset();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=\"test.pdf\"");
			renderer.createPDF(response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		facesContext.responseComplete();
	}

}
