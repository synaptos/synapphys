package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.facade.PhysicianFacade;
import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.producer.PhysicianProducer;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class PhysicianListController implements Serializable {

	private static final long serialVersionUID = -5202844333142838664L;

	@Inject
	@Deleted
	private Event<Physician> physicianDeleteEvent;

	@Inject
	private PhysicianFacade physicianFacade;

	@Inject
	private PhysicianProducer physicianProducer;

	private List<Physician> physicians;

	@PostConstruct
	private void init() {
		setPhysicians(physicianFacade.getPhysicians());
	}

	public List<Physician> getPhysicians() {
		return physicians;
	}

	private void setPhysicians(List<Physician> physicians) {
		this.physicians = physicians;
	}

	public String doAdd() {
		return doEdit(Physician.getInstance());
	}

	public String doEdit(Physician physician) {
		physicianProducer.setPreparedPhysician(physician.withBackUrl(PageUrl.PHYSICIAN_LIST));
		return PageUrlHelper.redirect(PageUrl.PHYSICIAN_EDIT);
	}

	public void doDelete(Physician physician) {
		physicianDeleteEvent.fire(physician);
		getPhysicians().remove(physician);
	}

}
