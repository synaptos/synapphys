package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.producer.HealthInsuranceProducer;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.PageUrlHelper;
import at.synaptos.synapphys.utils.TagAttribute;

@ViewScoped
@Named
public class HealthInsuranceEditController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 6877558206606938756L;

	@Inject
	protected Logger logger;

	@Inject
	@Added
	private Event<HealthInsurance> healthInsuranceAddEvent;
	@Inject
	@Updated
	private Event<HealthInsurance> healthInsuranceUpdateEvent;

	@Inject
	private HealthInsuranceProducer healthInsuranceProducer;

	public void doSave() {
		if (healthInsuranceProducer.getPreparedHealthInsurance().isNew()) {
			healthInsuranceAddEvent.fire(healthInsuranceProducer.getPreparedHealthInsurance());
		} else {
			healthInsuranceUpdateEvent.fire(healthInsuranceProducer.getPreparedHealthInsurance());
		}
		String msgText = addMessageI18n(TagAttribute.MESSAGE_FOR_EDIT_PANEL, FacesMessage.SEVERITY_INFO,
				GlobalNames.MESSAGE_KEY_ACTION_SAVE_SUCCESS);
		logger.debug(msgText);
	}

	public String doBack() {
		return PageUrlHelper.redirect(healthInsuranceProducer.getPreparedHealthInsurance().getBackUrl());
	}

}
