package at.synaptos.synapphys.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.facade.ServiceTypeFacade;
import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.producer.ServiceTypeProducer;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.PageUrlHelper;

@ViewScoped
@Named
public class ServiceTypeListController implements Serializable {

	private static final long serialVersionUID = 2538224711018021557L;

	@Inject
	@Deleted
	private Event<ServiceType> serviceTypeDeleteEvent;

	@Inject
	private ServiceTypeFacade serviceTypeFacade;

	@Inject
	private ServiceTypeProducer serviceTypeProducer;

	private List<ServiceType> serviceTypes;

	@PostConstruct
	private void init() {
		setServiceTypes(serviceTypeFacade.getServiceTypes());
	}

	public List<ServiceType> getServiceTypes() {
		return serviceTypes;
	}

	private void setServiceTypes(List<ServiceType> serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	public String doAdd() {
		return doEdit(ServiceType.getInstance());
	}

	public String doEdit(ServiceType serviceType) {
		serviceTypeProducer.setPreparedServiceType(serviceType.withBackUrl(PageUrl.SERVICE_TYPE_LIST));
		return PageUrlHelper.redirect(PageUrl.SERVICE_TYPE_EDIT);
	}

	public void doDelete(ServiceType serviceType) {
		serviceTypeDeleteEvent.fire(serviceType);
		getServiceTypes().remove(serviceType);
	}

}
