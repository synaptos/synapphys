package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named
public class SettingsLayoutController extends AbstractViewController implements Serializable {

	private static final long serialVersionUID = 1053300078834844220L;

	public void doChangeThemeColor(String themeColor) {
		configContext.setThemeColor(themeColor);
		configContext.updateProperties();
	}

	public void doChangeMenuLayout(String menuLayout) {
		configContext.setMenuLayout(menuLayout);
		configContext.updateProperties();
	}

}
