package at.synaptos.synapphys.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import at.synaptos.synapphys.model.BillingType;

@ViewScoped
@Named
public class BillingInvoiceListController extends AbstractBillingListController implements Serializable {

	private static final long serialVersionUID = 7492524594542838079L;

	@Override
	public BillingType getBillingType() {
		return BillingType.INVOICE;
	}

}
