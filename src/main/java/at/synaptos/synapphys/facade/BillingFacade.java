package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.domain.ConfigContext;
import at.synaptos.synapphys.fiskaltrust.ReceiptSigner;
import at.synaptos.synapphys.model.Billing;
import at.synaptos.synapphys.model.BillingItem;
import at.synaptos.synapphys.model.BillingSignature;
import at.synaptos.synapphys.model.BillingType;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.service.BillingService;
import at.synaptos.synapphys.service.BillingSignatureService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;
import at.synaptos.synapphys.utils.StringUtils;

@SessionScoped
public class BillingFacade implements Serializable {

	private static final long serialVersionUID = 5032796030869521984L;

	@Inject
	private BillingService billingService;

	@Inject
	private BillingSignatureService billingSignatureService;

	@Inject
	private ConfigContext configContext;

	public List<Billing> getBillings(BillingType billingType) {
		return billingService.getEntities(billingType);
	}

	public List<Billing> getBillings(BillingType billingType, Client client) {
		return billingService.getEntities(billingType, client);
	}

	public List<Billing> getBillings(BillingType billingType, Date startDate, Date endDate) {
		return billingService.getEntities(billingType, startDate, endDate);
	}

	public List<Billing> getUnpaidBillings(BillingType billingType) {
		return billingService.getUnpaidEntities(billingType);
	}

	public boolean existsBilling(BillingType billingType, Date paymentDate, String paymentNo) {
		return billingService.existsEntity(billingType, paymentDate, paymentNo);
	}

	public int getReferenceSequence(BillingType billingType, int referenceYear) {
		return billingService.getReferenceSequence(billingType, referenceYear);
	}

	protected void onAdded(@Observes @Added Billing billing) {
		int i = 0;
		for (BillingItem billingItem : billing.getBillingItems()) {
			billingItem.setId(null);
			billingItem.setSequenceNo(++i);
		}
		if (billing.isReceipt()) {
			billing.setFtCashboxId(configContext.getInstancePropertyFtCashboxId());
			billing.setFtAccessToken(configContext.getInstancePropertyFtAccessToken());
			billing.setCbTerminalId(configContext.getInstancePropertyCbTerminalId());
		}
		billingService.addEntity(billing);
		Billing managedBilling = billingService.getEntity(billing.getId());
		billing.setReferenceYear(managedBilling.getReferenceYear());
		billing.setReferenceSequence(managedBilling.getReferenceSequence());
		if (billing.isReceipt() && !StringUtils.isEmpty(billing.getFtCashboxId())) {
			BillingSignature signature = ReceiptSigner.sign(billing);
			if (signature != null) {
				signature.setBilling(billing);
				billingSignatureService.addEntity(signature);
				billing.setBillingSignature(signature);
			}
		}
	}

	protected void onUpdate(@Observes @Updated Billing billing) {
		billingService.updateEntity(billing);
	}

	protected void onUpdate(@Observes @Updated List<Billing> billings) {
		billingService.updateEntities(billings);
	}

	protected void onDeleted(@Observes @Deleted Billing billing) {
		billingService.deleteEntity(billing);
	}

}
