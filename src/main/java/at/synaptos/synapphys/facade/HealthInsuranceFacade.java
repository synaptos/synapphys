package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.service.HealthInsuranceService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class HealthInsuranceFacade implements Serializable {

	private static final long serialVersionUID = -8299532395266158629L;

	@Inject
	private HealthInsuranceService healthInsuranceService;

	public HealthInsurance getHealthInsurance(long healthInsuranceId) {
		return healthInsuranceService.getEntity(healthInsuranceId);
	}

	public List<HealthInsurance> getHealthInsurances() {
		return healthInsuranceService.getEntities();
	}

	public void onAdded(@Observes @Added HealthInsurance healthInsurance) {
		healthInsuranceService.addEntity(healthInsurance);
	}

	public void onUpdated(@Observes @Updated HealthInsurance healthInsurance) {
		healthInsuranceService.updateEntity(healthInsurance);
	}

	public void onDeleted(@Observes @Deleted HealthInsurance healthInsurance) {
		healthInsuranceService.deleteEntity(healthInsurance);
	}

}
