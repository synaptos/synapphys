package at.synaptos.synapphys.facade;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.model.FindingsSheet;
import at.synaptos.synapphys.service.FindingsSheetService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class FindingsSheetFacade implements Serializable {

	private static final long serialVersionUID = -9122520028620694937L;

	@Inject
	private FindingsSheetService findingsSheetService;

	public FindingsSheet getFindingsSheet(long findingsSheetId) {
		return findingsSheetService.getEntity(findingsSheetId);
	}

	public void onAdded(@Observes @Added FindingsSheet findingsSheet) {
		findingsSheetService.addEntity(findingsSheet);
		findingsSheet.getSeriesDocumentation().setFindingsSheet(findingsSheet);
	}

	public void onUpdated(@Observes @Updated FindingsSheet findingsSheet) {
		findingsSheetService.updateEntity(findingsSheet);
	}

}
