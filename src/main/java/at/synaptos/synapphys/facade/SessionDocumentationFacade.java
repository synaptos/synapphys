package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.domain.ScheduleEntry;
import at.synaptos.synapphys.model.ServiceAttachment;
import at.synaptos.synapphys.model.ServiceDocumentation;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.service.ServiceAttachmentService;
import at.synaptos.synapphys.service.ServiceDocumentationService;
import at.synaptos.synapphys.service.SessionDocumentationService;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class SessionDocumentationFacade implements Serializable {

	private static final long serialVersionUID = -3733342270049796123L;

	@Inject
	private SessionDocumentationService sessionDocumentationService;

	public List<SessionDocumentation> getSessionDocumentations(final Date startDate, final Date endDate) {
		return sessionDocumentationService.getEntitiesBetween(startDate, endDate);
	}

	public List<ScheduleEntry> createScheduleEntries(final Date startDate, final Date endDate) {
		return CollectionUtils.toList(getSessionDocumentations(startDate, endDate),
				sessionDocumentation -> ScheduleEntry.getInstance(sessionDocumentation));
	}

	public void onAdded(@Observes @Added SessionDocumentation sessionDocumentation) {
		sessionDocumentationService.addEntity(sessionDocumentation);
		sessionDocumentation.getSeriesDocumentation().getSessionDocumentations().add(sessionDocumentation);
		sessionDocumentation.getSeriesDocumentation().getSessionDocumentations().sort(
				(SessionDocumentation d1, SessionDocumentation d2) -> d2.getStartLdt().compareTo(d1.getStartLdt()));
	}

	public void onUpdated(@Observes @Updated SessionDocumentation sessionDocumentation) {
		sessionDocumentationService.updateEntity(sessionDocumentation);
		sessionDocumentation.getSeriesDocumentation().getSessionDocumentations().sort(
				(SessionDocumentation d1, SessionDocumentation d2) -> d2.getStartLdt().compareTo(d1.getStartLdt()));
	}

	public void onDeleted(@Observes @Deleted SessionDocumentation sessionDocumentation) {
		sessionDocumentationService.deleteEntity(sessionDocumentation);
		sessionDocumentation.getSeriesDocumentation().getSessionDocumentations().remove(sessionDocumentation);
	}

	// Service Documentations

	@Inject
	private ServiceDocumentationService serviceDocumentationService;

	public void onAdded(@Observes @Added ServiceDocumentation serviceDocumentation) {
		serviceDocumentationService.addEntity(serviceDocumentation);
		serviceDocumentation.getSessionDocumentation().getServiceDocumentations().add(serviceDocumentation);
	}

	public void onUpdated(@Observes @Updated ServiceDocumentation serviceDocumentation) {
		serviceDocumentationService.updateEntity(serviceDocumentation);
	}

	public void onDeleted(@Observes @Deleted ServiceDocumentation serviceDocumentation) {
		serviceDocumentationService.deleteEntity(serviceDocumentation);
		serviceDocumentation.getSessionDocumentation().getServiceDocumentations().remove(serviceDocumentation);
	}

	// Service Attachments

	@Inject
	private ServiceAttachmentService serviceAttachmentService;

	public void onAdded(@Observes @Added ServiceAttachment serviceAttachment) {
		serviceAttachmentService.addEntity(serviceAttachment);
		serviceAttachment.getSeriesDocumentation().getServiceAttachments().add(serviceAttachment);
	}

	public void onUpdated(@Observes @Updated ServiceAttachment serviceAttachment) {
		serviceAttachmentService.updateEntity(serviceAttachment);
	}

	public void onDeleted(@Observes @Deleted ServiceAttachment serviceAttachment) {
		serviceAttachmentService.deleteEntity(serviceAttachment);
		serviceAttachment.getSeriesDocumentation().getServiceAttachments().remove(serviceAttachment);
	}

}
