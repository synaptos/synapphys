package at.synaptos.synapphys.facade;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.model.FindingsLsp;
import at.synaptos.synapphys.service.FindingsLspService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class FindingsLspFacade implements Serializable {

	private static final long serialVersionUID = -9122520028620694937L;

	@Inject
	private FindingsLspService findingsLspService;

	public FindingsLsp getFindingsLsp(long findingsLspId) {
		return findingsLspService.getEntity(findingsLspId);
	}

	public void onAdded(@Observes @Added FindingsLsp findingsLsp) {
		findingsLspService.addEntity(findingsLsp);
		findingsLsp.getSeriesDocumentation().setFindingsLsp(findingsLsp);
	}

	public void onUpdated(@Observes @Updated FindingsLsp findingsLsp) {
		findingsLspService.updateEntity(findingsLsp);
		findingsLsp.getSeriesDocumentation().setFindingsLsp(findingsLsp);
	}

}
