package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.domain.OfficeBreaks;
import at.synaptos.synapphys.domain.OfficeHours;
import at.synaptos.synapphys.domain.ScheduleEntry;
import at.synaptos.synapphys.domain.ScheduleWeightings;
import at.synaptos.synapphys.model.ScheduleItem;
import at.synaptos.synapphys.model.SessionDocumentation;
import at.synaptos.synapphys.service.ScheduleItemService;
import at.synaptos.synapphys.utils.CollectionUtils;
import at.synaptos.synapphys.utils.DateUtils;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class ScheduleItemFacade implements Serializable {

	private static final long serialVersionUID = -2162429858529696175L;

	@Inject
	private ScheduleItemService scheduleItemService;

	@Inject
	private SessionDocumentationFacade sessionDocumentationFacade;

	public List<ScheduleItem> getScheduleItems(Date startDate, Date endDate) {
		return scheduleItemService.getEntities(startDate, endDate);
	}

	public void onAdded(@Observes @Added ScheduleItem scheduleItem) {
		scheduleItemService.addEntity(scheduleItem);
	}

	public void onUpdated(@Observes @Updated ScheduleItem scheduleItem) {
		scheduleItemService.updateEntity(scheduleItem);
	}

	public void onDeleted(@Observes @Deleted ScheduleItem scheduleItem) {
		scheduleItemService.deleteEntity(scheduleItem);
	}

	public List<ScheduleEntry> invertScheduleEntries(final Date startDate, final Date endDate,
			final List<ScheduleEntry> scheduleEntries) {
		final List<ScheduleEntry> invertedScheduleEntries = new ArrayList<ScheduleEntry>();

		scheduleEntries.sort((ScheduleEntry o1, ScheduleEntry o2) -> o1.getStartLdt().compareTo(o2.getStartLdt()));

		LocalDateTime currLdt = DateUtils.asLdt(startDate);
		for (ScheduleEntry currScheduleEntry : scheduleEntries) {
			if (currScheduleEntry.getStartLdt().compareTo(currLdt) > 0) {
				invertedScheduleEntries.add(ScheduleEntry.getInstance(currLdt, currScheduleEntry.getStartLdt()));
			}
			currLdt = currScheduleEntry.getEndLdt();
		}

		LocalDateTime endLdt = DateUtils.asLdt(endDate);
		if (endLdt.compareTo(currLdt) > 0) {
			invertedScheduleEntries.add(ScheduleEntry.getInstance(currLdt, endLdt));
		}

		return invertedScheduleEntries;
	}

	public List<ScheduleEntry> createScheduleEntries(final Date startDate, final Date endDate) {
		return CollectionUtils.toList(getScheduleItems(startDate, endDate),
				scheduleItem -> ScheduleEntry.getInstance(scheduleItem));
	}

	public List<ScheduleEntry> createScheduleEntries(final Date startDate, final OfficeHours officeHours) {
		final List<ScheduleEntry> currScheduleEntries = new ArrayList<ScheduleEntry>();

		LocalDateTime startLdt = DateUtils.asLdt(startDate);
		int startDayOfWeekNo = startLdt.getDayOfWeek().getValue() - 1;

		for (int dayOfWeekNo = startDayOfWeekNo; dayOfWeekNo < DayOfWeek.SUNDAY.getValue(); dayOfWeekNo++) {
			LocalDate currDate = startLdt.plusDays(dayOfWeekNo - startDayOfWeekNo).toLocalDate();

			if (officeHours.hoursPerDay(dayOfWeekNo).getMornStartTime() != null
					&& officeHours.hoursPerDay(dayOfWeekNo).getMornEndTime() != null) {
				LocalDateTime currEndLdt = LocalDateTime.of(currDate,
						officeHours.hoursPerDay(dayOfWeekNo).getMornEndTime());
				if (currEndLdt.compareTo(startLdt) > 0) {
					LocalDateTime currStartLdt = LocalDateTime.of(currDate,
							officeHours.hoursPerDay(dayOfWeekNo).getMornStartTime());
					if (currStartLdt.compareTo(startLdt) < 0) {
						currStartLdt = startLdt;
					}
					currScheduleEntries.add(ScheduleEntry.getInstance(currStartLdt, currEndLdt,
							officeHours.hoursPerDay(dayOfWeekNo).getMornWeighting()));
				}
			}

			if (officeHours.hoursPerDay(dayOfWeekNo).getAftnStartTime() != null
					&& officeHours.hoursPerDay(dayOfWeekNo).getAftnEndTime() != null) {
				LocalDateTime currEndLdt = LocalDateTime.of(currDate,
						officeHours.hoursPerDay(dayOfWeekNo).getAftnEndTime());
				if (currEndLdt.compareTo(startLdt) > 0) {
					LocalDateTime currStartLdt = LocalDateTime.of(currDate,
							officeHours.hoursPerDay(dayOfWeekNo).getAftnStartTime());
					if (currStartLdt.compareTo(startLdt) < 0) {
						currStartLdt = startLdt;
					}
					currScheduleEntries.add(ScheduleEntry.getInstance(currStartLdt, currEndLdt,
							officeHours.hoursPerDay(dayOfWeekNo).getAftnWeighting()));
				}
			}
		}

		return currScheduleEntries;
	}

	public List<ScheduleEntry> createNextScheduleEntries(final Date startDate, final Date endDate, int mins,
			final OfficeHours officeHours, final OfficeBreaks officeBreaks, final ScheduleWeightings scheduleWeightings,
			final List<SessionDocumentation> currSessionDocumentations) {
		final List<ScheduleEntry> officeHoursEntries = createScheduleEntries(startDate, officeHours);

		final List<ScheduleEntry> fixedScheduleEntries = createFixedScheduleEntries(startDate, endDate);

		final List<ScheduleEntry> freeScheduleEntries = createFreeScheduleEntries(startDate, endDate, mins,
				officeHoursEntries, officeBreaks, fixedScheduleEntries);

		applyScheduleWeightings(scheduleWeightings, currSessionDocumentations, officeHoursEntries, fixedScheduleEntries,
				freeScheduleEntries);

		return groupScheduleEntries(freeScheduleEntries);
	}

	private List<ScheduleEntry> createFixedScheduleEntries(final Date startDate, final Date endDate) {
		final List<ScheduleEntry> scheduleEntries = new ArrayList<ScheduleEntry>();

		scheduleEntries.addAll(createScheduleEntries(startDate, endDate));

		final List<SessionDocumentation> currSessionDocumentations = sessionDocumentationFacade
				.getSessionDocumentations(startDate, endDate);
		scheduleEntries.addAll(CollectionUtils.toList(currSessionDocumentations, sessionDocumentation -> ScheduleEntry
				.getInstance(sessionDocumentation.getStartDate(), sessionDocumentation.getEndDate())));

		return scheduleEntries;
	}

	private List<ScheduleEntry> createFreeScheduleEntries(final Date startDate, final Date endDate, int mins,
			final List<ScheduleEntry> officeHoursEntries, final OfficeBreaks officeBreaks,
			final List<ScheduleEntry> fixedScheduleEntries) {
		final List<ScheduleEntry> freeScheduleEntries = new ArrayList<ScheduleEntry>();

		final List<ScheduleEntry> reservedScheduleEntries = new ArrayList<ScheduleEntry>();
		reservedScheduleEntries.addAll(invertScheduleEntries(startDate, endDate, officeHoursEntries));
		reservedScheduleEntries.addAll(fixedScheduleEntries);

		final List<ScheduleEntry> freeScheduleBlocks = invertScheduleEntries(startDate, startDate,
				reservedScheduleEntries);

		if (officeBreaks.getAfterPeriodOfMins() > 0) {
			int blockMins = 0;
			for (ScheduleEntry currScheduleBlock : freeScheduleBlocks) {
				LocalDateTime currStartLdt = currScheduleBlock.getStartLdt();
				LocalDateTime currEndLdt = currStartLdt.plusMinutes(mins);

				while (currEndLdt.compareTo(currScheduleBlock.getEndLdt()) <= 0) {
					freeScheduleEntries.add(ScheduleEntry.getInstance(currStartLdt, currStartLdt.plusMinutes(mins)));
					blockMins += mins;
					if (blockMins >= officeBreaks.getAfterPeriodOfMins()) {
						currEndLdt = currEndLdt.plusMinutes(officeBreaks.getBreakMins());
						blockMins = 0;
					}
					currStartLdt = currEndLdt;
					currEndLdt = currEndLdt.plusMinutes(mins);
				}

				if (currStartLdt.plusMinutes(mins).compareTo(currScheduleBlock.getEndLdt()) <= 0) {
					freeScheduleEntries.add(ScheduleEntry.getInstance(currStartLdt, currStartLdt.plusMinutes(mins)));
				}
			}
		} else {
			for (ScheduleEntry currScheduleBlock : freeScheduleBlocks) {
				LocalDateTime currStartLdt = currScheduleBlock.getStartLdt();
				LocalDateTime currEndLdt = currStartLdt.plusMinutes(mins + officeBreaks.getBreakMins());

				while (currEndLdt.compareTo(currScheduleBlock.getEndLdt()) <= 0) {
					freeScheduleEntries.add(ScheduleEntry.getInstance(currStartLdt, currStartLdt.plusMinutes(mins)));
					currStartLdt = currEndLdt;
					currEndLdt = currEndLdt.plusMinutes(mins + officeBreaks.getBreakMins());
				}

				if (currStartLdt.plusMinutes(mins).compareTo(currScheduleBlock.getEndLdt()) <= 0) {
					freeScheduleEntries.add(ScheduleEntry.getInstance(currStartLdt, currStartLdt.plusMinutes(mins)));
				}
			}
		}

		return freeScheduleEntries;
	}

	private void applyScheduleWeightings(final ScheduleWeightings scheduleWeightings,
			final List<SessionDocumentation> sessionDocumentations, final List<ScheduleEntry> officeHoursEntries,
			final List<ScheduleEntry> fixedScheduleEntries, final List<ScheduleEntry> freeScheduleEntries) {
		if (CollectionUtils.isEmpty(freeScheduleEntries)) {
			return;
		}

		final Integer[][] sameTimeWeightings = createSameTimeWeightings(scheduleWeightings, sessionDocumentations);

		final Integer[][] followPreviousWeightings = createFollowPreviousWeightings(scheduleWeightings,
				officeHoursEntries, fixedScheduleEntries);

		for (ScheduleEntry freeScheduleEntry : freeScheduleEntries) {
			Integer weighting = null;
			if (sameTimeWeightings != null) {
				LocalDateTime currStartLdt = DateUtils.asLdt(freeScheduleEntry.getStartDate());
				weighting = sameTimeWeightings[0][currStartLdt.getHour() * 60 + currStartLdt.getMinute()];
				if (weighting != null) {
					freeScheduleEntry.plusWeighting(weighting);
				}
				weighting = sameTimeWeightings[1][currStartLdt.getDayOfWeek().getValue() - 1];
				if (weighting != null) {
					freeScheduleEntry.plusWeighting(weighting);
				}
			}
			if (followPreviousWeightings != null) {
				LocalDateTime currStartLdt = DateUtils.asLdt(freeScheduleEntry.getStartDate());
				weighting = followPreviousWeightings[currStartLdt.getDayOfWeek().getValue() - 1][currStartLdt.getHour()
						* 60 + currStartLdt.getMinute()];
				if (weighting != null) {
					freeScheduleEntry.plusWeighting(weighting);
				}
			}
			for (ScheduleEntry officeHoursEntry : officeHoursEntries) {
				if (freeScheduleEntry.getStartDate().getTime() < officeHoursEntry.getEndDate().getTime()
						&& freeScheduleEntry.getEndDate().getTime() > officeHoursEntry.getStartDate().getTime()) {
					freeScheduleEntry.plusWeighting(officeHoursEntry.getWeighting());
				}
			}
		}

		freeScheduleEntries.sort((ScheduleEntry o1, ScheduleEntry o2) -> {
			if (o2.getWeighting() != o1.getWeighting()) {
				return o2.getWeighting() - o1.getWeighting();
			}
			return o1.getStartLdt().compareTo(o2.getStartLdt());
		});
	}

	private Integer[][] createSameTimeWeightings(final ScheduleWeightings scheduleWeightings,
			final List<SessionDocumentation> sessionDocumentations) {
		if (CollectionUtils.isEmpty(sessionDocumentations)) {
			return null;
		}
		Integer[] sameTimeOfDayWeightings = new Integer[24 * 60];
		Arrays.fill(sameTimeOfDayWeightings, null);
		Integer[] sameDayOfWeekWeightings = new Integer[7];
		Arrays.fill(sameDayOfWeekWeightings, null);
		for (SessionDocumentation currSessionDocumentation : sessionDocumentations) {
			LocalDateTime currStartLdt = DateUtils.asLdt(currSessionDocumentation.getStartDate());
			sameTimeOfDayWeightings[currStartLdt.getHour() * 60 + currStartLdt.getMinute()] = scheduleWeightings
					.getSameTimeOfDay();
			sameDayOfWeekWeightings[currStartLdt.getDayOfWeek().getValue() - 1] = scheduleWeightings.getSameDayOfWeek();
		}
		return new Integer[][] { sameTimeOfDayWeightings, sameDayOfWeekWeightings };
	}

	private Integer[][] createFollowPreviousWeightings(final ScheduleWeightings scheduleWeightings,
			final List<ScheduleEntry> officeHoursScheduleEntries, final List<ScheduleEntry> fixedScheduleEntries) {
		if (CollectionUtils.isEmpty(officeHoursScheduleEntries) && CollectionUtils.isEmpty(fixedScheduleEntries)) {
			return null;
		}
		Integer[][] followPreviousWeightings = { new Integer[24 * 60], new Integer[24 * 60], new Integer[24 * 60],
				new Integer[24 * 60], new Integer[24 * 60], new Integer[24 * 60], new Integer[24 * 60] };
		for (int dayOfWeekNo = DayOfWeek.MONDAY.getValue() - 1; dayOfWeekNo < DayOfWeek.SUNDAY
				.getValue(); dayOfWeekNo++) {
			Arrays.fill(followPreviousWeightings[dayOfWeekNo], null);
		}
		if (!CollectionUtils.isEmpty(officeHoursScheduleEntries)) {
			LocalDateTime currEndLdt = null;
			for (ScheduleEntry currScheduleEntry : officeHoursScheduleEntries) {
				LocalDateTime currStartLdt = currScheduleEntry.getStartLdt();
				if (currEndLdt == null || currEndLdt.compareTo(currStartLdt) < 0) {
					followPreviousWeightings[currStartLdt.getDayOfWeek().getValue() - 1][currStartLdt.getHour() * 60
							+ currStartLdt.getMinute()] = scheduleWeightings.getFollowPrevious();
					currEndLdt = currScheduleEntry.getEndLdt();
				}
			}
		}
		if (!CollectionUtils.isEmpty(fixedScheduleEntries)) {
			for (ScheduleEntry currScheduleEntry : fixedScheduleEntries) {
				LocalDateTime currEndLdt = currScheduleEntry.getEndLdt();
				followPreviousWeightings[currEndLdt.getDayOfWeek().getValue() - 1][currEndLdt.getHour() * 60
						+ currEndLdt.getMinute()] = scheduleWeightings.getFollowPrevious();
			}
		}
		return followPreviousWeightings;
	}

	private List<ScheduleEntry> groupScheduleEntries(final List<ScheduleEntry> freeScheduleEntries) {
		final List<ScheduleEntry> entryGroups = new ArrayList<ScheduleEntry>();
		final Map<Date, ScheduleEntry> date2EntryGroup = new HashMap<Date, ScheduleEntry>();
		freeScheduleEntries.forEach(scheduleEntry -> {
			ScheduleEntry entryGroup = date2EntryGroup.get(scheduleEntry.getStartDayOfMonth());
			if (entryGroup == null) {
				entryGroup = scheduleEntry;
				entryGroup.setPreviewEntries(new ArrayList<ScheduleEntry>());
				entryGroups.add(entryGroup);
				date2EntryGroup.put(entryGroup.getStartDayOfMonth(), entryGroup);
			}
			entryGroup.getPreviewEntries().add(scheduleEntry);
		});
		return entryGroups;
	}

}
