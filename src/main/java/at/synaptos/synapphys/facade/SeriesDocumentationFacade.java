package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.model.BodymapItem;
import at.synaptos.synapphys.model.SeriesDocumentation;
import at.synaptos.synapphys.model.ServiceTemplate;
import at.synaptos.synapphys.service.BodymapItemService;
import at.synaptos.synapphys.service.SeriesDocumentationService;
import at.synaptos.synapphys.service.ServiceTemplateService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class SeriesDocumentationFacade implements Serializable {

	private static final long serialVersionUID = 5215676420567606742L;

	@Inject
	private SeriesDocumentationService seriesDocumentationService;

	public SeriesDocumentation getSeriesDocumentation(long seriesDocumentationId) {
		return seriesDocumentationService.getEntity(seriesDocumentationId);
	}

	public List<SeriesDocumentation> getSeriesDocumentations() {
		return seriesDocumentationService.getEntities();
	}

	// Series Documentations

	public void onAdded(@Observes @Added SeriesDocumentation seriesDocumentation) {
		seriesDocumentationService.addEntity(seriesDocumentation);
		seriesDocumentation.getClient().getSeriesDocumentations().add(seriesDocumentation);
		seriesDocumentation.getClient().getSeriesDocumentations().sort(
				(SeriesDocumentation d1, SeriesDocumentation d2) -> d2.getAddedDate().compareTo(d1.getAddedDate()));
	}

	public void onUpdated(@Observes @Updated SeriesDocumentation seriesDocumentation) {
		seriesDocumentationService.updateEntity(seriesDocumentation);
		seriesDocumentation.getClient().getSeriesDocumentations().sort(
				(SeriesDocumentation d1, SeriesDocumentation d2) -> d2.getAddedDate().compareTo(d1.getAddedDate()));
	}

	public void onDeleted(@Observes @Deleted SeriesDocumentation seriesDocumentation) {
		seriesDocumentationService.deleteEntity(seriesDocumentation);
		seriesDocumentation.getClient().getSeriesDocumentations().remove(seriesDocumentation);
	}

	// Service Templates

	@Inject
	private ServiceTemplateService serviceTemplateService;

	public void onAdded(@Observes @Added ServiceTemplate serviceTemplate) {
		serviceTemplateService.addEntity(serviceTemplate);
	}

	public void onUpdated(@Observes @Updated ServiceTemplate serviceTemplate) {
		serviceTemplateService.updateEntity(serviceTemplate);
	}

	public void onDeleted(@Observes @Deleted ServiceTemplate serviceTemplate) {
		serviceTemplateService.deleteEntity(serviceTemplate);
	}

	// Bodymap Items

	@Inject
	private BodymapItemService bodymapItemService;

	public void onAdded(@Observes @Added BodymapItem bodymapItem) {
		bodymapItemService.addEntity(bodymapItem);
	}

	public void onUpdated(@Observes @Updated BodymapItem bodymapItem) {
		bodymapItemService.updateEntity(bodymapItem);
	}

	public void onDeleted(@Observes @Deleted BodymapItem bodymapItem) {
		bodymapItemService.deleteEntity(bodymapItem);
	}

}
