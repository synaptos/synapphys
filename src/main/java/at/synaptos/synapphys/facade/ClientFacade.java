package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.model.HealthInsurance;
import at.synaptos.synapphys.model.PatientStatus;
import at.synaptos.synapphys.service.ClientService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class ClientFacade implements Serializable {

	private static final long serialVersionUID = 7785120404817213067L;

	@Inject
	private ClientService clientService;

	/**
	 * Liefert alle Klienten.
	 * 
	 * @return
	 */
	public List<Client> getClients() {
		return clientService.getEntities();
	}

	/**
	 * Liefert den Klienten über seinen Schlüssel.
	 * 
	 * @param clientId
	 * @return
	 */
	public Client getClient(long clientId) {
		return clientService.getEntity(clientId);
	}

	/**
	 * Liefert alle Klienten, wo der Suchschlüssel dem 1. Buchstabe des
	 * Nachnames entspricht.
	 * 
	 * @param searchKey
	 * @return
	 */
	public List<Client> getClients(String searchKey) {
		return clientService.getEntities(searchKey);
	}

	/**
	 * Liefert die aktuellen Klienten.
	 * 
	 * @return
	 */
	public List<Client> getCurrentClients() {
		return clientService.getEntities(PatientStatus.CURRENT);
	}

	/**
	 * Liefert die Klienten der Warteliste.
	 * 
	 * @return
	 */
	public List<Client> getBookmarkedClients() {
		return clientService.getEntities(PatientStatus.BOOKMARKED);
	}

	/**
	 * Liefert alle hauptversicherten Klienten.
	 * 
	 * @return
	 */
	public List<Client> getMainInsuredClients() {
		return clientService.getMainInsuredClients();
	}

	/**
	 * Prüft, ob ein anderer Klient mit Versicherungsnummer bereits erfasst
	 * wurde.
	 * 
	 * @param insuranceNo
	 *            Versicherungsnummer: In Ö wird generell die 10-stellige
	 *            Sozialversicherungsnummer (SVNR) verwendet, bestehend aus
	 *            einer 3-stelligen fortlaufenden Nummer >=100, gefolgt von
	 *            einer Prüfziffer und dem Geburtsdatum im Format ttmmyy.
	 *            Ausnahmen beim Geburtsdatum sind möglich.
	 * @return
	 */
	public boolean existsClient(String insuranceNo) {
		return clientService.existsEntity(insuranceNo);
	}

	/**
	 * Prüft, ob ein anderer Klient mit Versicherungsnummer und
	 * Krankenversicherung bereits erfasst wurde.
	 * 
	 * @param insuranceNo
	 *            Versicherungsnummer
	 * @param insurance
	 *            Krankenversicherung
	 * @return
	 */
	public boolean existsClient(String insuranceNo, HealthInsurance insurance) {
		return clientService.existsEntity(insuranceNo, insurance);
	}

	/**
	 * Gruppiert den 1. Buchstaben des Nachnames der Klienten und errechnet die
	 * Häufigkeit des Auftretens. Z.B.: [["A",3],["B",4],["E",7],["S",3]]
	 * 
	 * @return
	 */
	public List<Object[]> getSearchItems() {
		return clientService.getSearchItems();
	}

	/**
	 * Prüft, ob die Versicherungsnummer gültig ist. TODO PS: Hier beschreiben,
	 * wie das funktioniert!!!
	 * 
	 * TODO PS: besser ist, die volle Nummer zu übergeben!!! Dann kann das
	 * Geburtsdatum entfallen.
	 * 
	 * @param insuranceNo
	 * @param birthDate
	 * @return
	 */
	public boolean validateHealthInsuranceNo(int insuranceNo, LocalDate birthDate) {
		int sum = 0;
		int birthYear = birthDate.getYear();
		birthDate.getYear();
		int checkSum = insuranceNo % 10;
		int runNumber = insuranceNo / 10;

		sum += runNumber % 10 * 9;
		runNumber /= 10;

		sum += runNumber % 10 * 7;
		runNumber /= 10;

		sum += runNumber % 10 * 3;

		sum += (birthDate.getDayOfMonth() / 10) * 5;
		sum += birthDate.getDayOfMonth() % 10 * 8;
		sum += (birthDate.getMonthValue() / 10) * 4;
		sum += birthDate.getMonthValue() % 10 * 2;
		sum += birthYear % 10 * 6;
		birthYear /= 10;
		sum += birthYear % 10;

		if (sum % 11 == 10) {
			insuranceNo += 10;
			return validateHealthInsuranceNo(insuranceNo + 10, birthDate);
		}
		if (sum % 11 == checkSum) {
			return true;
		} else {
			return false;
		}
	}

	public void onAdded(@Observes @Added Client client) {
		clientService.addEntity(client);
		if (client.getClientOwner() != null) {
			client.getClientOwner().setClient(client);
		}
	}

	public void onUpdated(@Observes @Updated Client client) {
		clientService.updateEntity(client);
	}

	public void onDeleted(@Observes @Deleted Client client) {
		clientService.deleteEntity(client);
	}

}
