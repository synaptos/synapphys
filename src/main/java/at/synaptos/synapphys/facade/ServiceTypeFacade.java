package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.model.ServiceType;
import at.synaptos.synapphys.service.ServiceTypeService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class ServiceTypeFacade implements Serializable {

	private static final long serialVersionUID = -9035369483800122508L;

	@Inject
	private ServiceTypeService serviceTypeService;

	public ServiceType getServiceType(long serviceTypeId) {
		return serviceTypeService.getEntity(serviceTypeId);
	}

	public List<ServiceType> getServiceTypes() {
		return serviceTypeService.getEntities();
	}

	public void onAdded(@Observes @Added ServiceType serviceType) {
		serviceTypeService.addEntity(serviceType);
	}

	public void onUpdated(@Observes @Updated ServiceType serviceType) {
		serviceTypeService.updateEntity(serviceType);
	}

	public void onDeleted(@Observes @Deleted ServiceType serviceType) {
		serviceTypeService.deleteEntity(serviceType);
	}

}
