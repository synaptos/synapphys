package at.synaptos.synapphys.facade;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import at.synaptos.synapphys.model.Physician;
import at.synaptos.synapphys.service.PhysicianService;
import at.synaptos.synapphys.utils.Events.Added;
import at.synaptos.synapphys.utils.Events.Deleted;
import at.synaptos.synapphys.utils.Events.Updated;

@SessionScoped
public class PhysicianFacade implements Serializable {

	private static final long serialVersionUID = 5792715485951469141L;

	@Inject
	private PhysicianService physicianService;

	public Physician getPhysician(long physicianId) {
		return physicianService.getEntity(physicianId);
	}

	public List<Physician> getPhysicians() {
		return physicianService.getEntities();
	}

	public void onAdded(@Observes @Added Physician physician) {
		physicianService.addEntity(physician);
		if (physician.getPhysicianOwner() != null) {
			physician.getPhysicianOwner().setPhysician(physician);
		}
	}

	public void onUpdated(@Observes @Updated Physician physician) {
		physicianService.updateEntity(physician);
	}

	public void onDeleted(@Observes @Deleted Physician physician) {
		physicianService.deleteEntity(physician);
	}

}
