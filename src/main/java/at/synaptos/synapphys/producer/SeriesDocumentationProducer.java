package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.SeriesDocumentation;

@SessionScoped
public class SeriesDocumentationProducer implements Serializable {

	private static final long serialVersionUID = 4707246499994658398L;

	private SeriesDocumentation preparedSeriesDocumentation;

	@PostConstruct
	private void init() {
		setPreparedSeriesDocumentation(SeriesDocumentation.getInstance());
	}

	@Produces
	@Named
	public SeriesDocumentation getPreparedSeriesDocumentation() {
		return preparedSeriesDocumentation;
	}

	public void setPreparedSeriesDocumentation(SeriesDocumentation preparedSeriesDocumentation) {
		this.preparedSeriesDocumentation = preparedSeriesDocumentation;
	}

}
