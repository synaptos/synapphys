package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.Client;

@SessionScoped
public class ClientProducer implements Serializable {

	private static final long serialVersionUID = -2563857307992791883L;

	private Client preparedClient;

	private Client preparedMainInsuredClient;

	@PostConstruct
	private void init() {
		setPreparedClient(Client.getInstance());
		setPreparedMainInsuredClient(Client.getInstance());
	}

	@Produces
	@Named
	public Client getPreparedClient() {
		return preparedClient;
	}

	public void setPreparedClient(Client preparedClient) {
		this.preparedClient = preparedClient;
	}

	@Produces
	@Named
	public Client getPreparedMainInsuredClient() {
		return preparedMainInsuredClient;
	}

	public void setPreparedMainInsuredClient(Client preparedMainInsuredClient) {
		this.preparedMainInsuredClient = preparedMainInsuredClient;
	}

}
