package at.synaptos.synapphys.producer;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import at.synaptos.synapphys.domain.I18nContext;
import at.synaptos.synapphys.utils.GlobalI18n;
import at.synaptos.synapphys.utils.GlobalNames;

@SessionScoped
public class I18nBundleProducer implements Serializable {

	private static final long serialVersionUID = -1178889122542118161L;

	@Inject
	private FacesContext facesContext;

	private I18nContext i18n;

	@PostConstruct
	private void init() {
		i18n = new I18nContext(ResourceBundle.getBundle(GlobalNames.MESSAGE_RESOUCE_BUNDLE_BASENAME,
				facesContext.getViewRoot().getLocale()));
	}

	@Produces
	@GlobalI18n
	public I18nContext getI18n() {
		return i18n;
	}

}
