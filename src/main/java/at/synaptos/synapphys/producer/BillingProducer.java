package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.Billing;

@SessionScoped
public class BillingProducer implements Serializable {

	private static final long serialVersionUID = -3228771975461868200L;

	private Billing selectedBilling;

	@Produces
	@Named
	public Billing getSelectedBilling() {
		return selectedBilling;
	}

	public void setSelectedBilling(Billing selectedBilling) {
		this.selectedBilling = selectedBilling;
	}

}
