package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import at.synaptos.synapphys.model.AbstractEntity;
import at.synaptos.synapphys.model.Client;
import at.synaptos.synapphys.service.ClientService;
import at.synaptos.synapphys.utils.CollectionUtils;

@SessionScoped
public class SelectedContextKeeper implements Serializable {

	private static final long serialVersionUID = 5847495589621379977L;

	@Inject
	private ClientService clientService;

	private Client selectedClient;

	public Client getSelectedClient() {
		return selectedClient;
	}

	public void setSelectedClient(Client selectedClient) {
		this.selectedClient = selectedClient;
	}

	public void refreshSelectedClient() {
		if (!AbstractEntity.isValid(getSelectedClient())) {
			return;
		}

		Client managedClient = clientService.getEntity(getSelectedClient().getId());

		if (AbstractEntity.isValid(managedClient)) {
			managedClient.setBackUrl(getSelectedClient().getBackUrl());

			if (AbstractEntity.isValid(getSelectedClient().getSelectedScheduleItem())) {
				managedClient.setSelectedScheduleItem(CollectionUtils.getEqual(managedClient.getScheduleItems(),
						getSelectedClient().getSelectedScheduleItem()));
				if (AbstractEntity.isValid(managedClient.getSelectedScheduleItem())) {
					managedClient.getSelectedScheduleItem()
							.setBackUrl(getSelectedClient().getSelectedScheduleItem().getBackUrl());
				}
			}

			if (AbstractEntity.isValid(getSelectedClient().getSelectedSeriesDocumentation())) {
				managedClient.setSelectedSeriesDocumentation(CollectionUtils.getEqual(
						managedClient.getSeriesDocumentations(), getSelectedClient().getSelectedSeriesDocumentation()));

				if (AbstractEntity.isValid(managedClient.getSelectedSeriesDocumentation())) {
					managedClient.getSelectedSeriesDocumentation()
							.setBackUrl(getSelectedClient().getSelectedSeriesDocumentation().getBackUrl());

					if (AbstractEntity.isValid(getSelectedClient().getSelectedSessionDocumentation())) {
						managedClient.setSelectedSessionDocumentation(CollectionUtils.getEqual(
								managedClient.getSelectedSeriesDocumentation().getSessionDocumentations(),
								getSelectedClient().getSelectedSessionDocumentation()));

						if (AbstractEntity.isValid(managedClient.getSelectedSessionDocumentation())) {
							managedClient.getSelectedSessionDocumentation()
									.setBackUrl(getSelectedClient().getSelectedSessionDocumentation().getBackUrl());
						}
					}
				}
			}
		}

		setSelectedClient(managedClient);
	}

}
