package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.synaptos.synapphys.model.Client;

@ViewScoped
public class SelectedContextProducer implements Serializable {

	private static final long serialVersionUID = 9071424536472363797L;

	@Inject
	private SelectedContextKeeper selectedContextKeeper;

	@PostConstruct
	private void init() {
		selectedContextKeeper.refreshSelectedClient();
	}

	@Produces
	@Named
	public Client getSelectedClient() {
		return selectedContextKeeper.getSelectedClient();
	}

	public void setSelectedClient(Client selectedClient) {
		selectedContextKeeper.setSelectedClient(selectedClient);
	}

}
