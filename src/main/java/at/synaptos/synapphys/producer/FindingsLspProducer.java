package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.FindingsLsp;

@SessionScoped
public class FindingsLspProducer implements Serializable {

	private static final long serialVersionUID = 696907916146762950L;

	private FindingsLsp preparedFindingsLsp;

	@PostConstruct
	private void init() {
		setPreparedFindingsLsp(FindingsLsp.getInstance());
	}

	@Produces
	@Named
	public FindingsLsp getPreparedFindingsLsp() {
		return preparedFindingsLsp;
	}

	public void setPreparedFindingsLsp(FindingsLsp preparedFindingsLsp) {
		this.preparedFindingsLsp = preparedFindingsLsp;
	}

}
