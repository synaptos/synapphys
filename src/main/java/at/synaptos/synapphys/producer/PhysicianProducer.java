package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.Physician;

@SessionScoped
public class PhysicianProducer implements Serializable {

	private static final long serialVersionUID = 7854271606463236680L;

	private Physician preparedPhysician;

	@PostConstruct
	private void init() {
		setPreparedPhysician(Physician.getInstance());
	}

	@Produces
	@Named
	public Physician getPreparedPhysician() {
		return preparedPhysician;
	}

	public void setPreparedPhysician(Physician preparedPhysician) {
		this.preparedPhysician = preparedPhysician;
	}

}
