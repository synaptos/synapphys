package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.FindingsSheet;

@SessionScoped
public class FindingsSheetProducer implements Serializable {

	private static final long serialVersionUID = 3291148208467946078L;

	private FindingsSheet preparedFindingsSheet;

	@PostConstruct
	private void init() {
		setPreparedFindingsSheet(FindingsSheet.getInstance());
	}

	@Produces
	@Named
	public FindingsSheet getPreparedFindingsSheet() {
		return preparedFindingsSheet;
	}

	public void setPreparedFindingsSheet(FindingsSheet preparedFindingsSheet) {
		this.preparedFindingsSheet = preparedFindingsSheet;
	}

}
