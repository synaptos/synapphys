package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.HealthInsurance;

@SessionScoped
public class HealthInsuranceProducer implements Serializable {

	private static final long serialVersionUID = 5897659154129923098L;

	private HealthInsurance preparedHealthInsurance;

	@PostConstruct
	private void init() {
		setPreparedHealthInsurance(HealthInsurance.getInstance());
	}

	@Produces
	@Named
	public HealthInsurance getPreparedHealthInsurance() {
		return preparedHealthInsurance;
	}

	public void setPreparedHealthInsurance(HealthInsurance preparedHealthInsurance) {
		this.preparedHealthInsurance = preparedHealthInsurance;
	}

}
