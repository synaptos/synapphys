package at.synaptos.synapphys.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import at.synaptos.synapphys.model.ServiceType;

@SessionScoped
public class ServiceTypeProducer implements Serializable {

	private static final long serialVersionUID = -3359318794957859737L;

	private ServiceType preparedServiceType;

	@PostConstruct
	private void init() {
		setPreparedServiceType(ServiceType.getInstance());
	}

	@Produces
	@Named
	public ServiceType getPreparedServiceType() {
		return preparedServiceType;
	}

	public void setPreparedServiceType(ServiceType preparedServiceType) {
		this.preparedServiceType = preparedServiceType;
	}

}
