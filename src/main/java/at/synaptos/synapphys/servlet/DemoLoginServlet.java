package at.synaptos.synapphys.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.synaptos.synapphys.utils.PageUrl;

@WebServlet(urlPatterns = { "/demologin" })
public class DemoLoginServlet extends HttpServlet {

	private static final long serialVersionUID = -4971363156880772741L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			if (request.getSession(false) != null) {
				request.getSession(false).invalidate();
			}
			request.logout();
			request.login("demo.demo", "demo1234");

			response.sendRedirect(request.getContextPath() + PageUrl.CLIENT_SELECT + ".jsf");
		} catch (ServletException e) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
		}
	}

}