package at.synaptos.synapphys.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.xhtmlrenderer.pdf.ITextRenderer;

@WebServlet(urlPatterns = { "/print/*" })
public class PrintResourceServlet extends HttpServlet {

	private static final long serialVersionUID = 178574352589469284L;

	@Inject
	private Logger logger;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String sourceFilename = "billing.pdf";
		if (request.getPathInfo() != null && request.getPathInfo().length() > 1) {
			sourceFilename = request.getPathInfo().substring(1);
		}
		logger.trace("sourceFilename=" + sourceFilename);
		HttpSession session = request.getSession(false);
		String url = "http://localhost:8081/smarttherapy/pages/billing/billingPdf.jsf;jsessionid=" + session.getId();
		try {
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(url);
			renderer.layout();
			response.reset();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=\"" + sourceFilename + "\"");
			renderer.createPDF(response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}