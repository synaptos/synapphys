package at.synaptos.synapphys.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;

import at.synaptos.synapphys.utils.TenantUtils;

@WebServlet(urlPatterns = { "/custom/*" })
public class ExternalResourceServlet extends HttpServlet {

	private static final long serialVersionUID = 6905883472565988818L;

	@Inject
	private Logger logger;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String sourceFilename = request.getPathInfo().substring(1);
		String sourceDir;
		if (sourceFilename.startsWith("tmp_")) {
			sourceDir = System.getProperty("java.io.tmpdir");
		} else {
			sourceDir = TenantUtils.getUploadsDir(request.getUserPrincipal().getName());
		}
		File file = new File(sourceDir, sourceFilename);
		String fileLength = String.valueOf(file.length());
		String mimeType = getServletContext().getMimeType(sourceFilename);
		logger.trace("sourceDir=" + sourceDir + ", sourceFilename=" + sourceFilename + ", fileLength=" + fileLength
				+ ", mimeType=" + mimeType);
		response.setHeader("Content-Type", mimeType);
		response.setHeader("Content-Length", fileLength);
		response.setHeader("Content-Disposition", "inline; filename=\"" + sourceFilename + "\"");
		Files.copy(file.toPath(), response.getOutputStream());
	}

}