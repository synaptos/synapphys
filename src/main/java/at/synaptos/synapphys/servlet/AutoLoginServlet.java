package at.synaptos.synapphys.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.synaptos.synapphys.utils.GlobalNames;
import at.synaptos.synapphys.utils.PageUrl;
import at.synaptos.synapphys.utils.StringUtils;

@WebServlet(urlPatterns = { "/autologin" })
public class AutoLoginServlet extends HttpServlet {

	private static final long serialVersionUID = -5412168633813808108L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String authtoken = null;

			String queryString = request.getQueryString();

			if (!StringUtils.isEmpty(queryString)) {
				String[] queryArray = queryString.split("&");
				if (queryArray != null && queryArray.length > 0) {
					if (queryArray[0].startsWith(GlobalNames.APPLICATION_URL_PARAM_AUTHTOKEN + "=")) {
						authtoken = queryArray[0]
								.substring((GlobalNames.APPLICATION_URL_PARAM_AUTHTOKEN + "=").length());
					}
				}
			}

			if (authtoken != null) {
				if (request.getSession(false) != null) {
					request.getSession(false).invalidate();
				}
				request.logout();
				request.login("\t_" + GlobalNames.APPLICATION_URL_PARAM_AUTHTOKEN, authtoken);
			}

			response.sendRedirect(request.getContextPath() + PageUrl.CLIENT_SELECT + ".jsf");
		} catch (ServletException e) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
		}
	}

}