<?php
/**
 * Plugin Name: synapPhys Gateway
 * Description: A utility that helps interacting with synapPhys web application.
 * Author: synaptos
 * Author URI: http://www.synaptos.at
 * Version: 1.0.0
 */

// synapPhys plugin version
if ( ! defined( 'SYNAPPHYS_PLUGIN_VERSION' ) ) {
	define( 'SYNAPPHYS_PLUGIN_VERSION', '1.0.0' );	
}

if ( ! ( defined( 'ABSPATH' ) && function_exists( 'add_action' ) ) ) {
	die ( 'synapPhys: No direct access allowed. I\'m just a plugin, not much I can do when called directly ;-)' );
}
	
// Check if PMPro is active
if ( ! ( function_exists( 'pmpro_hasMembershipLevel' ) ) ) {
	trigger_error( 'PMPro plugin is not active', E_USER_WARNING );
	return;
}

// synapPhys version
if ( ! defined( 'SYNAPPHYS_VERSION' ) ) {
	define( 'SYNAPPHYS_VERSION', '1.0.0' );
}

// synapPhys namespace (plugin domain)
if ( ! defined( 'SYNAPPHYS_NAMESPACE' ) ) {
	define( 'SYNAPPHYS_NAMESPACE', 'synapphys' );
}

// synapPhys groups user capabilities (sorted from top to low level): premium, professional, etc.; Mind: in the groups configuration
// capability's full id is prefixed by SYNAPPHYS_NAMESPACE, followed by _ and its name, e.g. synapphys_premium, etc.
if ( ! defined( 'SYNAPPHYS_CAPABILITIES' ) ) {
	define( 'SYNAPPHYS_CAPABILITIES',
				SYNAPPHYS_NAMESPACE . '_premium' 
		. '|' . SYNAPPHYS_NAMESPACE . '_professional'
		. '|' . SYNAPPHYS_NAMESPACE . '_standard'
	);
}

// synapPhys option to support schema creation on activated subscription; if set to false schema creation 
// will take place on order completion
if ( ! defined( 'SYNAPPHYS_CREATE_SCHEMA_ON_ACTIVED_SUBSCRIPTION' ) ) {
	define( 'SYNAPPHYS_CREATE_SCHEMA_ON_ACTIVED_SUBSCRIPTION', true );
}

// synapPhys database host and login data; user needs minimum access rights: SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,INDEX,ALTER,LOCK TABLES,TRIGGER,SUPER
if ( ! defined( 'SYNAPPHYS_DB_HOST' ) ) {
	define( 'SYNAPPHYS_DB_HOST', ( defined( 'DB_HOST' ) ? ( stripos( DB_HOST , ':' ) !== false ? DB_HOST : DB_HOST . ':3306' ) : 'localhost:3306' ) );
}
if ( ! defined( 'SYNAPPHYS_DB_USER' ) ) {
	define( 'SYNAPPHYS_DB_USER', 'synapphys' );
}
if ( ! defined( 'SYNAPPHYS_DB_PASSWORD' ) ) {
	define( 'SYNAPPHYS_DB_PASSWORD', 'C283rByUcHE37QZ6' );
}

// synapPhys specific database settings: by now solely mysql has been implemented, but this pattern could easily be used to adapt for pgsql
if ( ! defined( 'SYNAPPHYS_DB_TYPE' ) ) {
	define( 'SYNAPPHYS_DB_TYPE', 'mysql' );
}
if ( ! defined( 'SYNAPPHYS_DB_SQL_CREATE_SCHEMA' ) ) {
	define( 'SYNAPPHYS_DB_SQL_CREATE_SCHEMA', 'CREATE DATABASE %s' );
}
if ( ! defined( 'SYNAPPHYS_DB_BATCH_IMPORT_SCHEMA' ) ) {
	// Schema build up command is supported for mysql. Adapt it to support pgsql: use pgsql schema dump utilities accordingly. 
	$options = explode( ':' , SYNAPPHYS_DB_HOST );
	define( 'SYNAPPHYS_DB_BATCH_IMPORT_SCHEMA', 'mysql'
		. ' --host=' . $options[0]
		. ' --port=' . $options[1]
		. ' --user=' . SYNAPPHYS_DB_USER
		. ' --password=' . SYNAPPHYS_DB_PASSWORD 
		. ' %s < '
		. dirname(__FILE__) . '/db-template/' . SYNAPPHYS_NAMESPACE . '-db-dump.sql' );
}
if ( ! defined( 'SYNAPPHYS_DB_CHECK_TABLE' ) ) {
	define( 'SYNAPPHYS_DB_CHECK_TABLE', 'service_type' );
}

// synapPhys URL: protocol
if ( ! defined( 'SYNAPPHYS_URL_PROTOCOL' ) ) {
	define( 'SYNAPPHYS_URL_PROTOCOL', 'https' );
}
// synapPhys URL: host (including port)
if ( ! defined( 'SYNAPPHYS_URL_HOST' ) ) {
	define( 'SYNAPPHYS_URL_HOST', 'app.synaptos.at' );
}
// synapPhys URL: base path
if ( ! defined( 'SYNAPPHYS_URL_ROOT_PATH' ) ) {
	define( 'SYNAPPHYS_URL_ROOT_PATH', '/' . SYNAPPHYS_NAMESPACE );
}
// synapPhys URL: autologin path
if ( ! defined( 'SYNAPPHYS_URL_AUTOLOGIN_PATH' ) ) {
	define( 'SYNAPPHYS_URL_AUTOLOGIN_PATH', SYNAPPHYS_URL_ROOT_PATH . '/autologin' );
}
// synapPhys URL: authentication token parameter name
if ( ! defined( 'SYNAPPHYS_URL_PARAM_AUTHTOKEN' ) ) {
	define( 'SYNAPPHYS_URL_PARAM_AUTHTOKEN', 'authtoken' );
}

// synapPhys URL
if ( ! defined( 'SYNAPPHYS_URL' ) ) {
	define( 'SYNAPPHYS_URL', SYNAPPHYS_URL_PROTOCOL . '://' . SYNAPPHYS_URL_HOST . SYNAPPHYS_URL_AUTOLOGIN_PATH );
}

require_once( ABSPATH . 'wp-includes/pluggable.php' );
require_once( ABSPATH . 'wp-includes/session.php' );
 
if ( ! class_exists( 'SynapPhys_Gateway' ) ) {
  
	/**
	 * Main synapPhys Gateway Class
	 *
	 * @class SynapPhys_Gateway
	 * @package  synapphys
	 * @category Class
	 * @author   Walter Quendler
	 
	 */  
	final class SynapPhys_Gateway {
	
		/**
		 * Hooks into actions and filters
		 */	
		public static function init() {
			self::write_log( array( 'method' => __METHOD__ ) );

			add_filter( 'xmlrpc_methods', array( __CLASS__, 'add_xmlrpc_methods' ) );
			add_filter( 'wp_nav_menu_objects', array( __CLASS__, 'add_menu_url_auth_token' ) );
			
			/* TODO WQ
			if ( SYNAPPHYS_CREATE_SCHEMA_ON_ACTIVED_SUBSCRIPTION === true ) {
				add_action( 'woocommerce_subscription_status_active', array( __CLASS__, 'custom_subscription_status_active' ) );
			} else {
				add_action( 'woocommerce_order_status_completed', array( __CLASS__, 'custom_order_status_completed' ) );
			}

			add_action( 'woocommerce_thankyou', array( __CLASS__, 'custom_thankyou' ) );
			*/
		}
		
		/**
		 * Tells a custom thank you
		 *
		 * @param int $order_id ID of current order
		 */		
		public static function custom_thankyou( $order_id ) {		
			self::write_log( array( 'method' => __METHOD__, '$order_id' => $order_id ) );
			
			if ( $auth_token = self::create_current_user_auth_token() ) {
				$url = SYNAPPHYS_URL . '?' . SYNAPPHYS_URL_PARAM_AUTHTOKEN . '=' . $auth_token;
				echo '<p>Ihre Registrierkasse ist nun eingerichtet. <a href="' . $url . '">Jetzt ausprobieren</a>.</p>';
			} else {
				echo 
					  '<ul class="woocommerce-error">'
					. '<li>Ihre Registrierkasse wird in Kürze eingerichtet sein. Wir informieren Sie per Mail.</li>'
					. '</ul>';
			}
		}

		/**
		 * Checks if current order contains at least one subscription and calls activated subscription handler
		 *
		 * @param int $order_id ID of current order
		 */
		public static function custom_order_status_completed( $order_id ) {
			self::write_log( array( 'method' => __METHOD__, '$order_id' => $order_id ) );
			
			if ( empty( $order_id ) ) {
				return;
			}

			$contains_subscription = wcs_order_contains_subscription( $order_id, 'any' );
			self::write_log( array( 'method' => __METHOD__, '$contains_subscription' => $contains_subscription ) );
			
			if ( ! $contains_subscription ) {
				return;
			}
						
			foreach ( wcs_get_subscriptions_for_order( $order_id, array( 'order_type' => 'any' ) ) as $subscription ) {
				self::custom_subscription_status_active( $subscription );
			}
		}
	
		/**
		 * Checks if current subscription has added synapPhys capabilities and creates/imports a new schema for user related to
		 *
		 * @param WC_Subscription $subscription Current subscription object
		 */	
		public static function custom_subscription_status_active( $subscription ) {			
			self::write_log( array( 'method' => __METHOD__, '$subscription' => $subscription ) );
			
			if ( empty( $subscription ) ) {
				return;
			}

			$group_capabilities = explode( '|', SYNAPPHYS_CAPABILITIES );
			self::write_log( array( 'method' => __METHOD__, '$group_capabilities' => $group_capabilities ) );
			
			$has_capability = false;
			foreach ( $subscription->get_items() as $item ) {
				$product_id = WC_Subscriptions_Order::get_items_product_id( $item );
				self::write_log( array( 'method' => __METHOD__, '$product_id' => $product_id ) );

				if ( empty( $product_id ) ) {
					continue;
				}
				
				$group_id = get_post_meta( $product_id, '_groups_groups', true );
				self::write_log( array( 'method' => __METHOD__, '$group_id' => $group_id ) );
				
				if ( empty( $group_id ) ) {
					continue;
				}
				
				$groups_group = new Groups_Group( $group_id );
				self::write_log( array( 'method' => __METHOD__, '$groups_group' => $groups_group ) );

				if ( empty( $groups_group ) ) {
					continue;
				}
				
				foreach ( $group_capabilities as $group_capability ) {
					if ( $groups_group->can( $group_capability ) ) {
						$has_capability = true;
						break 2;
					}
				}				
			}
			self::write_log( array( 'method' => __METHOD__, '$has_capability' => $has_capability ? 'true' : 'false' ) );

			if ( $has_capability === true ) {				
				if ( ! $pdo = self::create_pdo() ) {
					return;
				}
				
				$user_id = $subscription->get_user_id();
				self::write_log( array( 'method' => __METHOD__, '$user_id' => $user_id ) );
				
				if ( empty( $user_id ) ) {
					return;
				}
				
				$user = get_user_by( 'id', $user_id );
				self::write_log( array( 'method' => __METHOD__, '$user' => $user ) );
				
				if ( empty( $user ) ) {
					return;
				}
				
				$schema_name = self::get_schema_name( $user->user_login, SYNAPPHYS_NAMESPACE );
				
				if ( ! self::select_schema( $pdo, $schema_name ) ) {
					$creation_success = self::create_schema( $pdo, $schema_name );
					self::write_log( array( 'method' => __METHOD__, '$creation_success' => ( $creation_success ? 'true' : 'false' ) ) );

					$pdo = null;
					
					if ( ! $creation_success  ) {
						return;
					}

					self::import_schema( $schema_name );

					if ( ! $pdo = self::create_pdo( $schema_name ) ) {
						return;
					}
				}

				self::write_log( array( 'method' => __METHOD__, 'exists table ' . SYNAPPHYS_DB_CHECK_TABLE => ( self::exists_table( $pdo, SYNAPPHYS_DB_CHECK_TABLE ) ? 'true' : 'false' ) ) );

				$pdo = null;
			}
		}

		/**
		 * Creates a link to database, no schema selected
		 *
		 * @param string $schema_name Name of schema to be selected (optional)
		 * @return PDO if link could established properly, false otherwise
		 */
		public static function create_pdo( $schema_name = '' ) {
			self::write_log( array( 'method' => __METHOD__, '$schema_name' => $schema_name ) );
			
			try {
				$dsn = SYNAPPHYS_DB_TYPE . ':host=' . explode( ':' , SYNAPPHYS_DB_HOST )[0] . ( ! empty( $schema_name ) ? ( ';dbname=' . $schema_name ) : '' );
				self::write_log( array( 'method' => __METHOD__, '$dsn' => $dsn, 'SYNAPPHYS_DB_USER' => SYNAPPHYS_DB_USER ) );
				return new PDO( $dsn, SYNAPPHYS_DB_USER, SYNAPPHYS_DB_PASSWORD, array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ) );
			} catch ( PDOException $e ) {
				trigger_error( $e->getMessage(), E_USER_WARNING );
			}

			return false;
		}
		
		/**
		 * Selects a schema
		 *
		 * @param PDO $pdo Connection to database
		 * @param string $schema_name Name of schema to select
 		 * @return bool true if schema selection was successfull, false otherwise
		 */		
		public static function select_schema( $pdo, $schema_name ) {
			self::write_log( array( 'method' => __METHOD__, '$pdo' => ( ! empty( $pdo ) ? 'true' : 'false' ), '$schema_name' => $schema_name ) );

			// Validate arguments
			if ( ! $pdo ) {
				return false;
			}
			if ( empty( $schema_name )  ) {
				return false;
			}

			try {
				$pdo->exec( 'USE ' . $schema_name );
				trigger_error( 'That schema has been selected: ' . $schema_name );
				return true;
			} catch ( PDOException $e ) {
				trigger_error( $e->getMessage() );
			}
			
			return false;
		}	
		
		/**
		 * Creates a new schema
		 *
		 * @param PDO $pdo Connection to database
		 * @param string $schema_name Name of schema to create
 		 * @return bool true if schema creation was successfull, false otherwise
		 */
		public static function create_schema( $pdo, $schema_name ) {
			self::write_log( array( 'method' => __METHOD__, '$pdo' => ( ! empty( $pdo ) ? 'true' : 'false' ), '$schema_name' => $schema_name ) );
			
			// Validate arguments
			if ( ! $pdo ) {
				return false;
			}
			if ( empty( $schema_name )  ) {
				return false;
			}
			
			try {
				$pdo->exec( sprintf( SYNAPPHYS_DB_SQL_CREATE_SCHEMA, $schema_name ) );				
				trigger_error( 'New schema has been created: ' . $schema_name );
				return true;
			} catch ( PDOException $e ) {
				trigger_error( $e->getMessage(), E_USER_WARNING );
			}
			
			return false;
		}
		
		/**
		 * Check if a table exists
		 *
		 * @param PDO $pdo Connection to database
		 * @param string $table_name Name of table
		 * @return bool true if table exists, false otherwise
		 */
		public static function exists_table( $pdo, $table_name ) {
			self::write_log( array( 'method' => __METHOD__, '$pdo' => ( ! empty( $pdo ) ? 'true' : 'false' ), '$table_name' => $table_name ) );
			
			// Validate arguments
			if ( ! $pdo ) {
				return false;
			}
			if ( empty( $table_name )  ) {
				return false;
			}

			// Try a select statement against the table
			try {
				// Query result is either boolean false (no table found) or PDOStatement Object (table found)
				if ( $pdo->query( 'SELECT 1 FROM ' . $table_name . ' LIMIT 1' ) !== false ) {
					trigger_error( 'That table exists: ' . $table_name );
					return true;		
				}
			} catch ( PDOException $e ) {
				trigger_error( $e->getMessage() );
			}

			return false;
		}
	
		/**
		 * Imports schema content from export file (schema dump). Make sure any connection to schema is closed
		 *
		 * @param string $schema_name Name of schema to be imported
		 * @return bool true if schema has been imported, false otherwise
		 */
		public static function import_schema( $schema_name ) {
			self::write_log( array( 'method' => __METHOD__, '$schema_name' => $schema_name ) );
			
			if ( empty( $schema_name )  ) {
				return false;
			}
		
			exec( sprintf( SYNAPPHYS_DB_BATCH_IMPORT_SCHEMA, $schema_name ), $exec_output, $exec_status );
			self::write_log( array( 'method' => __METHOD__, '$exec_output' => $exec_output, '$exec_status' => $exec_status ) );
				
			if ( $exec_status !== 0 ) {
				trigger_error( "Couldn't import schema: " . $schema_name . ', $exec_status=' . $exec_status, E_USER_WARNING );
				return false;
			}

			return true;
		}
		
		/**
		 * Creates a schema name from user login
		 *
		 * @param string $user_login Current user login object
		 * @param string $env_identifier Current environment identifier
		 * @return string generated schema name
		 */
		private static function get_schema_name( $user_login, $env_identifier )  {
			self::write_log( array( 'method' => __METHOD__, '$user_login' => $user_login, '$env_identifier' => $env_identifier ) );
			
			if ( empty( $user_login ) ) {
				return null;
			}
			if ( ! $env_identifier ) {
				$env_identifier = '';
			}
			
			$schema_name = '`' . $env_identifier . '_' . str_replace( ' ', '_', $user_login ) . '`';
			self::write_log( array( 'method' => __METHOD__, '$schema_name' => $schema_name ) );
			
			return $schema_name;
		}
				
		/**
		 * Adds parameter to menu URL
		 *
		 * @param array $items Array of menu items
		 * @return array Menu items changed
		 */
		public static function add_menu_url_auth_token( $items ) {
			self::write_log( array( 'method' => __METHOD__, '$items' => ( ! empty( $items ) ? 'true' : 'false' ) ) );
						
			foreach( $items as $key => $item ) {
				if ( $item->url === SYNAPPHYS_URL ) {
					if ( $auth_token = self::create_current_user_auth_token() ) {
						$item->url = $item->url . '?' . SYNAPPHYS_URL_PARAM_AUTHTOKEN . '=' . $auth_token;
					} else {
						 unset( $items[$key] );
					}
				}
			}
			
			return $items;
		}
		
		/**
		 * Adds methods to call by RPC
		 *
		 * @param array $methods Array of methods added
		 * @return array Methods added
		 */
		public static function add_xmlrpc_methods( $methods ) {
			$methods[SYNAPPHYS_NAMESPACE . '.getRoleByAuthtoken'] = array( __CLASS__, 'get_role_by_authtoken' );
			$methods[SYNAPPHYS_NAMESPACE . '.getRoleByCredentials'] = array( __CLASS__, 'get_role_by_credentials' );		  
			return $methods;
		}
		
		/**
		 * Gets role according incoming arguments
		 *
		 * @param array $args Array of arguments
		 * @return array result data
		 */
		public static function get_role_by_credentials( $args ) {
			self::write_log( array( 'method' => __METHOD__, '$args' => ( ! empty( $args ) ? 'true' : 'false' ) ) );

			if ( empty( $args ) ) {
				return array();
			}
			global $wp_xmlrpc_server;
		  
			$wp_xmlrpc_server->escape( $args );

			if ( empty( $args[0] ) ) {
				return array();
			}			
			if ( count( $args ) < 2  || empty( $args[1] ) ) {
				return array();
			}
			
			if ( ! $user = $wp_xmlrpc_server->login( $args[0], $args[1] ) ) {
				return array();
			}
		  
			$capability = self::get_capability( $user );
		  
			if ( ! empty( $capability ) ) {
			  
				if ( ! $pdo = self::create_pdo() ) {
					return array();
				}

				$env_identifier = SYNAPPHYS_NAMESPACE;
				if ( ! empty( $args[2] ) ) {
					$identifier_elements = explode( '_', $args[2] );
					if ( ! empty( $identifier_elements ) ) {
						$env_identifier = $identifier_elements[0];
					}
				}
				
				if ( ! self::select_schema( $pdo, self::get_schema_name( $user->user_login, $env_identifier ) ) ) {
					$pdo = null;
					return array();
				}

				$check_success = self::exists_table( $pdo, SYNAPPHYS_DB_CHECK_TABLE );

				$pdo = null;

				if ( ! $check_success ) {
					return array();
				}
				
			}
		  
			return $capability;
		}
  
		/**
		 * Gets role according incoming arguments
		 *
		 * @param array $args Array of arguments
		 * @return array result data
		 */
		public static function get_role_by_authtoken( $args ) {
			self::write_log( array( 'method' => __METHOD__, '$args' => ( ! empty( $args ) ? 'true' : 'false' ) ) );

			if ( empty( $args ) ) {
				return array();
			}
			
			global $wp_xmlrpc_server;
		  
			$wp_xmlrpc_server->escape( $args );
	  
			$auth_token = $args[0];

			if ( empty( $auth_token ) ) {
				return array();
			}
	  
			if ( ! $plain_auth_token = self::AES_decode( $auth_token ) ) {
				return array();
			}

			self::write_log( array( 'method' => __METHOD__, '$plain_auth_token' => $plain_auth_token ) );
		  
			if ( ! $token_elements = explode( '|', $plain_auth_token ) ) {
				return array();
			}
		  
			$username = $token_elements[0];
			$session_token = $token_elements[1];
			$cookie_expired = $token_elements[2];
		  
			// Quick check to see if cookie has expired
			if ( $cookie_expired < time() ) {
				return array();
			}
	  
			if ( ! username_exists( $username ) || ! $user = get_user_by( 'login', $username ) ) {
				return array();
			}
	   
			$manager = WP_Session_Tokens::get_instance( $user->ID );
			if ( ! $manager->verify( $session_token ) ) {
				return array();
			}

			$capability = self::get_capability( $user );
		 
			array_push(
				$capability, 
				str_replace( '&amp;', '&', home_url() ),
				str_replace( '&amp;', '&', wp_logout_url( home_url() ) ),
				str_replace( '&amp;', '&', get_permalink( wc_get_page_id( 'myaccount' ) ) )
			);
		 
			return $capability;
		}
      
		/**
		 * Gets capability by user
		 *
		 * @param WP_User $user User given
		 * @return array result data
		 */
		private static function get_capability( $user ) {
			self::write_log( array( 'method' => __METHOD__, '$user' => $user ) );

			if ( ! $user ) {
				return array();
			}
	  
			$membership_level = pmpro_getMembershipLevelForUser( $user->ID );
			
			if ( empty( $membership_level ) ) {
				return array();
			}
		  
			$result_data = array( $membership_level->name, $user->user_login );
			self::write_log( array( 'method' => __METHOD__, '$result_data' => $result_data ) );
		  	  
			return $result_data;
		}    

		/**
		 * Creates auth info for current logged in user
		 *
		 * @return string current user's auth token
		 */
		private static function create_current_user_auth_token() {			
			self::write_log( array( 'method' => __METHOD__ ) );

			if ( ! is_user_logged_in() ) {
				return false;
			}

			$scheme = '';			
			$cookie_elements = wp_parse_auth_cookie( '', $scheme );
			if ( empty( $cookie_elements ) ) {
				$scheme = 'logged_in';
				$cookie_elements = wp_parse_auth_cookie( '', $scheme );
			}
			self::write_log( array( 'method' => __METHOD__, '$scheme' => $scheme, '$cookie_elements' => ( ! empty( $cookie_elements ) ? 'true' : 'false' ) ) );

			if ( empty( $cookie_elements ) ) {
				return false;
			}

			$user = wp_get_current_user();
			self::write_log( array( 'method' => __METHOD__, '$cookie_elements[\'username\']' => $cookie_elements['username'], '$user' => $user ) );
 			
			if ( $cookie_elements['username'] !== $user->user_login  ) {
				return false;
			}
			
			$user_capability = self::get_capability( $user );
			self::write_log( array( 'method' => __METHOD__, '$user_capability' => $user_capability ) );
			
			if ( empty( $user_capability ) ) {
				return false;
			}
			
			if ( ! $pdo = self::create_pdo() ) {
				return false;
			}
			
			if ( ! self::select_schema( $pdo, self::get_schema_name( $user->user_login, SYNAPPHYS_NAMESPACE ) ) ) {
				$pdo = null;
				return false;
			}

			$check_success = self::exists_table( $pdo, SYNAPPHYS_DB_CHECK_TABLE );
			
			$pdo = null;

			if ( ! $check_success ) {
				return false;
			}
			 
			$auth_token = self::AES_encode( $user->user_login . '|' . $cookie_elements['token'] . '|' . $cookie_elements['expiration'] );
			self::write_log( array( 'method' => __METHOD__, '$auth_token' => $auth_token ) );
			
			return $auth_token;
		}

		/**
		 * AES256 Encrypt
		 *
		 * @param string $plain_text Text to be encoded
		 * @return string Text given encrypted
		 */
		private static function AES_encode( $plain_text ) {
			return base64_encode( openssl_encrypt( $plain_text, "aes-256-cbc", LOGGED_IN_KEY, true, str_repeat( chr(0), 16 ) ) );
		}
  
		/**
		 * AES256 Decrypt
		 *
		 * @param string $base64_text Text to be decoded
		 * @return string Text given decrypted
		 */
		private static function AES_decode( $base64_text ) {
			return openssl_decrypt( base64_decode( $base64_text ), "aes-256-cbc", LOGGED_IN_KEY, true, str_repeat( chr(0), 16 ) );
		}

		/**
		 * Write an array/a string to debug log
		 *
		 * @param array $args Array of arguments to write into debug log
		 */
		private static function write_log( $args )  {
			if ( WP_DEBUG === true ) {
				if ( is_array( $args ) || is_object( $args ) ) {
					error_log( print_r( $args, true ) );
				} else {
					error_log( $args );
				}
			}
		}

	}

	SynapPhys_Gateway::init();
	
}

?>
