-- MySQL dump 10.13  Distrib 5.6.26, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: synapphys_template
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `billing`
--

DROP TABLE IF EXISTS `billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing` (
  `billing_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `billing_date` datetime NOT NULL,
  `billing_note` varchar(255) DEFAULT NULL,
  `billing_type` varchar(255) NOT NULL,
  `cb_terminal_id` varchar(255) DEFAULT NULL,
  `due_date` datetime NOT NULL,
  `footer_note` varchar(255) DEFAULT NULL,
  `ft_access_token` varchar(255) DEFAULT NULL,
  `ft_cashbox_id` varchar(255) DEFAULT NULL,
  `header_note` varchar(255) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_no` varchar(255) DEFAULT NULL,
  `payment_note` varchar(2000) DEFAULT NULL,
  `reference_sequence` int(11) NOT NULL,
  `reference_year` int(11) NOT NULL,
  `series_documentation` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`billing_id`),
  UNIQUE KEY `UK786pvfq1cgsnfjufh7bnwxeol` (`reference_sequence`,`reference_year`,`billing_type`),
  KEY `IDXfuvmwncm6hotf7mjeni63p0fk` (`billing_type`,`billing_date`),
  KEY `IDXosrrssv5i5bb3fusckc9noeh1` (`billing_type`,`payment_date`,`payment_no`),
  KEY `FKacl8krghswgu158iyykaqt0q3` (`series_documentation`),
  CONSTRAINT `FKacl8krghswgu158iyykaqt0q3` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing`
--

LOCK TABLES `billing` WRITE;
/*!40000 ALTER TABLE `billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE trigger `billing_reference_no` before insert on `billing` for each row begin set new.reference_year=year(new.billing_date); set new.reference_sequence=(select ifnull(max(reference_sequence),0)+1 from billing where billing_type = new.billing_type and billing_date >= makedate(year(new.billing_date), 1) and billing_date < makedate(year(new.billing_date)+1, 1)); end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `billing_item`
--

DROP TABLE IF EXISTS `billing_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_item` (
  `billing_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sequence_no` int(11) NOT NULL,
  `service_date` datetime DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `vat` decimal(19,2) DEFAULT NULL,
  `billing` bigint(20) NOT NULL,
  `service_documentation` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`billing_item_id`),
  KEY `FKjn8glvh16imt6hyci1y2fy6p8` (`billing`),
  KEY `FK2jr2ws4eronp3k85stihn9bvf` (`service_documentation`),
  CONSTRAINT `FK2jr2ws4eronp3k85stihn9bvf` FOREIGN KEY (`service_documentation`) REFERENCES `service_documentation` (`service_documentation_id`),
  CONSTRAINT `FKjn8glvh16imt6hyci1y2fy6p8` FOREIGN KEY (`billing`) REFERENCES `billing` (`billing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing_item`
--

LOCK TABLES `billing_item` WRITE;
/*!40000 ALTER TABLE `billing_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billing_signature`
--

DROP TABLE IF EXISTS `billing_signature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_signature` (
  `billing_signature_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cb_terminal_id` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `ft_cashbox_id` varchar(255) DEFAULT NULL,
  `ft_cashbox_name` varchar(255) DEFAULT NULL,
  `ft_receipt_id` varchar(255) DEFAULT NULL,
  `ft_receipt_moment` datetime DEFAULT NULL,
  `ft_receipt_response_log` text,
  `ft_receipt_signature` text,
  `ft_state_counting` bit(1) DEFAULT NULL,
  `ft_state_exception` varchar(255) DEFAULT NULL,
  `ft_state_flag` bigint(20) DEFAULT NULL,
  `ft_state_no` int(11) DEFAULT NULL,
  `ft_state_signing` bit(1) DEFAULT NULL,
  `ft_state_zero_receipt` bit(1) DEFAULT NULL,
  `billing` bigint(20) NOT NULL,
  PRIMARY KEY (`billing_signature_id`),
  KEY `FK1u2hg7lj7p5u78v7k2n9rycrv` (`billing`),
  CONSTRAINT `FK1u2hg7lj7p5u78v7k2n9rycrv` FOREIGN KEY (`billing`) REFERENCES `billing` (`billing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing_signature`
--

LOCK TABLES `billing_signature` WRITE;
/*!40000 ALTER TABLE `billing_signature` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing_signature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bodymap_item`
--

DROP TABLE IF EXISTS `bodymap_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bodymap_item` (
  `bodymap_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `finding_note` varchar(2000) DEFAULT NULL,
  `finding_position_x` double NOT NULL,
  `finding_position_y` double NOT NULL,
  `finding_text` varchar(2000) DEFAULT NULL,
  `finding_type` varchar(255) NOT NULL,
  `sequence_no` int(11) NOT NULL,
  `tip_position_x` double DEFAULT NULL,
  `tip_position_y` double DEFAULT NULL,
  `value_from` int(11) DEFAULT NULL,
  `value_to` int(11) DEFAULT NULL,
  `parent_item` bigint(20) DEFAULT NULL,
  `series_documentation` bigint(20) NOT NULL,
  `session_documentation` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`bodymap_item_id`),
  KEY `FK4dxgsv8hsu9m90l1jg6dpkdw4` (`parent_item`),
  KEY `FKikccj6q6x4u16yu11l4idwhp5` (`series_documentation`),
  KEY `FKokd4p9g23r6ppo5uiwgwgxlt5` (`session_documentation`),
  CONSTRAINT `FK4dxgsv8hsu9m90l1jg6dpkdw4` FOREIGN KEY (`parent_item`) REFERENCES `bodymap_item` (`bodymap_item_id`),
  CONSTRAINT `FKikccj6q6x4u16yu11l4idwhp5` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`),
  CONSTRAINT `FKokd4p9g23r6ppo5uiwgwgxlt5` FOREIGN KEY (`session_documentation`) REFERENCES `session_documentation` (`session_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bodymap_item`
--

LOCK TABLES `bodymap_item` WRITE;
/*!40000 ALTER TABLE `bodymap_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `bodymap_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `person_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `address_city` varchar(255) DEFAULT NULL,
  `address_postcode` varchar(255) DEFAULT NULL,
  `address_street` varchar(255) DEFAULT NULL,
  `contact_note` varchar(255) DEFAULT NULL,
  `person_firstname` varchar(255) DEFAULT NULL,
  `contact_home_email` varchar(255) DEFAULT NULL,
  `contact_home_fax` varchar(255) DEFAULT NULL,
  `contact_home_phone_1` varchar(255) DEFAULT NULL,
  `contact_home_phone_2` varchar(255) DEFAULT NULL,
  `person_lastname` varchar(255) NOT NULL,
  `person_title` varchar(255) DEFAULT NULL,
  `contact_work_email` varchar(255) DEFAULT NULL,
  `contact_work_fax` varchar(255) DEFAULT NULL,
  `contact_work_phone_1` varchar(255) DEFAULT NULL,
  `contact_work_phone_2` varchar(255) DEFAULT NULL,
  `bank_account_bankname` varchar(255) DEFAULT NULL,
  `bank_account_bic` varchar(255) DEFAULT NULL,
  `bank_account_iban` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `birth_place` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `health_insurance_no` varchar(255) DEFAULT NULL,
  `patient_status` varchar(255) DEFAULT NULL,
  `patient_status_date` datetime DEFAULT NULL,
  `patient_status_note` varchar(255) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `vip_date` datetime DEFAULT NULL,
  `vip_note` varchar(255) DEFAULT NULL,
  `health_insurance` bigint(20) DEFAULT NULL,
  `main_insured_client` bigint(20) DEFAULT NULL,
  `physician` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  KEY `IDXin7l3wuwfokt0c25jx29aedc5` (`person_lastname`,`person_firstname`,`birth_date`),
  KEY `IDX9vcpren9bbp60ihu45xgu7ww5` (`health_insurance_no`),
  KEY `IDXfv3lvjwp5v8ryp866ooo1l7yw` (`patient_status`),
  KEY `FK3kqu2af1g74ohhcwvdprubim9` (`health_insurance`),
  KEY `FKqn4emgg7o657mfj7kp7kdujfm` (`main_insured_client`),
  KEY `FKstpo48cji7f1rte7ahhiu2dvp` (`physician`),
  CONSTRAINT `FK3kqu2af1g74ohhcwvdprubim9` FOREIGN KEY (`health_insurance`) REFERENCES `health_insurance` (`health_insurance_id`),
  CONSTRAINT `FKqn4emgg7o657mfj7kp7kdujfm` FOREIGN KEY (`main_insured_client`) REFERENCES `client` (`person_id`),
  CONSTRAINT `FKstpo48cji7f1rte7ahhiu2dvp` FOREIGN KEY (`physician`) REFERENCES `physician` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `findings_lsp`
--

DROP TABLE IF EXISTS `findings_lsp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `findings_lsp` (
  `findings_lsp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active_test_extension` int(11) DEFAULT NULL,
  `active_test_flex_hws_flexion` int(11) DEFAULT NULL,
  `active_test_flex_latara_left` int(11) DEFAULT NULL,
  `active_test_flex_latara_right` int(11) DEFAULT NULL,
  `active_test_heel_walk_left` int(11) DEFAULT NULL,
  `active_test_heel_walk_right` int(11) DEFAULT NULL,
  `active_test_toe_stand_left` int(11) DEFAULT NULL,
  `active_test_toe_stand_right` int(11) DEFAULT NULL,
  `active_test_flexion` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `passiv_test_hip_ar_left` int(11) DEFAULT NULL,
  `passiv_test_hip_ar_right` int(11) DEFAULT NULL,
  `passiv_test_hip_flexion_left` int(11) DEFAULT NULL,
  `passiv_test_hip_flexion_right` int(11) DEFAULT NULL,
  `passiv_test_hip_ir_left` int(11) DEFAULT NULL,
  `passiv_test_hip_ir_right` int(11) DEFAULT NULL,
  `passiv_test_sig_i_lia_dorsal_left` int(11) DEFAULT NULL,
  `passiv_test_sig_i_lia_dorsal_right` int(11) DEFAULT NULL,
  `passiv_test_straight_leg_raise_left` int(11) DEFAULT NULL,
  `passiv_test_straight_leg_raise_right` int(11) DEFAULT NULL,
  `resistance_test_m_tibialis_anterior_left` bit(1) DEFAULT NULL,
  `resistance_test_m_tibialis_anterior_right` bit(1) DEFAULT NULL,
  `resistance_test_m_ehl_left` bit(1) DEFAULT NULL,
  `resistance_test_m_ehl_right` bit(1) DEFAULT NULL,
  `resistance_test_m_psoas_left` bit(1) DEFAULT NULL,
  `resistance_test_m_psoas_right` bit(1) DEFAULT NULL,
  `resistance_test_mm_peronei_left` bit(1) DEFAULT NULL,
  `resistance_test_mm_peronei_right` bit(1) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `series_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`findings_lsp_id`),
  KEY `FKsu23v1v08i8bv35gixc4tq45o` (`series_documentation`),
  CONSTRAINT `FKsu23v1v08i8bv35gixc4tq45o` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `findings_lsp`
--

LOCK TABLES `findings_lsp` WRITE;
/*!40000 ALTER TABLE `findings_lsp` DISABLE KEYS */;
/*!40000 ALTER TABLE `findings_lsp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `findings_sheet`
--

DROP TABLE IF EXISTS `findings_sheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `findings_sheet` (
  `findings_sheet_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `anamnesis_history` text,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `exclusionary_test` text,
  `fbl_constitution` text,
  `fbl_motion_behaviour` text,
  `fbl_reactive_hypertonus` text,
  `fbl_statics_of_site` text,
  `fbl_statics_of_front_and_back` text,
  `fbl_thrust_load` text,
  `functional_demo` text,
  `functional_examination` text,
  `inspection` text,
  `interpretation_hypothesis` text,
  `neurodynamicial_test` text,
  `neurological_examination` text,
  `pain_anamnesis` text,
  `palpation` text,
  `planned_therapeutical_functional_outcome` text,
  `planned_therapeutical_outcome_and_measures` text,
  `present_pain` text,
  `special_issues` text,
  `specific_measuring` text,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `series_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`findings_sheet_id`),
  KEY `FKdpeyyu6luf6v20o9jqdiqn58h` (`series_documentation`),
  CONSTRAINT `FKdpeyyu6luf6v20o9jqdiqn58h` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `findings_sheet`
--

LOCK TABLES `findings_sheet` WRITE;
/*!40000 ALTER TABLE `findings_sheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `findings_sheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `health_insurance`
--

DROP TABLE IF EXISTS `health_insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `health_insurance` (
  `health_insurance_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `address_city` varchar(255) DEFAULT NULL,
  `address_postcode` varchar(255) DEFAULT NULL,
  `address_street` varchar(255) DEFAULT NULL,
  `health_insurance_code` varchar(255) NOT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_fax` varchar(255) DEFAULT NULL,
  `contact_phone_1` varchar(255) DEFAULT NULL,
  `contact_phone_2` varchar(255) DEFAULT NULL,
  `contact_note` varchar(255) DEFAULT NULL,
  `health_insurance_name` varchar(255) NOT NULL,
  PRIMARY KEY (`health_insurance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `health_insurance`
--

LOCK TABLES `health_insurance` WRITE;
/*!40000 ALTER TABLE `health_insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `health_insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instance_property`
--

DROP TABLE IF EXISTS `instance_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instance_property` (
  `instance_property_key` varchar(255) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `instance_property_value` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`instance_property_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instance_property`
--

LOCK TABLES `instance_property` WRITE;
/*!40000 ALTER TABLE `instance_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `instance_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kpi_item`
--

DROP TABLE IF EXISTS `kpi_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kpi_item` (
  `kpi_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `kpi_type` varchar(255) NOT NULL,
  `kpi_value` int(11) NOT NULL,
  `session_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`kpi_item_id`),
  KEY `FK1vhpsrlyxf15awxmpt973uvyf` (`session_documentation`),
  CONSTRAINT `FK1vhpsrlyxf15awxmpt973uvyf` FOREIGN KEY (`session_documentation`) REFERENCES `session_documentation` (`session_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kpi_item`
--

LOCK TABLES `kpi_item` WRITE;
/*!40000 ALTER TABLE `kpi_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `kpi_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physician`
--

DROP TABLE IF EXISTS `physician`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physician` (
  `person_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `address_city` varchar(255) DEFAULT NULL,
  `address_postcode` varchar(255) DEFAULT NULL,
  `address_street` varchar(255) DEFAULT NULL,
  `contact_note` varchar(255) DEFAULT NULL,
  `person_firstname` varchar(255) DEFAULT NULL,
  `contact_home_email` varchar(255) DEFAULT NULL,
  `contact_home_fax` varchar(255) DEFAULT NULL,
  `contact_home_phone_1` varchar(255) DEFAULT NULL,
  `contact_home_phone_2` varchar(255) DEFAULT NULL,
  `person_lastname` varchar(255) NOT NULL,
  `person_title` varchar(255) DEFAULT NULL,
  `contact_work_email` varchar(255) DEFAULT NULL,
  `contact_work_fax` varchar(255) DEFAULT NULL,
  `contact_work_phone_1` varchar(255) DEFAULT NULL,
  `contact_work_phone_2` varchar(255) DEFAULT NULL,
  `office_hours` varchar(255) DEFAULT NULL,
  `speciality` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  KEY `IDXmiag74tqk1sxo909rbo9sq4c7` (`person_lastname`,`person_firstname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physician`
--

LOCK TABLES `physician` WRITE;
/*!40000 ALTER TABLE `physician` DISABLE KEYS */;
/*!40000 ALTER TABLE `physician` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_item`
--

DROP TABLE IF EXISTS `schedule_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_item` (
  `schedule_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `confirmed_date` datetime DEFAULT NULL,
  `schedule_end_date` datetime NOT NULL,
  `schedule_note` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `remind_by_sms` bit(1) DEFAULT NULL,
  `schedule_start_date` datetime NOT NULL,
  `schedule_subject` varchar(255) DEFAULT NULL,
  `client` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`schedule_item_id`),
  KEY `IDXiey6pag64fmf794fi3oo50etx` (`schedule_start_date`,`schedule_end_date`),
  KEY `FKavkws6epygvs0im5xmhfc9o13` (`client`),
  CONSTRAINT `FKavkws6epygvs0im5xmhfc9o13` FOREIGN KEY (`client`) REFERENCES `client` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_item`
--

LOCK TABLES `schedule_item` WRITE;
/*!40000 ALTER TABLE `schedule_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `series_documentation`
--

DROP TABLE IF EXISTS `series_documentation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `series_documentation` (
  `series_documentation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `approval_date` date DEFAULT NULL,
  `canceled_date` date DEFAULT NULL,
  `findingNote` varchar(255) DEFAULT NULL,
  `longterm_outcome` varchar(255) DEFAULT NULL,
  `medical_diagnosis` varchar(255) DEFAULT NULL,
  `referral_date` date DEFAULT NULL,
  `session_count` int(11) DEFAULT NULL,
  `session_duration` int(11) DEFAULT NULL,
  `shortterm_outcome` varchar(255) DEFAULT NULL,
  `client` bigint(20) NOT NULL,
  `physician` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`series_documentation_id`),
  KEY `FKmbwbbpe4rkddjhuhsh34drkcy` (`client`),
  KEY `FK97d6l8k3eirrmq9ilujq5ha7m` (`physician`),
  CONSTRAINT `FK97d6l8k3eirrmq9ilujq5ha7m` FOREIGN KEY (`physician`) REFERENCES `physician` (`person_id`),
  CONSTRAINT `FKmbwbbpe4rkddjhuhsh34drkcy` FOREIGN KEY (`client`) REFERENCES `client` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `series_documentation`
--

LOCK TABLES `series_documentation` WRITE;
/*!40000 ALTER TABLE `series_documentation` DISABLE KEYS */;
/*!40000 ALTER TABLE `series_documentation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_attachment`
--

DROP TABLE IF EXISTS `service_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_attachment` (
  `service_attachment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `service_attachment_category` varchar(255) DEFAULT NULL,
  `content_note` varchar(2000) DEFAULT NULL,
  `content_type` varchar(255) NOT NULL,
  `source_filename` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `target_filename` varchar(255) NOT NULL,
  `bodymap_item` bigint(20) DEFAULT NULL,
  `series_documentation` bigint(20) NOT NULL,
  `session_documentation` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`service_attachment_id`),
  KEY `FKcx06fejggqkod4ne4kf4xjvrj` (`bodymap_item`),
  KEY `FKay4vw94xbxr5758h2fil9q8op` (`series_documentation`),
  KEY `FKf7xvoareu7408h0etdkmvc72d` (`session_documentation`),
  CONSTRAINT `FKay4vw94xbxr5758h2fil9q8op` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`),
  CONSTRAINT `FKcx06fejggqkod4ne4kf4xjvrj` FOREIGN KEY (`bodymap_item`) REFERENCES `bodymap_item` (`bodymap_item_id`),
  CONSTRAINT `FKf7xvoareu7408h0etdkmvc72d` FOREIGN KEY (`session_documentation`) REFERENCES `session_documentation` (`session_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_attachment`
--

LOCK TABLES `service_attachment` WRITE;
/*!40000 ALTER TABLE `service_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_documentation`
--

DROP TABLE IF EXISTS `service_documentation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_documentation` (
  `service_documentation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `service_quantity` int(11) NOT NULL,
  `service_type` bigint(20) NOT NULL,
  `session_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`service_documentation_id`),
  KEY `FK9181it3ae2ic0mp8ow3dr6jmx` (`service_type`),
  KEY `FK7djur2sqohsms3tq5w18ycwkh` (`session_documentation`),
  CONSTRAINT `FK7djur2sqohsms3tq5w18ycwkh` FOREIGN KEY (`session_documentation`) REFERENCES `session_documentation` (`session_documentation_id`),
  CONSTRAINT `FK9181it3ae2ic0mp8ow3dr6jmx` FOREIGN KEY (`service_type`) REFERENCES `service_type` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_documentation`
--

LOCK TABLES `service_documentation` WRITE;
/*!40000 ALTER TABLE `service_documentation` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_documentation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_template`
--

DROP TABLE IF EXISTS `service_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_template` (
  `service_template_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `service_quantity` int(11) NOT NULL,
  `series_documentation` bigint(20) NOT NULL,
  `service_type` bigint(20) NOT NULL,
  PRIMARY KEY (`service_template_id`),
  KEY `FKtg7y1cex32pb7okh543uaq93e` (`series_documentation`),
  KEY `FKr0cyqmoqd0lensm5xtt0pljjx` (`service_type`),
  CONSTRAINT `FKr0cyqmoqd0lensm5xtt0pljjx` FOREIGN KEY (`service_type`) REFERENCES `service_type` (`service_type_id`),
  CONSTRAINT `FKtg7y1cex32pb7okh543uaq93e` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_template`
--

LOCK TABLES `service_template` WRITE;
/*!40000 ALTER TABLE `service_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_type`
--

DROP TABLE IF EXISTS `service_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_type` (
  `service_type_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `amount` decimal(19,2) NOT NULL,
  `category` varchar(255) NOT NULL,
  `service_type_code` varchar(255) NOT NULL,
  `service_type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`service_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_type`
--

LOCK TABLES `service_type` WRITE;
/*!40000 ALTER TABLE `service_type` DISABLE KEYS */;
INSERT INTO `service_type` VALUES (1,'2017-02-01 08:38:30','wquendler',NULL,NULL,'2017-02-01 08:38:30','wquendler',45.00,'C10_THERAPY','PH 45','Physiotherapie 45 min'),(2,'2017-02-01 08:38:30','wquendler',NULL,NULL,'2017-02-01 08:38:30','wquendler',30.00,'C10_THERAPY','PH 30','Physiotherapie 30 min'),(3,'2017-02-01 08:38:30','wquendler',NULL,NULL,'2017-02-01 08:38:30','wquendler',0.42,'C20_ADMIN','FK','Fahrtkosten'),(4,'2017-02-01 08:38:30','wquendler',NULL,NULL,'2017-02-01 08:38:30','wquendler',5.00,'C20_ADMIN','FKP 10','Fahrtkostenpauschale 10 km'),(5,'2017-02-01 08:38:30','wquendler',NULL,NULL,'2017-02-01 08:38:30','wquendler',5.00,'C30_PRODUCT','K-Tape','Kinesiotape');
/*!40000 ALTER TABLE `service_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_documentation`
--

DROP TABLE IF EXISTS `session_documentation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_documentation` (
  `session_documentation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `session_end_date` datetime NOT NULL,
  `remind_by_sms` bit(1) DEFAULT NULL,
  `schedule_note` varchar(255) DEFAULT NULL,
  `schedule_subject` varchar(255) DEFAULT NULL,
  `session_note` varchar(2000) DEFAULT NULL,
  `session_subject` varchar(255) DEFAULT NULL,
  `session_text` varchar(2000) DEFAULT NULL,
  `session_start_date` datetime NOT NULL,
  `series_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`session_documentation_id`),
  KEY `IDXiefi507r0834lp6at76c3eed5` (`session_start_date`,`session_end_date`),
  KEY `FK7xu8xtctnsn4qjmjefytttq4h` (`series_documentation`),
  CONSTRAINT `FK7xu8xtctnsn4qjmjefytttq4h` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_documentation`
--

LOCK TABLES `session_documentation` WRITE;
/*!40000 ALTER TABLE `session_documentation` DISABLE KEYS */;
/*!40000 ALTER TABLE `session_documentation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `therapeutical_anamnesis`
--

DROP TABLE IF EXISTS `therapeutical_anamnesis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `therapeutical_anamnesis` (
  `therapeutical_anamnesis_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `disease_aids` bit(1) DEFAULT NULL,
  `disease_diabetes` bit(1) DEFAULT NULL,
  `disease_heart_pacemaker` bit(1) DEFAULT NULL,
  `disease_note` varchar(255) DEFAULT NULL,
  `how_note` varchar(255) DEFAULT NULL,
  `medication_blood_coagulation` bit(1) DEFAULT NULL,
  `medication_note` varchar(255) DEFAULT NULL,
  `medication_pain_killer` bit(1) DEFAULT NULL,
  `medication_sleeping_drug` bit(1) DEFAULT NULL,
  `previous_treatment_note` varchar(255) DEFAULT NULL,
  `what_makes_problems_easier_note` varchar(255) DEFAULT NULL,
  `what_makes_problems_worser_note` varchar(255) DEFAULT NULL,
  `what_note` varchar(255) DEFAULT NULL,
  `when_note` varchar(255) DEFAULT NULL,
  `where_note` varchar(255) DEFAULT NULL,
  `women_coil` bit(1) DEFAULT NULL,
  `women_note` varchar(255) DEFAULT NULL,
  `women_pregnancy` bit(1) DEFAULT NULL,
  `series_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`therapeutical_anamnesis_id`),
  KEY `FK1r46hfd57tgoc3rjlp69egmnb` (`series_documentation`),
  CONSTRAINT `FK1r46hfd57tgoc3rjlp69egmnb` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `therapeutical_anamnesis`
--

LOCK TABLES `therapeutical_anamnesis` WRITE;
/*!40000 ALTER TABLE `therapeutical_anamnesis` DISABLE KEYS */;
/*!40000 ALTER TABLE `therapeutical_anamnesis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `therapeutical_diagnosis`
--

DROP TABLE IF EXISTS `therapeutical_diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `therapeutical_diagnosis` (
  `therapeutical_diagnosis_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) DEFAULT NULL,
  `direction` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `participation` varchar(255) DEFAULT NULL,
  `quality` varchar(255) DEFAULT NULL,
  `series_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`therapeutical_diagnosis_id`),
  KEY `FKb9ufmifo246u1i35x8392d1u2` (`series_documentation`),
  CONSTRAINT `FKb9ufmifo246u1i35x8392d1u2` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `therapeutical_diagnosis`
--

LOCK TABLES `therapeutical_diagnosis` WRITE;
/*!40000 ALTER TABLE `therapeutical_diagnosis` DISABLE KEYS */;
/*!40000 ALTER TABLE `therapeutical_diagnosis` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-01  8:40:03
