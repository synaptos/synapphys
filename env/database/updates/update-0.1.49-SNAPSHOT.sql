alter table `billing` add `payment_no` varchar(255) null default null after `payment_date`;
alter table `billing` add `payment_note` varchar(2000) null default null after `payment_no`;
alter table `billing` add index `IDXosrrssv5i5bb3fusckc9noeh1` (`billing_type`, `payment_date`, `payment_no`);
