/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

alter table `session_documentation` modify `session_note` varchar(2000);

--
-- Table structure for table `service_attachment`
--

DROP TABLE IF EXISTS `service_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_attachment` (
  `service_attachment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user` varchar(255) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `service_attachment_category` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `source_filename` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `target_filename` varchar(255) NOT NULL,
  `session_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`service_attachment_id`),
  KEY `FKf7xvoareu7408h0etdkmvc72d` (`session_documentation`),
  CONSTRAINT `FKf7xvoareu7408h0etdkmvc72d` FOREIGN KEY (`session_documentation`) REFERENCES `session_documentation` (`session_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_attachment`
--

LOCK TABLES `service_attachment` WRITE;
/*!40000 ALTER TABLE `service_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_attachment` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
