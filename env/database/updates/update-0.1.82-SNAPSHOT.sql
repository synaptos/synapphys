alter table `session_documentation` add column `session_text` varchar(2000);
update `session_documentation` set session_text = session_note;

alter table `service_attachment` drop foreign key `FKf7xvoareu7408h0etdkmvc72d`;

alter table `service_attachment` change `note` `content_note` varchar(2000);
alter table `service_attachment` change `session_documentation` `session_documentation_bak` bigint(20);

alter table `service_attachment` add column `bodymap_item` bigint(20);
alter table `service_attachment` add column `session_documentation` bigint(20);
alter table `service_attachment` add column `series_documentation` bigint(20) not null;

update service_attachment set session_documentation = session_documentation_bak;
update service_attachment a inner join session_documentation sd on sd.session_documentation_id = a.session_documentation set a.series_documentation = sd.series_documentation;

alter table `service_attachment` drop column `session_documentation_bak`;

alter table `service_attachment` add constraint `FKcx06fejggqkod4ne4kf4xjvrj` foreign key (`bodymap_item`) references `bodymap_item`(`bodymap_item_id`);
alter table `service_attachment` add constraint `FKf7xvoareu7408h0etdkmvc72d` foreign key (`session_documentation`) references `session_documentation`(`session_documentation_id`);
alter table `service_attachment` add constraint `FKay4vw94xbxr5758h2fil9q8op` foreign key (`series_documentation`) references `series_documentation`(`series_documentation_id`);

delete from bodymap_item;

alter table `bodymap_item` drop foreign key `FKikccj6q6x4u16yu11l4idwhp5`;

alter table `bodymap_item` change `note` `finding_note` varchar(2000);
alter table `bodymap_item` drop column `series_documentation`;

alter table `bodymap_item` add column `finding_text` varchar(2000);
alter table `bodymap_item` add column `sequence_no` int(11);
alter table `bodymap_item` add column `parent_item` bigint(20);
alter table `bodymap_item` add column `session_documentation` bigint(20);
alter table `bodymap_item` add column `series_documentation` bigint(20) not null;

alter table `bodymap_item` add constraint `FKqahcc02laun66grg80x4mvsp9` foreign key (`parent_item`) references `bodymap_item`(`bodymap_item_id`);
alter table `bodymap_item` add constraint `FKokd4p9g23r6ppo5uiwgwgxlt5` foreign key (`session_documentation`) references `session_documentation`(`session_documentation_id`);
alter table `bodymap_item` add constraint `FKikccj6q6x4u16yu11l4idwhp5` foreign key (`series_documentation`) references `series_documentation`(`series_documentation_id`);

alter table `billing_item` change `billing_item_position` `sequence_no` int(11);
