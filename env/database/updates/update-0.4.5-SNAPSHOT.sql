/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `findings_lsp`
--

DROP TABLE IF EXISTS `findings_lsp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `findings_lsp` (
  `findings_lsp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active_test_extension` int(11) DEFAULT NULL,
  `active_test_flex_hws_flexion` int(11) DEFAULT NULL,
  `active_test_flex_latara_left` int(11) DEFAULT NULL,
  `active_test_flex_latara_right` int(11) DEFAULT NULL,
  `active_test_heel_walk_left` int(11) DEFAULT NULL,
  `active_test_heel_walk_right` int(11) DEFAULT NULL,
  `active_test_toe_stand_left` int(11) DEFAULT NULL,
  `active_test_toe_stand_right` int(11) DEFAULT NULL,
  `active_test_flexion` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `passiv_test_hip_ar_left` int(11) DEFAULT NULL,
  `passiv_test_hip_ar_right` int(11) DEFAULT NULL,
  `passiv_test_hip_flexion_left` int(11) DEFAULT NULL,
  `passiv_test_hip_flexion_right` int(11) DEFAULT NULL,
  `passiv_test_hip_ir_left` int(11) DEFAULT NULL,
  `passiv_test_hip_ir_right` int(11) DEFAULT NULL,
  `passiv_test_sig_i_lia_dorsal_left` int(11) DEFAULT NULL,
  `passiv_test_sig_i_lia_dorsal_right` int(11) DEFAULT NULL,
  `passiv_test_straight_leg_raise_left` int(11) DEFAULT NULL,
  `passiv_test_straight_leg_raise_right` int(11) DEFAULT NULL,
  `resistance_test_m_tibialis_anterior_left` bit(1) DEFAULT NULL,
  `resistance_test_m_tibialis_anterior_right` bit(1) DEFAULT NULL,
  `resistance_test_m_ehl_left` bit(1) DEFAULT NULL,
  `resistance_test_m_ehl_right` bit(1) DEFAULT NULL,
  `resistance_test_m_psoas_left` bit(1) DEFAULT NULL,
  `resistance_test_m_psoas_right` bit(1) DEFAULT NULL,
  `resistance_test_mm_peronei_left` bit(1) DEFAULT NULL,
  `resistance_test_mm_peronei_right` bit(1) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `series_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`findings_lsp_id`),
  KEY `FKsu23v1v08i8bv35gixc4tq45o` (`series_documentation`),
  CONSTRAINT `FKsu23v1v08i8bv35gixc4tq45o` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `findings_lsp`
--

LOCK TABLES `findings_lsp` WRITE;
/*!40000 ALTER TABLE `findings_lsp` DISABLE KEYS */;
/*!40000 ALTER TABLE `findings_lsp` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
