/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `findings_sheet`
--

DROP TABLE IF EXISTS `findings_sheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `findings_sheet` (
  `findings_sheet_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `anamnesis_history` text,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `exclusionary_test` text,
  `fbl_constitution` text,
  `fbl_motion_behaviour` text,
  `fbl_reactive_hypertonus` text,
  `fbl_statics_of_site` text,
  `fbl_statics_of_front_and_back` text,
  `fbl_thrust_load` text,
  `functional_demo` text,
  `functional_examination` text,
  `inspection` text,
  `interpretation_hypothesis` text,
  `neurodynamicial_test` text,
  `neurological_examination` text,
  `pain_anamnesis` text,
  `palpation` text,
  `planned_therapeutical_functional_outcome` text,
  `planned_therapeutical_outcome_and_measures` text,
  `present_pain` text,
  `special_issues` text,
  `specific_measuring` text,
  `updated_date` datetime NOT NULL,
  `updated_user` varchar(255) NOT NULL,
  `series_documentation` bigint(20) NOT NULL,
  PRIMARY KEY (`findings_sheet_id`),
  KEY `FKdpeyyu6luf6v20o9jqdiqn58h` (`series_documentation`),
  CONSTRAINT `FKdpeyyu6luf6v20o9jqdiqn58h` FOREIGN KEY (`series_documentation`) REFERENCES `series_documentation` (`series_documentation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `findings_sheet`
--

LOCK TABLES `findings_sheet` WRITE;
/*!40000 ALTER TABLE `findings_sheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `findings_sheet` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
