/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `billing_signature`
--

DROP TABLE IF EXISTS `billing_signature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_signature` (
  `billing_signature_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cb_terminal_id` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_user` varchar(255) NOT NULL,
  `ft_cashbox_id` varchar(255) DEFAULT NULL,
  `ft_cashbox_name` varchar(255) DEFAULT NULL,
  `ft_receipt_id` varchar(255) DEFAULT NULL,
  `ft_receipt_moment` datetime DEFAULT NULL,
  `ft_receipt_response_log` text,
  `ft_receipt_signature` text,
  `ft_state_counting` bit(1) DEFAULT NULL,
  `ft_state_exception` varchar(255) DEFAULT NULL,
  `ft_state_flag` bigint(20) DEFAULT NULL,
  `ft_state_no` int(11) DEFAULT NULL,
  `ft_state_signing` bit(1) DEFAULT NULL,
  `ft_state_zero_receipt` bit(1) DEFAULT NULL,
  `billing` bigint(20) NOT NULL,
  PRIMARY KEY (`billing_signature_id`),
  KEY `FK1u2hg7lj7p5u78v7k2n9rycrv` (`billing`),
  CONSTRAINT `FK1u2hg7lj7p5u78v7k2n9rycrv` FOREIGN KEY (`billing`) REFERENCES `billing` (`billing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing_signature`
--

LOCK TABLES `billing_signature` WRITE;
/*!40000 ALTER TABLE `billing_signature` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing_signature` ENABLE KEYS */;
UNLOCK TABLES;

alter table `service_type` change `service_type_value` `amount` decimal(19,2);
alter table `billing_item` change `billing_item_value` `amount` decimal(19,2);
alter table `billing_item` change `billing_item_vat` `vat` decimal(19,2);

update `service_type` set `category` = 'C20_ADMIN' where `category` = 'C50_FEE';

alter table `billing` add column `ft_access_token` varchar(2000);
alter table `billing` add column `ft_cashbox_id` varchar(255);
alter table `billing` add column `cb_terminal_id` varchar(255);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
