alter table `client` drop foreign key `FKh0vkwhf0vvuvxotpj4dej9uiv`;

alter table `insurance` change `insurance_id` `health_insurance_id` bigint(20);
alter table `insurance` change `insurance_name` `health_insurance_name` varchar(255);
alter table `insurance` change `insurance_code` `health_insurance_code` varchar(255);
rename table `insurance` to `health_insurance`;

alter table `client` change `sin` `health_insurance_no` varchar(255);
alter table `client` change `phi` `health_insurance` bigint(20);
alter table `client` change `person_note` `contact_note` varchar(255);
alter table `client` drop column `vip`;
alter table `client` add column `vip_date` datetime;
alter table `client` add column `vip_note` varchar(255);
alter table `client` add column `patient_status` varchar(255);
alter table `client` add column `patient_status_date` datetime;
alter table `client` add column `patient_status_note` varchar(255);

alter table `client` add constraint `FKh0vkwhf0vvuvxotpj4dej9uiv` foreign key (`health_insurance`) references `health_insurance`(`health_insurance_id`);

update `client` set `health_insurance_no`=concat(`health_insurance_no`, date_format(`birth_date`, '%d%m%y')) where not `health_insurance_no` is null and not `birth_date` is null;

alter table `client` add index `IDXin7l3wuwfokt0c25jx29aedc5` (`person_lastname`, `person_firstname`, `birth_date`);
alter table `client` add index `IDX9vcpren9bbp60ihu45xgu7ww5` (`health_insurance_no`);

alter table `physician` change `person_note` `contact_note` varchar(255);

drop table cwl_item;

alter table `series_documentation` change `done_date` `canceled_date` datetime;

update client c inner join series_documentation sd on c.person_id = sd.client set patient_status = 'CURRENT', patient_status_date = sd.approval_date where sd.deleted_date is null and c.deleted_date is null and c.patient_status is null and not exists(select b.billing_id from billing b where b.series_documentation = sd.series_documentation_id and b.billing_type = 'INVOICE' and b.deleted_date is null);

alter table `client` modify `birth_date` datetime default null;
alter table `client` modify `gender` varchar(255) default null;
